//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/reactive-streams/org/reactivestreams/Publisher.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_OrgReactivestreamsPublisher")
#ifdef RESTRICT_OrgReactivestreamsPublisher
#define INCLUDE_ALL_OrgReactivestreamsPublisher 0
#else
#define INCLUDE_ALL_OrgReactivestreamsPublisher 1
#endif
#undef RESTRICT_OrgReactivestreamsPublisher

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (OrgReactivestreamsPublisher_) && (INCLUDE_ALL_OrgReactivestreamsPublisher || defined(INCLUDE_OrgReactivestreamsPublisher))
#define OrgReactivestreamsPublisher_

@protocol OrgReactivestreamsSubscriber;

@protocol OrgReactivestreamsPublisher < JavaObject >

- (void)subscribeWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

@end

J2OBJC_EMPTY_STATIC_INIT(OrgReactivestreamsPublisher)

J2OBJC_TYPE_LITERAL_HEADER(OrgReactivestreamsPublisher)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_OrgReactivestreamsPublisher")
