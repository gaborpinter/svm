//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/plugin/http/mappers/StringMapper.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/plugin/http/mappers/StringMapper.h"
#include "java/util/List.h"

@implementation ComScolvoCorePluginHttpMappersStringMapper

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  ComScolvoCorePluginHttpMappersStringMapper_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (NSString *)getWithId:(id)data
       withJavaUtilList:(id<JavaUtilList>)keys {
  return [nil_chk(data) description];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 0, 1, -1, 2, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(getWithId:withJavaUtilList:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "get", "LNSObject;LJavaUtilList;", "(Ljava/lang/Object;Ljava/util/List<Ljava/lang/String;>;)Ljava/lang/String;" };
  static const J2ObjcClassInfo _ComScolvoCorePluginHttpMappersStringMapper = { "StringMapper", "com.scolvo.core.plugin.http.mappers", ptrTable, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, -1, -1 };
  return &_ComScolvoCorePluginHttpMappersStringMapper;
}

@end

void ComScolvoCorePluginHttpMappersStringMapper_init(ComScolvoCorePluginHttpMappersStringMapper *self) {
  NSObject_init(self);
}

ComScolvoCorePluginHttpMappersStringMapper *new_ComScolvoCorePluginHttpMappersStringMapper_init() {
  J2OBJC_NEW_IMPL(ComScolvoCorePluginHttpMappersStringMapper, init)
}

ComScolvoCorePluginHttpMappersStringMapper *create_ComScolvoCorePluginHttpMappersStringMapper_init() {
  J2OBJC_CREATE_IMPL(ComScolvoCorePluginHttpMappersStringMapper, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCorePluginHttpMappersStringMapper)
