//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/plugin/http/HttpResponse.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/plugin/http/HttpResponse.h"

@interface ComScolvoCorePluginHttpHttpResponse () {
 @public
  jint statusCode_;
  id data_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCorePluginHttpHttpResponse, data_, id)

@implementation ComScolvoCorePluginHttpHttpResponse

- (instancetype __nonnull)initWithInt:(jint)statusCode
                               withId:(id)data {
  ComScolvoCorePluginHttpHttpResponse_initWithInt_withId_(self, statusCode, data);
  return self;
}

- (jint)getStatusCode {
  return statusCode_;
}

- (id)getData {
  return data_;
}

- (NSString *)description {
  return JreStrcat("$I$@C", @"HttpResponse [statusCode=", statusCode_, @", data=", data_, ']');
}

- (void)dealloc {
  RELEASE_(data_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithInt:withId:);
  methods[1].selector = @selector(getStatusCode);
  methods[2].selector = @selector(getData);
  methods[3].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "statusCode_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "data_", "LNSObject;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "ILNSObject;", "toString" };
  static const J2ObjcClassInfo _ComScolvoCorePluginHttpHttpResponse = { "HttpResponse", "com.scolvo.core.plugin.http", ptrTable, methods, fields, 7, 0x1, 4, 2, -1, -1, -1, -1, -1 };
  return &_ComScolvoCorePluginHttpHttpResponse;
}

@end

void ComScolvoCorePluginHttpHttpResponse_initWithInt_withId_(ComScolvoCorePluginHttpHttpResponse *self, jint statusCode, id data) {
  NSObject_init(self);
  self->statusCode_ = statusCode;
  JreStrongAssign(&self->data_, data);
}

ComScolvoCorePluginHttpHttpResponse *new_ComScolvoCorePluginHttpHttpResponse_initWithInt_withId_(jint statusCode, id data) {
  J2OBJC_NEW_IMPL(ComScolvoCorePluginHttpHttpResponse, initWithInt_withId_, statusCode, data)
}

ComScolvoCorePluginHttpHttpResponse *create_ComScolvoCorePluginHttpHttpResponse_initWithInt_withId_(jint statusCode, id data) {
  J2OBJC_CREATE_IMPL(ComScolvoCorePluginHttpHttpResponse, initWithInt_withId_, statusCode, data)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCorePluginHttpHttpResponse)
