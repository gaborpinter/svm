//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/plugin/http/HttpClient.java
//

#include "IOSClass.h"
#include "IOSPrimitiveArray.h"
#include "J2ObjC_source.h"
#include "com/google/gson/Gson.h"
#include "com/scolvo/core/plugin/ScolvoGson.h"
#include "com/scolvo/core/plugin/base64/Base64.h"
#include "com/scolvo/core/plugin/http/HttpClient.h"
#include "com/scolvo/core/plugin/http/HttpClientException.h"
#include "com/scolvo/core/plugin/http/HttpConfig.h"
#include "com/scolvo/core/plugin/http/HttpResponse.h"
#include "com/scolvo/core/plugin/http/RequestMethod.h"
#include "com/scolvo/core/plugin/http/mappers/Mapper.h"
#include "java/io/BufferedReader.h"
#include "java/io/InputStream.h"
#include "java/io/InputStreamReader.h"
#include "java/io/OutputStream.h"
#include "java/io/PrintStream.h"
#include "java/lang/Exception.h"
#include "java/lang/StringBuilder.h"
#include "java/lang/System.h"
#include "java/lang/Throwable.h"
#include "java/net/HttpURLConnection.h"
#include "java/net/URL.h"
#include "java/net/URLConnection.h"
#include "java/net/URLEncoder.h"
#include "java/util/List.h"
#include "java/util/Map.h"
#include "java/util/Set.h"
#include "javax/net/ssl/HttpsURLConnection.h"
#include "javax/net/ssl/SSLSocketFactory.h"

@interface ComScolvoCorePluginHttpHttpClient () {
 @public
  JavaxNetSslSSLSocketFactory *sslSocketFactory_;
}

- (ComScolvoCorePluginHttpHttpResponse *)communicationWithComScolvoCorePluginHttpHttpConfig:(ComScolvoCorePluginHttpHttpConfig *)config;

- (id)readResponseWithJavaIoBufferedReader:(JavaIoBufferedReader *)reader
     withComScolvoCorePluginHttpHttpConfig:(ComScolvoCorePluginHttpHttpConfig *)config;

- (NSString *)readErrorResponseWithJavaIoBufferedReader:(JavaIoBufferedReader *)reader;

- (JavaNetHttpURLConnection *)setupConnectionWithNSString:(NSString *)url
                    withComScolvoCorePluginHttpHttpConfig:(ComScolvoCorePluginHttpHttpConfig *)config;

- (void)setBasicAuthWithJavaNetHttpURLConnection:(JavaNetHttpURLConnection *)conn
                                    withNSString:(NSString *)user
                                    withNSString:(NSString *)pw;

- (void)writePostInputWithJavaNetHttpURLConnection:(JavaNetHttpURLConnection *)conn
             withComScolvoCorePluginHttpHttpConfig:(ComScolvoCorePluginHttpHttpConfig *)config;

- (IOSByteArray *)buildPostDataWithJavaUtilMap:(id<JavaUtilMap>)params;

@end

J2OBJC_FIELD_SETTER(ComScolvoCorePluginHttpHttpClient, sslSocketFactory_, JavaxNetSslSSLSocketFactory *)

__attribute__((unused)) static ComScolvoCorePluginHttpHttpResponse *ComScolvoCorePluginHttpHttpClient_communicationWithComScolvoCorePluginHttpHttpConfig_(ComScolvoCorePluginHttpHttpClient *self, ComScolvoCorePluginHttpHttpConfig *config);

__attribute__((unused)) static id ComScolvoCorePluginHttpHttpClient_readResponseWithJavaIoBufferedReader_withComScolvoCorePluginHttpHttpConfig_(ComScolvoCorePluginHttpHttpClient *self, JavaIoBufferedReader *reader, ComScolvoCorePluginHttpHttpConfig *config);

__attribute__((unused)) static NSString *ComScolvoCorePluginHttpHttpClient_readErrorResponseWithJavaIoBufferedReader_(ComScolvoCorePluginHttpHttpClient *self, JavaIoBufferedReader *reader);

__attribute__((unused)) static JavaNetHttpURLConnection *ComScolvoCorePluginHttpHttpClient_setupConnectionWithNSString_withComScolvoCorePluginHttpHttpConfig_(ComScolvoCorePluginHttpHttpClient *self, NSString *url, ComScolvoCorePluginHttpHttpConfig *config);

__attribute__((unused)) static void ComScolvoCorePluginHttpHttpClient_setBasicAuthWithJavaNetHttpURLConnection_withNSString_withNSString_(ComScolvoCorePluginHttpHttpClient *self, JavaNetHttpURLConnection *conn, NSString *user, NSString *pw);

__attribute__((unused)) static void ComScolvoCorePluginHttpHttpClient_writePostInputWithJavaNetHttpURLConnection_withComScolvoCorePluginHttpHttpConfig_(ComScolvoCorePluginHttpHttpClient *self, JavaNetHttpURLConnection *conn, ComScolvoCorePluginHttpHttpConfig *config);

__attribute__((unused)) static IOSByteArray *ComScolvoCorePluginHttpHttpClient_buildPostDataWithJavaUtilMap_(ComScolvoCorePluginHttpHttpClient *self, id<JavaUtilMap> params);

@implementation ComScolvoCorePluginHttpHttpClient

- (instancetype __nonnull)initWithJavaxNetSslSSLSocketFactory:(JavaxNetSslSSLSocketFactory *)sslSocketFactory {
  ComScolvoCorePluginHttpHttpClient_initWithJavaxNetSslSSLSocketFactory_(self, sslSocketFactory);
  return self;
}

- (ComScolvoCorePluginHttpHttpResponse *)callWithJavaUtilMap:(id<JavaUtilMap>)configJson {
  NSString *url = nil;
  @try {
    NSString *json = [((ComGoogleGsonGson *) nil_chk(JreLoadStatic(ComScolvoCorePluginScolvoGson, INSTANCE))) toJsonWithId:configJson];
    ComScolvoCorePluginHttpHttpConfig *config = [JreLoadStatic(ComScolvoCorePluginScolvoGson, INSTANCE) fromJsonWithNSString:json withIOSClass:ComScolvoCorePluginHttpHttpConfig_class_()];
    url = [((ComScolvoCorePluginHttpHttpConfig *) nil_chk(config)) getUrl];
    return ComScolvoCorePluginHttpHttpClient_communicationWithComScolvoCorePluginHttpHttpConfig_(self, config);
  }
  @catch (JavaLangException *e) {
    [((JavaIoPrintStream *) nil_chk(JreLoadStatic(JavaLangSystem, out))) printlnWithNSString:JreStrcat("$$$$", @"HttpClient.call failed. ", [[e java_getClass] getCanonicalName], @" : ", [e getMessage])];
    return create_ComScolvoCorePluginHttpHttpResponse_initWithInt_withId_(500, JreStrcat("$$C$", @"Unknown exception happened. ", url, ' ', [e getMessage]));
  }
}

- (ComScolvoCorePluginHttpHttpResponse *)communicationWithComScolvoCorePluginHttpHttpConfig:(ComScolvoCorePluginHttpHttpConfig *)config {
  return ComScolvoCorePluginHttpHttpClient_communicationWithComScolvoCorePluginHttpHttpConfig_(self, config);
}

- (id)readResponseWithJavaIoBufferedReader:(JavaIoBufferedReader *)reader
     withComScolvoCorePluginHttpHttpConfig:(ComScolvoCorePluginHttpHttpConfig *)config {
  return ComScolvoCorePluginHttpHttpClient_readResponseWithJavaIoBufferedReader_withComScolvoCorePluginHttpHttpConfig_(self, reader, config);
}

- (NSString *)readErrorResponseWithJavaIoBufferedReader:(JavaIoBufferedReader *)reader {
  return ComScolvoCorePluginHttpHttpClient_readErrorResponseWithJavaIoBufferedReader_(self, reader);
}

- (JavaNetHttpURLConnection *)setupConnectionWithNSString:(NSString *)url
                    withComScolvoCorePluginHttpHttpConfig:(ComScolvoCorePluginHttpHttpConfig *)config {
  return ComScolvoCorePluginHttpHttpClient_setupConnectionWithNSString_withComScolvoCorePluginHttpHttpConfig_(self, url, config);
}

- (void)setBasicAuthWithJavaNetHttpURLConnection:(JavaNetHttpURLConnection *)conn
                                    withNSString:(NSString *)user
                                    withNSString:(NSString *)pw {
  ComScolvoCorePluginHttpHttpClient_setBasicAuthWithJavaNetHttpURLConnection_withNSString_withNSString_(self, conn, user, pw);
}

- (void)writePostInputWithJavaNetHttpURLConnection:(JavaNetHttpURLConnection *)conn
             withComScolvoCorePluginHttpHttpConfig:(ComScolvoCorePluginHttpHttpConfig *)config {
  ComScolvoCorePluginHttpHttpClient_writePostInputWithJavaNetHttpURLConnection_withComScolvoCorePluginHttpHttpConfig_(self, conn, config);
}

- (IOSByteArray *)buildPostDataWithJavaUtilMap:(id<JavaUtilMap>)params {
  return ComScolvoCorePluginHttpHttpClient_buildPostDataWithJavaUtilMap_(self, params);
}

- (void)dealloc {
  RELEASE_(sslSocketFactory_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LComScolvoCorePluginHttpHttpResponse;", 0x1, 1, 2, -1, 3, -1, -1 },
    { NULL, "LComScolvoCorePluginHttpHttpResponse;", 0x2, 4, 5, 6, -1, -1, -1 },
    { NULL, "LNSObject;", 0x2, 7, 8, 6, -1, -1, -1 },
    { NULL, "LNSString;", 0x2, 9, 10, 6, -1, -1, -1 },
    { NULL, "LJavaNetHttpURLConnection;", 0x2, 11, 12, 6, -1, -1, -1 },
    { NULL, "V", 0x2, 13, 14, -1, -1, -1, -1 },
    { NULL, "V", 0x2, 15, 16, 17, -1, -1, -1 },
    { NULL, "[B", 0x2, 18, 2, 19, 20, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaxNetSslSSLSocketFactory:);
  methods[1].selector = @selector(callWithJavaUtilMap:);
  methods[2].selector = @selector(communicationWithComScolvoCorePluginHttpHttpConfig:);
  methods[3].selector = @selector(readResponseWithJavaIoBufferedReader:withComScolvoCorePluginHttpHttpConfig:);
  methods[4].selector = @selector(readErrorResponseWithJavaIoBufferedReader:);
  methods[5].selector = @selector(setupConnectionWithNSString:withComScolvoCorePluginHttpHttpConfig:);
  methods[6].selector = @selector(setBasicAuthWithJavaNetHttpURLConnection:withNSString:withNSString:);
  methods[7].selector = @selector(writePostInputWithJavaNetHttpURLConnection:withComScolvoCorePluginHttpHttpConfig:);
  methods[8].selector = @selector(buildPostDataWithJavaUtilMap:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "sslSocketFactory_", "LJavaxNetSslSSLSocketFactory;", .constantValue.asLong = 0, 0x12, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LJavaxNetSslSSLSocketFactory;", "call", "LJavaUtilMap;", "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)Lcom/scolvo/core/plugin/http/HttpResponse;", "communication", "LComScolvoCorePluginHttpHttpConfig;", "LJavaIoIOException;", "readResponse", "LJavaIoBufferedReader;LComScolvoCorePluginHttpHttpConfig;", "readErrorResponse", "LJavaIoBufferedReader;", "setupConnection", "LNSString;LComScolvoCorePluginHttpHttpConfig;", "setBasicAuth", "LJavaNetHttpURLConnection;LNSString;LNSString;", "writePostInput", "LJavaNetHttpURLConnection;LComScolvoCorePluginHttpHttpConfig;", "LJavaIoUnsupportedEncodingException;LJavaIoIOException;", "buildPostData", "LJavaIoUnsupportedEncodingException;", "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)[B" };
  static const J2ObjcClassInfo _ComScolvoCorePluginHttpHttpClient = { "HttpClient", "com.scolvo.core.plugin.http", ptrTable, methods, fields, 7, 0x1, 9, 1, -1, -1, -1, -1, -1 };
  return &_ComScolvoCorePluginHttpHttpClient;
}

@end

void ComScolvoCorePluginHttpHttpClient_initWithJavaxNetSslSSLSocketFactory_(ComScolvoCorePluginHttpHttpClient *self, JavaxNetSslSSLSocketFactory *sslSocketFactory) {
  NSObject_init(self);
  JreStrongAssign(&self->sslSocketFactory_, sslSocketFactory);
}

ComScolvoCorePluginHttpHttpClient *new_ComScolvoCorePluginHttpHttpClient_initWithJavaxNetSslSSLSocketFactory_(JavaxNetSslSSLSocketFactory *sslSocketFactory) {
  J2OBJC_NEW_IMPL(ComScolvoCorePluginHttpHttpClient, initWithJavaxNetSslSSLSocketFactory_, sslSocketFactory)
}

ComScolvoCorePluginHttpHttpClient *create_ComScolvoCorePluginHttpHttpClient_initWithJavaxNetSslSSLSocketFactory_(JavaxNetSslSSLSocketFactory *sslSocketFactory) {
  J2OBJC_CREATE_IMPL(ComScolvoCorePluginHttpHttpClient, initWithJavaxNetSslSSLSocketFactory_, sslSocketFactory)
}

ComScolvoCorePluginHttpHttpResponse *ComScolvoCorePluginHttpHttpClient_communicationWithComScolvoCorePluginHttpHttpConfig_(ComScolvoCorePluginHttpHttpClient *self, ComScolvoCorePluginHttpHttpConfig *config) {
  JavaNetHttpURLConnection *conn = ComScolvoCorePluginHttpHttpClient_setupConnectionWithNSString_withComScolvoCorePluginHttpHttpConfig_(self, [((ComScolvoCorePluginHttpHttpConfig *) nil_chk(config)) getUrl], config);
  jint responseCode = [((JavaNetHttpURLConnection *) nil_chk(conn)) getResponseCode];
  [((JavaIoPrintStream *) nil_chk(JreLoadStatic(JavaLangSystem, out))) printlnWithNSString:JreStrcat("$I$$", @"HTTP response ", responseCode, @" on: ", [config getUrl])];
  if (responseCode == JavaNetHttpURLConnection_HTTP_MOVED_PERM || responseCode == JavaNetHttpURLConnection_HTTP_MOVED_TEMP || responseCode == JavaNetHttpURLConnection_HTTP_SEE_OTHER) {
    NSString *redirect = [conn getHeaderFieldWithNSString:@"Location"];
    [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$I$$", @"Follow redirection code: ", responseCode, @" Url: ", redirect)];
    if (redirect != nil) {
      conn = ComScolvoCorePluginHttpHttpClient_setupConnectionWithNSString_withComScolvoCorePluginHttpHttpConfig_(self, redirect, config);
      responseCode = [((JavaNetHttpURLConnection *) nil_chk(conn)) getResponseCode];
      [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$I$$", @"HTTP ", responseCode, @" on ", redirect)];
    }
  }
  [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$I", @".... ", responseCode)];
  if (responseCode > 299) {
    NSString *data = nil;
    {
      JavaIoInputStreamReader *in = create_JavaIoInputStreamReader_initWithJavaIoInputStream_withNSString_([conn getErrorStream], @"UTF-8");
      JavaLangThrowable *__primaryException2 = nil;
      @try {
        JavaIoBufferedReader *reader = create_JavaIoBufferedReader_initWithJavaIoReader_(in);
        JavaLangThrowable *__primaryException1 = nil;
        @try {
          data = ComScolvoCorePluginHttpHttpClient_readErrorResponseWithJavaIoBufferedReader_(self, reader);
          [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$$", @"HTTP communication error: ", data)];
        }
        @catch (JavaLangThrowable *e) {
          __primaryException1 = e;
          @throw e;
        }
        @finally {
          if (reader != nil) {
            if (__primaryException1 != nil) {
              @try {
                [reader close];
              }
              @catch (JavaLangThrowable *e) {
                [__primaryException1 addSuppressedWithJavaLangThrowable:e];
              }
            }
            else {
              [reader close];
            }
          }
        }
      }
      @catch (JavaLangThrowable *e) {
        __primaryException2 = e;
        @throw e;
      }
      @finally {
        if (in != nil) {
          if (__primaryException2 != nil) {
            @try {
              [in close];
            }
            @catch (JavaLangThrowable *e) {
              [__primaryException2 addSuppressedWithJavaLangThrowable:e];
            }
          }
          else {
            [in close];
          }
        }
      }
    }
    return create_ComScolvoCorePluginHttpHttpResponse_initWithInt_withId_(responseCode, data);
  }
  {
    JavaIoInputStreamReader *in = create_JavaIoInputStreamReader_initWithJavaIoInputStream_withNSString_([conn getInputStream], @"UTF-8");
    JavaLangThrowable *__primaryException2 = nil;
    @try {
      JavaIoBufferedReader *reader = create_JavaIoBufferedReader_initWithJavaIoReader_(in);
      JavaLangThrowable *__primaryException1 = nil;
      @try {
        return create_ComScolvoCorePluginHttpHttpResponse_initWithInt_withId_(responseCode, ComScolvoCorePluginHttpHttpClient_readResponseWithJavaIoBufferedReader_withComScolvoCorePluginHttpHttpConfig_(self, reader, config));
      }
      @catch (JavaLangThrowable *e) {
        __primaryException1 = e;
        @throw e;
      }
      @finally {
        if (reader != nil) {
          if (__primaryException1 != nil) {
            @try {
              [reader close];
            }
            @catch (JavaLangThrowable *e) {
              [__primaryException1 addSuppressedWithJavaLangThrowable:e];
            }
          }
          else {
            [reader close];
          }
        }
      }
    }
    @catch (JavaLangThrowable *e) {
      __primaryException2 = e;
      @throw e;
    }
    @finally {
      if (in != nil) {
        if (__primaryException2 != nil) {
          @try {
            [in close];
          }
          @catch (JavaLangThrowable *e) {
            [__primaryException2 addSuppressedWithJavaLangThrowable:e];
          }
        }
        else {
          [in close];
        }
      }
    }
  }
}

id ComScolvoCorePluginHttpHttpClient_readResponseWithJavaIoBufferedReader_withComScolvoCorePluginHttpHttpConfig_(ComScolvoCorePluginHttpHttpClient *self, JavaIoBufferedReader *reader, ComScolvoCorePluginHttpHttpConfig *config) {
  JavaLangStringBuilder *sb = create_JavaLangStringBuilder_init();
  NSString *output;
  while ((output = [((JavaIoBufferedReader *) nil_chk(reader)) readLine]) != nil) {
    [sb appendWithNSString:output];
  }
  @try {
    return [((id<ComScolvoCorePluginHttpMappersMapper>) nil_chk(((id<ComScolvoCorePluginHttpMappersMapper>) cast_check([((IOSClass *) nil_chk(IOSClass_forName_([((ComScolvoCorePluginHttpHttpConfig *) nil_chk(config)) getMapper]))) newInstance], ComScolvoCorePluginHttpMappersMapper_class_())))) getWithId:sb withJavaUtilList:[config getMappingKeys]];
  }
  @catch (JavaLangException *e) {
    @throw create_ComScolvoCorePluginHttpHttpClientException_initWithNSString_withJavaLangThrowable_(JreStrcat("$$", @"Mapper not executable ", [config getMapper]), e);
  }
}

NSString *ComScolvoCorePluginHttpHttpClient_readErrorResponseWithJavaIoBufferedReader_(ComScolvoCorePluginHttpHttpClient *self, JavaIoBufferedReader *reader) {
  JavaLangStringBuilder *sb = create_JavaLangStringBuilder_init();
  NSString *output;
  while ((output = [((JavaIoBufferedReader *) nil_chk(reader)) readLine]) != nil) {
    [sb appendWithNSString:output];
  }
  return [NSString java_stringWithBytes:[((NSString *) nil_chk([sb description])) java_getBytes] charsetName:@"UTF-8"];
}

JavaNetHttpURLConnection *ComScolvoCorePluginHttpHttpClient_setupConnectionWithNSString_withComScolvoCorePluginHttpHttpConfig_(ComScolvoCorePluginHttpHttpClient *self, NSString *url, ComScolvoCorePluginHttpHttpConfig *config) {
  JavaNetHttpURLConnection *conn = (JavaNetHttpURLConnection *) cast_chk([create_JavaNetURL_initWithNSString_(url) openConnection], [JavaNetHttpURLConnection class]);
  if ([conn isKindOfClass:[JavaxNetSslHttpsURLConnection class]] && self->sslSocketFactory_ != nil) {
    [((JavaxNetSslHttpsURLConnection *) nil_chk([JavaxNetSslHttpsURLConnection_class_() cast:conn])) setSSLSocketFactoryWithJavaxNetSslSSLSocketFactory:self->sslSocketFactory_];
  }
  if ([((ComScolvoCorePluginHttpHttpConfig *) nil_chk(config)) isBaseAuth]) {
    ComScolvoCorePluginHttpHttpClient_setBasicAuthWithJavaNetHttpURLConnection_withNSString_withNSString_(self, conn, [config getBaseAuthUser], [config getBaseAuthPw]);
  }
  [((JavaNetHttpURLConnection *) nil_chk(conn)) setConnectTimeoutWithInt:[config getConnectTimeout]];
  [conn setReadTimeoutWithInt:[config getReadTimeout]];
  [conn setRequestMethodWithNSString:[((ComScolvoCorePluginHttpRequestMethod *) nil_chk([config getRequestMethod])) name]];
  [conn setDoOutputWithBoolean:true];
  for (NSString * __strong key in nil_chk([((id<JavaUtilMap>) nil_chk([config getHeaderParams])) keySet])) {
    [conn setRequestPropertyWithNSString:key withNSString:[((id<JavaUtilMap>) nil_chk([config getHeaderParams])) getWithId:key]];
  }
  if ([config getRequestMethod] == JreLoadEnum(ComScolvoCorePluginHttpRequestMethod, POST) || [config getRequestMethod] == JreLoadEnum(ComScolvoCorePluginHttpRequestMethod, PUT)) {
    ComScolvoCorePluginHttpHttpClient_writePostInputWithJavaNetHttpURLConnection_withComScolvoCorePluginHttpHttpConfig_(self, conn, config);
  }
  return conn;
}

void ComScolvoCorePluginHttpHttpClient_setBasicAuthWithJavaNetHttpURLConnection_withNSString_withNSString_(ComScolvoCorePluginHttpHttpClient *self, JavaNetHttpURLConnection *conn, NSString *user, NSString *pw) {
  NSString *userpass = JreStrcat("$C$", user, ':', pw);
  NSString *basicAuth = JreStrcat("$$", @"Basic ", [NSString java_stringWithBytes:ComScolvoCorePluginBase64Base64_encodeBase64WithByteArray_([userpass java_getBytes])]);
  [((JavaNetHttpURLConnection *) nil_chk(conn)) setRequestPropertyWithNSString:@"Authorization" withNSString:basicAuth];
}

void ComScolvoCorePluginHttpHttpClient_writePostInputWithJavaNetHttpURLConnection_withComScolvoCorePluginHttpHttpConfig_(ComScolvoCorePluginHttpHttpClient *self, JavaNetHttpURLConnection *conn, ComScolvoCorePluginHttpHttpConfig *config) {
  [((JavaNetHttpURLConnection *) nil_chk(conn)) setRequestPropertyWithNSString:@"Content-Type" withNSString:[((ComScolvoCorePluginHttpHttpConfig *) nil_chk(config)) getContentType]];
  if ([@"application/json" java_equalsIgnoreCase:[config getContentType]]) {
    IOSByteArray *bytes = [((NSString *) nil_chk([config getJsonData])) java_getBytes];
    [conn setRequestPropertyWithNSString:@"Content-Length" withNSString:NSString_java_valueOfInt_(((IOSByteArray *) nil_chk(bytes))->size_)];
    [((JavaIoOutputStream *) nil_chk([conn getOutputStream])) writeWithByteArray:bytes];
  }
  else if ([@"application/x-www-form-urlencoded" java_equalsIgnoreCase:[config getContentType]]) {
    IOSByteArray *postDataBytes = ComScolvoCorePluginHttpHttpClient_buildPostDataWithJavaUtilMap_(self, [config getPostInput]);
    [conn setRequestPropertyWithNSString:@"Content-Length" withNSString:NSString_java_valueOfInt_(((IOSByteArray *) nil_chk(postDataBytes))->size_)];
    [((JavaIoOutputStream *) nil_chk([conn getOutputStream])) writeWithByteArray:postDataBytes];
  }
}

IOSByteArray *ComScolvoCorePluginHttpHttpClient_buildPostDataWithJavaUtilMap_(ComScolvoCorePluginHttpHttpClient *self, id<JavaUtilMap> params) {
  JavaLangStringBuilder *postData = create_JavaLangStringBuilder_init();
  for (id<JavaUtilMap_Entry> __strong param in nil_chk([((id<JavaUtilMap>) nil_chk(params)) entrySet])) {
    if ([postData java_length] != 0) {
      [postData appendWithChar:'&'];
    }
    [((JavaLangStringBuilder *) nil_chk([postData appendWithNSString:JavaNetURLEncoder_encodeWithNSString_withNSString_([((id<JavaUtilMap_Entry>) nil_chk(param)) getKey], @"UTF-8")])) appendWithChar:'='];
    [postData appendWithNSString:JavaNetURLEncoder_encodeWithNSString_withNSString_(NSString_java_valueOf_([param getValue]), @"UTF-8")];
  }
  return [((NSString *) nil_chk([postData description])) java_getBytesWithCharsetName:@"UTF-8"];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCorePluginHttpHttpClient)
