//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/plugin/http/InstallCert.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "IOSPrimitiveArray.h"
#include "J2ObjC_source.h"
#include "com/scolvo/core/plugin/http/InstallCert.h"
#include "java/io/File.h"
#include "java/io/FileInputStream.h"
#include "java/io/FileOutputStream.h"
#include "java/io/InputStream.h"
#include "java/io/OutputStream.h"
#include "java/io/PrintStream.h"
#include "java/lang/RuntimeException.h"
#include "java/lang/StringBuilder.h"
#include "java/lang/System.h"
#include "java/lang/Throwable.h"
#include "java/lang/UnsupportedOperationException.h"
#include "java/net/Socket.h"
#include "java/security/KeyStore.h"
#include "java/security/MessageDigest.h"
#include "java/security/Principal.h"
#include "java/security/cert/X509Certificate.h"
#include "javax/net/ssl/SSLContext.h"
#include "javax/net/ssl/SSLException.h"
#include "javax/net/ssl/SSLSocket.h"
#include "javax/net/ssl/SSLSocketFactory.h"
#include "javax/net/ssl/TrustManager.h"
#include "javax/net/ssl/TrustManagerFactory.h"
#include "javax/net/ssl/X509TrustManager.h"

@interface ComScolvoCorePluginHttpInstallCert ()

+ (NSString *)toHexStringWithByteArray:(IOSByteArray *)bytes;

@end

inline IOSCharArray *ComScolvoCorePluginHttpInstallCert_get_HEXDIGITS(void);
static IOSCharArray *ComScolvoCorePluginHttpInstallCert_HEXDIGITS;
J2OBJC_STATIC_FIELD_OBJ_FINAL(ComScolvoCorePluginHttpInstallCert, HEXDIGITS, IOSCharArray *)

__attribute__((unused)) static NSString *ComScolvoCorePluginHttpInstallCert_toHexStringWithByteArray_(IOSByteArray *bytes);

@interface ComScolvoCorePluginHttpInstallCert_SavingTrustManager : NSObject < JavaxNetSslX509TrustManager > {
 @public
  id<JavaxNetSslX509TrustManager> tm_;
  IOSObjectArray *chain_;
}

- (instancetype __nonnull)initWithJavaxNetSslX509TrustManager:(id<JavaxNetSslX509TrustManager>)tm;

- (IOSObjectArray *)getAcceptedIssuers;

- (void)checkClientTrustedWithJavaSecurityCertX509CertificateArray:(IOSObjectArray *)chain
                                                      withNSString:(NSString *)authType;

- (void)checkServerTrustedWithJavaSecurityCertX509CertificateArray:(IOSObjectArray *)chain
                                                      withNSString:(NSString *)authType;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCorePluginHttpInstallCert_SavingTrustManager)

J2OBJC_FIELD_SETTER(ComScolvoCorePluginHttpInstallCert_SavingTrustManager, tm_, id<JavaxNetSslX509TrustManager>)
J2OBJC_FIELD_SETTER(ComScolvoCorePluginHttpInstallCert_SavingTrustManager, chain_, IOSObjectArray *)

__attribute__((unused)) static void ComScolvoCorePluginHttpInstallCert_SavingTrustManager_initWithJavaxNetSslX509TrustManager_(ComScolvoCorePluginHttpInstallCert_SavingTrustManager *self, id<JavaxNetSslX509TrustManager> tm);

__attribute__((unused)) static ComScolvoCorePluginHttpInstallCert_SavingTrustManager *new_ComScolvoCorePluginHttpInstallCert_SavingTrustManager_initWithJavaxNetSslX509TrustManager_(id<JavaxNetSslX509TrustManager> tm) NS_RETURNS_RETAINED;

__attribute__((unused)) static ComScolvoCorePluginHttpInstallCert_SavingTrustManager *create_ComScolvoCorePluginHttpInstallCert_SavingTrustManager_initWithJavaxNetSslX509TrustManager_(id<JavaxNetSslX509TrustManager> tm);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCorePluginHttpInstallCert_SavingTrustManager)

J2OBJC_INITIALIZED_DEFN(ComScolvoCorePluginHttpInstallCert)

@implementation ComScolvoCorePluginHttpInstallCert

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  ComScolvoCorePluginHttpInstallCert_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

+ (void)installWithNSString:(NSString *)host
                    withInt:(jint)port
               withNSString:(NSString *)passphrase {
  ComScolvoCorePluginHttpInstallCert_installWithNSString_withInt_withNSString_(host, port, passphrase);
}

+ (NSString *)toHexStringWithByteArray:(IOSByteArray *)bytes {
  return ComScolvoCorePluginHttpInstallCert_toHexStringWithByteArray_(bytes);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x9, 0, 1, 2, -1, -1, -1 },
    { NULL, "LNSString;", 0xa, 3, 4, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(installWithNSString:withInt:withNSString:);
  methods[2].selector = @selector(toHexStringWithByteArray:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "HEXDIGITS", "[C", .constantValue.asLong = 0, 0x1a, -1, 5, -1, -1 },
  };
  static const void *ptrTable[] = { "install", "LNSString;ILNSString;", "LJavaIoFileNotFoundException;LJavaIoIOException;LJavaSecurityKeyStoreException;LJavaSecurityNoSuchAlgorithmException;LJavaSecurityCertCertificateException;LJavaSecurityKeyManagementException;", "toHexString", "[B", &ComScolvoCorePluginHttpInstallCert_HEXDIGITS, "LComScolvoCorePluginHttpInstallCert_SavingTrustManager;" };
  static const J2ObjcClassInfo _ComScolvoCorePluginHttpInstallCert = { "InstallCert", "com.scolvo.core.plugin.http", ptrTable, methods, fields, 7, 0x1, 3, 1, -1, 6, -1, -1, -1 };
  return &_ComScolvoCorePluginHttpInstallCert;
}

+ (void)initialize {
  if (self == [ComScolvoCorePluginHttpInstallCert class]) {
    JreStrongAssign(&ComScolvoCorePluginHttpInstallCert_HEXDIGITS, [@"0123456789abcdef" java_toCharArray]);
    J2OBJC_SET_INITIALIZED(ComScolvoCorePluginHttpInstallCert)
  }
}

@end

void ComScolvoCorePluginHttpInstallCert_init(ComScolvoCorePluginHttpInstallCert *self) {
  NSObject_init(self);
}

ComScolvoCorePluginHttpInstallCert *new_ComScolvoCorePluginHttpInstallCert_init() {
  J2OBJC_NEW_IMPL(ComScolvoCorePluginHttpInstallCert, init)
}

ComScolvoCorePluginHttpInstallCert *create_ComScolvoCorePluginHttpInstallCert_init() {
  J2OBJC_CREATE_IMPL(ComScolvoCorePluginHttpInstallCert, init)
}

void ComScolvoCorePluginHttpInstallCert_installWithNSString_withInt_withNSString_(NSString *host, jint port, NSString *passphrase) {
  ComScolvoCorePluginHttpInstallCert_initialize();
  [((JavaIoPrintStream *) nil_chk(JreLoadStatic(JavaLangSystem, out))) printlnWithNSString:JreStrcat("$$CI$$", @"Install cert for host ", host, ':', port, @" with passphrase ", passphrase)];
  if (host == nil || [host java_isEmpty] || port < 1) {
    @throw create_JavaLangRuntimeException_initWithNSString_(@"Usage: java new InstallCert().install(require:host, require:port, optional:passphrase);");
  }
  IOSCharArray *passphraseChars = (passphrase == nil) ? [@"changeit" java_toCharArray] : [((NSString *) nil_chk(passphrase)) java_toCharArray];
  JavaIoFile *file = create_JavaIoFile_initWithNSString_(@"jssecacerts");
  if ([file isFile] == false) {
    jchar SEP = JreLoadStatic(JavaIoFile, separatorChar);
    JavaIoFile *dir = create_JavaIoFile_initWithNSString_(JreStrcat("$C$C$", JavaLangSystem_getPropertyWithNSString_(@"java.home"), SEP, @"lib", SEP, @"security"));
    file = create_JavaIoFile_initWithJavaIoFile_withNSString_(dir, @"jssecacerts");
    if ([file isFile] == false) {
      file = create_JavaIoFile_initWithJavaIoFile_withNSString_(dir, @"cacerts");
    }
  }
  [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$@", @"Loading KeyStore to ", file)];
  JavaxNetSslSSLContext *context = JavaxNetSslSSLContext_getInstanceWithNSString_(@"TLS");
  JavaxNetSslTrustManagerFactory *tmf = JavaxNetSslTrustManagerFactory_getInstanceWithNSString_(JavaxNetSslTrustManagerFactory_getDefaultAlgorithm());
  JavaSecurityKeyStore *ks = nil;
  {
    JavaIoInputStream *in = create_JavaIoFileInputStream_initWithJavaIoFile_(file);
    JavaLangThrowable *__primaryException1 = nil;
    @try {
      ks = JavaSecurityKeyStore_getInstanceWithNSString_(JavaSecurityKeyStore_getDefaultType());
      [((JavaSecurityKeyStore *) nil_chk(ks)) load__WithJavaIoInputStream:in withCharArray:passphraseChars];
      [((JavaxNetSslTrustManagerFactory *) nil_chk(tmf)) init__WithJavaSecurityKeyStore:ks];
    }
    @catch (JavaLangThrowable *e) {
      __primaryException1 = e;
      @throw e;
    }
    @finally {
      if (in != nil) {
        if (__primaryException1 != nil) {
          @try {
            [in close];
          }
          @catch (JavaLangThrowable *e) {
            [__primaryException1 addSuppressedWithJavaLangThrowable:e];
          }
        }
        else {
          [in close];
        }
      }
    }
  }
  id<JavaxNetSslX509TrustManager> defaultTrustManager = (id<JavaxNetSslX509TrustManager>) cast_check(IOSObjectArray_Get(nil_chk([((JavaxNetSslTrustManagerFactory *) nil_chk(tmf)) getTrustManagers]), 0), JavaxNetSslX509TrustManager_class_());
  ComScolvoCorePluginHttpInstallCert_SavingTrustManager *tm = create_ComScolvoCorePluginHttpInstallCert_SavingTrustManager_initWithJavaxNetSslX509TrustManager_(defaultTrustManager);
  [((JavaxNetSslSSLContext *) nil_chk(context)) init__WithJavaxNetSslKeyManagerArray:nil withJavaxNetSslTrustManagerArray:[IOSObjectArray arrayWithObjects:(id[]){ tm } count:1 type:JavaxNetSslTrustManager_class_()] withJavaSecuritySecureRandom:nil];
  JavaxNetSslSSLSocketFactory *factory = [context getSocketFactory];
  [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$$CI$", @"Opening connection to ", host, ':', port, @"...")];
  JavaxNetSslSSLSocket *socket = (JavaxNetSslSSLSocket *) cast_chk([((JavaxNetSslSSLSocketFactory *) nil_chk(factory)) createSocketWithNSString:host withInt:port], [JavaxNetSslSSLSocket class]);
  [((JavaxNetSslSSLSocket *) nil_chk(socket)) setSoTimeoutWithInt:10000];
  @try {
    [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:@"Starting SSL handshake..."];
    [socket startHandshake];
    [socket close];
    [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:@"\nNo errors, certificate is already trusted"];
  }
  @catch (JavaxNetSslSSLException *e) {
    [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:@"SSL exception."];
    [e printStackTrace];
  }
  IOSObjectArray *chain = tm->chain_;
  if (chain == nil) {
    [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:@"Could not obtain server certificate chain"];
    return;
  }
  [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$I$", @"Server sent ", chain->size_, @" certificate(s):")];
  JavaSecurityMessageDigest *sha1 = JavaSecurityMessageDigest_getInstanceWithNSString_(@"SHA1");
  JavaSecurityMessageDigest *md5 = JavaSecurityMessageDigest_getInstanceWithNSString_(@"MD5");
  for (jint i = 0; i < chain->size_; i++) {
    JavaSecurityCertX509Certificate *cert = IOSObjectArray_Get(chain, i);
    [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("I$@", (i + 1), @" Subject {} ", [((JavaSecurityCertX509Certificate *) nil_chk(cert)) getSubjectDN])];
    [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$@", @"   Issuer  ", [cert getIssuerDN])];
    [((JavaSecurityMessageDigest *) nil_chk(sha1)) updateWithByteArray:[cert getEncoded]];
    [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$$", @"   sha1    ", ComScolvoCorePluginHttpInstallCert_toHexStringWithByteArray_([sha1 digest]))];
    [((JavaSecurityMessageDigest *) nil_chk(md5)) updateWithByteArray:[cert getEncoded]];
    [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$$", @"   md5     ", ComScolvoCorePluginHttpInstallCert_toHexStringWithByteArray_([md5 digest]))];
  }
  JavaSecurityCertX509Certificate *cert = IOSObjectArray_Get(chain, 0);
  NSString *alias = JreStrcat("$CI", host, '-', (1));
  [((JavaSecurityKeyStore *) nil_chk(ks)) setCertificateEntryWithNSString:alias withJavaSecurityCertCertificate:cert];
  {
    JavaIoOutputStream *out = create_JavaIoFileOutputStream_initWithNSString_(@"jssecacerts");
    JavaLangThrowable *__primaryException1 = nil;
    @try {
      [ks storeWithJavaIoOutputStream:out withCharArray:passphraseChars];
    }
    @catch (JavaLangThrowable *e) {
      __primaryException1 = e;
      @throw e;
    }
    @finally {
      if (out != nil) {
        if (__primaryException1 != nil) {
          @try {
            [out close];
          }
          @catch (JavaLangThrowable *e) {
            [__primaryException1 addSuppressedWithJavaLangThrowable:e];
          }
        }
        else {
          [out close];
        }
      }
    }
  }
  [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:[((JavaSecurityCertX509Certificate *) nil_chk(cert)) description]];
  [JreLoadStatic(JavaLangSystem, out) printlnWithNSString:JreStrcat("$$C", @"Added certificate to keystore 'jssecacerts' using alias '", alias, '\'')];
}

NSString *ComScolvoCorePluginHttpInstallCert_toHexStringWithByteArray_(IOSByteArray *bytes) {
  ComScolvoCorePluginHttpInstallCert_initialize();
  JavaLangStringBuilder *sb = create_JavaLangStringBuilder_initWithInt_(((IOSByteArray *) nil_chk(bytes))->size_ * 3);
  {
    IOSByteArray *a__ = bytes;
    jbyte const *b__ = a__->buffer_;
    jbyte const *e__ = b__ + a__->size_;
    while (b__ < e__) {
      jint b = *b__++;
      b &= (jint) 0xff;
      [sb appendWithChar:IOSCharArray_Get(nil_chk(ComScolvoCorePluginHttpInstallCert_HEXDIGITS), JreRShift32(b, 4))];
      [sb appendWithChar:IOSCharArray_Get(ComScolvoCorePluginHttpInstallCert_HEXDIGITS, b & 15)];
      [sb appendWithChar:' '];
    }
  }
  return [sb description];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCorePluginHttpInstallCert)

@implementation ComScolvoCorePluginHttpInstallCert_SavingTrustManager

- (instancetype __nonnull)initWithJavaxNetSslX509TrustManager:(id<JavaxNetSslX509TrustManager>)tm {
  ComScolvoCorePluginHttpInstallCert_SavingTrustManager_initWithJavaxNetSslX509TrustManager_(self, tm);
  return self;
}

- (IOSObjectArray *)getAcceptedIssuers {
  @throw create_JavaLangUnsupportedOperationException_init();
}

- (void)checkClientTrustedWithJavaSecurityCertX509CertificateArray:(IOSObjectArray *)chain
                                                      withNSString:(NSString *)authType {
  @throw create_JavaLangUnsupportedOperationException_init();
}

- (void)checkServerTrustedWithJavaSecurityCertX509CertificateArray:(IOSObjectArray *)chain
                                                      withNSString:(NSString *)authType {
  JreStrongAssign(&self->chain_, chain);
  [((id<JavaxNetSslX509TrustManager>) nil_chk(tm_)) checkServerTrustedWithJavaSecurityCertX509CertificateArray:chain withNSString:authType];
}

- (void)dealloc {
  RELEASE_(tm_);
  RELEASE_(chain_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "[LJavaSecurityCertX509Certificate;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, 3, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 2, 3, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaxNetSslX509TrustManager:);
  methods[1].selector = @selector(getAcceptedIssuers);
  methods[2].selector = @selector(checkClientTrustedWithJavaSecurityCertX509CertificateArray:withNSString:);
  methods[3].selector = @selector(checkServerTrustedWithJavaSecurityCertX509CertificateArray:withNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "tm_", "LJavaxNetSslX509TrustManager;", .constantValue.asLong = 0, 0x12, -1, -1, -1, -1 },
    { "chain_", "[LJavaSecurityCertX509Certificate;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LJavaxNetSslX509TrustManager;", "checkClientTrusted", "[LJavaSecurityCertX509Certificate;LNSString;", "LJavaSecurityCertCertificateException;", "checkServerTrusted", "LComScolvoCorePluginHttpInstallCert;" };
  static const J2ObjcClassInfo _ComScolvoCorePluginHttpInstallCert_SavingTrustManager = { "SavingTrustManager", "com.scolvo.core.plugin.http", ptrTable, methods, fields, 7, 0xa, 4, 2, 5, -1, -1, -1, -1 };
  return &_ComScolvoCorePluginHttpInstallCert_SavingTrustManager;
}

@end

void ComScolvoCorePluginHttpInstallCert_SavingTrustManager_initWithJavaxNetSslX509TrustManager_(ComScolvoCorePluginHttpInstallCert_SavingTrustManager *self, id<JavaxNetSslX509TrustManager> tm) {
  NSObject_init(self);
  JreStrongAssign(&self->tm_, tm);
}

ComScolvoCorePluginHttpInstallCert_SavingTrustManager *new_ComScolvoCorePluginHttpInstallCert_SavingTrustManager_initWithJavaxNetSslX509TrustManager_(id<JavaxNetSslX509TrustManager> tm) {
  J2OBJC_NEW_IMPL(ComScolvoCorePluginHttpInstallCert_SavingTrustManager, initWithJavaxNetSslX509TrustManager_, tm)
}

ComScolvoCorePluginHttpInstallCert_SavingTrustManager *create_ComScolvoCorePluginHttpInstallCert_SavingTrustManager_initWithJavaxNetSslX509TrustManager_(id<JavaxNetSslX509TrustManager> tm) {
  J2OBJC_CREATE_IMPL(ComScolvoCorePluginHttpInstallCert_SavingTrustManager, initWithJavaxNetSslX509TrustManager_, tm)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCorePluginHttpInstallCert_SavingTrustManager)
