//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/plugin/base64/BaseNCodec.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCorePluginBase64BaseNCodec")
#ifdef RESTRICT_ComScolvoCorePluginBase64BaseNCodec
#define INCLUDE_ALL_ComScolvoCorePluginBase64BaseNCodec 0
#else
#define INCLUDE_ALL_ComScolvoCorePluginBase64BaseNCodec 1
#endif
#undef RESTRICT_ComScolvoCorePluginBase64BaseNCodec

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCorePluginBase64BaseNCodec_) && (INCLUDE_ALL_ComScolvoCorePluginBase64BaseNCodec || defined(INCLUDE_ComScolvoCorePluginBase64BaseNCodec))
#define ComScolvoCorePluginBase64BaseNCodec_

@class ComScolvoCorePluginBase64BaseNCodec_Context;
@class IOSByteArray;

@interface ComScolvoCorePluginBase64BaseNCodec : NSObject {
 @public
  jint lineLength_;
}

+ (jint)EOF_;

+ (jint)MIME_CHUNK_SIZE;

+ (jint)PEM_CHUNK_SIZE;

+ (jint)MASK_8BITS;

+ (jbyte)PAD_DEFAULT;

+ (jbyte)PAD;

#pragma mark Public

+ (NSString *)bytesAsUtf8StringWithByteArray:(IOSByteArray *)bytes;

- (IOSByteArray *)decodeWithByteArray:(IOSByteArray *)pArray;

- (id)decodeWithId:(id)obj;

- (IOSByteArray *)decodeWithNSString:(NSString *)pArray;

- (IOSByteArray *)encodeWithByteArray:(IOSByteArray *)pArray;

- (id)encodeWithId:(id)obj;

- (NSString *)encodeAsStringWithByteArray:(IOSByteArray *)pArray;

- (NSString *)encodeToStringWithByteArray:(IOSByteArray *)pArray;

- (jlong)getEncodedLengthWithByteArray:(IOSByteArray *)pArray;

- (jboolean)isInAlphabetWithByteArray:(IOSByteArray *)arrayOctet
                          withBoolean:(jboolean)allowWSPad;

- (jboolean)isInAlphabetWithNSString:(NSString *)basen;

+ (IOSByteArray *)stringAsUTF8BytesWithNSString:(NSString *)str;

#pragma mark Protected

- (instancetype __nonnull)initWithInt:(jint)unencodedBlockSize
                              withInt:(jint)encodedBlockSize
                              withInt:(jint)lineLength
                              withInt:(jint)chunkSeparatorLength;

- (jboolean)containsAlphabetOrPadWithByteArray:(IOSByteArray *)arrayOctet;

- (IOSByteArray *)ensureBufferSizeWithInt:(jint)size
withComScolvoCorePluginBase64BaseNCodec_Context:(ComScolvoCorePluginBase64BaseNCodec_Context *)context;

- (jint)getDefaultBufferSize;

- (jboolean)isInAlphabetWithByte:(jbyte)value;

+ (jboolean)isWhiteSpaceWithByte:(jbyte)byteToCheck;

#pragma mark Package-Private

- (jint)availableWithComScolvoCorePluginBase64BaseNCodec_Context:(ComScolvoCorePluginBase64BaseNCodec_Context *)context;

- (void)decodeWithByteArray:(IOSByteArray *)pArray
                    withInt:(jint)i
                    withInt:(jint)length
withComScolvoCorePluginBase64BaseNCodec_Context:(ComScolvoCorePluginBase64BaseNCodec_Context *)context;

- (void)encodeWithByteArray:(IOSByteArray *)pArray
                    withInt:(jint)i
                    withInt:(jint)length
withComScolvoCorePluginBase64BaseNCodec_Context:(ComScolvoCorePluginBase64BaseNCodec_Context *)context;

- (jboolean)hasDataWithComScolvoCorePluginBase64BaseNCodec_Context:(ComScolvoCorePluginBase64BaseNCodec_Context *)context;

- (jint)readResultsWithByteArray:(IOSByteArray *)b
                         withInt:(jint)bPos
                         withInt:(jint)bAvail
withComScolvoCorePluginBase64BaseNCodec_Context:(ComScolvoCorePluginBase64BaseNCodec_Context *)context;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCorePluginBase64BaseNCodec)

inline jint ComScolvoCorePluginBase64BaseNCodec_get_EOF(void);
#define ComScolvoCorePluginBase64BaseNCodec_EOF -1
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCorePluginBase64BaseNCodec, EOF, jint)

inline jint ComScolvoCorePluginBase64BaseNCodec_get_MIME_CHUNK_SIZE(void);
#define ComScolvoCorePluginBase64BaseNCodec_MIME_CHUNK_SIZE 76
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCorePluginBase64BaseNCodec, MIME_CHUNK_SIZE, jint)

inline jint ComScolvoCorePluginBase64BaseNCodec_get_PEM_CHUNK_SIZE(void);
#define ComScolvoCorePluginBase64BaseNCodec_PEM_CHUNK_SIZE 64
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCorePluginBase64BaseNCodec, PEM_CHUNK_SIZE, jint)

inline jint ComScolvoCorePluginBase64BaseNCodec_get_MASK_8BITS(void);
#define ComScolvoCorePluginBase64BaseNCodec_MASK_8BITS 255
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCorePluginBase64BaseNCodec, MASK_8BITS, jint)

inline jbyte ComScolvoCorePluginBase64BaseNCodec_get_PAD_DEFAULT(void);
#define ComScolvoCorePluginBase64BaseNCodec_PAD_DEFAULT 61
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCorePluginBase64BaseNCodec, PAD_DEFAULT, jbyte)

inline jbyte ComScolvoCorePluginBase64BaseNCodec_get_PAD(void);
#define ComScolvoCorePluginBase64BaseNCodec_PAD 61
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCorePluginBase64BaseNCodec, PAD, jbyte)

FOUNDATION_EXPORT void ComScolvoCorePluginBase64BaseNCodec_initWithInt_withInt_withInt_withInt_(ComScolvoCorePluginBase64BaseNCodec *self, jint unencodedBlockSize, jint encodedBlockSize, jint lineLength, jint chunkSeparatorLength);

FOUNDATION_EXPORT jboolean ComScolvoCorePluginBase64BaseNCodec_isWhiteSpaceWithByte_(jbyte byteToCheck);

FOUNDATION_EXPORT NSString *ComScolvoCorePluginBase64BaseNCodec_bytesAsUtf8StringWithByteArray_(IOSByteArray *bytes);

FOUNDATION_EXPORT IOSByteArray *ComScolvoCorePluginBase64BaseNCodec_stringAsUTF8BytesWithNSString_(NSString *str);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCorePluginBase64BaseNCodec)

#endif

#if !defined (ComScolvoCorePluginBase64BaseNCodec_Context_) && (INCLUDE_ALL_ComScolvoCorePluginBase64BaseNCodec || defined(INCLUDE_ComScolvoCorePluginBase64BaseNCodec_Context))
#define ComScolvoCorePluginBase64BaseNCodec_Context_

@class IOSByteArray;

@interface ComScolvoCorePluginBase64BaseNCodec_Context : NSObject {
 @public
  jint ibitWorkArea_;
  jlong lbitWorkArea_;
  IOSByteArray *buffer_;
  jint pos_;
  jint readPos_;
  jboolean eof_;
  jint currentLinePos_;
  jint modulus_;
}

#pragma mark Public

- (NSString *)description;

#pragma mark Package-Private

- (instancetype __nonnull)init;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCorePluginBase64BaseNCodec_Context)

J2OBJC_FIELD_SETTER(ComScolvoCorePluginBase64BaseNCodec_Context, buffer_, IOSByteArray *)

FOUNDATION_EXPORT void ComScolvoCorePluginBase64BaseNCodec_Context_init(ComScolvoCorePluginBase64BaseNCodec_Context *self);

FOUNDATION_EXPORT ComScolvoCorePluginBase64BaseNCodec_Context *new_ComScolvoCorePluginBase64BaseNCodec_Context_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCorePluginBase64BaseNCodec_Context *create_ComScolvoCorePluginBase64BaseNCodec_Context_init(void);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCorePluginBase64BaseNCodec_Context)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCorePluginBase64BaseNCodec")
