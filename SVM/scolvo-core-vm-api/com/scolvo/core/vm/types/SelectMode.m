//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/vm/types/SelectMode.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "com/scolvo/core/vm/types/SelectMode.h"
#include "java/lang/Enum.h"
#include "java/lang/IllegalArgumentException.h"

__attribute__((unused)) static void ComScolvoCoreVmTypesSelectMode_initWithNSString_withInt_(ComScolvoCoreVmTypesSelectMode *self, NSString *__name, jint __ordinal);

J2OBJC_INITIALIZED_DEFN(ComScolvoCoreVmTypesSelectMode)

ComScolvoCoreVmTypesSelectMode *ComScolvoCoreVmTypesSelectMode_values_[3];

@implementation ComScolvoCoreVmTypesSelectMode

+ (ComScolvoCoreVmTypesSelectMode *)none {
  return JreEnum(ComScolvoCoreVmTypesSelectMode, none);
}

+ (ComScolvoCoreVmTypesSelectMode *)multi {
  return JreEnum(ComScolvoCoreVmTypesSelectMode, multi);
}

+ (ComScolvoCoreVmTypesSelectMode *)single {
  return JreEnum(ComScolvoCoreVmTypesSelectMode, single);
}

+ (IOSObjectArray *)values {
  return ComScolvoCoreVmTypesSelectMode_values();
}

+ (ComScolvoCoreVmTypesSelectMode *)valueOfWithNSString:(NSString *)name {
  return ComScolvoCoreVmTypesSelectMode_valueOfWithNSString_(name);
}

- (ComScolvoCoreVmTypesSelectMode_Enum)toNSEnum {
  return (ComScolvoCoreVmTypesSelectMode_Enum)[self ordinal];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "[LComScolvoCoreVmTypesSelectMode;", 0x9, -1, -1, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreVmTypesSelectMode;", 0x9, 0, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(values);
  methods[1].selector = @selector(valueOfWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "none", "LComScolvoCoreVmTypesSelectMode;", .constantValue.asLong = 0, 0x4019, -1, 2, -1, -1 },
    { "multi", "LComScolvoCoreVmTypesSelectMode;", .constantValue.asLong = 0, 0x4019, -1, 3, -1, -1 },
    { "single", "LComScolvoCoreVmTypesSelectMode;", .constantValue.asLong = 0, 0x4019, -1, 4, -1, -1 },
  };
  static const void *ptrTable[] = { "valueOf", "LNSString;", &JreEnum(ComScolvoCoreVmTypesSelectMode, none), &JreEnum(ComScolvoCoreVmTypesSelectMode, multi), &JreEnum(ComScolvoCoreVmTypesSelectMode, single), "Ljava/lang/Enum<Lcom/scolvo/core/vm/types/SelectMode;>;" };
  static const J2ObjcClassInfo _ComScolvoCoreVmTypesSelectMode = { "SelectMode", "com.scolvo.core.vm.types", ptrTable, methods, fields, 7, 0x4011, 2, 3, -1, -1, -1, 5, -1 };
  return &_ComScolvoCoreVmTypesSelectMode;
}

+ (void)initialize {
  if (self == [ComScolvoCoreVmTypesSelectMode class]) {
    size_t objSize = class_getInstanceSize(self);
    size_t allocSize = 3 * objSize;
    uintptr_t ptr = (uintptr_t)calloc(allocSize, 1);
    id e;
    for (jint i = 0; i < 3; i++) {
      ((void)(ComScolvoCoreVmTypesSelectMode_values_[i] = e = objc_constructInstance(self, (void *)ptr)), ptr += objSize);
      ComScolvoCoreVmTypesSelectMode_initWithNSString_withInt_(e, JreEnumConstantName(ComScolvoCoreVmTypesSelectMode_class_(), i), i);
    }
    J2OBJC_SET_INITIALIZED(ComScolvoCoreVmTypesSelectMode)
  }
}

@end

void ComScolvoCoreVmTypesSelectMode_initWithNSString_withInt_(ComScolvoCoreVmTypesSelectMode *self, NSString *__name, jint __ordinal) {
  JavaLangEnum_initWithNSString_withInt_(self, __name, __ordinal);
}

IOSObjectArray *ComScolvoCoreVmTypesSelectMode_values() {
  ComScolvoCoreVmTypesSelectMode_initialize();
  return [IOSObjectArray arrayWithObjects:ComScolvoCoreVmTypesSelectMode_values_ count:3 type:ComScolvoCoreVmTypesSelectMode_class_()];
}

ComScolvoCoreVmTypesSelectMode *ComScolvoCoreVmTypesSelectMode_valueOfWithNSString_(NSString *name) {
  ComScolvoCoreVmTypesSelectMode_initialize();
  for (int i = 0; i < 3; i++) {
    ComScolvoCoreVmTypesSelectMode *e = ComScolvoCoreVmTypesSelectMode_values_[i];
    if ([name isEqual:[e name]]) {
      return e;
    }
  }
  @throw create_JavaLangIllegalArgumentException_initWithNSString_(name);
  return nil;
}

ComScolvoCoreVmTypesSelectMode *ComScolvoCoreVmTypesSelectMode_fromOrdinal(NSUInteger ordinal) {
  ComScolvoCoreVmTypesSelectMode_initialize();
  if (ordinal >= 3) {
    return nil;
  }
  return ComScolvoCoreVmTypesSelectMode_values_[ordinal];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreVmTypesSelectMode)
