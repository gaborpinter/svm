//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/vm/types/SelectMode.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreVmTypesSelectMode")
#ifdef RESTRICT_ComScolvoCoreVmTypesSelectMode
#define INCLUDE_ALL_ComScolvoCoreVmTypesSelectMode 0
#else
#define INCLUDE_ALL_ComScolvoCoreVmTypesSelectMode 1
#endif
#undef RESTRICT_ComScolvoCoreVmTypesSelectMode

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreVmTypesSelectMode_) && (INCLUDE_ALL_ComScolvoCoreVmTypesSelectMode || defined(INCLUDE_ComScolvoCoreVmTypesSelectMode))
#define ComScolvoCoreVmTypesSelectMode_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

@class IOSObjectArray;

typedef NS_ENUM(NSUInteger, ComScolvoCoreVmTypesSelectMode_Enum) {
  ComScolvoCoreVmTypesSelectMode_Enum_none = 0,
  ComScolvoCoreVmTypesSelectMode_Enum_multi = 1,
  ComScolvoCoreVmTypesSelectMode_Enum_single = 2,
};

@interface ComScolvoCoreVmTypesSelectMode : JavaLangEnum

+ (ComScolvoCoreVmTypesSelectMode * __nonnull)none;

+ (ComScolvoCoreVmTypesSelectMode * __nonnull)multi;

+ (ComScolvoCoreVmTypesSelectMode * __nonnull)single;

#pragma mark Public

+ (ComScolvoCoreVmTypesSelectMode *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (ComScolvoCoreVmTypesSelectMode_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(ComScolvoCoreVmTypesSelectMode)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT ComScolvoCoreVmTypesSelectMode *ComScolvoCoreVmTypesSelectMode_values_[];

inline ComScolvoCoreVmTypesSelectMode *ComScolvoCoreVmTypesSelectMode_get_none(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreVmTypesSelectMode, none)

inline ComScolvoCoreVmTypesSelectMode *ComScolvoCoreVmTypesSelectMode_get_multi(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreVmTypesSelectMode, multi)

inline ComScolvoCoreVmTypesSelectMode *ComScolvoCoreVmTypesSelectMode_get_single(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreVmTypesSelectMode, single)

FOUNDATION_EXPORT IOSObjectArray *ComScolvoCoreVmTypesSelectMode_values(void);

FOUNDATION_EXPORT ComScolvoCoreVmTypesSelectMode *ComScolvoCoreVmTypesSelectMode_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT ComScolvoCoreVmTypesSelectMode *ComScolvoCoreVmTypesSelectMode_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreVmTypesSelectMode)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreVmTypesSelectMode")
