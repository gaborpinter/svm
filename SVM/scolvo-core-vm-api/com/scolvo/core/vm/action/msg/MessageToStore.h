//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/vm/action/msg/MessageToStore.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreVmActionMsgMessageToStore")
#ifdef RESTRICT_ComScolvoCoreVmActionMsgMessageToStore
#define INCLUDE_ALL_ComScolvoCoreVmActionMsgMessageToStore 0
#else
#define INCLUDE_ALL_ComScolvoCoreVmActionMsgMessageToStore 1
#endif
#undef RESTRICT_ComScolvoCoreVmActionMsgMessageToStore

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreVmActionMsgMessageToStore_) && (INCLUDE_ALL_ComScolvoCoreVmActionMsgMessageToStore || defined(INCLUDE_ComScolvoCoreVmActionMsgMessageToStore))
#define ComScolvoCoreVmActionMsgMessageToStore_

#define RESTRICT_JavaIoSerializable 1
#define INCLUDE_JavaIoSerializable 1
#include "java/io/Serializable.h"

@interface ComScolvoCoreVmActionMsgMessageToStore : NSObject < JavaIoSerializable >

#pragma mark Public

- (instancetype __nonnull)initWithNSString:(NSString *)message;

- (NSString *)getMessage;

- (NSString *)description;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreVmActionMsgMessageToStore)

FOUNDATION_EXPORT void ComScolvoCoreVmActionMsgMessageToStore_initWithNSString_(ComScolvoCoreVmActionMsgMessageToStore *self, NSString *message);

FOUNDATION_EXPORT ComScolvoCoreVmActionMsgMessageToStore *new_ComScolvoCoreVmActionMsgMessageToStore_initWithNSString_(NSString *message) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreVmActionMsgMessageToStore *create_ComScolvoCoreVmActionMsgMessageToStore_initWithNSString_(NSString *message);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreVmActionMsgMessageToStore)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreVmActionMsgMessageToStore")
