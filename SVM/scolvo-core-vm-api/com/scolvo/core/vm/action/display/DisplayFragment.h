//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/vm/action/display/DisplayFragment.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreVmActionDisplayDisplayFragment")
#ifdef RESTRICT_ComScolvoCoreVmActionDisplayDisplayFragment
#define INCLUDE_ALL_ComScolvoCoreVmActionDisplayDisplayFragment 0
#else
#define INCLUDE_ALL_ComScolvoCoreVmActionDisplayDisplayFragment 1
#endif
#undef RESTRICT_ComScolvoCoreVmActionDisplayDisplayFragment

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreVmActionDisplayDisplayFragment_) && (INCLUDE_ALL_ComScolvoCoreVmActionDisplayDisplayFragment || defined(INCLUDE_ComScolvoCoreVmActionDisplayDisplayFragment))
#define ComScolvoCoreVmActionDisplayDisplayFragment_

#define RESTRICT_ComScolvoCoreVmActionUserInterfaceMsg 1
#define INCLUDE_ComScolvoCoreVmActionUserInterfaceMsg 1
#include "com/scolvo/core/vm/action/UserInterfaceMsg.h"

@class ComScolvoCoreModelPageFragment;
@protocol JavaUtilList;
@protocol JavaUtilMap;

@interface ComScolvoCoreVmActionDisplayDisplayFragment : ComScolvoCoreVmActionUserInterfaceMsg

#pragma mark Public

- (instancetype __nonnull)initWithNSString:(NSString *)originId
                              withNSString:(NSString *)fragmentContainerName
        withComScolvoCoreModelPageFragment:(ComScolvoCoreModelPageFragment *)fragment
                           withJavaUtilMap:(id<JavaUtilMap>)data
                           withJavaUtilMap:(id<JavaUtilMap>)dictionary;

- (id<JavaUtilList>)getActionEvents;

- (id<JavaUtilMap>)getData;

- (id<JavaUtilMap>)getDictionary;

- (ComScolvoCoreModelPageFragment *)getFragment;

- (NSString *)getFragmentContainerName;

- (void)setActionEventsWithJavaUtilList:(id<JavaUtilList>)actionEvents;

- (NSString *)description;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithNSString:(NSString *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreVmActionDisplayDisplayFragment)

FOUNDATION_EXPORT void ComScolvoCoreVmActionDisplayDisplayFragment_initWithNSString_withNSString_withComScolvoCoreModelPageFragment_withJavaUtilMap_withJavaUtilMap_(ComScolvoCoreVmActionDisplayDisplayFragment *self, NSString *originId, NSString *fragmentContainerName, ComScolvoCoreModelPageFragment *fragment, id<JavaUtilMap> data, id<JavaUtilMap> dictionary);

FOUNDATION_EXPORT ComScolvoCoreVmActionDisplayDisplayFragment *new_ComScolvoCoreVmActionDisplayDisplayFragment_initWithNSString_withNSString_withComScolvoCoreModelPageFragment_withJavaUtilMap_withJavaUtilMap_(NSString *originId, NSString *fragmentContainerName, ComScolvoCoreModelPageFragment *fragment, id<JavaUtilMap> data, id<JavaUtilMap> dictionary) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreVmActionDisplayDisplayFragment *create_ComScolvoCoreVmActionDisplayDisplayFragment_initWithNSString_withNSString_withComScolvoCoreModelPageFragment_withJavaUtilMap_withJavaUtilMap_(NSString *originId, NSString *fragmentContainerName, ComScolvoCoreModelPageFragment *fragment, id<JavaUtilMap> data, id<JavaUtilMap> dictionary);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreVmActionDisplayDisplayFragment)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreVmActionDisplayDisplayFragment")
