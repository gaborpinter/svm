//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/vm/action/display/DisplayLoginPage.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreVmActionDisplayDisplayLoginPage")
#ifdef RESTRICT_ComScolvoCoreVmActionDisplayDisplayLoginPage
#define INCLUDE_ALL_ComScolvoCoreVmActionDisplayDisplayLoginPage 0
#else
#define INCLUDE_ALL_ComScolvoCoreVmActionDisplayDisplayLoginPage 1
#endif
#undef RESTRICT_ComScolvoCoreVmActionDisplayDisplayLoginPage

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreVmActionDisplayDisplayLoginPage_) && (INCLUDE_ALL_ComScolvoCoreVmActionDisplayDisplayLoginPage || defined(INCLUDE_ComScolvoCoreVmActionDisplayDisplayLoginPage))
#define ComScolvoCoreVmActionDisplayDisplayLoginPage_

#define RESTRICT_ComScolvoCoreVmActionUserInterfaceMsg 1
#define INCLUDE_ComScolvoCoreVmActionUserInterfaceMsg 1
#include "com/scolvo/core/vm/action/UserInterfaceMsg.h"

@interface ComScolvoCoreVmActionDisplayDisplayLoginPage : ComScolvoCoreVmActionUserInterfaceMsg

#pragma mark Public

- (instancetype __nonnull)initWithNSString:(NSString *)originId;

- (NSString *)description;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreVmActionDisplayDisplayLoginPage)

FOUNDATION_EXPORT void ComScolvoCoreVmActionDisplayDisplayLoginPage_initWithNSString_(ComScolvoCoreVmActionDisplayDisplayLoginPage *self, NSString *originId);

FOUNDATION_EXPORT ComScolvoCoreVmActionDisplayDisplayLoginPage *new_ComScolvoCoreVmActionDisplayDisplayLoginPage_initWithNSString_(NSString *originId) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreVmActionDisplayDisplayLoginPage *create_ComScolvoCoreVmActionDisplayDisplayLoginPage_initWithNSString_(NSString *originId);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreVmActionDisplayDisplayLoginPage)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreVmActionDisplayDisplayLoginPage")
