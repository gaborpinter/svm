//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/vm/action/rp/Response.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/vm/action/rp/Response.h"
#include "com/scolvo/core/vm/action/rp/ResponseBody.h"
#include "com/scolvo/core/vm/action/rp/ResponseHeader.h"
#include "com/scolvo/core/vm/action/rq/Request.h"
#include "com/scolvo/core/vm/action/rq/RequestHeader.h"
#include "com/scolvo/core/vm/types/MessagingStatus.h"
#include "java/lang/Long.h"
#include "java/lang/System.h"
#include "java/util/UUID.h"

@interface ComScolvoCoreVmActionRpResponse () {
 @public
  ComScolvoCoreVmActionRpResponseHeader *header_;
  id<ComScolvoCoreVmActionRpResponseBody> body_;
}

- (instancetype __nonnull)init;

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreVmActionRpResponse, header_, ComScolvoCoreVmActionRpResponseHeader *)
J2OBJC_FIELD_SETTER(ComScolvoCoreVmActionRpResponse, body_, id<ComScolvoCoreVmActionRpResponseBody>)

inline jlong ComScolvoCoreVmActionRpResponse_get_serialVersionUID(void);
#define ComScolvoCoreVmActionRpResponse_serialVersionUID 2775443920890122960LL
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCoreVmActionRpResponse, serialVersionUID, jlong)

__attribute__((unused)) static void ComScolvoCoreVmActionRpResponse_init(ComScolvoCoreVmActionRpResponse *self);

__attribute__((unused)) static ComScolvoCoreVmActionRpResponse *new_ComScolvoCoreVmActionRpResponse_init(void) NS_RETURNS_RETAINED;

__attribute__((unused)) static ComScolvoCoreVmActionRpResponse *create_ComScolvoCoreVmActionRpResponse_init(void);

@implementation ComScolvoCoreVmActionRpResponse

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  ComScolvoCoreVmActionRpResponse_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (ComScolvoCoreVmActionRpResponseHeader *)getHeader {
  return create_ComScolvoCoreVmActionRpResponseHeader_initWithComScolvoCoreVmActionRpResponseHeader_(self->header_);
}

- (void)setHeaderWithComScolvoCoreVmActionRpResponseHeader:(ComScolvoCoreVmActionRpResponseHeader *)header {
  JreStrongAssignAndConsume(&self->header_, new_ComScolvoCoreVmActionRpResponseHeader_initWithComScolvoCoreVmActionRpResponseHeader_(header));
}

- (id<ComScolvoCoreVmActionRpResponseBody>)getBody {
  return self->body_;
}

- (void)setBodyWithComScolvoCoreVmActionRpResponseBody:(id<ComScolvoCoreVmActionRpResponseBody>)body {
  JreStrongAssign(&self->body_, body);
}

+ (ComScolvoCoreVmActionRpResponse *)fromRequestWithComScolvoCoreVmActionRqRequest:(ComScolvoCoreVmActionRqRequest *)rq {
  return ComScolvoCoreVmActionRpResponse_fromRequestWithComScolvoCoreVmActionRqRequest_(rq);
}

+ (ComScolvoCoreVmActionRpResponse *)fromRequestWithComScolvoCoreVmActionRqRequest:(ComScolvoCoreVmActionRqRequest *)rq
                                           withComScolvoCoreVmTypesMessagingStatus:(ComScolvoCoreVmTypesMessagingStatus *)status {
  return ComScolvoCoreVmActionRpResponse_fromRequestWithComScolvoCoreVmActionRqRequest_withComScolvoCoreVmTypesMessagingStatus_(rq, status);
}

- (NSString *)description {
  return JreStrcat("$@$@C", @"Response [header=", header_, @", body=", body_, ']');
}

- (void)dealloc {
  RELEASE_(header_);
  RELEASE_(body_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x2, -1, -1, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreVmActionRpResponseHeader;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreVmActionRpResponseBody;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreVmActionRpResponse;", 0x9, 4, 5, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreVmActionRpResponse;", 0x9, 4, 6, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 7, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(getHeader);
  methods[2].selector = @selector(setHeaderWithComScolvoCoreVmActionRpResponseHeader:);
  methods[3].selector = @selector(getBody);
  methods[4].selector = @selector(setBodyWithComScolvoCoreVmActionRpResponseBody:);
  methods[5].selector = @selector(fromRequestWithComScolvoCoreVmActionRqRequest:);
  methods[6].selector = @selector(fromRequestWithComScolvoCoreVmActionRqRequest:withComScolvoCoreVmTypesMessagingStatus:);
  methods[7].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = ComScolvoCoreVmActionRpResponse_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "header_", "LComScolvoCoreVmActionRpResponseHeader;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "body_", "LComScolvoCoreVmActionRpResponseBody;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "setHeader", "LComScolvoCoreVmActionRpResponseHeader;", "setBody", "LComScolvoCoreVmActionRpResponseBody;", "fromRequest", "LComScolvoCoreVmActionRqRequest;", "LComScolvoCoreVmActionRqRequest;LComScolvoCoreVmTypesMessagingStatus;", "toString" };
  static const J2ObjcClassInfo _ComScolvoCoreVmActionRpResponse = { "Response", "com.scolvo.core.vm.action.rp", ptrTable, methods, fields, 7, 0x1, 8, 3, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreVmActionRpResponse;
}

@end

void ComScolvoCoreVmActionRpResponse_init(ComScolvoCoreVmActionRpResponse *self) {
  NSObject_init(self);
}

ComScolvoCoreVmActionRpResponse *new_ComScolvoCoreVmActionRpResponse_init() {
  J2OBJC_NEW_IMPL(ComScolvoCoreVmActionRpResponse, init)
}

ComScolvoCoreVmActionRpResponse *create_ComScolvoCoreVmActionRpResponse_init() {
  J2OBJC_CREATE_IMPL(ComScolvoCoreVmActionRpResponse, init)
}

ComScolvoCoreVmActionRpResponse *ComScolvoCoreVmActionRpResponse_fromRequestWithComScolvoCoreVmActionRqRequest_(ComScolvoCoreVmActionRqRequest *rq) {
  ComScolvoCoreVmActionRpResponse_initialize();
  return ComScolvoCoreVmActionRpResponse_fromRequestWithComScolvoCoreVmActionRqRequest_withComScolvoCoreVmTypesMessagingStatus_(rq, JreLoadEnum(ComScolvoCoreVmTypesMessagingStatus, OK));
}

ComScolvoCoreVmActionRpResponse *ComScolvoCoreVmActionRpResponse_fromRequestWithComScolvoCoreVmActionRqRequest_withComScolvoCoreVmTypesMessagingStatus_(ComScolvoCoreVmActionRqRequest *rq, ComScolvoCoreVmTypesMessagingStatus *status) {
  ComScolvoCoreVmActionRpResponse_initialize();
  ComScolvoCoreVmActionRpResponse *response = create_ComScolvoCoreVmActionRpResponse_init();
  ComScolvoCoreVmActionRpResponseHeader *header = create_ComScolvoCoreVmActionRpResponseHeader_init();
  [header setCorrelationIdWithNSString:[((ComScolvoCoreVmActionRqRequestHeader *) nil_chk([((ComScolvoCoreVmActionRqRequest *) nil_chk(rq)) getHeader])) getMessageId]];
  [header setMessageTypeWithNSString:[((NSString *) nil_chk([((ComScolvoCoreVmActionRqRequestHeader *) nil_chk([rq getHeader])) getMessageType])) java_replaceAll:@"Rq$" withReplacement:@"Rp"]];
  [header setStatusWithComScolvoCoreVmTypesMessagingStatus:status];
  [header setMessageIdWithNSString:[((JavaUtilUUID *) nil_chk(JavaUtilUUID_randomUUID())) description]];
  [header setCreatedWithJavaLangLong:JavaLangLong_valueOfWithLong_(JavaLangSystem_currentTimeMillis())];
  [response setHeaderWithComScolvoCoreVmActionRpResponseHeader:header];
  return response;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreVmActionRpResponse)
