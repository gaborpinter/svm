//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/vm/action/rp/ResponseBody.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/vm/action/rp/ResponseBody.h"

@interface ComScolvoCoreVmActionRpResponseBody : NSObject

@end

@implementation ComScolvoCoreVmActionRpResponseBody

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LNSString;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(getOriginId);
  methods[1].selector = @selector(setOriginIdWithNSString:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "setOriginId", "LNSString;" };
  static const J2ObjcClassInfo _ComScolvoCoreVmActionRpResponseBody = { "ResponseBody", "com.scolvo.core.vm.action.rp", ptrTable, methods, NULL, 7, 0x609, 2, 0, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreVmActionRpResponseBody;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(ComScolvoCoreVmActionRpResponseBody)
