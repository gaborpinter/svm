//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/vm/action/Logout.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreVmActionLogout")
#ifdef RESTRICT_ComScolvoCoreVmActionLogout
#define INCLUDE_ALL_ComScolvoCoreVmActionLogout 0
#else
#define INCLUDE_ALL_ComScolvoCoreVmActionLogout 1
#endif
#undef RESTRICT_ComScolvoCoreVmActionLogout

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreVmActionLogout_) && (INCLUDE_ALL_ComScolvoCoreVmActionLogout || defined(INCLUDE_ComScolvoCoreVmActionLogout))
#define ComScolvoCoreVmActionLogout_

#define RESTRICT_JavaIoSerializable 1
#define INCLUDE_JavaIoSerializable 1
#include "java/io/Serializable.h"

@interface ComScolvoCoreVmActionLogout : NSObject < JavaIoSerializable >

#pragma mark Public

- (instancetype __nonnull)initWithNSString:(NSString *)originId;

- (NSString *)getOriginId;

- (NSString *)description;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreVmActionLogout)

FOUNDATION_EXPORT void ComScolvoCoreVmActionLogout_initWithNSString_(ComScolvoCoreVmActionLogout *self, NSString *originId);

FOUNDATION_EXPORT ComScolvoCoreVmActionLogout *new_ComScolvoCoreVmActionLogout_initWithNSString_(NSString *originId) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreVmActionLogout *create_ComScolvoCoreVmActionLogout_initWithNSString_(NSString *originId);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreVmActionLogout)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreVmActionLogout")
