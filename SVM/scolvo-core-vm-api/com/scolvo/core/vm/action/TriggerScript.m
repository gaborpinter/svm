//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/vm/action/TriggerScript.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/vm/action/ScriptMsg.h"
#include "com/scolvo/core/vm/action/TriggerScript.h"

@interface ComScolvoCoreVmActionTriggerScript () {
 @public
  NSString *trigger_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreVmActionTriggerScript, trigger_, NSString *)

inline jlong ComScolvoCoreVmActionTriggerScript_get_serialVersionUID(void);
#define ComScolvoCoreVmActionTriggerScript_serialVersionUID 4668700873772972933LL
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCoreVmActionTriggerScript, serialVersionUID, jlong)

@implementation ComScolvoCoreVmActionTriggerScript

- (instancetype __nonnull)initWithNSString:(NSString *)trigger
                              withNSString:(NSString *)originId {
  ComScolvoCoreVmActionTriggerScript_initWithNSString_withNSString_(self, trigger, originId);
  return self;
}

- (NSString *)getTrigger {
  return trigger_;
}

- (NSString *)description {
  return JreStrcat("$$$$C", @"TriggerScript [trigger=", trigger_, @", getOriginId()=", [self getOriginId], ']');
}

- (void)dealloc {
  RELEASE_(trigger_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithNSString:withNSString:);
  methods[1].selector = @selector(getTrigger);
  methods[2].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = ComScolvoCoreVmActionTriggerScript_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "trigger_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LNSString;LNSString;", "toString" };
  static const J2ObjcClassInfo _ComScolvoCoreVmActionTriggerScript = { "TriggerScript", "com.scolvo.core.vm.action", ptrTable, methods, fields, 7, 0x1, 3, 2, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreVmActionTriggerScript;
}

@end

void ComScolvoCoreVmActionTriggerScript_initWithNSString_withNSString_(ComScolvoCoreVmActionTriggerScript *self, NSString *trigger, NSString *originId) {
  ComScolvoCoreVmActionScriptMsg_initWithNSString_(self, originId);
  JreStrongAssign(&self->trigger_, trigger);
}

ComScolvoCoreVmActionTriggerScript *new_ComScolvoCoreVmActionTriggerScript_initWithNSString_withNSString_(NSString *trigger, NSString *originId) {
  J2OBJC_NEW_IMPL(ComScolvoCoreVmActionTriggerScript, initWithNSString_withNSString_, trigger, originId)
}

ComScolvoCoreVmActionTriggerScript *create_ComScolvoCoreVmActionTriggerScript_initWithNSString_withNSString_(NSString *trigger, NSString *originId) {
  J2OBJC_CREATE_IMPL(ComScolvoCoreVmActionTriggerScript, initWithNSString_withNSString_, trigger, originId)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreVmActionTriggerScript)
