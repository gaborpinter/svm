//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/vm/action/BackendState.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreVmActionBackendState")
#ifdef RESTRICT_ComScolvoCoreVmActionBackendState
#define INCLUDE_ALL_ComScolvoCoreVmActionBackendState 0
#else
#define INCLUDE_ALL_ComScolvoCoreVmActionBackendState 1
#endif
#undef RESTRICT_ComScolvoCoreVmActionBackendState

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreVmActionBackendState_) && (INCLUDE_ALL_ComScolvoCoreVmActionBackendState || defined(INCLUDE_ComScolvoCoreVmActionBackendState))
#define ComScolvoCoreVmActionBackendState_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

@class IOSObjectArray;

typedef NS_ENUM(NSUInteger, ComScolvoCoreVmActionBackendState_Enum) {
  ComScolvoCoreVmActionBackendState_Enum_AVAILABLE = 0,
  ComScolvoCoreVmActionBackendState_Enum_NOT_AVAILABLE = 1,
};

@interface ComScolvoCoreVmActionBackendState : JavaLangEnum

+ (ComScolvoCoreVmActionBackendState * __nonnull)AVAILABLE;

+ (ComScolvoCoreVmActionBackendState * __nonnull)NOT_AVAILABLE;

#pragma mark Public

+ (ComScolvoCoreVmActionBackendState *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (ComScolvoCoreVmActionBackendState_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(ComScolvoCoreVmActionBackendState)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT ComScolvoCoreVmActionBackendState *ComScolvoCoreVmActionBackendState_values_[];

inline ComScolvoCoreVmActionBackendState *ComScolvoCoreVmActionBackendState_get_AVAILABLE(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreVmActionBackendState, AVAILABLE)

inline ComScolvoCoreVmActionBackendState *ComScolvoCoreVmActionBackendState_get_NOT_AVAILABLE(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreVmActionBackendState, NOT_AVAILABLE)

FOUNDATION_EXPORT IOSObjectArray *ComScolvoCoreVmActionBackendState_values(void);

FOUNDATION_EXPORT ComScolvoCoreVmActionBackendState *ComScolvoCoreVmActionBackendState_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT ComScolvoCoreVmActionBackendState *ComScolvoCoreVmActionBackendState_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreVmActionBackendState)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreVmActionBackendState")
