//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/model/page/MasterDetails.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/model/page/Content.h"
#include "com/scolvo/core/model/page/MasterDetails.h"
#include "com/scolvo/core/model/page/OrderedList.h"
#include "com/scolvo/core/model/page/form/Form.h"

@interface ComScolvoCoreModelPageMasterDetails () {
 @public
  ComScolvoCoreModelPageOrderedList *masterPart_;
  ComScolvoCoreModelPageFormForm *detailsPart_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreModelPageMasterDetails, masterPart_, ComScolvoCoreModelPageOrderedList *)
J2OBJC_FIELD_SETTER(ComScolvoCoreModelPageMasterDetails, detailsPart_, ComScolvoCoreModelPageFormForm *)

inline jlong ComScolvoCoreModelPageMasterDetails_get_serialVersionUID(void);
#define ComScolvoCoreModelPageMasterDetails_serialVersionUID 8699376133081374450LL
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCoreModelPageMasterDetails, serialVersionUID, jlong)

@implementation ComScolvoCoreModelPageMasterDetails

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  ComScolvoCoreModelPageMasterDetails_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (ComScolvoCoreModelPageOrderedList *)getMasterPart {
  return masterPart_;
}

- (void)setMasterPartWithComScolvoCoreModelPageOrderedList:(ComScolvoCoreModelPageOrderedList *)masterPart {
  JreStrongAssign(&self->masterPart_, masterPart);
}

- (ComScolvoCoreModelPageFormForm *)getDetailsPart {
  return detailsPart_;
}

- (void)setDetailsPartWithComScolvoCoreModelPageFormForm:(ComScolvoCoreModelPageFormForm *)detailsPart {
  JreStrongAssign(&self->detailsPart_, detailsPart);
}

- (NSString *)description {
  return JreStrcat("$@$@C", @"MasterDetails [masterPart=", masterPart_, @", detailsPart=", detailsPart_, ']');
}

- (void)dealloc {
  RELEASE_(masterPart_);
  RELEASE_(detailsPart_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreModelPageOrderedList;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreModelPageFormForm;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 4, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(getMasterPart);
  methods[2].selector = @selector(setMasterPartWithComScolvoCoreModelPageOrderedList:);
  methods[3].selector = @selector(getDetailsPart);
  methods[4].selector = @selector(setDetailsPartWithComScolvoCoreModelPageFormForm:);
  methods[5].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = ComScolvoCoreModelPageMasterDetails_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "masterPart_", "LComScolvoCoreModelPageOrderedList;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "detailsPart_", "LComScolvoCoreModelPageFormForm;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "setMasterPart", "LComScolvoCoreModelPageOrderedList;", "setDetailsPart", "LComScolvoCoreModelPageFormForm;", "toString" };
  static const J2ObjcClassInfo _ComScolvoCoreModelPageMasterDetails = { "MasterDetails", "com.scolvo.core.model.page", ptrTable, methods, fields, 7, 0x1, 6, 3, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreModelPageMasterDetails;
}

@end

void ComScolvoCoreModelPageMasterDetails_init(ComScolvoCoreModelPageMasterDetails *self) {
  ComScolvoCoreModelPageContent_init(self);
}

ComScolvoCoreModelPageMasterDetails *new_ComScolvoCoreModelPageMasterDetails_init() {
  J2OBJC_NEW_IMPL(ComScolvoCoreModelPageMasterDetails, init)
}

ComScolvoCoreModelPageMasterDetails *create_ComScolvoCoreModelPageMasterDetails_init() {
  J2OBJC_CREATE_IMPL(ComScolvoCoreModelPageMasterDetails, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreModelPageMasterDetails)
