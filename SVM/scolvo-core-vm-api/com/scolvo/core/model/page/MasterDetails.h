//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/model/page/MasterDetails.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreModelPageMasterDetails")
#ifdef RESTRICT_ComScolvoCoreModelPageMasterDetails
#define INCLUDE_ALL_ComScolvoCoreModelPageMasterDetails 0
#else
#define INCLUDE_ALL_ComScolvoCoreModelPageMasterDetails 1
#endif
#undef RESTRICT_ComScolvoCoreModelPageMasterDetails

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreModelPageMasterDetails_) && (INCLUDE_ALL_ComScolvoCoreModelPageMasterDetails || defined(INCLUDE_ComScolvoCoreModelPageMasterDetails))
#define ComScolvoCoreModelPageMasterDetails_

#define RESTRICT_ComScolvoCoreModelPageContent 1
#define INCLUDE_ComScolvoCoreModelPageContent 1
#include "com/scolvo/core/model/page/Content.h"

@class ComScolvoCoreModelPageFormForm;
@class ComScolvoCoreModelPageOrderedList;

@interface ComScolvoCoreModelPageMasterDetails : ComScolvoCoreModelPageContent

#pragma mark Public

- (instancetype __nonnull)init;

- (ComScolvoCoreModelPageFormForm *)getDetailsPart;

- (ComScolvoCoreModelPageOrderedList *)getMasterPart;

- (void)setDetailsPartWithComScolvoCoreModelPageFormForm:(ComScolvoCoreModelPageFormForm *)detailsPart;

- (void)setMasterPartWithComScolvoCoreModelPageOrderedList:(ComScolvoCoreModelPageOrderedList *)masterPart;

- (NSString *)description;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreModelPageMasterDetails)

FOUNDATION_EXPORT void ComScolvoCoreModelPageMasterDetails_init(ComScolvoCoreModelPageMasterDetails *self);

FOUNDATION_EXPORT ComScolvoCoreModelPageMasterDetails *new_ComScolvoCoreModelPageMasterDetails_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreModelPageMasterDetails *create_ComScolvoCoreModelPageMasterDetails_init(void);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreModelPageMasterDetails)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreModelPageMasterDetails")
