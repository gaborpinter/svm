//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/model/page/Fragment.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/model/page/Container.h"
#include "com/scolvo/core/model/page/Fragment.h"

inline jlong ComScolvoCoreModelPageFragment_get_serialVersionUID(void);
#define ComScolvoCoreModelPageFragment_serialVersionUID 7023283575053112610LL
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCoreModelPageFragment, serialVersionUID, jlong)

@implementation ComScolvoCoreModelPageFragment

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  ComScolvoCoreModelPageFragment_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = ComScolvoCoreModelPageFragment_serialVersionUID, 0x1a, -1, -1, -1, -1 },
  };
  static const J2ObjcClassInfo _ComScolvoCoreModelPageFragment = { "Fragment", "com.scolvo.core.model.page", NULL, methods, fields, 7, 0x1, 1, 1, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreModelPageFragment;
}

@end

void ComScolvoCoreModelPageFragment_init(ComScolvoCoreModelPageFragment *self) {
  ComScolvoCoreModelPageContainer_init(self);
}

ComScolvoCoreModelPageFragment *new_ComScolvoCoreModelPageFragment_init() {
  J2OBJC_NEW_IMPL(ComScolvoCoreModelPageFragment, init)
}

ComScolvoCoreModelPageFragment *create_ComScolvoCoreModelPageFragment_init() {
  J2OBJC_CREATE_IMPL(ComScolvoCoreModelPageFragment, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreModelPageFragment)
