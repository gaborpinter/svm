//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/model/page/Image.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/model/page/Content.h"
#include "com/scolvo/core/model/page/Image.h"
#include "java/lang/Integer.h"

@interface ComScolvoCoreModelPageImage () {
 @public
  NSString *image_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreModelPageImage, image_, NSString *)

inline jlong ComScolvoCoreModelPageImage_get_serialVersionUID(void);
#define ComScolvoCoreModelPageImage_serialVersionUID -7968645804846259895LL
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCoreModelPageImage, serialVersionUID, jlong)

@implementation ComScolvoCoreModelPageImage

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  ComScolvoCoreModelPageImage_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (NSString *)getImage {
  return image_;
}

- (void)setImageWithNSString:(NSString *)image {
  JreStrongAssign(&self->image_, image);
}

- (NSString *)description {
  return JreStrcat("$$$$$@$$C", @"Image [image=", image_, @", getName()=", [self getName], @", getSpan()=", [self getSpan], @", getTemplate()=", [self getTemplate], ']');
}

- (void)dealloc {
  RELEASE_(image_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 2, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(getImage);
  methods[2].selector = @selector(setImageWithNSString:);
  methods[3].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = ComScolvoCoreModelPageImage_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "image_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "setImage", "LNSString;", "toString" };
  static const J2ObjcClassInfo _ComScolvoCoreModelPageImage = { "Image", "com.scolvo.core.model.page", ptrTable, methods, fields, 7, 0x1, 4, 2, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreModelPageImage;
}

@end

void ComScolvoCoreModelPageImage_init(ComScolvoCoreModelPageImage *self) {
  ComScolvoCoreModelPageContent_init(self);
}

ComScolvoCoreModelPageImage *new_ComScolvoCoreModelPageImage_init() {
  J2OBJC_NEW_IMPL(ComScolvoCoreModelPageImage, init)
}

ComScolvoCoreModelPageImage *create_ComScolvoCoreModelPageImage_init() {
  J2OBJC_CREATE_IMPL(ComScolvoCoreModelPageImage, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreModelPageImage)
