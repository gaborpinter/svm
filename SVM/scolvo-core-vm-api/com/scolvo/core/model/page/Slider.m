//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/model/page/Slider.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/model/page/Content.h"
#include "com/scolvo/core/model/page/Slider.h"
#include "java/lang/Integer.h"
#include "java/util/Collections.h"
#include "java/util/List.h"

@interface ComScolvoCoreModelPageSlider () {
 @public
  jint min_;
  jint max_;
  jint value_;
  jint step_;
  id<JavaUtilList> markedSteps_;
  id<JavaUtilList> images_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreModelPageSlider, markedSteps_, id<JavaUtilList>)
J2OBJC_FIELD_SETTER(ComScolvoCoreModelPageSlider, images_, id<JavaUtilList>)

inline jlong ComScolvoCoreModelPageSlider_get_serialVersionUID(void);
#define ComScolvoCoreModelPageSlider_serialVersionUID -365656926073596980LL
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCoreModelPageSlider, serialVersionUID, jlong)

@implementation ComScolvoCoreModelPageSlider

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  ComScolvoCoreModelPageSlider_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (jint)getMin {
  return min_;
}

- (void)setMinWithInt:(jint)min {
  self->min_ = min;
}

- (jint)getMax {
  return max_;
}

- (void)setMaxWithInt:(jint)max {
  self->max_ = max;
}

- (jint)getValue {
  return value_;
}

- (void)setValueWithInt:(jint)value {
  self->value_ = value;
}

- (jint)getStep {
  return step_;
}

- (void)setStepWithInt:(jint)step {
  self->step_ = step;
}

- (id<JavaUtilList>)getMarkedSteps {
  return markedSteps_;
}

- (void)setMarkedStepsWithJavaUtilList:(id<JavaUtilList>)markedSteps {
  JreStrongAssign(&self->markedSteps_, markedSteps);
}

- (id<JavaUtilList>)getImages {
  return images_;
}

- (void)setImagesWithJavaUtilList:(id<JavaUtilList>)images {
  JreStrongAssign(&self->images_, images);
}

- (NSString *)description {
  return JreStrcat("$I$I$I$I$@$@$$$@$$C", @"Slider [min=", min_, @", max=", max_, @", value=", value_, @", step=", step_, @", markedSteps=", markedSteps_, @", images=", images_, @", getName()=", [self getName], @", getSpan()=", [self getSpan], @", getTemplate()=", [self getTemplate], ']');
}

- (void)dealloc {
  RELEASE_(markedSteps_);
  RELEASE_(images_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilList;", 0x1, -1, -1, -1, 5, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, 8, -1, -1 },
    { NULL, "LJavaUtilList;", 0x1, -1, -1, -1, 9, -1, -1 },
    { NULL, "V", 0x1, 10, 7, -1, 11, -1, -1 },
    { NULL, "LNSString;", 0x1, 12, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(getMin);
  methods[2].selector = @selector(setMinWithInt:);
  methods[3].selector = @selector(getMax);
  methods[4].selector = @selector(setMaxWithInt:);
  methods[5].selector = @selector(getValue);
  methods[6].selector = @selector(setValueWithInt:);
  methods[7].selector = @selector(getStep);
  methods[8].selector = @selector(setStepWithInt:);
  methods[9].selector = @selector(getMarkedSteps);
  methods[10].selector = @selector(setMarkedStepsWithJavaUtilList:);
  methods[11].selector = @selector(getImages);
  methods[12].selector = @selector(setImagesWithJavaUtilList:);
  methods[13].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = ComScolvoCoreModelPageSlider_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "min_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "max_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "value_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "step_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "markedSteps_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 13, -1 },
    { "images_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 14, -1 },
  };
  static const void *ptrTable[] = { "setMin", "I", "setMax", "setValue", "setStep", "()Ljava/util/List<Ljava/lang/Integer;>;", "setMarkedSteps", "LJavaUtilList;", "(Ljava/util/List<Ljava/lang/Integer;>;)V", "()Ljava/util/List<Ljava/lang/String;>;", "setImages", "(Ljava/util/List<Ljava/lang/String;>;)V", "toString", "Ljava/util/List<Ljava/lang/Integer;>;", "Ljava/util/List<Ljava/lang/String;>;" };
  static const J2ObjcClassInfo _ComScolvoCoreModelPageSlider = { "Slider", "com.scolvo.core.model.page", ptrTable, methods, fields, 7, 0x1, 14, 7, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreModelPageSlider;
}

@end

void ComScolvoCoreModelPageSlider_init(ComScolvoCoreModelPageSlider *self) {
  ComScolvoCoreModelPageContent_init(self);
  JreStrongAssign(&self->markedSteps_, JavaUtilCollections_emptyList());
  JreStrongAssign(&self->images_, JavaUtilCollections_emptyList());
}

ComScolvoCoreModelPageSlider *new_ComScolvoCoreModelPageSlider_init() {
  J2OBJC_NEW_IMPL(ComScolvoCoreModelPageSlider, init)
}

ComScolvoCoreModelPageSlider *create_ComScolvoCoreModelPageSlider_init() {
  J2OBJC_CREATE_IMPL(ComScolvoCoreModelPageSlider, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreModelPageSlider)
