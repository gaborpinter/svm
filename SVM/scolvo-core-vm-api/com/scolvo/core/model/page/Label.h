//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/model/page/Label.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreModelPageLabel")
#ifdef RESTRICT_ComScolvoCoreModelPageLabel
#define INCLUDE_ALL_ComScolvoCoreModelPageLabel 0
#else
#define INCLUDE_ALL_ComScolvoCoreModelPageLabel 1
#endif
#undef RESTRICT_ComScolvoCoreModelPageLabel

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreModelPageLabel_) && (INCLUDE_ALL_ComScolvoCoreModelPageLabel || defined(INCLUDE_ComScolvoCoreModelPageLabel))
#define ComScolvoCoreModelPageLabel_

#define RESTRICT_ComScolvoCoreModelPageContent 1
#define INCLUDE_ComScolvoCoreModelPageContent 1
#include "com/scolvo/core/model/page/Content.h"

@class ComScolvoCoreModelPageAlignment;

@interface ComScolvoCoreModelPageLabel : ComScolvoCoreModelPageContent

#pragma mark Public

- (instancetype __nonnull)init;

- (ComScolvoCoreModelPageAlignment *)getAlignment;

- (jint)getFontSize;

- (jboolean)isBold;

- (void)setAlignmentWithComScolvoCoreModelPageAlignment:(ComScolvoCoreModelPageAlignment *)aligment;

- (void)setBoldWithBoolean:(jboolean)bold;

- (void)setFontSizeWithInt:(jint)fontSize;

- (NSString *)description;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreModelPageLabel)

FOUNDATION_EXPORT void ComScolvoCoreModelPageLabel_init(ComScolvoCoreModelPageLabel *self);

FOUNDATION_EXPORT ComScolvoCoreModelPageLabel *new_ComScolvoCoreModelPageLabel_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreModelPageLabel *create_ComScolvoCoreModelPageLabel_init(void);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreModelPageLabel)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreModelPageLabel")
