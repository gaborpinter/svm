//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/model/page/form/FormFieldsetDecl.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreModelPageFormFormFieldsetDecl")
#ifdef RESTRICT_ComScolvoCoreModelPageFormFormFieldsetDecl
#define INCLUDE_ALL_ComScolvoCoreModelPageFormFormFieldsetDecl 0
#else
#define INCLUDE_ALL_ComScolvoCoreModelPageFormFormFieldsetDecl 1
#endif
#undef RESTRICT_ComScolvoCoreModelPageFormFormFieldsetDecl

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreModelPageFormFormFieldsetDecl_) && (INCLUDE_ALL_ComScolvoCoreModelPageFormFormFieldsetDecl || defined(INCLUDE_ComScolvoCoreModelPageFormFormFieldsetDecl))
#define ComScolvoCoreModelPageFormFormFieldsetDecl_

#define RESTRICT_ComScolvoCoreModelPageFormFormField 1
#define INCLUDE_ComScolvoCoreModelPageFormFormField 1
#include "com/scolvo/core/model/page/form/FormField.h"

@protocol JavaUtilList;

@interface ComScolvoCoreModelPageFormFormFieldsetDecl : ComScolvoCoreModelPageFormFormField

#pragma mark Public

- (instancetype __nonnull)init;

- (id<JavaUtilList>)getFields;

- (void)setFieldsWithJavaUtilList:(id<JavaUtilList>)fields;

- (NSString *)description;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreModelPageFormFormFieldsetDecl)

FOUNDATION_EXPORT void ComScolvoCoreModelPageFormFormFieldsetDecl_init(ComScolvoCoreModelPageFormFormFieldsetDecl *self);

FOUNDATION_EXPORT ComScolvoCoreModelPageFormFormFieldsetDecl *new_ComScolvoCoreModelPageFormFormFieldsetDecl_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreModelPageFormFormFieldsetDecl *create_ComScolvoCoreModelPageFormFormFieldsetDecl_init(void);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreModelPageFormFormFieldsetDecl)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreModelPageFormFormFieldsetDecl")
