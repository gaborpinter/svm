//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-vm-api/com/scolvo/core/model/typedefinition/StringType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreModelTypedefinitionStringType")
#ifdef RESTRICT_ComScolvoCoreModelTypedefinitionStringType
#define INCLUDE_ALL_ComScolvoCoreModelTypedefinitionStringType 0
#else
#define INCLUDE_ALL_ComScolvoCoreModelTypedefinitionStringType 1
#endif
#undef RESTRICT_ComScolvoCoreModelTypedefinitionStringType

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreModelTypedefinitionStringType_) && (INCLUDE_ALL_ComScolvoCoreModelTypedefinitionStringType || defined(INCLUDE_ComScolvoCoreModelTypedefinitionStringType))
#define ComScolvoCoreModelTypedefinitionStringType_

#define RESTRICT_ComScolvoCoreModelTypedefinitionElementType 1
#define INCLUDE_ComScolvoCoreModelTypedefinitionElementType 1
#include "com/scolvo/core/model/typedefinition/ElementType.h"

@class JavaLangInteger;

@interface ComScolvoCoreModelTypedefinitionStringType : ComScolvoCoreModelTypedefinitionElementType

#pragma mark Public

- (instancetype __nonnull)init;

- (JavaLangInteger *)getMaxLength;

- (JavaLangInteger *)getMinLength;

- (void)setMaxLengthWithJavaLangInteger:(JavaLangInteger *)maxLength;

- (void)setMinLengthWithJavaLangInteger:(JavaLangInteger *)minLength;

- (NSString *)description;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreModelTypedefinitionStringType)

FOUNDATION_EXPORT void ComScolvoCoreModelTypedefinitionStringType_init(ComScolvoCoreModelTypedefinitionStringType *self);

FOUNDATION_EXPORT ComScolvoCoreModelTypedefinitionStringType *new_ComScolvoCoreModelTypedefinitionStringType_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreModelTypedefinitionStringType *create_ComScolvoCoreModelTypedefinitionStringType_init(void);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreModelTypedefinitionStringType)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreModelTypedefinitionStringType")
