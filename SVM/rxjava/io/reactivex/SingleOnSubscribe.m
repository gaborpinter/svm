//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/SingleOnSubscribe.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/SingleOnSubscribe.h"

@interface IoReactivexSingleOnSubscribe : NSObject

@end

@implementation IoReactivexSingleOnSubscribe

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "V", 0x401, 0, 1, 2, 3, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(subscribeWithIoReactivexSingleEmitter:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "subscribe", "LIoReactivexSingleEmitter;", "LJavaLangException;", "(Lio/reactivex/SingleEmitter<TT;>;)V", "<T:Ljava/lang/Object;>Ljava/lang/Object;" };
  static const J2ObjcClassInfo _IoReactivexSingleOnSubscribe = { "SingleOnSubscribe", "io.reactivex", ptrTable, methods, NULL, 7, 0x609, 1, 0, -1, -1, -1, 4, -1 };
  return &_IoReactivexSingleOnSubscribe;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(IoReactivexSingleOnSubscribe)
