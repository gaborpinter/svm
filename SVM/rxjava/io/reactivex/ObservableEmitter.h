//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/ObservableEmitter.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexObservableEmitter")
#ifdef RESTRICT_IoReactivexObservableEmitter
#define INCLUDE_ALL_IoReactivexObservableEmitter 0
#else
#define INCLUDE_ALL_IoReactivexObservableEmitter 1
#endif
#undef RESTRICT_IoReactivexObservableEmitter

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexObservableEmitter_) && (INCLUDE_ALL_IoReactivexObservableEmitter || defined(INCLUDE_IoReactivexObservableEmitter))
#define IoReactivexObservableEmitter_

#define RESTRICT_IoReactivexEmitter 1
#define INCLUDE_IoReactivexEmitter 1
#include "io/reactivex/Emitter.h"

@class JavaLangThrowable;
@protocol IoReactivexDisposablesDisposable;
@protocol IoReactivexFunctionsCancellable;

@protocol IoReactivexObservableEmitter < IoReactivexEmitter, JavaObject >

- (void)setDisposableWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable> __nullable)d;

- (void)setCancellableWithIoReactivexFunctionsCancellable:(id<IoReactivexFunctionsCancellable> __nullable)c;

- (jboolean)isDisposed;

- (id<IoReactivexObservableEmitter> __nonnull)serialize;

- (jboolean)tryOnErrorWithJavaLangThrowable:(JavaLangThrowable * __nonnull)t;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexObservableEmitter)

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexObservableEmitter)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexObservableEmitter")
