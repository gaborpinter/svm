//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/observables/GroupedObservable.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Observable.h"
#include "io/reactivex/observables/GroupedObservable.h"

@implementation IoReactivexObservablesGroupedObservable

- (instancetype __nonnull)initWithId:(id __nullable)key {
  IoReactivexObservablesGroupedObservable_initWithId_(self, key);
  return self;
}

- (id __nullable)getKey {
  return key_;
}

- (void)dealloc {
  RELEASE_(key_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x4, -1, 0, -1, 1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, -1, 2, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithId:);
  methods[1].selector = @selector(getKey);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "key_", "LNSObject;", .constantValue.asLong = 0, 0x10, -1, -1, 3, -1 },
  };
  static const void *ptrTable[] = { "LNSObject;", "(TK;)V", "()TK;", "TK;", "<K:Ljava/lang/Object;T:Ljava/lang/Object;>Lio/reactivex/Observable<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexObservablesGroupedObservable = { "GroupedObservable", "io.reactivex.observables", ptrTable, methods, fields, 7, 0x401, 2, 1, -1, -1, -1, 4, -1 };
  return &_IoReactivexObservablesGroupedObservable;
}

@end

void IoReactivexObservablesGroupedObservable_initWithId_(IoReactivexObservablesGroupedObservable *self, id key) {
  IoReactivexObservable_init(self);
  JreStrongAssign(&self->key_, key);
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexObservablesGroupedObservable)
