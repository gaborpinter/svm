//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/Notification.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexNotification")
#ifdef RESTRICT_IoReactivexNotification
#define INCLUDE_ALL_IoReactivexNotification 0
#else
#define INCLUDE_ALL_IoReactivexNotification 1
#endif
#undef RESTRICT_IoReactivexNotification

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexNotification_) && (INCLUDE_ALL_IoReactivexNotification || defined(INCLUDE_IoReactivexNotification))
#define IoReactivexNotification_

@class JavaLangThrowable;

@interface IoReactivexNotification : NSObject {
 @public
  id value_;
}

+ (IoReactivexNotification *)COMPLETE;

#pragma mark Public

+ (IoReactivexNotification * __nonnull)createOnComplete;

+ (IoReactivexNotification * __nonnull)createOnErrorWithJavaLangThrowable:(JavaLangThrowable * __nonnull)error;

+ (IoReactivexNotification * __nonnull)createOnNextWithId:(id __nonnull)value;

- (jboolean)isEqual:(id)obj;

- (JavaLangThrowable * __nullable)getError;

- (id __nullable)getValue;

- (NSUInteger)hash;

- (jboolean)isOnComplete;

- (jboolean)isOnError;

- (jboolean)isOnNext;

- (NSString *)description;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(IoReactivexNotification)

J2OBJC_FIELD_SETTER(IoReactivexNotification, value_, id)

inline IoReactivexNotification *IoReactivexNotification_get_COMPLETE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT IoReactivexNotification *IoReactivexNotification_COMPLETE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexNotification, COMPLETE, IoReactivexNotification *)

FOUNDATION_EXPORT IoReactivexNotification *IoReactivexNotification_createOnNextWithId_(id value);

FOUNDATION_EXPORT IoReactivexNotification *IoReactivexNotification_createOnErrorWithJavaLangThrowable_(JavaLangThrowable *error);

FOUNDATION_EXPORT IoReactivexNotification *IoReactivexNotification_createOnComplete(void);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexNotification)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexNotification")
