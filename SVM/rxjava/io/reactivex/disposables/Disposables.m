//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/disposables/Disposables.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/disposables/ActionDisposable.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/disposables/Disposables.h"
#include "io/reactivex/disposables/FutureDisposable.h"
#include "io/reactivex/disposables/RunnableDisposable.h"
#include "io/reactivex/disposables/SubscriptionDisposable.h"
#include "io/reactivex/functions/Action.h"
#include "io/reactivex/internal/disposables/EmptyDisposable.h"
#include "io/reactivex/internal/functions/Functions.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "java/lang/IllegalStateException.h"
#include "java/lang/Runnable.h"
#include "java/util/concurrent/Future.h"
#include "org/reactivestreams/Subscription.h"

@interface IoReactivexDisposablesDisposables ()

- (instancetype __nonnull)init;

@end

__attribute__((unused)) static void IoReactivexDisposablesDisposables_init(IoReactivexDisposablesDisposables *self);

__attribute__((unused)) static IoReactivexDisposablesDisposables *new_IoReactivexDisposablesDisposables_init(void) NS_RETURNS_RETAINED;

__attribute__((unused)) static IoReactivexDisposablesDisposables *create_IoReactivexDisposablesDisposables_init(void);

@implementation IoReactivexDisposablesDisposables

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  IoReactivexDisposablesDisposables_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

+ (id<IoReactivexDisposablesDisposable> __nonnull)fromRunnableWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run {
  return IoReactivexDisposablesDisposables_fromRunnableWithJavaLangRunnable_(run);
}

+ (id<IoReactivexDisposablesDisposable> __nonnull)fromActionWithIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction> __nonnull)run {
  return IoReactivexDisposablesDisposables_fromActionWithIoReactivexFunctionsAction_(run);
}

+ (id<IoReactivexDisposablesDisposable> __nonnull)fromFutureWithJavaUtilConcurrentFuture:(id<JavaUtilConcurrentFuture> __nonnull)future {
  return IoReactivexDisposablesDisposables_fromFutureWithJavaUtilConcurrentFuture_(future);
}

+ (id<IoReactivexDisposablesDisposable> __nonnull)fromFutureWithJavaUtilConcurrentFuture:(id<JavaUtilConcurrentFuture> __nonnull)future
                                                                             withBoolean:(jboolean)allowInterrupt {
  return IoReactivexDisposablesDisposables_fromFutureWithJavaUtilConcurrentFuture_withBoolean_(future, allowInterrupt);
}

+ (id<IoReactivexDisposablesDisposable> __nonnull)fromSubscriptionWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription> __nonnull)subscription {
  return IoReactivexDisposablesDisposables_fromSubscriptionWithOrgReactivestreamsSubscription_(subscription);
}

+ (id<IoReactivexDisposablesDisposable> __nonnull)empty {
  return IoReactivexDisposablesDisposables_empty();
}

+ (id<IoReactivexDisposablesDisposable> __nonnull)disposed {
  return IoReactivexDisposablesDisposables_disposed();
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x2, -1, -1, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x9, 0, 1, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x9, 2, 3, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x9, 4, 5, -1, 6, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x9, 4, 7, -1, 8, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x9, 9, 10, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x9, -1, -1, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x9, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(fromRunnableWithJavaLangRunnable:);
  methods[2].selector = @selector(fromActionWithIoReactivexFunctionsAction:);
  methods[3].selector = @selector(fromFutureWithJavaUtilConcurrentFuture:);
  methods[4].selector = @selector(fromFutureWithJavaUtilConcurrentFuture:withBoolean:);
  methods[5].selector = @selector(fromSubscriptionWithOrgReactivestreamsSubscription:);
  methods[6].selector = @selector(empty);
  methods[7].selector = @selector(disposed);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "fromRunnable", "LJavaLangRunnable;", "fromAction", "LIoReactivexFunctionsAction;", "fromFuture", "LJavaUtilConcurrentFuture;", "(Ljava/util/concurrent/Future<*>;)Lio/reactivex/disposables/Disposable;", "LJavaUtilConcurrentFuture;Z", "(Ljava/util/concurrent/Future<*>;Z)Lio/reactivex/disposables/Disposable;", "fromSubscription", "LOrgReactivestreamsSubscription;" };
  static const J2ObjcClassInfo _IoReactivexDisposablesDisposables = { "Disposables", "io.reactivex.disposables", ptrTable, methods, NULL, 7, 0x11, 8, 0, -1, -1, -1, -1, -1 };
  return &_IoReactivexDisposablesDisposables;
}

@end

void IoReactivexDisposablesDisposables_init(IoReactivexDisposablesDisposables *self) {
  NSObject_init(self);
  @throw create_JavaLangIllegalStateException_initWithNSString_(@"No instances!");
}

IoReactivexDisposablesDisposables *new_IoReactivexDisposablesDisposables_init() {
  J2OBJC_NEW_IMPL(IoReactivexDisposablesDisposables, init)
}

IoReactivexDisposablesDisposables *create_IoReactivexDisposablesDisposables_init() {
  J2OBJC_CREATE_IMPL(IoReactivexDisposablesDisposables, init)
}

id<IoReactivexDisposablesDisposable> IoReactivexDisposablesDisposables_fromRunnableWithJavaLangRunnable_(id<JavaLangRunnable> run) {
  IoReactivexDisposablesDisposables_initialize();
  IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_(run, @"run is null");
  return create_IoReactivexDisposablesRunnableDisposable_initWithJavaLangRunnable_(run);
}

id<IoReactivexDisposablesDisposable> IoReactivexDisposablesDisposables_fromActionWithIoReactivexFunctionsAction_(id<IoReactivexFunctionsAction> run) {
  IoReactivexDisposablesDisposables_initialize();
  IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_(run, @"run is null");
  return create_IoReactivexDisposablesActionDisposable_initWithIoReactivexFunctionsAction_(run);
}

id<IoReactivexDisposablesDisposable> IoReactivexDisposablesDisposables_fromFutureWithJavaUtilConcurrentFuture_(id<JavaUtilConcurrentFuture> future) {
  IoReactivexDisposablesDisposables_initialize();
  IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_(future, @"future is null");
  return IoReactivexDisposablesDisposables_fromFutureWithJavaUtilConcurrentFuture_withBoolean_(future, true);
}

id<IoReactivexDisposablesDisposable> IoReactivexDisposablesDisposables_fromFutureWithJavaUtilConcurrentFuture_withBoolean_(id<JavaUtilConcurrentFuture> future, jboolean allowInterrupt) {
  IoReactivexDisposablesDisposables_initialize();
  IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_(future, @"future is null");
  return create_IoReactivexDisposablesFutureDisposable_initWithJavaUtilConcurrentFuture_withBoolean_(future, allowInterrupt);
}

id<IoReactivexDisposablesDisposable> IoReactivexDisposablesDisposables_fromSubscriptionWithOrgReactivestreamsSubscription_(id<OrgReactivestreamsSubscription> subscription) {
  IoReactivexDisposablesDisposables_initialize();
  IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_(subscription, @"subscription is null");
  return create_IoReactivexDisposablesSubscriptionDisposable_initWithOrgReactivestreamsSubscription_(subscription);
}

id<IoReactivexDisposablesDisposable> IoReactivexDisposablesDisposables_empty() {
  IoReactivexDisposablesDisposables_initialize();
  return IoReactivexDisposablesDisposables_fromRunnableWithJavaLangRunnable_(JreLoadStatic(IoReactivexInternalFunctionsFunctions, EMPTY_RUNNABLE));
}

id<IoReactivexDisposablesDisposable> IoReactivexDisposablesDisposables_disposed() {
  IoReactivexDisposablesDisposables_initialize();
  return JreLoadEnum(IoReactivexInternalDisposablesEmptyDisposable, INSTANCE);
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexDisposablesDisposables)
