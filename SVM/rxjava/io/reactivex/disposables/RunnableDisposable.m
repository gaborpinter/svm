//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/disposables/RunnableDisposable.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/disposables/ReferenceDisposable.h"
#include "io/reactivex/disposables/RunnableDisposable.h"
#include "java/lang/Runnable.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexDisposablesRunnableDisposable_get_serialVersionUID(void);
#define IoReactivexDisposablesRunnableDisposable_serialVersionUID -8219729196779211169LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexDisposablesRunnableDisposable, serialVersionUID, jlong)

@implementation IoReactivexDisposablesRunnableDisposable

- (instancetype __nonnull)initWithJavaLangRunnable:(id<JavaLangRunnable>)value {
  IoReactivexDisposablesRunnableDisposable_initWithJavaLangRunnable_(self, value);
  return self;
}

- (void)onDisposedWithId:(id<JavaLangRunnable> __nonnull)value {
  [((id<JavaLangRunnable>) nil_chk(value)) run];
}

- (NSString *)description {
  return JreStrcat("$Z$@C", @"RunnableDisposable(disposed=", [self isDisposed], @", ", [self get], ')');
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 1, 0, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 2, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaLangRunnable:);
  methods[1].selector = @selector(onDisposedWithId:);
  methods[2].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexDisposablesRunnableDisposable_serialVersionUID, 0x1a, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LJavaLangRunnable;", "onDisposed", "toString", "Lio/reactivex/disposables/ReferenceDisposable<Ljava/lang/Runnable;>;" };
  static const J2ObjcClassInfo _IoReactivexDisposablesRunnableDisposable = { "RunnableDisposable", "io.reactivex.disposables", ptrTable, methods, fields, 7, 0x10, 3, 1, -1, -1, -1, 3, -1 };
  return &_IoReactivexDisposablesRunnableDisposable;
}

@end

void IoReactivexDisposablesRunnableDisposable_initWithJavaLangRunnable_(IoReactivexDisposablesRunnableDisposable *self, id<JavaLangRunnable> value) {
  IoReactivexDisposablesReferenceDisposable_initWithId_(self, value);
}

IoReactivexDisposablesRunnableDisposable *new_IoReactivexDisposablesRunnableDisposable_initWithJavaLangRunnable_(id<JavaLangRunnable> value) {
  J2OBJC_NEW_IMPL(IoReactivexDisposablesRunnableDisposable, initWithJavaLangRunnable_, value)
}

IoReactivexDisposablesRunnableDisposable *create_IoReactivexDisposablesRunnableDisposable_initWithJavaLangRunnable_(id<JavaLangRunnable> value) {
  J2OBJC_CREATE_IMPL(IoReactivexDisposablesRunnableDisposable, initWithJavaLangRunnable_, value)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexDisposablesRunnableDisposable)
