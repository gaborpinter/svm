//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/processors/UnicastProcessor.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexProcessorsUnicastProcessor")
#ifdef RESTRICT_IoReactivexProcessorsUnicastProcessor
#define INCLUDE_ALL_IoReactivexProcessorsUnicastProcessor 0
#else
#define INCLUDE_ALL_IoReactivexProcessorsUnicastProcessor 1
#endif
#undef RESTRICT_IoReactivexProcessorsUnicastProcessor

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexProcessorsUnicastProcessor_) && (INCLUDE_ALL_IoReactivexProcessorsUnicastProcessor || defined(INCLUDE_IoReactivexProcessorsUnicastProcessor))
#define IoReactivexProcessorsUnicastProcessor_

#define RESTRICT_IoReactivexProcessorsFlowableProcessor 1
#define INCLUDE_IoReactivexProcessorsFlowableProcessor 1
#include "io/reactivex/processors/FlowableProcessor.h"

@class IoReactivexInternalQueueSpscLinkedArrayQueue;
@class IoReactivexInternalSubscriptionsBasicIntQueueSubscription;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicBoolean;
@class JavaUtilConcurrentAtomicAtomicLong;
@class JavaUtilConcurrentAtomicAtomicReference;
@protocol JavaLangRunnable;
@protocol OrgReactivestreamsSubscriber;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexProcessorsUnicastProcessor : IoReactivexProcessorsFlowableProcessor {
 @public
  IoReactivexInternalQueueSpscLinkedArrayQueue *queue_;
  JavaUtilConcurrentAtomicAtomicReference *onTerminate_;
  jboolean delayError_;
  volatile_jboolean done_;
  JavaLangThrowable *error_;
  JavaUtilConcurrentAtomicAtomicReference *downstream_;
  volatile_jboolean cancelled_;
  JavaUtilConcurrentAtomicAtomicBoolean *once_;
  IoReactivexInternalSubscriptionsBasicIntQueueSubscription *wip_;
  JavaUtilConcurrentAtomicAtomicLong *requested_;
  jboolean enableOperatorFusion_;
}

#pragma mark Public

+ (IoReactivexProcessorsUnicastProcessor * __nonnull)create;

+ (IoReactivexProcessorsUnicastProcessor * __nonnull)createWithBoolean:(jboolean)delayError;

+ (IoReactivexProcessorsUnicastProcessor * __nonnull)createWithInt:(jint)capacityHint;

+ (IoReactivexProcessorsUnicastProcessor * __nonnull)createWithInt:(jint)capacityHint
                                              withJavaLangRunnable:(id<JavaLangRunnable>)onCancelled;

+ (IoReactivexProcessorsUnicastProcessor * __nonnull)createWithInt:(jint)capacityHint
                                              withJavaLangRunnable:(id<JavaLangRunnable>)onCancelled
                                                       withBoolean:(jboolean)delayError;

- (JavaLangThrowable * __nullable)getThrowable;

- (jboolean)hasComplete;

- (jboolean)hasSubscribers;

- (jboolean)hasThrowable;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

#pragma mark Package-Private

- (instancetype __nonnull)initWithInt:(jint)capacityHint;

- (instancetype __nonnull)initWithInt:(jint)capacityHint
                 withJavaLangRunnable:(id<JavaLangRunnable>)onTerminate;

- (instancetype __nonnull)initWithInt:(jint)capacityHint
                 withJavaLangRunnable:(id<JavaLangRunnable>)onTerminate
                          withBoolean:(jboolean)delayError;

- (jboolean)checkTerminatedWithBoolean:(jboolean)failFast
                           withBoolean:(jboolean)d
                           withBoolean:(jboolean)empty
      withOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)a
withIoReactivexInternalQueueSpscLinkedArrayQueue:(IoReactivexInternalQueueSpscLinkedArrayQueue *)q;

- (void)doTerminate;

- (void)drain;

- (void)drainFusedWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)a;

- (void)drainRegularWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)a;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexProcessorsUnicastProcessor)

J2OBJC_FIELD_SETTER(IoReactivexProcessorsUnicastProcessor, queue_, IoReactivexInternalQueueSpscLinkedArrayQueue *)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsUnicastProcessor, onTerminate_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsUnicastProcessor, error_, JavaLangThrowable *)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsUnicastProcessor, downstream_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsUnicastProcessor, once_, JavaUtilConcurrentAtomicAtomicBoolean *)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsUnicastProcessor, wip_, IoReactivexInternalSubscriptionsBasicIntQueueSubscription *)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsUnicastProcessor, requested_, JavaUtilConcurrentAtomicAtomicLong *)

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *IoReactivexProcessorsUnicastProcessor_create(void);

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *IoReactivexProcessorsUnicastProcessor_createWithInt_(jint capacityHint);

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *IoReactivexProcessorsUnicastProcessor_createWithBoolean_(jboolean delayError);

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *IoReactivexProcessorsUnicastProcessor_createWithInt_withJavaLangRunnable_(jint capacityHint, id<JavaLangRunnable> onCancelled);

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *IoReactivexProcessorsUnicastProcessor_createWithInt_withJavaLangRunnable_withBoolean_(jint capacityHint, id<JavaLangRunnable> onCancelled, jboolean delayError);

FOUNDATION_EXPORT void IoReactivexProcessorsUnicastProcessor_initWithInt_(IoReactivexProcessorsUnicastProcessor *self, jint capacityHint);

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *new_IoReactivexProcessorsUnicastProcessor_initWithInt_(jint capacityHint) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *create_IoReactivexProcessorsUnicastProcessor_initWithInt_(jint capacityHint);

FOUNDATION_EXPORT void IoReactivexProcessorsUnicastProcessor_initWithInt_withJavaLangRunnable_(IoReactivexProcessorsUnicastProcessor *self, jint capacityHint, id<JavaLangRunnable> onTerminate);

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *new_IoReactivexProcessorsUnicastProcessor_initWithInt_withJavaLangRunnable_(jint capacityHint, id<JavaLangRunnable> onTerminate) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *create_IoReactivexProcessorsUnicastProcessor_initWithInt_withJavaLangRunnable_(jint capacityHint, id<JavaLangRunnable> onTerminate);

FOUNDATION_EXPORT void IoReactivexProcessorsUnicastProcessor_initWithInt_withJavaLangRunnable_withBoolean_(IoReactivexProcessorsUnicastProcessor *self, jint capacityHint, id<JavaLangRunnable> onTerminate, jboolean delayError);

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *new_IoReactivexProcessorsUnicastProcessor_initWithInt_withJavaLangRunnable_withBoolean_(jint capacityHint, id<JavaLangRunnable> onTerminate, jboolean delayError) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor *create_IoReactivexProcessorsUnicastProcessor_initWithInt_withJavaLangRunnable_withBoolean_(jint capacityHint, id<JavaLangRunnable> onTerminate, jboolean delayError);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexProcessorsUnicastProcessor)

#endif

#if !defined (IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription_) && (INCLUDE_ALL_IoReactivexProcessorsUnicastProcessor || defined(INCLUDE_IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription))
#define IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription_

#define RESTRICT_IoReactivexInternalSubscriptionsBasicIntQueueSubscription 1
#define INCLUDE_IoReactivexInternalSubscriptionsBasicIntQueueSubscription 1
#include "io/reactivex/internal/subscriptions/BasicIntQueueSubscription.h"

@class IoReactivexProcessorsUnicastProcessor;

@interface IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription : IoReactivexInternalSubscriptionsBasicIntQueueSubscription

#pragma mark Public

- (void)cancel;

- (void)clear;

- (NSUInteger)hash;

- (jboolean)isEmpty;

- (jboolean)isEqual:(id)obj;

- (id __nullable)poll;

- (void)requestWithLong:(jlong)n;

- (jint)requestFusionWithInt:(jint)requestedMode;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexProcessorsUnicastProcessor:(IoReactivexProcessorsUnicastProcessor *)outer$;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription)

FOUNDATION_EXPORT void IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription_initWithIoReactivexProcessorsUnicastProcessor_(IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription *self, IoReactivexProcessorsUnicastProcessor *outer$);

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription *new_IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription_initWithIoReactivexProcessorsUnicastProcessor_(IoReactivexProcessorsUnicastProcessor *outer$) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription *create_IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription_initWithIoReactivexProcessorsUnicastProcessor_(IoReactivexProcessorsUnicastProcessor *outer$);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexProcessorsUnicastProcessor_UnicastQueueSubscription)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexProcessorsUnicastProcessor")
