//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/processors/BehaviorProcessor.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexProcessorsBehaviorProcessor")
#ifdef RESTRICT_IoReactivexProcessorsBehaviorProcessor
#define INCLUDE_ALL_IoReactivexProcessorsBehaviorProcessor 0
#else
#define INCLUDE_ALL_IoReactivexProcessorsBehaviorProcessor 1
#endif
#undef RESTRICT_IoReactivexProcessorsBehaviorProcessor

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexProcessorsBehaviorProcessor_) && (INCLUDE_ALL_IoReactivexProcessorsBehaviorProcessor || defined(INCLUDE_IoReactivexProcessorsBehaviorProcessor))
#define IoReactivexProcessorsBehaviorProcessor_

#define RESTRICT_IoReactivexProcessorsFlowableProcessor 1
#define INCLUDE_IoReactivexProcessorsFlowableProcessor 1
#include "io/reactivex/processors/FlowableProcessor.h"

@class IOSObjectArray;
@class IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicReference;
@protocol JavaUtilConcurrentLocksLock;
@protocol JavaUtilConcurrentLocksReadWriteLock;
@protocol OrgReactivestreamsSubscriber;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexProcessorsBehaviorProcessor : IoReactivexProcessorsFlowableProcessor {
 @public
  JavaUtilConcurrentAtomicAtomicReference *subscribers_;
  id<JavaUtilConcurrentLocksReadWriteLock> lock_;
  id<JavaUtilConcurrentLocksLock> readLock_;
  id<JavaUtilConcurrentLocksLock> writeLock_;
  JavaUtilConcurrentAtomicAtomicReference *value_;
  JavaUtilConcurrentAtomicAtomicReference *terminalEvent_;
  jlong index_;
}

+ (IOSObjectArray *)EMPTY_ARRAY;

+ (IOSObjectArray *)EMPTY;

+ (IOSObjectArray *)TERMINATED;

#pragma mark Public

+ (IoReactivexProcessorsBehaviorProcessor * __nonnull)create;

+ (IoReactivexProcessorsBehaviorProcessor * __nonnull)createDefaultWithId:(id)defaultValue;

- (JavaLangThrowable * __nullable)getThrowable;

- (id __nullable)getValue;

- (IOSObjectArray *)getValues;

- (IOSObjectArray *)getValuesWithNSObjectArray:(IOSObjectArray *)array;

- (jboolean)hasComplete;

- (jboolean)hasSubscribers;

- (jboolean)hasThrowable;

- (jboolean)hasValue;

- (jboolean)offerWithId:(id)t;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

#pragma mark Package-Private

- (instancetype __nonnull)init;

- (instancetype __nonnull)initWithId:(id)defaultValue;

- (jboolean)addWithIoReactivexProcessorsBehaviorProcessor_BehaviorSubscription:(IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription *)rs;

- (void)removeWithIoReactivexProcessorsBehaviorProcessor_BehaviorSubscription:(IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription *)rs;

- (void)setCurrentWithId:(id)o;

- (jint)subscriberCount;

- (IOSObjectArray *)terminateWithId:(id)terminalValue;

@end

J2OBJC_STATIC_INIT(IoReactivexProcessorsBehaviorProcessor)

J2OBJC_FIELD_SETTER(IoReactivexProcessorsBehaviorProcessor, subscribers_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsBehaviorProcessor, lock_, id<JavaUtilConcurrentLocksReadWriteLock>)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsBehaviorProcessor, readLock_, id<JavaUtilConcurrentLocksLock>)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsBehaviorProcessor, writeLock_, id<JavaUtilConcurrentLocksLock>)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsBehaviorProcessor, value_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsBehaviorProcessor, terminalEvent_, JavaUtilConcurrentAtomicAtomicReference *)

inline IOSObjectArray *IoReactivexProcessorsBehaviorProcessor_get_EMPTY_ARRAY(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT IOSObjectArray *IoReactivexProcessorsBehaviorProcessor_EMPTY_ARRAY;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexProcessorsBehaviorProcessor, EMPTY_ARRAY, IOSObjectArray *)

inline IOSObjectArray *IoReactivexProcessorsBehaviorProcessor_get_EMPTY(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT IOSObjectArray *IoReactivexProcessorsBehaviorProcessor_EMPTY;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexProcessorsBehaviorProcessor, EMPTY, IOSObjectArray *)

inline IOSObjectArray *IoReactivexProcessorsBehaviorProcessor_get_TERMINATED(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT IOSObjectArray *IoReactivexProcessorsBehaviorProcessor_TERMINATED;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexProcessorsBehaviorProcessor, TERMINATED, IOSObjectArray *)

FOUNDATION_EXPORT IoReactivexProcessorsBehaviorProcessor *IoReactivexProcessorsBehaviorProcessor_create(void);

FOUNDATION_EXPORT IoReactivexProcessorsBehaviorProcessor *IoReactivexProcessorsBehaviorProcessor_createDefaultWithId_(id defaultValue);

FOUNDATION_EXPORT void IoReactivexProcessorsBehaviorProcessor_init(IoReactivexProcessorsBehaviorProcessor *self);

FOUNDATION_EXPORT IoReactivexProcessorsBehaviorProcessor *new_IoReactivexProcessorsBehaviorProcessor_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexProcessorsBehaviorProcessor *create_IoReactivexProcessorsBehaviorProcessor_init(void);

FOUNDATION_EXPORT void IoReactivexProcessorsBehaviorProcessor_initWithId_(IoReactivexProcessorsBehaviorProcessor *self, id defaultValue);

FOUNDATION_EXPORT IoReactivexProcessorsBehaviorProcessor *new_IoReactivexProcessorsBehaviorProcessor_initWithId_(id defaultValue) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexProcessorsBehaviorProcessor *create_IoReactivexProcessorsBehaviorProcessor_initWithId_(id defaultValue);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexProcessorsBehaviorProcessor)

#endif

#if !defined (IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription_) && (INCLUDE_ALL_IoReactivexProcessorsBehaviorProcessor || defined(INCLUDE_IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription))
#define IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicLong 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicLong 1
#include "java/util/concurrent/atomic/AtomicLong.h"

#define RESTRICT_OrgReactivestreamsSubscription 1
#define INCLUDE_OrgReactivestreamsSubscription 1
#include "org/reactivestreams/Subscription.h"

#define RESTRICT_IoReactivexInternalUtilAppendOnlyLinkedArrayList 1
#define INCLUDE_IoReactivexInternalUtilAppendOnlyLinkedArrayList_NonThrowingPredicate 1
#include "io/reactivex/internal/util/AppendOnlyLinkedArrayList.h"

@class IoReactivexInternalUtilAppendOnlyLinkedArrayList;
@class IoReactivexProcessorsBehaviorProcessor;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription : JavaUtilConcurrentAtomicAtomicLong < OrgReactivestreamsSubscription, IoReactivexInternalUtilAppendOnlyLinkedArrayList_NonThrowingPredicate > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  IoReactivexProcessorsBehaviorProcessor *state_;
  jboolean next_;
  jboolean emitting_;
  IoReactivexInternalUtilAppendOnlyLinkedArrayList *queue_;
  jboolean fastPath_;
  volatile_jboolean cancelled_;
  jlong index_;
}

#pragma mark Public

- (void)cancel;

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (jboolean)isFull;

- (void)requestWithLong:(jlong)n;

- (jboolean)testWithId:(id)o;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                    withIoReactivexProcessorsBehaviorProcessor:(IoReactivexProcessorsBehaviorProcessor *)state;

- (void)emitFirst;

- (void)emitLoop;

- (void)emitNextWithId:(id)value
              withLong:(jlong)stateIndex;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithLong:(jlong)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription)

J2OBJC_FIELD_SETTER(IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription, state_, IoReactivexProcessorsBehaviorProcessor *)
J2OBJC_FIELD_SETTER(IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription, queue_, IoReactivexInternalUtilAppendOnlyLinkedArrayList *)

FOUNDATION_EXPORT void IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsBehaviorProcessor_(IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription *self, id<OrgReactivestreamsSubscriber> actual, IoReactivexProcessorsBehaviorProcessor *state);

FOUNDATION_EXPORT IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription *new_IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsBehaviorProcessor_(id<OrgReactivestreamsSubscriber> actual, IoReactivexProcessorsBehaviorProcessor *state) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription *create_IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsBehaviorProcessor_(id<OrgReactivestreamsSubscriber> actual, IoReactivexProcessorsBehaviorProcessor *state);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexProcessorsBehaviorProcessor_BehaviorSubscription)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexProcessorsBehaviorProcessor")
