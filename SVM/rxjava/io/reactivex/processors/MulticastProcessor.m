//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/processors/MulticastProcessor.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/annotations/BackpressureKind.h"
#include "io/reactivex/annotations/BackpressureSupport.h"
#include "io/reactivex/annotations/CheckReturnValue.h"
#include "io/reactivex/annotations/SchedulerSupport.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/exceptions/MissingBackpressureException.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/fuseable/QueueFuseable.h"
#include "io/reactivex/internal/fuseable/QueueSubscription.h"
#include "io/reactivex/internal/fuseable/SimpleQueue.h"
#include "io/reactivex/internal/queue/SpscArrayQueue.h"
#include "io/reactivex/internal/queue/SpscLinkedArrayQueue.h"
#include "io/reactivex/internal/subscriptions/EmptySubscription.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "io/reactivex/processors/FlowableProcessor.h"
#include "io/reactivex/processors/MulticastProcessor.h"
#include "java/lang/Long.h"
#include "java/lang/Math.h"
#include "java/lang/System.h"
#include "java/lang/Throwable.h"
#include "java/lang/annotation/Annotation.h"
#include "java/util/concurrent/atomic/AtomicBoolean.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"
#include "java/util/concurrent/atomic/AtomicLong.h"
#include "java/util/concurrent/atomic/AtomicReference.h"
#include "org/reactivestreams/Subscriber.h"
#include "org/reactivestreams/Subscription.h"

__attribute__((unused)) static IOSObjectArray *IoReactivexProcessorsMulticastProcessor__Annotations$0(void);

__attribute__((unused)) static IOSObjectArray *IoReactivexProcessorsMulticastProcessor__Annotations$1(void);

__attribute__((unused)) static IOSObjectArray *IoReactivexProcessorsMulticastProcessor__Annotations$2(void);

__attribute__((unused)) static IOSObjectArray *IoReactivexProcessorsMulticastProcessor__Annotations$3(void);

__attribute__((unused)) static IOSObjectArray *IoReactivexProcessorsMulticastProcessor__Annotations$4(void);

inline jlong IoReactivexProcessorsMulticastProcessor_MulticastSubscription_get_serialVersionUID(void);
#define IoReactivexProcessorsMulticastProcessor_MulticastSubscription_serialVersionUID -363282618957264509LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexProcessorsMulticastProcessor_MulticastSubscription, serialVersionUID, jlong)

J2OBJC_INITIALIZED_DEFN(IoReactivexProcessorsMulticastProcessor)

IOSObjectArray *IoReactivexProcessorsMulticastProcessor_EMPTY;
IOSObjectArray *IoReactivexProcessorsMulticastProcessor_TERMINATED;

@implementation IoReactivexProcessorsMulticastProcessor

+ (IOSObjectArray *)EMPTY {
  return IoReactivexProcessorsMulticastProcessor_EMPTY;
}

+ (IOSObjectArray *)TERMINATED {
  return IoReactivexProcessorsMulticastProcessor_TERMINATED;
}

+ (IoReactivexProcessorsMulticastProcessor * __nonnull)create {
  return IoReactivexProcessorsMulticastProcessor_create();
}

+ (IoReactivexProcessorsMulticastProcessor * __nonnull)createWithBoolean:(jboolean)refCount {
  return IoReactivexProcessorsMulticastProcessor_createWithBoolean_(refCount);
}

+ (IoReactivexProcessorsMulticastProcessor * __nonnull)createWithInt:(jint)bufferSize {
  return IoReactivexProcessorsMulticastProcessor_createWithInt_(bufferSize);
}

+ (IoReactivexProcessorsMulticastProcessor * __nonnull)createWithInt:(jint)bufferSize
                                                         withBoolean:(jboolean)refCount {
  return IoReactivexProcessorsMulticastProcessor_createWithInt_withBoolean_(bufferSize, refCount);
}

- (instancetype __nonnull)initWithInt:(jint)bufferSize
                          withBoolean:(jboolean)refCount {
  IoReactivexProcessorsMulticastProcessor_initWithInt_withBoolean_(self, bufferSize, refCount);
  return self;
}

- (void)start {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withOrgReactivestreamsSubscription_(upstream_, JreLoadEnum(IoReactivexInternalSubscriptionsEmptySubscription, INSTANCE))) {
    JreVolatileStrongAssign(&queue_, create_IoReactivexInternalQueueSpscArrayQueue_initWithInt_(bufferSize_));
  }
}

- (void)startUnbounded {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withOrgReactivestreamsSubscription_(upstream_, JreLoadEnum(IoReactivexInternalSubscriptionsEmptySubscription, INSTANCE))) {
    JreVolatileStrongAssign(&queue_, create_IoReactivexInternalQueueSpscLinkedArrayQueue_initWithInt_(bufferSize_));
  }
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withOrgReactivestreamsSubscription_(upstream_, s)) {
    if ([IoReactivexInternalFuseableQueueSubscription_class_() isInstance:s]) {
      id<IoReactivexInternalFuseableQueueSubscription> qs = (id<IoReactivexInternalFuseableQueueSubscription>) cast_check(s, IoReactivexInternalFuseableQueueSubscription_class_());
      jint m = [((id<IoReactivexInternalFuseableQueueSubscription>) nil_chk(qs)) requestFusionWithInt:IoReactivexInternalFuseableQueueFuseable_ANY];
      if (m == IoReactivexInternalFuseableQueueFuseable_SYNC) {
        fusionMode_ = m;
        JreVolatileStrongAssign(&queue_, qs);
        JreAssignVolatileBoolean(&done_, true);
        [self drain];
        return;
      }
      if (m == IoReactivexInternalFuseableQueueFuseable_ASYNC) {
        fusionMode_ = m;
        JreVolatileStrongAssign(&queue_, qs);
        [((id<OrgReactivestreamsSubscription>) nil_chk(s)) requestWithLong:bufferSize_];
        return;
      }
    }
    JreVolatileStrongAssign(&queue_, create_IoReactivexInternalQueueSpscArrayQueue_initWithInt_(bufferSize_));
    [((id<OrgReactivestreamsSubscription>) nil_chk(s)) requestWithLong:bufferSize_];
  }
}

- (void)onNextWithId:(id)t {
  if ([((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(once_)) get]) {
    return;
  }
  if (fusionMode_ == IoReactivexInternalFuseableQueueFuseable_NONE) {
    IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_(t, @"onNext called with null. Null values are generally not allowed in 2.x operators and sources.");
    if (![((id<IoReactivexInternalFuseableSimpleQueue>) nil_chk(JreLoadVolatileId(&queue_))) offerWithId:t]) {
      IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
      [self onErrorWithJavaLangThrowable:create_IoReactivexExceptionsMissingBackpressureException_init()];
      return;
    }
  }
  [self drain];
}

- (jboolean)offerWithId:(id)t {
  if ([((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(once_)) get]) {
    return false;
  }
  IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_(t, @"offer called with null. Null values are generally not allowed in 2.x operators and sources.");
  if (fusionMode_ == IoReactivexInternalFuseableQueueFuseable_NONE) {
    if ([((id<IoReactivexInternalFuseableSimpleQueue>) nil_chk(JreLoadVolatileId(&queue_))) offerWithId:t]) {
      [self drain];
      return true;
    }
  }
  return false;
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_(t, @"onError called with null. Null values are generally not allowed in 2.x operators and sources.");
  if ([((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(once_)) compareAndSetWithBoolean:false withBoolean:true]) {
    JreVolatileStrongAssign(&error_, t);
    JreAssignVolatileBoolean(&done_, true);
    [self drain];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
  }
}

- (void)onComplete {
  if ([((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(once_)) compareAndSetWithBoolean:false withBoolean:true]) {
    JreAssignVolatileBoolean(&done_, true);
    [self drain];
  }
}

- (jboolean)hasSubscribers {
  return ((IOSObjectArray *) nil_chk([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(subscribers_)) get]))->size_ != 0;
}

- (jboolean)hasThrowable {
  return [((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(once_)) get] && JreLoadVolatileId(&error_) != nil;
}

- (jboolean)hasComplete {
  return [((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(once_)) get] && JreLoadVolatileId(&error_) == nil;
}

- (JavaLangThrowable *)getThrowable {
  return [((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(once_)) get] ? JreLoadVolatileId(&error_) : nil;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  IoReactivexProcessorsMulticastProcessor_MulticastSubscription *ms = create_IoReactivexProcessorsMulticastProcessor_MulticastSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsMulticastProcessor_(s, self);
  [((id<OrgReactivestreamsSubscriber>) nil_chk(s)) onSubscribeWithOrgReactivestreamsSubscription:ms];
  if ([self addWithIoReactivexProcessorsMulticastProcessor_MulticastSubscription:ms]) {
    if ([ms get] == JavaLangLong_MIN_VALUE) {
      [self removeWithIoReactivexProcessorsMulticastProcessor_MulticastSubscription:ms];
    }
    else {
      [self drain];
    }
  }
  else {
    if ([((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(once_)) get] || !refcount_) {
      JavaLangThrowable *ex = JreLoadVolatileId(&error_);
      if (ex != nil) {
        [s onErrorWithJavaLangThrowable:ex];
        return;
      }
    }
    [s onComplete];
  }
}

- (jboolean)addWithIoReactivexProcessorsMulticastProcessor_MulticastSubscription:(IoReactivexProcessorsMulticastProcessor_MulticastSubscription *)inner {
  for (; ; ) {
    IOSObjectArray *a = [((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(subscribers_)) get];
    if (a == IoReactivexProcessorsMulticastProcessor_TERMINATED) {
      return false;
    }
    jint n = ((IOSObjectArray *) nil_chk(a))->size_;
    IOSObjectArray *b = [IOSObjectArray arrayWithLength:n + 1 type:IoReactivexProcessorsMulticastProcessor_MulticastSubscription_class_()];
    JavaLangSystem_arraycopyWithId_withInt_withId_withInt_withInt_(a, 0, b, 0, n);
    IOSObjectArray_Set(b, n, inner);
    if ([subscribers_ compareAndSetWithId:a withId:b]) {
      return true;
    }
  }
}

- (void)removeWithIoReactivexProcessorsMulticastProcessor_MulticastSubscription:(IoReactivexProcessorsMulticastProcessor_MulticastSubscription *)inner {
  for (; ; ) {
    IOSObjectArray *a = [((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(subscribers_)) get];
    jint n = ((IOSObjectArray *) nil_chk(a))->size_;
    if (n == 0) {
      return;
    }
    jint j = -1;
    for (jint i = 0; i < n; i++) {
      if (IOSObjectArray_Get(a, i) == inner) {
        j = i;
        break;
      }
    }
    if (j < 0) {
      break;
    }
    if (n == 1) {
      if (refcount_) {
        if ([subscribers_ compareAndSetWithId:a withId:IoReactivexProcessorsMulticastProcessor_TERMINATED]) {
          IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
          [((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(once_)) setWithBoolean:true];
          break;
        }
      }
      else {
        if ([subscribers_ compareAndSetWithId:a withId:IoReactivexProcessorsMulticastProcessor_EMPTY]) {
          break;
        }
      }
    }
    else {
      IOSObjectArray *b = [IOSObjectArray arrayWithLength:n - 1 type:IoReactivexProcessorsMulticastProcessor_MulticastSubscription_class_()];
      JavaLangSystem_arraycopyWithId_withInt_withId_withInt_withInt_(a, 0, b, 0, j);
      JavaLangSystem_arraycopyWithId_withInt_withId_withInt_withInt_(a, j + 1, b, j, n - j - 1);
      if ([subscribers_ compareAndSetWithId:a withId:b]) {
        break;
      }
    }
  }
}

- (void)drain {
  if ([((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(wip_)) getAndIncrement] != 0) {
    return;
  }
  jint missed = 1;
  JavaUtilConcurrentAtomicAtomicReference *subs = subscribers_;
  jint c = consumed_;
  jint lim = limit_;
  jint fm = fusionMode_;
  for (; ; ) {
    {
      id<IoReactivexInternalFuseableSimpleQueue> q = JreLoadVolatileId(&queue_);
      if (q != nil) {
        IOSObjectArray *as = [((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(subs)) get];
        jint n = ((IOSObjectArray *) nil_chk(as))->size_;
        if (n != 0) {
          jlong r = -1LL;
          {
            IOSObjectArray *a__ = as;
            IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *b__ = a__->buffer_;
            IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *e__ = b__ + a__->size_;
            while (b__ < e__) {
              IoReactivexProcessorsMulticastProcessor_MulticastSubscription *a = *b__++;
              jlong ra = [((IoReactivexProcessorsMulticastProcessor_MulticastSubscription *) nil_chk(a)) get];
              if (ra >= 0LL) {
                if (r == -1LL) {
                  r = ra - a->emitted_;
                }
                else {
                  r = JavaLangMath_minWithLong_withLong_(r, ra - a->emitted_);
                }
              }
            }
          }
          while (r > 0LL) {
            IOSObjectArray *bs = [subs get];
            if (bs == IoReactivexProcessorsMulticastProcessor_TERMINATED) {
              [q clear];
              return;
            }
            if (as != bs) {
              goto continue_outer;
            }
            jboolean d = JreLoadVolatileBoolean(&done_);
            id v;
            @try {
              v = [q poll];
            }
            @catch (JavaLangThrowable *ex) {
              IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
              IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
              d = true;
              v = nil;
              JreVolatileStrongAssign(&error_, ex);
              JreAssignVolatileBoolean(&done_, true);
            }
            jboolean empty = v == nil;
            if (d && empty) {
              JavaLangThrowable *ex = JreLoadVolatileId(&error_);
              if (ex != nil) {
                {
                  IOSObjectArray *a__ = [subs getAndSetWithId:IoReactivexProcessorsMulticastProcessor_TERMINATED];
                  IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
                  IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *e__ = b__ + a__->size_;
                  while (b__ < e__) {
                    IoReactivexProcessorsMulticastProcessor_MulticastSubscription *inner = *b__++;
                    [((IoReactivexProcessorsMulticastProcessor_MulticastSubscription *) nil_chk(inner)) onErrorWithJavaLangThrowable:ex];
                  }
                }
              }
              else {
                {
                  IOSObjectArray *a__ = [subs getAndSetWithId:IoReactivexProcessorsMulticastProcessor_TERMINATED];
                  IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
                  IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *e__ = b__ + a__->size_;
                  while (b__ < e__) {
                    IoReactivexProcessorsMulticastProcessor_MulticastSubscription *inner = *b__++;
                    [((IoReactivexProcessorsMulticastProcessor_MulticastSubscription *) nil_chk(inner)) onComplete];
                  }
                }
              }
              return;
            }
            if (empty) {
              break;
            }
            {
              IOSObjectArray *a__ = as;
              IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *b__ = a__->buffer_;
              IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *e__ = b__ + a__->size_;
              while (b__ < e__) {
                IoReactivexProcessorsMulticastProcessor_MulticastSubscription *inner = *b__++;
                [((IoReactivexProcessorsMulticastProcessor_MulticastSubscription *) nil_chk(inner)) onNextWithId:v];
              }
            }
            r--;
            if (fm != IoReactivexInternalFuseableQueueFuseable_SYNC) {
              if (++c == lim) {
                c = 0;
                [((id<OrgReactivestreamsSubscription>) nil_chk([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(upstream_)) get])) requestWithLong:lim];
              }
            }
          }
          if (r == 0) {
            IOSObjectArray *bs = [subs get];
            if (bs == IoReactivexProcessorsMulticastProcessor_TERMINATED) {
              [q clear];
              return;
            }
            if (as != bs) {
              goto continue_outer;
            }
            if (JreLoadVolatileBoolean(&done_) && [q isEmpty]) {
              JavaLangThrowable *ex = JreLoadVolatileId(&error_);
              if (ex != nil) {
                {
                  IOSObjectArray *a__ = [subs getAndSetWithId:IoReactivexProcessorsMulticastProcessor_TERMINATED];
                  IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
                  IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *e__ = b__ + a__->size_;
                  while (b__ < e__) {
                    IoReactivexProcessorsMulticastProcessor_MulticastSubscription *inner = *b__++;
                    [((IoReactivexProcessorsMulticastProcessor_MulticastSubscription *) nil_chk(inner)) onErrorWithJavaLangThrowable:ex];
                  }
                }
              }
              else {
                {
                  IOSObjectArray *a__ = [subs getAndSetWithId:IoReactivexProcessorsMulticastProcessor_TERMINATED];
                  IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
                  IoReactivexProcessorsMulticastProcessor_MulticastSubscription * const *e__ = b__ + a__->size_;
                  while (b__ < e__) {
                    IoReactivexProcessorsMulticastProcessor_MulticastSubscription *inner = *b__++;
                    [((IoReactivexProcessorsMulticastProcessor_MulticastSubscription *) nil_chk(inner)) onComplete];
                  }
                }
              }
              return;
            }
          }
        }
      }
      missed = [wip_ addAndGetWithInt:-missed];
      if (missed == 0) {
        break;
      }
    }
    continue_outer: ;
  }
}

- (void)__javaClone:(IoReactivexProcessorsMulticastProcessor *)original {
  [super __javaClone:original];
  JreCloneVolatileStrong(&queue_, &original->queue_);
  JreCloneVolatileStrong(&error_, &original->error_);
}

- (void)dealloc {
  RELEASE_(wip_);
  RELEASE_(upstream_);
  RELEASE_(subscribers_);
  RELEASE_(once_);
  JreReleaseVolatile(&queue_);
  JreReleaseVolatile(&error_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LIoReactivexProcessorsMulticastProcessor;", 0x9, -1, -1, -1, 0, 1, -1 },
    { NULL, "LIoReactivexProcessorsMulticastProcessor;", 0x9, 2, 3, -1, 4, 5, -1 },
    { NULL, "LIoReactivexProcessorsMulticastProcessor;", 0x9, 2, 6, -1, 7, 8, -1 },
    { NULL, "LIoReactivexProcessorsMulticastProcessor;", 0x9, 2, 9, -1, 10, 11, -1 },
    { NULL, NULL, 0x0, -1, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 15, -1, 16, -1, -1 },
    { NULL, "Z", 0x1, 17, 15, -1, 18, -1, -1 },
    { NULL, "V", 0x1, 19, 20, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaLangThrowable;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 21, 22, -1, 23, -1, -1 },
    { NULL, "Z", 0x0, 24, 25, -1, 26, -1, -1 },
    { NULL, "V", 0x0, 27, 25, -1, 28, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(create);
  methods[1].selector = @selector(createWithBoolean:);
  methods[2].selector = @selector(createWithInt:);
  methods[3].selector = @selector(createWithInt:withBoolean:);
  methods[4].selector = @selector(initWithInt:withBoolean:);
  methods[5].selector = @selector(start);
  methods[6].selector = @selector(startUnbounded);
  methods[7].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[8].selector = @selector(onNextWithId:);
  methods[9].selector = @selector(offerWithId:);
  methods[10].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[11].selector = @selector(onComplete);
  methods[12].selector = @selector(hasSubscribers);
  methods[13].selector = @selector(hasThrowable);
  methods[14].selector = @selector(hasComplete);
  methods[15].selector = @selector(getThrowable);
  methods[16].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  methods[17].selector = @selector(addWithIoReactivexProcessorsMulticastProcessor_MulticastSubscription:);
  methods[18].selector = @selector(removeWithIoReactivexProcessorsMulticastProcessor_MulticastSubscription:);
  methods[19].selector = @selector(drain);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "wip_", "LJavaUtilConcurrentAtomicAtomicInteger;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LJavaUtilConcurrentAtomicAtomicReference;", .constantValue.asLong = 0, 0x10, -1, -1, 29, -1 },
    { "subscribers_", "LJavaUtilConcurrentAtomicAtomicReference;", .constantValue.asLong = 0, 0x10, -1, -1, 30, -1 },
    { "once_", "LJavaUtilConcurrentAtomicAtomicBoolean;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "bufferSize_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "limit_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "refcount_", "Z", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "queue_", "LIoReactivexInternalFuseableSimpleQueue;", .constantValue.asLong = 0, 0x40, -1, -1, 31, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "error_", "LJavaLangThrowable;", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "consumed_", "I", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "fusionMode_", "I", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "EMPTY", "[LIoReactivexProcessorsMulticastProcessor_MulticastSubscription;", .constantValue.asLong = 0, 0x18, -1, 32, -1, -1 },
    { "TERMINATED", "[LIoReactivexProcessorsMulticastProcessor_MulticastSubscription;", .constantValue.asLong = 0, 0x18, -1, 33, -1, -1 },
  };
  static const void *ptrTable[] = { "<T:Ljava/lang/Object;>()Lio/reactivex/processors/MulticastProcessor<TT;>;", (void *)&IoReactivexProcessorsMulticastProcessor__Annotations$0, "create", "Z", "<T:Ljava/lang/Object;>(Z)Lio/reactivex/processors/MulticastProcessor<TT;>;", (void *)&IoReactivexProcessorsMulticastProcessor__Annotations$1, "I", "<T:Ljava/lang/Object;>(I)Lio/reactivex/processors/MulticastProcessor<TT;>;", (void *)&IoReactivexProcessorsMulticastProcessor__Annotations$2, "IZ", "<T:Ljava/lang/Object;>(IZ)Lio/reactivex/processors/MulticastProcessor<TT;>;", (void *)&IoReactivexProcessorsMulticastProcessor__Annotations$3, "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "(TT;)V", "offer", "(TT;)Z", "onError", "LJavaLangThrowable;", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "add", "LIoReactivexProcessorsMulticastProcessor_MulticastSubscription;", "(Lio/reactivex/processors/MulticastProcessor$MulticastSubscription<TT;>;)Z", "remove", "(Lio/reactivex/processors/MulticastProcessor$MulticastSubscription<TT;>;)V", "Ljava/util/concurrent/atomic/AtomicReference<Lorg/reactivestreams/Subscription;>;", "Ljava/util/concurrent/atomic/AtomicReference<[Lio/reactivex/processors/MulticastProcessor$MulticastSubscription<TT;>;>;", "Lio/reactivex/internal/fuseable/SimpleQueue<TT;>;", &IoReactivexProcessorsMulticastProcessor_EMPTY, &IoReactivexProcessorsMulticastProcessor_TERMINATED, "<T:Ljava/lang/Object;>Lio/reactivex/processors/FlowableProcessor<TT;>;", (void *)&IoReactivexProcessorsMulticastProcessor__Annotations$4 };
  static const J2ObjcClassInfo _IoReactivexProcessorsMulticastProcessor = { "MulticastProcessor", "io.reactivex.processors", ptrTable, methods, fields, 7, 0x11, 20, 14, -1, 25, -1, 34, 35 };
  return &_IoReactivexProcessorsMulticastProcessor;
}

+ (void)initialize {
  if (self == [IoReactivexProcessorsMulticastProcessor class]) {
    JreStrongAssignAndConsume(&IoReactivexProcessorsMulticastProcessor_EMPTY, [IOSObjectArray newArrayWithLength:0 type:IoReactivexProcessorsMulticastProcessor_MulticastSubscription_class_()]);
    JreStrongAssignAndConsume(&IoReactivexProcessorsMulticastProcessor_TERMINATED, [IOSObjectArray newArrayWithLength:0 type:IoReactivexProcessorsMulticastProcessor_MulticastSubscription_class_()]);
    J2OBJC_SET_INITIALIZED(IoReactivexProcessorsMulticastProcessor)
  }
}

@end

IoReactivexProcessorsMulticastProcessor *IoReactivexProcessorsMulticastProcessor_create() {
  IoReactivexProcessorsMulticastProcessor_initialize();
  return create_IoReactivexProcessorsMulticastProcessor_initWithInt_withBoolean_(IoReactivexFlowable_bufferSize(), false);
}

IoReactivexProcessorsMulticastProcessor *IoReactivexProcessorsMulticastProcessor_createWithBoolean_(jboolean refCount) {
  IoReactivexProcessorsMulticastProcessor_initialize();
  return create_IoReactivexProcessorsMulticastProcessor_initWithInt_withBoolean_(IoReactivexFlowable_bufferSize(), refCount);
}

IoReactivexProcessorsMulticastProcessor *IoReactivexProcessorsMulticastProcessor_createWithInt_(jint bufferSize) {
  IoReactivexProcessorsMulticastProcessor_initialize();
  return create_IoReactivexProcessorsMulticastProcessor_initWithInt_withBoolean_(bufferSize, false);
}

IoReactivexProcessorsMulticastProcessor *IoReactivexProcessorsMulticastProcessor_createWithInt_withBoolean_(jint bufferSize, jboolean refCount) {
  IoReactivexProcessorsMulticastProcessor_initialize();
  return create_IoReactivexProcessorsMulticastProcessor_initWithInt_withBoolean_(bufferSize, refCount);
}

void IoReactivexProcessorsMulticastProcessor_initWithInt_withBoolean_(IoReactivexProcessorsMulticastProcessor *self, jint bufferSize, jboolean refCount) {
  IoReactivexProcessorsFlowableProcessor_init(self);
  IoReactivexInternalFunctionsObjectHelper_verifyPositiveWithInt_withNSString_(bufferSize, @"bufferSize");
  self->bufferSize_ = bufferSize;
  self->limit_ = bufferSize - (JreRShift32(bufferSize, 2));
  JreStrongAssignAndConsume(&self->wip_, new_JavaUtilConcurrentAtomicAtomicInteger_init());
  JreStrongAssignAndConsume(&self->subscribers_, new_JavaUtilConcurrentAtomicAtomicReference_initWithId_(IoReactivexProcessorsMulticastProcessor_EMPTY));
  JreStrongAssignAndConsume(&self->upstream_, new_JavaUtilConcurrentAtomicAtomicReference_init());
  self->refcount_ = refCount;
  JreStrongAssignAndConsume(&self->once_, new_JavaUtilConcurrentAtomicAtomicBoolean_init());
}

IoReactivexProcessorsMulticastProcessor *new_IoReactivexProcessorsMulticastProcessor_initWithInt_withBoolean_(jint bufferSize, jboolean refCount) {
  J2OBJC_NEW_IMPL(IoReactivexProcessorsMulticastProcessor, initWithInt_withBoolean_, bufferSize, refCount)
}

IoReactivexProcessorsMulticastProcessor *create_IoReactivexProcessorsMulticastProcessor_initWithInt_withBoolean_(jint bufferSize, jboolean refCount) {
  J2OBJC_CREATE_IMPL(IoReactivexProcessorsMulticastProcessor, initWithInt_withBoolean_, bufferSize, refCount)
}

IOSObjectArray *IoReactivexProcessorsMulticastProcessor__Annotations$0() {
  return [IOSObjectArray arrayWithObjects:(id[]){ create_IoReactivexAnnotationsCheckReturnValue() } count:1 type:JavaLangAnnotationAnnotation_class_()];
}

IOSObjectArray *IoReactivexProcessorsMulticastProcessor__Annotations$1() {
  return [IOSObjectArray arrayWithObjects:(id[]){ create_IoReactivexAnnotationsCheckReturnValue() } count:1 type:JavaLangAnnotationAnnotation_class_()];
}

IOSObjectArray *IoReactivexProcessorsMulticastProcessor__Annotations$2() {
  return [IOSObjectArray arrayWithObjects:(id[]){ create_IoReactivexAnnotationsCheckReturnValue() } count:1 type:JavaLangAnnotationAnnotation_class_()];
}

IOSObjectArray *IoReactivexProcessorsMulticastProcessor__Annotations$3() {
  return [IOSObjectArray arrayWithObjects:(id[]){ create_IoReactivexAnnotationsCheckReturnValue() } count:1 type:JavaLangAnnotationAnnotation_class_()];
}

IOSObjectArray *IoReactivexProcessorsMulticastProcessor__Annotations$4() {
  return [IOSObjectArray arrayWithObjects:(id[]){ create_IoReactivexAnnotationsBackpressureSupport(JreLoadEnum(IoReactivexAnnotationsBackpressureKind, FULL)), create_IoReactivexAnnotationsSchedulerSupport(@"none") } count:2 type:JavaLangAnnotationAnnotation_class_()];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexProcessorsMulticastProcessor)

@implementation IoReactivexProcessorsMulticastProcessor_MulticastSubscription

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                   withIoReactivexProcessorsMulticastProcessor:(IoReactivexProcessorsMulticastProcessor *)parent {
  IoReactivexProcessorsMulticastProcessor_MulticastSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsMulticastProcessor_(self, actual, parent);
  return self;
}

- (void)requestWithLong:(jlong)n {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_validateWithLong_(n)) {
    for (; ; ) {
      jlong r = [self get];
      if (r == JavaLangLong_MIN_VALUE || r == JavaLangLong_MAX_VALUE) {
        break;
      }
      jlong u = r + n;
      if (u < 0LL) {
        u = JavaLangLong_MAX_VALUE;
      }
      if ([self compareAndSetWithLong:r withLong:u]) {
        [((IoReactivexProcessorsMulticastProcessor *) nil_chk(parent_)) drain];
        break;
      }
    }
  }
}

- (void)cancel {
  if ([self getAndSetWithLong:JavaLangLong_MIN_VALUE] != JavaLangLong_MIN_VALUE) {
    [((IoReactivexProcessorsMulticastProcessor *) nil_chk(parent_)) removeWithIoReactivexProcessorsMulticastProcessor_MulticastSubscription:self];
  }
}

- (void)onNextWithId:(id)t {
  if ([self get] != JavaLangLong_MIN_VALUE) {
    emitted_++;
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:t];
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if ([self get] != JavaLangLong_MIN_VALUE) {
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
  }
}

- (void)onComplete {
  if ([self get] != JavaLangLong_MIN_VALUE) {
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onComplete];
  }
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(parent_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x0, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:withIoReactivexProcessorsMulticastProcessor:);
  methods[1].selector = @selector(requestWithLong:);
  methods[2].selector = @selector(cancel);
  methods[3].selector = @selector(onNextWithId:);
  methods[4].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[5].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexProcessorsMulticastProcessor_MulticastSubscription_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LOrgReactivestreamsSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "parent_", "LIoReactivexProcessorsMulticastProcessor;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "emitted_", "J", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;LIoReactivexProcessorsMulticastProcessor;", "(Lorg/reactivestreams/Subscriber<-TT;>;Lio/reactivex/processors/MulticastProcessor<TT;>;)V", "request", "J", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lorg/reactivestreams/Subscriber<-TT;>;", "Lio/reactivex/processors/MulticastProcessor<TT;>;", "LIoReactivexProcessorsMulticastProcessor;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicLong;Lorg/reactivestreams/Subscription;" };
  static const J2ObjcClassInfo _IoReactivexProcessorsMulticastProcessor_MulticastSubscription = { "MulticastSubscription", "io.reactivex.processors", ptrTable, methods, fields, 7, 0x18, 6, 4, 11, -1, -1, 12, -1 };
  return &_IoReactivexProcessorsMulticastProcessor_MulticastSubscription;
}

@end

void IoReactivexProcessorsMulticastProcessor_MulticastSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsMulticastProcessor_(IoReactivexProcessorsMulticastProcessor_MulticastSubscription *self, id<OrgReactivestreamsSubscriber> actual, IoReactivexProcessorsMulticastProcessor *parent) {
  JavaUtilConcurrentAtomicAtomicLong_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->parent_, parent);
}

IoReactivexProcessorsMulticastProcessor_MulticastSubscription *new_IoReactivexProcessorsMulticastProcessor_MulticastSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsMulticastProcessor_(id<OrgReactivestreamsSubscriber> actual, IoReactivexProcessorsMulticastProcessor *parent) {
  J2OBJC_NEW_IMPL(IoReactivexProcessorsMulticastProcessor_MulticastSubscription, initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsMulticastProcessor_, actual, parent)
}

IoReactivexProcessorsMulticastProcessor_MulticastSubscription *create_IoReactivexProcessorsMulticastProcessor_MulticastSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsMulticastProcessor_(id<OrgReactivestreamsSubscriber> actual, IoReactivexProcessorsMulticastProcessor *parent) {
  J2OBJC_CREATE_IMPL(IoReactivexProcessorsMulticastProcessor_MulticastSubscription, initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsMulticastProcessor_, actual, parent)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexProcessorsMulticastProcessor_MulticastSubscription)
