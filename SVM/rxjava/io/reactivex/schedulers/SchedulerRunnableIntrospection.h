//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/schedulers/SchedulerRunnableIntrospection.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexSchedulersSchedulerRunnableIntrospection")
#ifdef RESTRICT_IoReactivexSchedulersSchedulerRunnableIntrospection
#define INCLUDE_ALL_IoReactivexSchedulersSchedulerRunnableIntrospection 0
#else
#define INCLUDE_ALL_IoReactivexSchedulersSchedulerRunnableIntrospection 1
#endif
#undef RESTRICT_IoReactivexSchedulersSchedulerRunnableIntrospection

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexSchedulersSchedulerRunnableIntrospection_) && (INCLUDE_ALL_IoReactivexSchedulersSchedulerRunnableIntrospection || defined(INCLUDE_IoReactivexSchedulersSchedulerRunnableIntrospection))
#define IoReactivexSchedulersSchedulerRunnableIntrospection_

@protocol JavaLangRunnable;

@protocol IoReactivexSchedulersSchedulerRunnableIntrospection < JavaObject >

- (id<JavaLangRunnable> __nonnull)getWrappedRunnable;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexSchedulersSchedulerRunnableIntrospection)

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexSchedulersSchedulerRunnableIntrospection)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexSchedulersSchedulerRunnableIntrospection")
