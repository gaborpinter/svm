//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/CompletableEmitter.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexCompletableEmitter")
#ifdef RESTRICT_IoReactivexCompletableEmitter
#define INCLUDE_ALL_IoReactivexCompletableEmitter 0
#else
#define INCLUDE_ALL_IoReactivexCompletableEmitter 1
#endif
#undef RESTRICT_IoReactivexCompletableEmitter

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexCompletableEmitter_) && (INCLUDE_ALL_IoReactivexCompletableEmitter || defined(INCLUDE_IoReactivexCompletableEmitter))
#define IoReactivexCompletableEmitter_

@class JavaLangThrowable;
@protocol IoReactivexDisposablesDisposable;
@protocol IoReactivexFunctionsCancellable;

@protocol IoReactivexCompletableEmitter < JavaObject >

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable * __nonnull)t;

- (void)setDisposableWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable> __nullable)d;

- (void)setCancellableWithIoReactivexFunctionsCancellable:(id<IoReactivexFunctionsCancellable> __nullable)c;

- (jboolean)isDisposed;

- (jboolean)tryOnErrorWithJavaLangThrowable:(JavaLangThrowable * __nonnull)t;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexCompletableEmitter)

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexCompletableEmitter)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexCompletableEmitter")
