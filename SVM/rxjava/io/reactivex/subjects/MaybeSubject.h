//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/subjects/MaybeSubject.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexSubjectsMaybeSubject")
#ifdef RESTRICT_IoReactivexSubjectsMaybeSubject
#define INCLUDE_ALL_IoReactivexSubjectsMaybeSubject 0
#else
#define INCLUDE_ALL_IoReactivexSubjectsMaybeSubject 1
#endif
#undef RESTRICT_IoReactivexSubjectsMaybeSubject

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexSubjectsMaybeSubject_) && (INCLUDE_ALL_IoReactivexSubjectsMaybeSubject || defined(INCLUDE_IoReactivexSubjectsMaybeSubject))
#define IoReactivexSubjectsMaybeSubject_

#define RESTRICT_IoReactivexMaybe 1
#define INCLUDE_IoReactivexMaybe 1
#include "io/reactivex/Maybe.h"

#define RESTRICT_IoReactivexMaybeObserver 1
#define INCLUDE_IoReactivexMaybeObserver 1
#include "io/reactivex/MaybeObserver.h"

@class IOSObjectArray;
@class IoReactivexSubjectsMaybeSubject_MaybeDisposable;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicBoolean;
@class JavaUtilConcurrentAtomicAtomicReference;
@protocol IoReactivexDisposablesDisposable;

@interface IoReactivexSubjectsMaybeSubject : IoReactivexMaybe < IoReactivexMaybeObserver > {
 @public
  JavaUtilConcurrentAtomicAtomicReference *observers_;
  JavaUtilConcurrentAtomicAtomicBoolean *once_;
  id value_;
  JavaLangThrowable *error_;
}

+ (IOSObjectArray *)EMPTY;

+ (IOSObjectArray *)TERMINATED;

#pragma mark Public

+ (IoReactivexSubjectsMaybeSubject * __nonnull)create;

- (JavaLangThrowable * __nullable)getThrowable;

- (id __nullable)getValue;

- (jboolean)hasComplete;

- (jboolean)hasObservers;

- (jboolean)hasThrowable;

- (jboolean)hasValue;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (void)onSuccessWithId:(id)value;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexMaybeObserver:(id<IoReactivexMaybeObserver>)observer;

#pragma mark Package-Private

- (instancetype __nonnull)init;

- (jboolean)addWithIoReactivexSubjectsMaybeSubject_MaybeDisposable:(IoReactivexSubjectsMaybeSubject_MaybeDisposable *)inner;

- (jint)observerCount;

- (void)removeWithIoReactivexSubjectsMaybeSubject_MaybeDisposable:(IoReactivexSubjectsMaybeSubject_MaybeDisposable *)inner;

@end

J2OBJC_STATIC_INIT(IoReactivexSubjectsMaybeSubject)

J2OBJC_FIELD_SETTER(IoReactivexSubjectsMaybeSubject, observers_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexSubjectsMaybeSubject, once_, JavaUtilConcurrentAtomicAtomicBoolean *)
J2OBJC_FIELD_SETTER(IoReactivexSubjectsMaybeSubject, value_, id)
J2OBJC_FIELD_SETTER(IoReactivexSubjectsMaybeSubject, error_, JavaLangThrowable *)

inline IOSObjectArray *IoReactivexSubjectsMaybeSubject_get_EMPTY(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT IOSObjectArray *IoReactivexSubjectsMaybeSubject_EMPTY;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexSubjectsMaybeSubject, EMPTY, IOSObjectArray *)

inline IOSObjectArray *IoReactivexSubjectsMaybeSubject_get_TERMINATED(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT IOSObjectArray *IoReactivexSubjectsMaybeSubject_TERMINATED;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexSubjectsMaybeSubject, TERMINATED, IOSObjectArray *)

FOUNDATION_EXPORT IoReactivexSubjectsMaybeSubject *IoReactivexSubjectsMaybeSubject_create(void);

FOUNDATION_EXPORT void IoReactivexSubjectsMaybeSubject_init(IoReactivexSubjectsMaybeSubject *self);

FOUNDATION_EXPORT IoReactivexSubjectsMaybeSubject *new_IoReactivexSubjectsMaybeSubject_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexSubjectsMaybeSubject *create_IoReactivexSubjectsMaybeSubject_init(void);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexSubjectsMaybeSubject)

#endif

#if !defined (IoReactivexSubjectsMaybeSubject_MaybeDisposable_) && (INCLUDE_ALL_IoReactivexSubjectsMaybeSubject || defined(INCLUDE_IoReactivexSubjectsMaybeSubject_MaybeDisposable))
#define IoReactivexSubjectsMaybeSubject_MaybeDisposable_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class IoReactivexSubjectsMaybeSubject;
@protocol IoReactivexMaybeObserver;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;

@interface IoReactivexSubjectsMaybeSubject_MaybeDisposable : JavaUtilConcurrentAtomicAtomicReference < IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexMaybeObserver> downstream_;
}

#pragma mark Public

- (IoReactivexSubjectsMaybeSubject *)accumulateAndGetWithId:(IoReactivexSubjectsMaybeSubject *)arg0
                         withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)dispose;

- (IoReactivexSubjectsMaybeSubject *)get;

- (IoReactivexSubjectsMaybeSubject *)getAndAccumulateWithId:(IoReactivexSubjectsMaybeSubject *)arg0
                         withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (IoReactivexSubjectsMaybeSubject *)getAndSetWithId:(IoReactivexSubjectsMaybeSubject *)arg0;

- (IoReactivexSubjectsMaybeSubject *)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (jboolean)isDisposed;

- (IoReactivexSubjectsMaybeSubject *)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexMaybeObserver:(id<IoReactivexMaybeObserver>)actual
                       withIoReactivexSubjectsMaybeSubject:(IoReactivexSubjectsMaybeSubject *)parent;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexSubjectsMaybeSubject_MaybeDisposable)

J2OBJC_FIELD_SETTER(IoReactivexSubjectsMaybeSubject_MaybeDisposable, downstream_, id<IoReactivexMaybeObserver>)

FOUNDATION_EXPORT void IoReactivexSubjectsMaybeSubject_MaybeDisposable_initWithIoReactivexMaybeObserver_withIoReactivexSubjectsMaybeSubject_(IoReactivexSubjectsMaybeSubject_MaybeDisposable *self, id<IoReactivexMaybeObserver> actual, IoReactivexSubjectsMaybeSubject *parent);

FOUNDATION_EXPORT IoReactivexSubjectsMaybeSubject_MaybeDisposable *new_IoReactivexSubjectsMaybeSubject_MaybeDisposable_initWithIoReactivexMaybeObserver_withIoReactivexSubjectsMaybeSubject_(id<IoReactivexMaybeObserver> actual, IoReactivexSubjectsMaybeSubject *parent) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexSubjectsMaybeSubject_MaybeDisposable *create_IoReactivexSubjectsMaybeSubject_MaybeDisposable_initWithIoReactivexMaybeObserver_withIoReactivexSubjectsMaybeSubject_(id<IoReactivexMaybeObserver> actual, IoReactivexSubjectsMaybeSubject *parent);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexSubjectsMaybeSubject_MaybeDisposable)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexSubjectsMaybeSubject")
