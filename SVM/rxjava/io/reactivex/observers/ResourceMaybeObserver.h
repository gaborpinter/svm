//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/observers/ResourceMaybeObserver.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexObserversResourceMaybeObserver")
#ifdef RESTRICT_IoReactivexObserversResourceMaybeObserver
#define INCLUDE_ALL_IoReactivexObserversResourceMaybeObserver 0
#else
#define INCLUDE_ALL_IoReactivexObserversResourceMaybeObserver 1
#endif
#undef RESTRICT_IoReactivexObserversResourceMaybeObserver

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexObserversResourceMaybeObserver_) && (INCLUDE_ALL_IoReactivexObserversResourceMaybeObserver || defined(INCLUDE_IoReactivexObserversResourceMaybeObserver))
#define IoReactivexObserversResourceMaybeObserver_

#define RESTRICT_IoReactivexMaybeObserver 1
#define INCLUDE_IoReactivexMaybeObserver 1
#include "io/reactivex/MaybeObserver.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@interface IoReactivexObserversResourceMaybeObserver : NSObject < IoReactivexMaybeObserver, IoReactivexDisposablesDisposable >

#pragma mark Public

- (instancetype __nonnull)init;

- (void)addWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable> __nonnull)resource;

- (void)dispose;

- (jboolean)isDisposed;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable> __nonnull)d;

#pragma mark Protected

- (void)onStart;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexObserversResourceMaybeObserver)

FOUNDATION_EXPORT void IoReactivexObserversResourceMaybeObserver_init(IoReactivexObserversResourceMaybeObserver *self);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexObserversResourceMaybeObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexObserversResourceMaybeObserver")
