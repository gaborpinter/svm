//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/schedulers/ScheduledDirectPeriodicTask.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/internal/schedulers/AbstractDirectTask.h"
#include "io/reactivex/internal/schedulers/ScheduledDirectPeriodicTask.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Runnable.h"
#include "java/lang/Thread.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/FutureTask.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalSchedulersScheduledDirectPeriodicTask_get_serialVersionUID(void);
#define IoReactivexInternalSchedulersScheduledDirectPeriodicTask_serialVersionUID 1811839108042568751LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalSchedulersScheduledDirectPeriodicTask, serialVersionUID, jlong)

@implementation IoReactivexInternalSchedulersScheduledDirectPeriodicTask

- (instancetype __nonnull)initWithJavaLangRunnable:(id<JavaLangRunnable>)runnable {
  IoReactivexInternalSchedulersScheduledDirectPeriodicTask_initWithJavaLangRunnable_(self, runnable);
  return self;
}

- (void)run {
  JreStrongAssign(&runner_, JavaLangThread_currentThread());
  @try {
    [((id<JavaLangRunnable>) nil_chk(runnable_)) run];
    JreStrongAssign(&runner_, nil);
  }
  @catch (JavaLangThrowable *ex) {
    JreStrongAssign(&runner_, nil);
    [self lazySetWithId:JreLoadStatic(IoReactivexInternalSchedulersAbstractDirectTask, FINISHED)];
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaLangRunnable:);
  methods[1].selector = @selector(run);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalSchedulersScheduledDirectPeriodicTask_serialVersionUID, 0x1a, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LJavaLangRunnable;" };
  static const J2ObjcClassInfo _IoReactivexInternalSchedulersScheduledDirectPeriodicTask = { "ScheduledDirectPeriodicTask", "io.reactivex.internal.schedulers", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, -1, -1, -1, -1 };
  return &_IoReactivexInternalSchedulersScheduledDirectPeriodicTask;
}

@end

void IoReactivexInternalSchedulersScheduledDirectPeriodicTask_initWithJavaLangRunnable_(IoReactivexInternalSchedulersScheduledDirectPeriodicTask *self, id<JavaLangRunnable> runnable) {
  IoReactivexInternalSchedulersAbstractDirectTask_initWithJavaLangRunnable_(self, runnable);
}

IoReactivexInternalSchedulersScheduledDirectPeriodicTask *new_IoReactivexInternalSchedulersScheduledDirectPeriodicTask_initWithJavaLangRunnable_(id<JavaLangRunnable> runnable) {
  J2OBJC_NEW_IMPL(IoReactivexInternalSchedulersScheduledDirectPeriodicTask, initWithJavaLangRunnable_, runnable)
}

IoReactivexInternalSchedulersScheduledDirectPeriodicTask *create_IoReactivexInternalSchedulersScheduledDirectPeriodicTask_initWithJavaLangRunnable_(id<JavaLangRunnable> runnable) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalSchedulersScheduledDirectPeriodicTask, initWithJavaLangRunnable_, runnable)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSchedulersScheduledDirectPeriodicTask)
