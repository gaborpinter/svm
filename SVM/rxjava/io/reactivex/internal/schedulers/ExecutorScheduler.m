//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/schedulers/ExecutorScheduler.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "io/reactivex/Scheduler.h"
#include "io/reactivex/disposables/CompositeDisposable.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/disposables/EmptyDisposable.h"
#include "io/reactivex/internal/disposables/SequentialDisposable.h"
#include "io/reactivex/internal/functions/Functions.h"
#include "io/reactivex/internal/queue/MpscLinkedQueue.h"
#include "io/reactivex/internal/schedulers/DisposeOnCancel.h"
#include "io/reactivex/internal/schedulers/ExecutorScheduler.h"
#include "io/reactivex/internal/schedulers/ScheduledDirectPeriodicTask.h"
#include "io/reactivex/internal/schedulers/ScheduledDirectTask.h"
#include "io/reactivex/internal/schedulers/ScheduledRunnable.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "io/reactivex/schedulers/Schedulers.h"
#include "java/lang/Runnable.h"
#include "java/util/concurrent/Executor.h"
#include "java/util/concurrent/ExecutorService.h"
#include "java/util/concurrent/Future.h"
#include "java/util/concurrent/RejectedExecutionException.h"
#include "java/util/concurrent/ScheduledExecutorService.h"
#include "java/util/concurrent/ScheduledFuture.h"
#include "java/util/concurrent/TimeUnit.h"
#include "java/util/concurrent/atomic/AtomicBoolean.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"
#include "java/util/concurrent/atomic/AtomicReference.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable_get_serialVersionUID(void);
#define IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable_serialVersionUID -2421395018820541164LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable, serialVersionUID, jlong)

@interface IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose () {
 @public
  IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker *this$0_;
  IoReactivexInternalDisposablesSequentialDisposable *mar_;
  id<JavaLangRunnable> decoratedRun_;
}

@end

J2OBJC_FIELD_SETTER(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose, mar_, IoReactivexInternalDisposablesSequentialDisposable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose, decoratedRun_, id<JavaLangRunnable>)

inline jlong IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_get_serialVersionUID(void);
#define IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_serialVersionUID -4101336210206799084LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable, serialVersionUID, jlong)

@interface IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose () {
 @public
  IoReactivexInternalSchedulersExecutorScheduler *this$0_;
  IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *dr_;
}

@end

J2OBJC_FIELD_SETTER(IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose, dr_, IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *)

J2OBJC_INITIALIZED_DEFN(IoReactivexInternalSchedulersExecutorScheduler)

IoReactivexScheduler *IoReactivexInternalSchedulersExecutorScheduler_HELPER;

@implementation IoReactivexInternalSchedulersExecutorScheduler

+ (IoReactivexScheduler *)HELPER {
  return IoReactivexInternalSchedulersExecutorScheduler_HELPER;
}

- (instancetype __nonnull)initWithJavaUtilConcurrentExecutor:(id<JavaUtilConcurrentExecutor> __nonnull)executor {
  IoReactivexInternalSchedulersExecutorScheduler_initWithJavaUtilConcurrentExecutor_(self, executor);
  return self;
}

- (IoReactivexScheduler_Worker * __nonnull)createWorker {
  return create_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_initWithJavaUtilConcurrentExecutor_(executor_);
}

- (id<IoReactivexDisposablesDisposable> __nonnull)scheduleDirectWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run {
  id<JavaLangRunnable> decoratedRun = IoReactivexPluginsRxJavaPlugins_onScheduleWithJavaLangRunnable_(run);
  @try {
    if ([JavaUtilConcurrentExecutorService_class_() isInstance:executor_]) {
      IoReactivexInternalSchedulersScheduledDirectTask *task = create_IoReactivexInternalSchedulersScheduledDirectTask_initWithJavaLangRunnable_(decoratedRun);
      id<JavaUtilConcurrentFuture> f = [((id<JavaUtilConcurrentExecutorService>) nil_chk(((id<JavaUtilConcurrentExecutorService>) cast_check(executor_, JavaUtilConcurrentExecutorService_class_())))) submitWithJavaUtilConcurrentCallable:task];
      [task setFutureWithJavaUtilConcurrentFuture:f];
      return task;
    }
    IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable *br = create_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable_initWithJavaLangRunnable_(decoratedRun);
    [((id<JavaUtilConcurrentExecutor>) nil_chk(executor_)) executeWithJavaLangRunnable:br];
    return br;
  }
  @catch (JavaUtilConcurrentRejectedExecutionException *ex) {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
    return JreLoadEnum(IoReactivexInternalDisposablesEmptyDisposable, INSTANCE);
  }
}

- (id<IoReactivexDisposablesDisposable> __nonnull)scheduleDirectWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run
                                                                            withLong:(jlong)delay
                                                      withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit {
  id<JavaLangRunnable> decoratedRun = IoReactivexPluginsRxJavaPlugins_onScheduleWithJavaLangRunnable_(run);
  if ([JavaUtilConcurrentScheduledExecutorService_class_() isInstance:executor_]) {
    @try {
      IoReactivexInternalSchedulersScheduledDirectTask *task = create_IoReactivexInternalSchedulersScheduledDirectTask_initWithJavaLangRunnable_(decoratedRun);
      id<JavaUtilConcurrentFuture> f = [((id<JavaUtilConcurrentScheduledExecutorService>) nil_chk(((id<JavaUtilConcurrentScheduledExecutorService>) cast_check(executor_, JavaUtilConcurrentScheduledExecutorService_class_())))) scheduleWithJavaUtilConcurrentCallable:task withLong:delay withJavaUtilConcurrentTimeUnit:unit];
      [task setFutureWithJavaUtilConcurrentFuture:f];
      return task;
    }
    @catch (JavaUtilConcurrentRejectedExecutionException *ex) {
      IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
      return JreLoadEnum(IoReactivexInternalDisposablesEmptyDisposable, INSTANCE);
    }
  }
  IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *dr = create_IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_initWithJavaLangRunnable_(decoratedRun);
  id<IoReactivexDisposablesDisposable> delayed = [((IoReactivexScheduler *) nil_chk(IoReactivexInternalSchedulersExecutorScheduler_HELPER)) scheduleDirectWithJavaLangRunnable:create_IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose_initWithIoReactivexInternalSchedulersExecutorScheduler_withIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_(self, dr) withLong:delay withJavaUtilConcurrentTimeUnit:unit];
  [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(dr->timed_)) replaceWithIoReactivexDisposablesDisposable:delayed];
  return dr;
}

- (id<IoReactivexDisposablesDisposable> __nonnull)schedulePeriodicallyDirectWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run
                                                                                        withLong:(jlong)initialDelay
                                                                                        withLong:(jlong)period
                                                                  withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit {
  if ([JavaUtilConcurrentScheduledExecutorService_class_() isInstance:executor_]) {
    id<JavaLangRunnable> decoratedRun = IoReactivexPluginsRxJavaPlugins_onScheduleWithJavaLangRunnable_(run);
    @try {
      IoReactivexInternalSchedulersScheduledDirectPeriodicTask *task = create_IoReactivexInternalSchedulersScheduledDirectPeriodicTask_initWithJavaLangRunnable_(decoratedRun);
      id<JavaUtilConcurrentFuture> f = [((id<JavaUtilConcurrentScheduledExecutorService>) nil_chk(((id<JavaUtilConcurrentScheduledExecutorService>) cast_check(executor_, JavaUtilConcurrentScheduledExecutorService_class_())))) scheduleAtFixedRateWithJavaLangRunnable:task withLong:initialDelay withLong:period withJavaUtilConcurrentTimeUnit:unit];
      [task setFutureWithJavaUtilConcurrentFuture:f];
      return task;
    }
    @catch (JavaUtilConcurrentRejectedExecutionException *ex) {
      IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
      return JreLoadEnum(IoReactivexInternalDisposablesEmptyDisposable, INSTANCE);
    }
  }
  return [super schedulePeriodicallyDirectWithJavaLangRunnable:run withLong:initialDelay withLong:period withJavaUtilConcurrentTimeUnit:unit];
}

- (void)dealloc {
  RELEASE_(executor_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LIoReactivexScheduler_Worker;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 1, 3, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 4, 5, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaUtilConcurrentExecutor:);
  methods[1].selector = @selector(createWorker);
  methods[2].selector = @selector(scheduleDirectWithJavaLangRunnable:);
  methods[3].selector = @selector(scheduleDirectWithJavaLangRunnable:withLong:withJavaUtilConcurrentTimeUnit:);
  methods[4].selector = @selector(schedulePeriodicallyDirectWithJavaLangRunnable:withLong:withLong:withJavaUtilConcurrentTimeUnit:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "executor_", "LJavaUtilConcurrentExecutor;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "HELPER", "LIoReactivexScheduler;", .constantValue.asLong = 0, 0x18, -1, 6, -1, -1 },
  };
  static const void *ptrTable[] = { "LJavaUtilConcurrentExecutor;", "scheduleDirect", "LJavaLangRunnable;", "LJavaLangRunnable;JLJavaUtilConcurrentTimeUnit;", "schedulePeriodicallyDirect", "LJavaLangRunnable;JJLJavaUtilConcurrentTimeUnit;", &IoReactivexInternalSchedulersExecutorScheduler_HELPER, "LIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker;LIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable;LIoReactivexInternalSchedulersExecutorScheduler_DelayedDispose;" };
  static const J2ObjcClassInfo _IoReactivexInternalSchedulersExecutorScheduler = { "ExecutorScheduler", "io.reactivex.internal.schedulers", ptrTable, methods, fields, 7, 0x11, 5, 2, -1, 7, -1, -1, -1 };
  return &_IoReactivexInternalSchedulersExecutorScheduler;
}

+ (void)initialize {
  if (self == [IoReactivexInternalSchedulersExecutorScheduler class]) {
    JreStrongAssign(&IoReactivexInternalSchedulersExecutorScheduler_HELPER, IoReactivexSchedulersSchedulers_single());
    J2OBJC_SET_INITIALIZED(IoReactivexInternalSchedulersExecutorScheduler)
  }
}

@end

void IoReactivexInternalSchedulersExecutorScheduler_initWithJavaUtilConcurrentExecutor_(IoReactivexInternalSchedulersExecutorScheduler *self, id<JavaUtilConcurrentExecutor> executor) {
  IoReactivexScheduler_init(self);
  JreStrongAssign(&self->executor_, executor);
}

IoReactivexInternalSchedulersExecutorScheduler *new_IoReactivexInternalSchedulersExecutorScheduler_initWithJavaUtilConcurrentExecutor_(id<JavaUtilConcurrentExecutor> executor) {
  J2OBJC_NEW_IMPL(IoReactivexInternalSchedulersExecutorScheduler, initWithJavaUtilConcurrentExecutor_, executor)
}

IoReactivexInternalSchedulersExecutorScheduler *create_IoReactivexInternalSchedulersExecutorScheduler_initWithJavaUtilConcurrentExecutor_(id<JavaUtilConcurrentExecutor> executor) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalSchedulersExecutorScheduler, initWithJavaUtilConcurrentExecutor_, executor)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSchedulersExecutorScheduler)

@implementation IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker

- (instancetype __nonnull)initWithJavaUtilConcurrentExecutor:(id<JavaUtilConcurrentExecutor>)executor {
  IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_initWithJavaUtilConcurrentExecutor_(self, executor);
  return self;
}

- (id<IoReactivexDisposablesDisposable> __nonnull)scheduleWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run {
  if (JreLoadVolatileBoolean(&disposed_)) {
    return JreLoadEnum(IoReactivexInternalDisposablesEmptyDisposable, INSTANCE);
  }
  id<JavaLangRunnable> decoratedRun = IoReactivexPluginsRxJavaPlugins_onScheduleWithJavaLangRunnable_(run);
  IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable *br = create_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable_initWithJavaLangRunnable_(decoratedRun);
  [((IoReactivexInternalQueueMpscLinkedQueue *) nil_chk(queue_)) offerWithId:br];
  if ([((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(wip_)) getAndIncrement] == 0) {
    @try {
      [((id<JavaUtilConcurrentExecutor>) nil_chk(executor_)) executeWithJavaLangRunnable:self];
    }
    @catch (JavaUtilConcurrentRejectedExecutionException *ex) {
      JreAssignVolatileBoolean(&disposed_, true);
      [queue_ clear];
      IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
      return JreLoadEnum(IoReactivexInternalDisposablesEmptyDisposable, INSTANCE);
    }
  }
  return br;
}

- (id<IoReactivexDisposablesDisposable> __nonnull)scheduleWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run
                                                                      withLong:(jlong)delay
                                                withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit * __nonnull)unit {
  if (delay <= 0) {
    return [self scheduleWithJavaLangRunnable:run];
  }
  if (JreLoadVolatileBoolean(&disposed_)) {
    return JreLoadEnum(IoReactivexInternalDisposablesEmptyDisposable, INSTANCE);
  }
  IoReactivexInternalDisposablesSequentialDisposable *first = create_IoReactivexInternalDisposablesSequentialDisposable_init();
  IoReactivexInternalDisposablesSequentialDisposable *mar = create_IoReactivexInternalDisposablesSequentialDisposable_initWithIoReactivexDisposablesDisposable_(first);
  id<JavaLangRunnable> decoratedRun = IoReactivexPluginsRxJavaPlugins_onScheduleWithJavaLangRunnable_(run);
  IoReactivexInternalSchedulersScheduledRunnable *sr = create_IoReactivexInternalSchedulersScheduledRunnable_initWithJavaLangRunnable_withIoReactivexInternalDisposablesDisposableContainer_(create_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose_initWithIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_withIoReactivexInternalDisposablesSequentialDisposable_withJavaLangRunnable_(self, mar, decoratedRun), tasks_);
  [((IoReactivexDisposablesCompositeDisposable *) nil_chk(tasks_)) addWithIoReactivexDisposablesDisposable:sr];
  if ([JavaUtilConcurrentScheduledExecutorService_class_() isInstance:executor_]) {
    @try {
      id<JavaUtilConcurrentFuture> f = [((id<JavaUtilConcurrentScheduledExecutorService>) nil_chk(((id<JavaUtilConcurrentScheduledExecutorService>) cast_check(executor_, JavaUtilConcurrentScheduledExecutorService_class_())))) scheduleWithJavaUtilConcurrentCallable:sr withLong:delay withJavaUtilConcurrentTimeUnit:unit];
      [sr setFutureWithJavaUtilConcurrentFuture:f];
    }
    @catch (JavaUtilConcurrentRejectedExecutionException *ex) {
      JreAssignVolatileBoolean(&disposed_, true);
      IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
      return JreLoadEnum(IoReactivexInternalDisposablesEmptyDisposable, INSTANCE);
    }
  }
  else {
    id<IoReactivexDisposablesDisposable> d = [((IoReactivexScheduler *) nil_chk(JreLoadStatic(IoReactivexInternalSchedulersExecutorScheduler, HELPER))) scheduleDirectWithJavaLangRunnable:sr withLong:delay withJavaUtilConcurrentTimeUnit:unit];
    [sr setFutureWithJavaUtilConcurrentFuture:create_IoReactivexInternalSchedulersDisposeOnCancel_initWithIoReactivexDisposablesDisposable_(d)];
  }
  [first replaceWithIoReactivexDisposablesDisposable:sr];
  return mar;
}

- (void)dispose {
  if (!JreLoadVolatileBoolean(&disposed_)) {
    JreAssignVolatileBoolean(&disposed_, true);
    [((IoReactivexDisposablesCompositeDisposable *) nil_chk(tasks_)) dispose];
    if ([((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(wip_)) getAndIncrement] == 0) {
      [((IoReactivexInternalQueueMpscLinkedQueue *) nil_chk(queue_)) clear];
    }
  }
}

- (jboolean)isDisposed {
  return JreLoadVolatileBoolean(&disposed_);
}

- (void)run {
  jint missed = 1;
  IoReactivexInternalQueueMpscLinkedQueue *q = queue_;
  for (; ; ) {
    if (JreLoadVolatileBoolean(&disposed_)) {
      [((IoReactivexInternalQueueMpscLinkedQueue *) nil_chk(q)) clear];
      return;
    }
    for (; ; ) {
      id<JavaLangRunnable> run = [((IoReactivexInternalQueueMpscLinkedQueue *) nil_chk(q)) poll];
      if (run == nil) {
        break;
      }
      [run run];
      if (JreLoadVolatileBoolean(&disposed_)) {
        [q clear];
        return;
      }
    }
    if (JreLoadVolatileBoolean(&disposed_)) {
      [q clear];
      return;
    }
    missed = [((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(wip_)) addAndGetWithInt:-missed];
    if (missed == 0) {
      break;
    }
  }
}

- (void)dealloc {
  RELEASE_(executor_);
  RELEASE_(queue_);
  RELEASE_(wip_);
  RELEASE_(tasks_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 1, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaUtilConcurrentExecutor:);
  methods[1].selector = @selector(scheduleWithJavaLangRunnable:);
  methods[2].selector = @selector(scheduleWithJavaLangRunnable:withLong:withJavaUtilConcurrentTimeUnit:);
  methods[3].selector = @selector(dispose);
  methods[4].selector = @selector(isDisposed);
  methods[5].selector = @selector(run);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "executor_", "LJavaUtilConcurrentExecutor;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "queue_", "LIoReactivexInternalQueueMpscLinkedQueue;", .constantValue.asLong = 0, 0x10, -1, -1, 4, -1 },
    { "disposed_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "wip_", "LJavaUtilConcurrentAtomicAtomicInteger;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "tasks_", "LIoReactivexDisposablesCompositeDisposable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LJavaUtilConcurrentExecutor;", "schedule", "LJavaLangRunnable;", "LJavaLangRunnable;JLJavaUtilConcurrentTimeUnit;", "Lio/reactivex/internal/queue/MpscLinkedQueue<Ljava/lang/Runnable;>;", "LIoReactivexInternalSchedulersExecutorScheduler;", "LIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable;LIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose;" };
  static const J2ObjcClassInfo _IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker = { "ExecutorWorker", "io.reactivex.internal.schedulers", ptrTable, methods, fields, 7, 0x19, 6, 5, 5, 6, -1, -1, -1 };
  return &_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker;
}

@end

void IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_initWithJavaUtilConcurrentExecutor_(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker *self, id<JavaUtilConcurrentExecutor> executor) {
  IoReactivexScheduler_Worker_init(self);
  JreStrongAssignAndConsume(&self->wip_, new_JavaUtilConcurrentAtomicAtomicInteger_init());
  JreStrongAssignAndConsume(&self->tasks_, new_IoReactivexDisposablesCompositeDisposable_init());
  JreStrongAssign(&self->executor_, executor);
  JreStrongAssignAndConsume(&self->queue_, new_IoReactivexInternalQueueMpscLinkedQueue_init());
}

IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker *new_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_initWithJavaUtilConcurrentExecutor_(id<JavaUtilConcurrentExecutor> executor) {
  J2OBJC_NEW_IMPL(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker, initWithJavaUtilConcurrentExecutor_, executor)
}

IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker *create_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_initWithJavaUtilConcurrentExecutor_(id<JavaUtilConcurrentExecutor> executor) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker, initWithJavaUtilConcurrentExecutor_, executor)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker)

@implementation IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable

- (instancetype __nonnull)initWithJavaLangRunnable:(id<JavaLangRunnable>)actual {
  IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable_initWithJavaLangRunnable_(self, actual);
  return self;
}

- (void)run {
  if ([self get]) {
    return;
  }
  @try {
    [((id<JavaLangRunnable>) nil_chk(actual_)) run];
  }
  @finally {
    [self lazySetWithBoolean:true];
  }
}

- (void)dispose {
  [self lazySetWithBoolean:true];
}

- (jboolean)isDisposed {
  return [self get];
}

- (void)dealloc {
  RELEASE_(actual_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaLangRunnable:);
  methods[1].selector = @selector(run);
  methods[2].selector = @selector(dispose);
  methods[3].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "actual_", "LJavaLangRunnable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LJavaLangRunnable;", "LIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker;" };
  static const J2ObjcClassInfo _IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable = { "BooleanRunnable", "io.reactivex.internal.schedulers", ptrTable, methods, fields, 7, 0x18, 4, 2, 1, -1, -1, -1, -1 };
  return &_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable;
}

@end

void IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable_initWithJavaLangRunnable_(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable *self, id<JavaLangRunnable> actual) {
  JavaUtilConcurrentAtomicAtomicBoolean_init(self);
  JreStrongAssign(&self->actual_, actual);
}

IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable *new_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable_initWithJavaLangRunnable_(id<JavaLangRunnable> actual) {
  J2OBJC_NEW_IMPL(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable, initWithJavaLangRunnable_, actual)
}

IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable *create_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable_initWithJavaLangRunnable_(id<JavaLangRunnable> actual) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable, initWithJavaLangRunnable_, actual)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_BooleanRunnable)

@implementation IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose

- (instancetype __nonnull)initWithIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker:(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker *)outer$
                                         withIoReactivexInternalDisposablesSequentialDisposable:(IoReactivexInternalDisposablesSequentialDisposable *)mar
                                                                           withJavaLangRunnable:(id<JavaLangRunnable>)decoratedRun {
  IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose_initWithIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_withIoReactivexInternalDisposablesSequentialDisposable_withJavaLangRunnable_(self, outer$, mar, decoratedRun);
  return self;
}

- (void)run {
  [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(mar_)) replaceWithIoReactivexDisposablesDisposable:[this$0_ scheduleWithJavaLangRunnable:decoratedRun_]];
}

- (void)dealloc {
  RELEASE_(this$0_);
  RELEASE_(mar_);
  RELEASE_(decoratedRun_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker:withIoReactivexInternalDisposablesSequentialDisposable:withJavaLangRunnable:);
  methods[1].selector = @selector(run);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "this$0_", "LIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker;", .constantValue.asLong = 0, 0x1012, -1, -1, -1, -1 },
    { "mar_", "LIoReactivexInternalDisposablesSequentialDisposable;", .constantValue.asLong = 0, 0x12, -1, -1, -1, -1 },
    { "decoratedRun_", "LJavaLangRunnable;", .constantValue.asLong = 0, 0x12, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexInternalDisposablesSequentialDisposable;LJavaLangRunnable;", "LIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker;" };
  static const J2ObjcClassInfo _IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose = { "SequentialDispose", "io.reactivex.internal.schedulers", ptrTable, methods, fields, 7, 0x10, 2, 3, 1, -1, -1, -1, -1 };
  return &_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose;
}

@end

void IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose_initWithIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_withIoReactivexInternalDisposablesSequentialDisposable_withJavaLangRunnable_(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose *self, IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker *outer$, IoReactivexInternalDisposablesSequentialDisposable *mar, id<JavaLangRunnable> decoratedRun) {
  JreStrongAssign(&self->this$0_, outer$);
  NSObject_init(self);
  JreStrongAssign(&self->mar_, mar);
  JreStrongAssign(&self->decoratedRun_, decoratedRun);
}

IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose *new_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose_initWithIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_withIoReactivexInternalDisposablesSequentialDisposable_withJavaLangRunnable_(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker *outer$, IoReactivexInternalDisposablesSequentialDisposable *mar, id<JavaLangRunnable> decoratedRun) {
  J2OBJC_NEW_IMPL(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose, initWithIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_withIoReactivexInternalDisposablesSequentialDisposable_withJavaLangRunnable_, outer$, mar, decoratedRun)
}

IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose *create_IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose_initWithIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_withIoReactivexInternalDisposablesSequentialDisposable_withJavaLangRunnable_(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker *outer$, IoReactivexInternalDisposablesSequentialDisposable *mar, id<JavaLangRunnable> decoratedRun) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose, initWithIoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_withIoReactivexInternalDisposablesSequentialDisposable_withJavaLangRunnable_, outer$, mar, decoratedRun)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSchedulersExecutorScheduler_ExecutorWorker_SequentialDispose)

@implementation IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable

- (instancetype __nonnull)initWithJavaLangRunnable:(id<JavaLangRunnable>)run {
  IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_initWithJavaLangRunnable_(self, run);
  return self;
}

- (void)run {
  id<JavaLangRunnable> r = [self get];
  if (r != nil) {
    @try {
      [r run];
    }
    @finally {
      [self lazySetWithId:nil];
      [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(timed_)) lazySetWithId:JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED)];
      [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(direct_)) lazySetWithId:JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED)];
    }
  }
}

- (jboolean)isDisposed {
  return [self get] == nil;
}

- (void)dispose {
  if ([self getAndSetWithId:nil] != nil) {
    [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(timed_)) dispose];
    [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(direct_)) dispose];
  }
}

- (id<JavaLangRunnable>)getWrappedRunnable {
  id<JavaLangRunnable> r = [self get];
  return r != nil ? r : JreLoadStatic(IoReactivexInternalFunctionsFunctions, EMPTY_RUNNABLE);
}

- (void)dealloc {
  RELEASE_(timed_);
  RELEASE_(direct_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaLangRunnable;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaLangRunnable:);
  methods[1].selector = @selector(run);
  methods[2].selector = @selector(isDisposed);
  methods[3].selector = @selector(dispose);
  methods[4].selector = @selector(getWrappedRunnable);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "timed_", "LIoReactivexInternalDisposablesSequentialDisposable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "direct_", "LIoReactivexInternalDisposablesSequentialDisposable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LJavaLangRunnable;", "LIoReactivexInternalSchedulersExecutorScheduler;", "Ljava/util/concurrent/atomic/AtomicReference<Ljava/lang/Runnable;>;Ljava/lang/Runnable;Lio/reactivex/disposables/Disposable;Lio/reactivex/schedulers/SchedulerRunnableIntrospection;" };
  static const J2ObjcClassInfo _IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable = { "DelayedRunnable", "io.reactivex.internal.schedulers", ptrTable, methods, fields, 7, 0x18, 5, 3, 1, -1, -1, 2, -1 };
  return &_IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable;
}

@end

void IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_initWithJavaLangRunnable_(IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *self, id<JavaLangRunnable> run) {
  JavaUtilConcurrentAtomicAtomicReference_initWithId_(self, run);
  JreStrongAssignAndConsume(&self->timed_, new_IoReactivexInternalDisposablesSequentialDisposable_init());
  JreStrongAssignAndConsume(&self->direct_, new_IoReactivexInternalDisposablesSequentialDisposable_init());
}

IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *new_IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_initWithJavaLangRunnable_(id<JavaLangRunnable> run) {
  J2OBJC_NEW_IMPL(IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable, initWithJavaLangRunnable_, run)
}

IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *create_IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_initWithJavaLangRunnable_(id<JavaLangRunnable> run) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable, initWithJavaLangRunnable_, run)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable)

@implementation IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose

- (instancetype __nonnull)initWithIoReactivexInternalSchedulersExecutorScheduler:(IoReactivexInternalSchedulersExecutorScheduler *)outer$
              withIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable:(IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *)dr {
  IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose_initWithIoReactivexInternalSchedulersExecutorScheduler_withIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_(self, outer$, dr);
  return self;
}

- (void)run {
  [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(((IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *) nil_chk(dr_))->direct_)) replaceWithIoReactivexDisposablesDisposable:[this$0_ scheduleDirectWithJavaLangRunnable:dr_]];
}

- (void)dealloc {
  RELEASE_(this$0_);
  RELEASE_(dr_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalSchedulersExecutorScheduler:withIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable:);
  methods[1].selector = @selector(run);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "this$0_", "LIoReactivexInternalSchedulersExecutorScheduler;", .constantValue.asLong = 0, 0x1012, -1, -1, -1, -1 },
    { "dr_", "LIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable;", .constantValue.asLong = 0, 0x12, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable;", "LIoReactivexInternalSchedulersExecutorScheduler;" };
  static const J2ObjcClassInfo _IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose = { "DelayedDispose", "io.reactivex.internal.schedulers", ptrTable, methods, fields, 7, 0x10, 2, 2, 1, -1, -1, -1, -1 };
  return &_IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose;
}

@end

void IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose_initWithIoReactivexInternalSchedulersExecutorScheduler_withIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_(IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose *self, IoReactivexInternalSchedulersExecutorScheduler *outer$, IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *dr) {
  JreStrongAssign(&self->this$0_, outer$);
  NSObject_init(self);
  JreStrongAssign(&self->dr_, dr);
}

IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose *new_IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose_initWithIoReactivexInternalSchedulersExecutorScheduler_withIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_(IoReactivexInternalSchedulersExecutorScheduler *outer$, IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *dr) {
  J2OBJC_NEW_IMPL(IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose, initWithIoReactivexInternalSchedulersExecutorScheduler_withIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_, outer$, dr)
}

IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose *create_IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose_initWithIoReactivexInternalSchedulersExecutorScheduler_withIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_(IoReactivexInternalSchedulersExecutorScheduler *outer$, IoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable *dr) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose, initWithIoReactivexInternalSchedulersExecutorScheduler_withIoReactivexInternalSchedulersExecutorScheduler_DelayedRunnable_, outer$, dr)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSchedulersExecutorScheduler_DelayedDispose)
