//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/schedulers/InstantPeriodicTask.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalSchedulersInstantPeriodicTask")
#ifdef RESTRICT_IoReactivexInternalSchedulersInstantPeriodicTask
#define INCLUDE_ALL_IoReactivexInternalSchedulersInstantPeriodicTask 0
#else
#define INCLUDE_ALL_IoReactivexInternalSchedulersInstantPeriodicTask 1
#endif
#undef RESTRICT_IoReactivexInternalSchedulersInstantPeriodicTask

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalSchedulersInstantPeriodicTask_) && (INCLUDE_ALL_IoReactivexInternalSchedulersInstantPeriodicTask || defined(INCLUDE_IoReactivexInternalSchedulersInstantPeriodicTask))
#define IoReactivexInternalSchedulersInstantPeriodicTask_

#define RESTRICT_JavaUtilConcurrentCallable 1
#define INCLUDE_JavaUtilConcurrentCallable 1
#include "java/util/concurrent/Callable.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class JavaLangThread;
@class JavaLangVoid;
@class JavaUtilConcurrentAtomicAtomicReference;
@class JavaUtilConcurrentFutureTask;
@protocol JavaLangRunnable;
@protocol JavaUtilConcurrentExecutorService;
@protocol JavaUtilConcurrentFuture;

@interface IoReactivexInternalSchedulersInstantPeriodicTask : NSObject < JavaUtilConcurrentCallable, IoReactivexDisposablesDisposable > {
 @public
  id<JavaLangRunnable> task_;
  JavaUtilConcurrentAtomicAtomicReference *rest_;
  JavaUtilConcurrentAtomicAtomicReference *first_;
  id<JavaUtilConcurrentExecutorService> executor_;
  JavaLangThread *runner_;
}

+ (JavaUtilConcurrentFutureTask *)CANCELLED;

#pragma mark Public

- (JavaLangVoid *)call;

- (void)dispose;

- (jboolean)isDisposed;

#pragma mark Package-Private

- (instancetype __nonnull)initWithJavaLangRunnable:(id<JavaLangRunnable>)task
             withJavaUtilConcurrentExecutorService:(id<JavaUtilConcurrentExecutorService>)executor;

- (void)setFirstWithJavaUtilConcurrentFuture:(id<JavaUtilConcurrentFuture>)f;

- (void)setRestWithJavaUtilConcurrentFuture:(id<JavaUtilConcurrentFuture>)f;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(IoReactivexInternalSchedulersInstantPeriodicTask)

J2OBJC_FIELD_SETTER(IoReactivexInternalSchedulersInstantPeriodicTask, task_, id<JavaLangRunnable>)
J2OBJC_FIELD_SETTER(IoReactivexInternalSchedulersInstantPeriodicTask, rest_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexInternalSchedulersInstantPeriodicTask, first_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexInternalSchedulersInstantPeriodicTask, executor_, id<JavaUtilConcurrentExecutorService>)
J2OBJC_FIELD_SETTER(IoReactivexInternalSchedulersInstantPeriodicTask, runner_, JavaLangThread *)

inline JavaUtilConcurrentFutureTask *IoReactivexInternalSchedulersInstantPeriodicTask_get_CANCELLED(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT JavaUtilConcurrentFutureTask *IoReactivexInternalSchedulersInstantPeriodicTask_CANCELLED;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalSchedulersInstantPeriodicTask, CANCELLED, JavaUtilConcurrentFutureTask *)

FOUNDATION_EXPORT void IoReactivexInternalSchedulersInstantPeriodicTask_initWithJavaLangRunnable_withJavaUtilConcurrentExecutorService_(IoReactivexInternalSchedulersInstantPeriodicTask *self, id<JavaLangRunnable> task, id<JavaUtilConcurrentExecutorService> executor);

FOUNDATION_EXPORT IoReactivexInternalSchedulersInstantPeriodicTask *new_IoReactivexInternalSchedulersInstantPeriodicTask_initWithJavaLangRunnable_withJavaUtilConcurrentExecutorService_(id<JavaLangRunnable> task, id<JavaUtilConcurrentExecutorService> executor) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalSchedulersInstantPeriodicTask *create_IoReactivexInternalSchedulersInstantPeriodicTask_initWithJavaLangRunnable_withJavaUtilConcurrentExecutorService_(id<JavaLangRunnable> task, id<JavaUtilConcurrentExecutorService> executor);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalSchedulersInstantPeriodicTask)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalSchedulersInstantPeriodicTask")
