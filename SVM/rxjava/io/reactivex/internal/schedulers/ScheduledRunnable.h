//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/schedulers/ScheduledRunnable.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalSchedulersScheduledRunnable")
#ifdef RESTRICT_IoReactivexInternalSchedulersScheduledRunnable
#define INCLUDE_ALL_IoReactivexInternalSchedulersScheduledRunnable 0
#else
#define INCLUDE_ALL_IoReactivexInternalSchedulersScheduledRunnable 1
#endif
#undef RESTRICT_IoReactivexInternalSchedulersScheduledRunnable

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalSchedulersScheduledRunnable_) && (INCLUDE_ALL_IoReactivexInternalSchedulersScheduledRunnable || defined(INCLUDE_IoReactivexInternalSchedulersScheduledRunnable))
#define IoReactivexInternalSchedulersScheduledRunnable_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReferenceArray 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReferenceArray 1
#include "java/util/concurrent/atomic/AtomicReferenceArray.h"

#define RESTRICT_JavaLangRunnable 1
#define INCLUDE_JavaLangRunnable 1
#include "java/lang/Runnable.h"

#define RESTRICT_JavaUtilConcurrentCallable 1
#define INCLUDE_JavaUtilConcurrentCallable 1
#include "java/util/concurrent/Callable.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class IOSObjectArray;
@protocol IoReactivexInternalDisposablesDisposableContainer;
@protocol JavaUtilConcurrentFuture;

@interface IoReactivexInternalSchedulersScheduledRunnable : JavaUtilConcurrentAtomicAtomicReferenceArray < JavaLangRunnable, JavaUtilConcurrentCallable, IoReactivexDisposablesDisposable > {
 @public
  id<JavaLangRunnable> actual_;
}

+ (id)PARENT_DISPOSED;

+ (id)SYNC_DISPOSED;

+ (id)ASYNC_DISPOSED;

+ (id)DONE;

+ (jint)PARENT_INDEX;

+ (jint)FUTURE_INDEX;

+ (jint)THREAD_INDEX;

#pragma mark Public

- (instancetype __nonnull)initWithJavaLangRunnable:(id<JavaLangRunnable>)actual
withIoReactivexInternalDisposablesDisposableContainer:(id<IoReactivexInternalDisposablesDisposableContainer>)parent;

- (id)call;

- (void)dispose;

- (jboolean)isDisposed;

- (void)run;

- (void)setFutureWithJavaUtilConcurrentFuture:(id<JavaUtilConcurrentFuture>)f;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

- (instancetype __nonnull)initWithNSObjectArray:(IOSObjectArray *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(IoReactivexInternalSchedulersScheduledRunnable)

J2OBJC_FIELD_SETTER(IoReactivexInternalSchedulersScheduledRunnable, actual_, id<JavaLangRunnable>)

inline id IoReactivexInternalSchedulersScheduledRunnable_get_PARENT_DISPOSED(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT id IoReactivexInternalSchedulersScheduledRunnable_PARENT_DISPOSED;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalSchedulersScheduledRunnable, PARENT_DISPOSED, id)

inline id IoReactivexInternalSchedulersScheduledRunnable_get_SYNC_DISPOSED(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT id IoReactivexInternalSchedulersScheduledRunnable_SYNC_DISPOSED;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalSchedulersScheduledRunnable, SYNC_DISPOSED, id)

inline id IoReactivexInternalSchedulersScheduledRunnable_get_ASYNC_DISPOSED(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT id IoReactivexInternalSchedulersScheduledRunnable_ASYNC_DISPOSED;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalSchedulersScheduledRunnable, ASYNC_DISPOSED, id)

inline id IoReactivexInternalSchedulersScheduledRunnable_get_DONE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT id IoReactivexInternalSchedulersScheduledRunnable_DONE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalSchedulersScheduledRunnable, DONE, id)

inline jint IoReactivexInternalSchedulersScheduledRunnable_get_PARENT_INDEX(void);
#define IoReactivexInternalSchedulersScheduledRunnable_PARENT_INDEX 0
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalSchedulersScheduledRunnable, PARENT_INDEX, jint)

inline jint IoReactivexInternalSchedulersScheduledRunnable_get_FUTURE_INDEX(void);
#define IoReactivexInternalSchedulersScheduledRunnable_FUTURE_INDEX 1
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalSchedulersScheduledRunnable, FUTURE_INDEX, jint)

inline jint IoReactivexInternalSchedulersScheduledRunnable_get_THREAD_INDEX(void);
#define IoReactivexInternalSchedulersScheduledRunnable_THREAD_INDEX 2
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalSchedulersScheduledRunnable, THREAD_INDEX, jint)

FOUNDATION_EXPORT void IoReactivexInternalSchedulersScheduledRunnable_initWithJavaLangRunnable_withIoReactivexInternalDisposablesDisposableContainer_(IoReactivexInternalSchedulersScheduledRunnable *self, id<JavaLangRunnable> actual, id<IoReactivexInternalDisposablesDisposableContainer> parent);

FOUNDATION_EXPORT IoReactivexInternalSchedulersScheduledRunnable *new_IoReactivexInternalSchedulersScheduledRunnable_initWithJavaLangRunnable_withIoReactivexInternalDisposablesDisposableContainer_(id<JavaLangRunnable> actual, id<IoReactivexInternalDisposablesDisposableContainer> parent) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalSchedulersScheduledRunnable *create_IoReactivexInternalSchedulersScheduledRunnable_initWithJavaLangRunnable_withIoReactivexInternalDisposablesDisposableContainer_(id<JavaLangRunnable> actual, id<IoReactivexInternalDisposablesDisposableContainer> parent);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalSchedulersScheduledRunnable)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalSchedulersScheduledRunnable")
