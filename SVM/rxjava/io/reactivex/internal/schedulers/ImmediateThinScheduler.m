//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/schedulers/ImmediateThinScheduler.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Scheduler.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/disposables/Disposables.h"
#include "io/reactivex/internal/schedulers/ImmediateThinScheduler.h"
#include "java/lang/Runnable.h"
#include "java/lang/UnsupportedOperationException.h"
#include "java/util/concurrent/TimeUnit.h"

@interface IoReactivexInternalSchedulersImmediateThinScheduler ()

- (instancetype __nonnull)init;

@end

__attribute__((unused)) static void IoReactivexInternalSchedulersImmediateThinScheduler_init(IoReactivexInternalSchedulersImmediateThinScheduler *self);

__attribute__((unused)) static IoReactivexInternalSchedulersImmediateThinScheduler *new_IoReactivexInternalSchedulersImmediateThinScheduler_init(void) NS_RETURNS_RETAINED;

__attribute__((unused)) static IoReactivexInternalSchedulersImmediateThinScheduler *create_IoReactivexInternalSchedulersImmediateThinScheduler_init(void);

J2OBJC_INITIALIZED_DEFN(IoReactivexInternalSchedulersImmediateThinScheduler)

IoReactivexScheduler *IoReactivexInternalSchedulersImmediateThinScheduler_INSTANCE;
IoReactivexScheduler_Worker *IoReactivexInternalSchedulersImmediateThinScheduler_WORKER;
id<IoReactivexDisposablesDisposable> IoReactivexInternalSchedulersImmediateThinScheduler_DISPOSED;

@implementation IoReactivexInternalSchedulersImmediateThinScheduler

+ (IoReactivexScheduler *)INSTANCE {
  return IoReactivexInternalSchedulersImmediateThinScheduler_INSTANCE;
}

+ (IoReactivexScheduler_Worker *)WORKER {
  return IoReactivexInternalSchedulersImmediateThinScheduler_WORKER;
}

+ (id<IoReactivexDisposablesDisposable>)DISPOSED {
  return IoReactivexInternalSchedulersImmediateThinScheduler_DISPOSED;
}

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  IoReactivexInternalSchedulersImmediateThinScheduler_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (id<IoReactivexDisposablesDisposable> __nonnull)scheduleDirectWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run {
  [((id<JavaLangRunnable>) nil_chk(run)) run];
  return IoReactivexInternalSchedulersImmediateThinScheduler_DISPOSED;
}

- (id<IoReactivexDisposablesDisposable> __nonnull)scheduleDirectWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run
                                                                            withLong:(jlong)delay
                                                      withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit {
  @throw create_JavaLangUnsupportedOperationException_initWithNSString_(@"This scheduler doesn't support delayed execution");
}

- (id<IoReactivexDisposablesDisposable> __nonnull)schedulePeriodicallyDirectWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run
                                                                                        withLong:(jlong)initialDelay
                                                                                        withLong:(jlong)period
                                                                  withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit {
  @throw create_JavaLangUnsupportedOperationException_initWithNSString_(@"This scheduler doesn't support periodic execution");
}

- (IoReactivexScheduler_Worker * __nonnull)createWorker {
  return IoReactivexInternalSchedulersImmediateThinScheduler_WORKER;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x2, -1, -1, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 0, 2, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "LIoReactivexScheduler_Worker;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(scheduleDirectWithJavaLangRunnable:);
  methods[2].selector = @selector(scheduleDirectWithJavaLangRunnable:withLong:withJavaUtilConcurrentTimeUnit:);
  methods[3].selector = @selector(schedulePeriodicallyDirectWithJavaLangRunnable:withLong:withLong:withJavaUtilConcurrentTimeUnit:);
  methods[4].selector = @selector(createWorker);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "INSTANCE", "LIoReactivexScheduler;", .constantValue.asLong = 0, 0x19, -1, 5, -1, -1 },
    { "WORKER", "LIoReactivexScheduler_Worker;", .constantValue.asLong = 0, 0x18, -1, 6, -1, -1 },
    { "DISPOSED", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x18, -1, 7, -1, -1 },
  };
  static const void *ptrTable[] = { "scheduleDirect", "LJavaLangRunnable;", "LJavaLangRunnable;JLJavaUtilConcurrentTimeUnit;", "schedulePeriodicallyDirect", "LJavaLangRunnable;JJLJavaUtilConcurrentTimeUnit;", &IoReactivexInternalSchedulersImmediateThinScheduler_INSTANCE, &IoReactivexInternalSchedulersImmediateThinScheduler_WORKER, &IoReactivexInternalSchedulersImmediateThinScheduler_DISPOSED, "LIoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker;" };
  static const J2ObjcClassInfo _IoReactivexInternalSchedulersImmediateThinScheduler = { "ImmediateThinScheduler", "io.reactivex.internal.schedulers", ptrTable, methods, fields, 7, 0x11, 5, 3, -1, 8, -1, -1, -1 };
  return &_IoReactivexInternalSchedulersImmediateThinScheduler;
}

+ (void)initialize {
  if (self == [IoReactivexInternalSchedulersImmediateThinScheduler class]) {
    JreStrongAssignAndConsume(&IoReactivexInternalSchedulersImmediateThinScheduler_INSTANCE, new_IoReactivexInternalSchedulersImmediateThinScheduler_init());
    JreStrongAssignAndConsume(&IoReactivexInternalSchedulersImmediateThinScheduler_WORKER, new_IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker_init());
    {
      JreStrongAssign(&IoReactivexInternalSchedulersImmediateThinScheduler_DISPOSED, IoReactivexDisposablesDisposables_empty());
      [((id<IoReactivexDisposablesDisposable>) nil_chk(IoReactivexInternalSchedulersImmediateThinScheduler_DISPOSED)) dispose];
    }
    J2OBJC_SET_INITIALIZED(IoReactivexInternalSchedulersImmediateThinScheduler)
  }
}

@end

void IoReactivexInternalSchedulersImmediateThinScheduler_init(IoReactivexInternalSchedulersImmediateThinScheduler *self) {
  IoReactivexScheduler_init(self);
}

IoReactivexInternalSchedulersImmediateThinScheduler *new_IoReactivexInternalSchedulersImmediateThinScheduler_init() {
  J2OBJC_NEW_IMPL(IoReactivexInternalSchedulersImmediateThinScheduler, init)
}

IoReactivexInternalSchedulersImmediateThinScheduler *create_IoReactivexInternalSchedulersImmediateThinScheduler_init() {
  J2OBJC_CREATE_IMPL(IoReactivexInternalSchedulersImmediateThinScheduler, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSchedulersImmediateThinScheduler)

@implementation IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)dispose {
}

- (jboolean)isDisposed {
  return false;
}

- (id<IoReactivexDisposablesDisposable> __nonnull)scheduleWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run {
  [((id<JavaLangRunnable>) nil_chk(run)) run];
  return JreLoadStatic(IoReactivexInternalSchedulersImmediateThinScheduler, DISPOSED);
}

- (id<IoReactivexDisposablesDisposable> __nonnull)scheduleWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run
                                                                      withLong:(jlong)delay
                                                withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit * __nonnull)unit {
  @throw create_JavaLangUnsupportedOperationException_initWithNSString_(@"This scheduler doesn't support delayed execution");
}

- (id<IoReactivexDisposablesDisposable> __nonnull)schedulePeriodicallyWithJavaLangRunnable:(id<JavaLangRunnable> __nonnull)run
                                                                                  withLong:(jlong)initialDelay
                                                                                  withLong:(jlong)period
                                                            withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit {
  @throw create_JavaLangUnsupportedOperationException_initWithNSString_(@"This scheduler doesn't support periodic execution");
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 0, 2, -1, -1, -1, -1 },
    { NULL, "LIoReactivexDisposablesDisposable;", 0x1, 3, 4, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(dispose);
  methods[2].selector = @selector(isDisposed);
  methods[3].selector = @selector(scheduleWithJavaLangRunnable:);
  methods[4].selector = @selector(scheduleWithJavaLangRunnable:withLong:withJavaUtilConcurrentTimeUnit:);
  methods[5].selector = @selector(schedulePeriodicallyWithJavaLangRunnable:withLong:withLong:withJavaUtilConcurrentTimeUnit:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "schedule", "LJavaLangRunnable;", "LJavaLangRunnable;JLJavaUtilConcurrentTimeUnit;", "schedulePeriodically", "LJavaLangRunnable;JJLJavaUtilConcurrentTimeUnit;", "LIoReactivexInternalSchedulersImmediateThinScheduler;" };
  static const J2ObjcClassInfo _IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker = { "ImmediateThinWorker", "io.reactivex.internal.schedulers", ptrTable, methods, NULL, 7, 0x18, 6, 0, 5, -1, -1, -1, -1 };
  return &_IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker;
}

@end

void IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker_init(IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker *self) {
  IoReactivexScheduler_Worker_init(self);
}

IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker *new_IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker_init() {
  J2OBJC_NEW_IMPL(IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker, init)
}

IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker *create_IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker_init() {
  J2OBJC_CREATE_IMPL(IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSchedulersImmediateThinScheduler_ImmediateThinWorker)
