//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/disposables/CancellableDisposable.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Cancellable.h"
#include "io/reactivex/internal/disposables/CancellableDisposable.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Exception.h"
#include "java/util/concurrent/atomic/AtomicReference.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalDisposablesCancellableDisposable_get_serialVersionUID(void);
#define IoReactivexInternalDisposablesCancellableDisposable_serialVersionUID 5718521705281392066LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalDisposablesCancellableDisposable, serialVersionUID, jlong)

@implementation IoReactivexInternalDisposablesCancellableDisposable

- (instancetype __nonnull)initWithIoReactivexFunctionsCancellable:(id<IoReactivexFunctionsCancellable>)cancellable {
  IoReactivexInternalDisposablesCancellableDisposable_initWithIoReactivexFunctionsCancellable_(self, cancellable);
  return self;
}

- (jboolean)isDisposed {
  return [self get] == nil;
}

- (void)dispose {
  if ([self get] != nil) {
    id<IoReactivexFunctionsCancellable> c = [self getAndSetWithId:nil];
    if (c != nil) {
      @try {
        [c cancel];
      }
      @catch (JavaLangException *ex) {
        IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
        IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
      }
    }
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFunctionsCancellable:);
  methods[1].selector = @selector(isDisposed);
  methods[2].selector = @selector(dispose);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalDisposablesCancellableDisposable_serialVersionUID, 0x1a, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexFunctionsCancellable;", "Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/functions/Cancellable;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalDisposablesCancellableDisposable = { "CancellableDisposable", "io.reactivex.internal.disposables", ptrTable, methods, fields, 7, 0x11, 3, 1, -1, -1, -1, 1, -1 };
  return &_IoReactivexInternalDisposablesCancellableDisposable;
}

@end

void IoReactivexInternalDisposablesCancellableDisposable_initWithIoReactivexFunctionsCancellable_(IoReactivexInternalDisposablesCancellableDisposable *self, id<IoReactivexFunctionsCancellable> cancellable) {
  JavaUtilConcurrentAtomicAtomicReference_initWithId_(self, cancellable);
}

IoReactivexInternalDisposablesCancellableDisposable *new_IoReactivexInternalDisposablesCancellableDisposable_initWithIoReactivexFunctionsCancellable_(id<IoReactivexFunctionsCancellable> cancellable) {
  J2OBJC_NEW_IMPL(IoReactivexInternalDisposablesCancellableDisposable, initWithIoReactivexFunctionsCancellable_, cancellable)
}

IoReactivexInternalDisposablesCancellableDisposable *create_IoReactivexInternalDisposablesCancellableDisposable_initWithIoReactivexFunctionsCancellable_(id<IoReactivexFunctionsCancellable> cancellable) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalDisposablesCancellableDisposable, initWithIoReactivexFunctionsCancellable_, cancellable)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalDisposablesCancellableDisposable)
