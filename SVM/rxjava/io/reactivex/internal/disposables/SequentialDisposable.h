//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/disposables/SequentialDisposable.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalDisposablesSequentialDisposable")
#ifdef RESTRICT_IoReactivexInternalDisposablesSequentialDisposable
#define INCLUDE_ALL_IoReactivexInternalDisposablesSequentialDisposable 0
#else
#define INCLUDE_ALL_IoReactivexInternalDisposablesSequentialDisposable 1
#endif
#undef RESTRICT_IoReactivexInternalDisposablesSequentialDisposable

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalDisposablesSequentialDisposable_) && (INCLUDE_ALL_IoReactivexInternalDisposablesSequentialDisposable || defined(INCLUDE_IoReactivexInternalDisposablesSequentialDisposable))
#define IoReactivexInternalDisposablesSequentialDisposable_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;

@interface IoReactivexInternalDisposablesSequentialDisposable : JavaUtilConcurrentAtomicAtomicReference < IoReactivexDisposablesDisposable >

#pragma mark Public

- (instancetype __nonnull)init;

- (instancetype __nonnull)initWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)initial;

- (id<IoReactivexDisposablesDisposable>)accumulateAndGetWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)dispose;

- (id<IoReactivexDisposablesDisposable>)get;

- (id<IoReactivexDisposablesDisposable>)getAndAccumulateWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)getAndSetWithId:(id<IoReactivexDisposablesDisposable>)arg0;

- (id<IoReactivexDisposablesDisposable>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (jboolean)isDisposed;

- (jboolean)replaceWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)next;

- (jboolean)updateWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)next;

- (id<IoReactivexDisposablesDisposable>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalDisposablesSequentialDisposable)

FOUNDATION_EXPORT void IoReactivexInternalDisposablesSequentialDisposable_init(IoReactivexInternalDisposablesSequentialDisposable *self);

FOUNDATION_EXPORT IoReactivexInternalDisposablesSequentialDisposable *new_IoReactivexInternalDisposablesSequentialDisposable_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalDisposablesSequentialDisposable *create_IoReactivexInternalDisposablesSequentialDisposable_init(void);

FOUNDATION_EXPORT void IoReactivexInternalDisposablesSequentialDisposable_initWithIoReactivexDisposablesDisposable_(IoReactivexInternalDisposablesSequentialDisposable *self, id<IoReactivexDisposablesDisposable> initial);

FOUNDATION_EXPORT IoReactivexInternalDisposablesSequentialDisposable *new_IoReactivexInternalDisposablesSequentialDisposable_initWithIoReactivexDisposablesDisposable_(id<IoReactivexDisposablesDisposable> initial) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalDisposablesSequentialDisposable *create_IoReactivexInternalDisposablesSequentialDisposable_initWithIoReactivexDisposablesDisposable_(id<IoReactivexDisposablesDisposable> initial);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalDisposablesSequentialDisposable)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalDisposablesSequentialDisposable")
