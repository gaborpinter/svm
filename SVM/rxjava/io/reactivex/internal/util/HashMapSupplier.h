//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/util/HashMapSupplier.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalUtilHashMapSupplier")
#ifdef RESTRICT_IoReactivexInternalUtilHashMapSupplier
#define INCLUDE_ALL_IoReactivexInternalUtilHashMapSupplier 0
#else
#define INCLUDE_ALL_IoReactivexInternalUtilHashMapSupplier 1
#endif
#undef RESTRICT_IoReactivexInternalUtilHashMapSupplier

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalUtilHashMapSupplier_) && (INCLUDE_ALL_IoReactivexInternalUtilHashMapSupplier || defined(INCLUDE_IoReactivexInternalUtilHashMapSupplier))
#define IoReactivexInternalUtilHashMapSupplier_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

#define RESTRICT_JavaUtilConcurrentCallable 1
#define INCLUDE_JavaUtilConcurrentCallable 1
#include "java/util/concurrent/Callable.h"

@class IOSObjectArray;
@protocol JavaUtilMap;

typedef NS_ENUM(NSUInteger, IoReactivexInternalUtilHashMapSupplier_Enum) {
  IoReactivexInternalUtilHashMapSupplier_Enum_INSTANCE = 0,
};

@interface IoReactivexInternalUtilHashMapSupplier : JavaLangEnum < JavaUtilConcurrentCallable >

+ (IoReactivexInternalUtilHashMapSupplier * __nonnull)INSTANCE;

#pragma mark Public

+ (id<JavaUtilConcurrentCallable>)asCallable;

- (id<JavaUtilMap>)call;

+ (IoReactivexInternalUtilHashMapSupplier *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (IoReactivexInternalUtilHashMapSupplier_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(IoReactivexInternalUtilHashMapSupplier)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT IoReactivexInternalUtilHashMapSupplier *IoReactivexInternalUtilHashMapSupplier_values_[];

inline IoReactivexInternalUtilHashMapSupplier *IoReactivexInternalUtilHashMapSupplier_get_INSTANCE(void);
J2OBJC_ENUM_CONSTANT(IoReactivexInternalUtilHashMapSupplier, INSTANCE)

FOUNDATION_EXPORT id<JavaUtilConcurrentCallable> IoReactivexInternalUtilHashMapSupplier_asCallable(void);

FOUNDATION_EXPORT IOSObjectArray *IoReactivexInternalUtilHashMapSupplier_values(void);

FOUNDATION_EXPORT IoReactivexInternalUtilHashMapSupplier *IoReactivexInternalUtilHashMapSupplier_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT IoReactivexInternalUtilHashMapSupplier *IoReactivexInternalUtilHashMapSupplier_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalUtilHashMapSupplier)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalUtilHashMapSupplier")
