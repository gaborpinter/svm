//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/util/MergerBiFunction.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalUtilMergerBiFunction")
#ifdef RESTRICT_IoReactivexInternalUtilMergerBiFunction
#define INCLUDE_ALL_IoReactivexInternalUtilMergerBiFunction 0
#else
#define INCLUDE_ALL_IoReactivexInternalUtilMergerBiFunction 1
#endif
#undef RESTRICT_IoReactivexInternalUtilMergerBiFunction

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalUtilMergerBiFunction_) && (INCLUDE_ALL_IoReactivexInternalUtilMergerBiFunction || defined(INCLUDE_IoReactivexInternalUtilMergerBiFunction))
#define IoReactivexInternalUtilMergerBiFunction_

#define RESTRICT_IoReactivexFunctionsBiFunction 1
#define INCLUDE_IoReactivexFunctionsBiFunction 1
#include "io/reactivex/functions/BiFunction.h"

@protocol JavaUtilComparator;
@protocol JavaUtilList;

@interface IoReactivexInternalUtilMergerBiFunction : NSObject < IoReactivexFunctionsBiFunction > {
 @public
  id<JavaUtilComparator> comparator_;
}

#pragma mark Public

- (instancetype __nonnull)initWithJavaUtilComparator:(id<JavaUtilComparator>)comparator;

- (id<JavaUtilList>)applyWithId:(id<JavaUtilList>)a
                         withId:(id<JavaUtilList>)b;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalUtilMergerBiFunction)

J2OBJC_FIELD_SETTER(IoReactivexInternalUtilMergerBiFunction, comparator_, id<JavaUtilComparator>)

FOUNDATION_EXPORT void IoReactivexInternalUtilMergerBiFunction_initWithJavaUtilComparator_(IoReactivexInternalUtilMergerBiFunction *self, id<JavaUtilComparator> comparator);

FOUNDATION_EXPORT IoReactivexInternalUtilMergerBiFunction *new_IoReactivexInternalUtilMergerBiFunction_initWithJavaUtilComparator_(id<JavaUtilComparator> comparator) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalUtilMergerBiFunction *create_IoReactivexInternalUtilMergerBiFunction_initWithJavaUtilComparator_(id<JavaUtilComparator> comparator);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalUtilMergerBiFunction)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalUtilMergerBiFunction")
