//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/util/ListAddBiConsumer.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalUtilListAddBiConsumer")
#ifdef RESTRICT_IoReactivexInternalUtilListAddBiConsumer
#define INCLUDE_ALL_IoReactivexInternalUtilListAddBiConsumer 0
#else
#define INCLUDE_ALL_IoReactivexInternalUtilListAddBiConsumer 1
#endif
#undef RESTRICT_IoReactivexInternalUtilListAddBiConsumer

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalUtilListAddBiConsumer_) && (INCLUDE_ALL_IoReactivexInternalUtilListAddBiConsumer || defined(INCLUDE_IoReactivexInternalUtilListAddBiConsumer))
#define IoReactivexInternalUtilListAddBiConsumer_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

#define RESTRICT_IoReactivexFunctionsBiFunction 1
#define INCLUDE_IoReactivexFunctionsBiFunction 1
#include "io/reactivex/functions/BiFunction.h"

@class IOSObjectArray;
@protocol JavaUtilList;

typedef NS_ENUM(NSUInteger, IoReactivexInternalUtilListAddBiConsumer_Enum) {
  IoReactivexInternalUtilListAddBiConsumer_Enum_INSTANCE = 0,
};

@interface IoReactivexInternalUtilListAddBiConsumer : JavaLangEnum < IoReactivexFunctionsBiFunction >

+ (IoReactivexInternalUtilListAddBiConsumer * __nonnull)INSTANCE;

#pragma mark Public

- (id<JavaUtilList>)applyWithId:(id<JavaUtilList>)t1
                         withId:(id)t2;

+ (id<IoReactivexFunctionsBiFunction>)instance;

+ (IoReactivexInternalUtilListAddBiConsumer *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (IoReactivexInternalUtilListAddBiConsumer_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(IoReactivexInternalUtilListAddBiConsumer)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT IoReactivexInternalUtilListAddBiConsumer *IoReactivexInternalUtilListAddBiConsumer_values_[];

inline IoReactivexInternalUtilListAddBiConsumer *IoReactivexInternalUtilListAddBiConsumer_get_INSTANCE(void);
J2OBJC_ENUM_CONSTANT(IoReactivexInternalUtilListAddBiConsumer, INSTANCE)

FOUNDATION_EXPORT id<IoReactivexFunctionsBiFunction> IoReactivexInternalUtilListAddBiConsumer_instance(void);

FOUNDATION_EXPORT IOSObjectArray *IoReactivexInternalUtilListAddBiConsumer_values(void);

FOUNDATION_EXPORT IoReactivexInternalUtilListAddBiConsumer *IoReactivexInternalUtilListAddBiConsumer_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT IoReactivexInternalUtilListAddBiConsumer *IoReactivexInternalUtilListAddBiConsumer_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalUtilListAddBiConsumer)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalUtilListAddBiConsumer")
