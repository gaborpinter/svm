//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/subscriptions/SubscriptionArbiter.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalSubscriptionsSubscriptionArbiter")
#ifdef RESTRICT_IoReactivexInternalSubscriptionsSubscriptionArbiter
#define INCLUDE_ALL_IoReactivexInternalSubscriptionsSubscriptionArbiter 0
#else
#define INCLUDE_ALL_IoReactivexInternalSubscriptionsSubscriptionArbiter 1
#endif
#undef RESTRICT_IoReactivexInternalSubscriptionsSubscriptionArbiter

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalSubscriptionsSubscriptionArbiter_) && (INCLUDE_ALL_IoReactivexInternalSubscriptionsSubscriptionArbiter || defined(INCLUDE_IoReactivexInternalSubscriptionsSubscriptionArbiter))
#define IoReactivexInternalSubscriptionsSubscriptionArbiter_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicInteger 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicInteger 1
#include "java/util/concurrent/atomic/AtomicInteger.h"

#define RESTRICT_OrgReactivestreamsSubscription 1
#define INCLUDE_OrgReactivestreamsSubscription 1
#include "org/reactivestreams/Subscription.h"

@class JavaUtilConcurrentAtomicAtomicLong;
@class JavaUtilConcurrentAtomicAtomicReference;

@interface IoReactivexInternalSubscriptionsSubscriptionArbiter : JavaUtilConcurrentAtomicAtomicInteger < OrgReactivestreamsSubscription > {
 @public
  id<OrgReactivestreamsSubscription> actual_;
  jlong requested_;
  JavaUtilConcurrentAtomicAtomicReference *missedSubscription_;
  JavaUtilConcurrentAtomicAtomicLong *missedRequested_;
  JavaUtilConcurrentAtomicAtomicLong *missedProduced_;
  jboolean cancelOnReplace_;
  volatile_jboolean cancelled_;
  jboolean unbounded_;
}

#pragma mark Public

- (instancetype __nonnull)initWithBoolean:(jboolean)cancelOnReplace;

- (void)cancel;

- (NSUInteger)hash;

- (jboolean)isCancelled;

- (jboolean)isEqual:(id)obj;

- (jboolean)isUnbounded;

- (void)producedWithLong:(jlong)n;

- (void)requestWithLong:(jlong)n;

- (void)setSubscriptionWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

#pragma mark Package-Private

- (void)drain;

- (void)drainLoop;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalSubscriptionsSubscriptionArbiter)

J2OBJC_FIELD_SETTER(IoReactivexInternalSubscriptionsSubscriptionArbiter, actual_, id<OrgReactivestreamsSubscription>)
J2OBJC_FIELD_SETTER(IoReactivexInternalSubscriptionsSubscriptionArbiter, missedSubscription_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexInternalSubscriptionsSubscriptionArbiter, missedRequested_, JavaUtilConcurrentAtomicAtomicLong *)
J2OBJC_FIELD_SETTER(IoReactivexInternalSubscriptionsSubscriptionArbiter, missedProduced_, JavaUtilConcurrentAtomicAtomicLong *)

FOUNDATION_EXPORT void IoReactivexInternalSubscriptionsSubscriptionArbiter_initWithBoolean_(IoReactivexInternalSubscriptionsSubscriptionArbiter *self, jboolean cancelOnReplace);

FOUNDATION_EXPORT IoReactivexInternalSubscriptionsSubscriptionArbiter *new_IoReactivexInternalSubscriptionsSubscriptionArbiter_initWithBoolean_(jboolean cancelOnReplace) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalSubscriptionsSubscriptionArbiter *create_IoReactivexInternalSubscriptionsSubscriptionArbiter_initWithBoolean_(jboolean cancelOnReplace);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalSubscriptionsSubscriptionArbiter)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalSubscriptionsSubscriptionArbiter")
