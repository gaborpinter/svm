//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/subscriptions/BasicQueueSubscription.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalSubscriptionsBasicQueueSubscription")
#ifdef RESTRICT_IoReactivexInternalSubscriptionsBasicQueueSubscription
#define INCLUDE_ALL_IoReactivexInternalSubscriptionsBasicQueueSubscription 0
#else
#define INCLUDE_ALL_IoReactivexInternalSubscriptionsBasicQueueSubscription 1
#endif
#undef RESTRICT_IoReactivexInternalSubscriptionsBasicQueueSubscription

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalSubscriptionsBasicQueueSubscription_) && (INCLUDE_ALL_IoReactivexInternalSubscriptionsBasicQueueSubscription || defined(INCLUDE_IoReactivexInternalSubscriptionsBasicQueueSubscription))
#define IoReactivexInternalSubscriptionsBasicQueueSubscription_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicLong 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicLong 1
#include "java/util/concurrent/atomic/AtomicLong.h"

#define RESTRICT_IoReactivexInternalFuseableQueueSubscription 1
#define INCLUDE_IoReactivexInternalFuseableQueueSubscription 1
#include "io/reactivex/internal/fuseable/QueueSubscription.h"

@interface IoReactivexInternalSubscriptionsBasicQueueSubscription : JavaUtilConcurrentAtomicAtomicLong < IoReactivexInternalFuseableQueueSubscription >

#pragma mark Public

- (instancetype __nonnull)init;

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (jboolean)offerWithId:(id)e;

- (jboolean)offerWithId:(id)v1
                 withId:(id)v2;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalSubscriptionsBasicQueueSubscription)

FOUNDATION_EXPORT void IoReactivexInternalSubscriptionsBasicQueueSubscription_init(IoReactivexInternalSubscriptionsBasicQueueSubscription *self);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalSubscriptionsBasicQueueSubscription)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalSubscriptionsBasicQueueSubscription")
