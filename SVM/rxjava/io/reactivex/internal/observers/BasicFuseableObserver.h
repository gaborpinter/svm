//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/observers/BasicFuseableObserver.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalObserversBasicFuseableObserver")
#ifdef RESTRICT_IoReactivexInternalObserversBasicFuseableObserver
#define INCLUDE_ALL_IoReactivexInternalObserversBasicFuseableObserver 0
#else
#define INCLUDE_ALL_IoReactivexInternalObserversBasicFuseableObserver 1
#endif
#undef RESTRICT_IoReactivexInternalObserversBasicFuseableObserver

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalObserversBasicFuseableObserver_) && (INCLUDE_ALL_IoReactivexInternalObserversBasicFuseableObserver || defined(INCLUDE_IoReactivexInternalObserversBasicFuseableObserver))
#define IoReactivexInternalObserversBasicFuseableObserver_

#define RESTRICT_IoReactivexObserver 1
#define INCLUDE_IoReactivexObserver 1
#include "io/reactivex/Observer.h"

#define RESTRICT_IoReactivexInternalFuseableQueueDisposable 1
#define INCLUDE_IoReactivexInternalFuseableQueueDisposable 1
#include "io/reactivex/internal/fuseable/QueueDisposable.h"

@class JavaLangThrowable;
@protocol IoReactivexDisposablesDisposable;

@interface IoReactivexInternalObserversBasicFuseableObserver : NSObject < IoReactivexObserver, IoReactivexInternalFuseableQueueDisposable > {
 @public
  id<IoReactivexObserver> downstream_;
  id<IoReactivexDisposablesDisposable> upstream_;
  id<IoReactivexInternalFuseableQueueDisposable> qd_;
  jboolean done_;
  jint sourceMode_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)downstream;

- (void)clear;

- (void)dispose;

- (jboolean)isDisposed;

- (jboolean)isEmpty;

- (jboolean)offerWithId:(id)e;

- (jboolean)offerWithId:(id)v1
                 withId:(id)v2;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

#pragma mark Protected

- (void)afterDownstream;

- (jboolean)beforeDownstream;

- (void)failWithJavaLangThrowable:(JavaLangThrowable *)t;

- (jint)transitiveBoundaryFusionWithInt:(jint)mode;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalObserversBasicFuseableObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalObserversBasicFuseableObserver, downstream_, id<IoReactivexObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalObserversBasicFuseableObserver, upstream_, id<IoReactivexDisposablesDisposable>)
J2OBJC_FIELD_SETTER(IoReactivexInternalObserversBasicFuseableObserver, qd_, id<IoReactivexInternalFuseableQueueDisposable>)

FOUNDATION_EXPORT void IoReactivexInternalObserversBasicFuseableObserver_initWithIoReactivexObserver_(IoReactivexInternalObserversBasicFuseableObserver *self, id<IoReactivexObserver> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalObserversBasicFuseableObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalObserversBasicFuseableObserver")
