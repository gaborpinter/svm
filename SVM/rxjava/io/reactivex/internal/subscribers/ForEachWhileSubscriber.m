//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/subscribers/ForEachWhileSubscriber.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "io/reactivex/exceptions/CompositeException.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Action.h"
#include "io/reactivex/functions/Consumer.h"
#include "io/reactivex/functions/Predicate.h"
#include "io/reactivex/internal/subscribers/ForEachWhileSubscriber.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Long.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/atomic/AtomicReference.h"
#include "org/reactivestreams/Subscription.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalSubscribersForEachWhileSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalSubscribersForEachWhileSubscriber_serialVersionUID -4403180040475402120LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalSubscribersForEachWhileSubscriber, serialVersionUID, jlong)

@implementation IoReactivexInternalSubscribersForEachWhileSubscriber

- (instancetype __nonnull)initWithIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)onNext
                               withIoReactivexFunctionsConsumer:(id<IoReactivexFunctionsConsumer>)onError
                                 withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onComplete {
  IoReactivexInternalSubscribersForEachWhileSubscriber_initWithIoReactivexFunctionsPredicate_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_(self, onNext, onError, onComplete);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  IoReactivexInternalSubscriptionsSubscriptionHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withOrgReactivestreamsSubscription_withLong_(self, s, JavaLangLong_MAX_VALUE);
}

- (void)onNextWithId:(id)t {
  if (done_) {
    return;
  }
  jboolean b;
  @try {
    b = [((id<IoReactivexFunctionsPredicate>) nil_chk(onNext_)) testWithId:t];
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    [self dispose];
    [self onErrorWithJavaLangThrowable:ex];
    return;
  }
  if (!b) {
    [self dispose];
    [self onComplete];
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if (done_) {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
    return;
  }
  done_ = true;
  @try {
    [((id<IoReactivexFunctionsConsumer>) nil_chk(onError_)) acceptWithId:t];
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(create_IoReactivexExceptionsCompositeException_initWithJavaLangThrowableArray_([IOSObjectArray arrayWithObjects:(id[]){ t, ex } count:2 type:JavaLangThrowable_class_()]));
  }
}

- (void)onComplete {
  if (done_) {
    return;
  }
  done_ = true;
  @try {
    [((id<IoReactivexFunctionsAction>) nil_chk(onComplete_)) run];
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
  }
}

- (void)dispose {
  IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(self);
}

- (jboolean)isDisposed {
  return [self get] == JreLoadEnum(IoReactivexInternalSubscriptionsSubscriptionHelper, CANCELLED);
}

- (void)dealloc {
  RELEASE_(onNext_);
  RELEASE_(onError_);
  RELEASE_(onComplete_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFunctionsPredicate:withIoReactivexFunctionsConsumer:withIoReactivexFunctionsAction:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(dispose);
  methods[6].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalSubscribersForEachWhileSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "onNext_", "LIoReactivexFunctionsPredicate;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "onError_", "LIoReactivexFunctionsConsumer;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "onComplete_", "LIoReactivexFunctionsAction;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexFunctionsPredicate;LIoReactivexFunctionsConsumer;LIoReactivexFunctionsAction;", "(Lio/reactivex/functions/Predicate<-TT;>;Lio/reactivex/functions/Consumer<-Ljava/lang/Throwable;>;Lio/reactivex/functions/Action;)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/functions/Predicate<-TT;>;", "Lio/reactivex/functions/Consumer<-Ljava/lang/Throwable;>;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicReference<Lorg/reactivestreams/Subscription;>;Lio/reactivex/FlowableSubscriber<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalSubscribersForEachWhileSubscriber = { "ForEachWhileSubscriber", "io.reactivex.internal.subscribers", ptrTable, methods, fields, 7, 0x11, 7, 5, -1, -1, -1, 11, -1 };
  return &_IoReactivexInternalSubscribersForEachWhileSubscriber;
}

@end

void IoReactivexInternalSubscribersForEachWhileSubscriber_initWithIoReactivexFunctionsPredicate_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_(IoReactivexInternalSubscribersForEachWhileSubscriber *self, id<IoReactivexFunctionsPredicate> onNext, id<IoReactivexFunctionsConsumer> onError, id<IoReactivexFunctionsAction> onComplete) {
  JavaUtilConcurrentAtomicAtomicReference_init(self);
  JreStrongAssign(&self->onNext_, onNext);
  JreStrongAssign(&self->onError_, onError);
  JreStrongAssign(&self->onComplete_, onComplete);
}

IoReactivexInternalSubscribersForEachWhileSubscriber *new_IoReactivexInternalSubscribersForEachWhileSubscriber_initWithIoReactivexFunctionsPredicate_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_(id<IoReactivexFunctionsPredicate> onNext, id<IoReactivexFunctionsConsumer> onError, id<IoReactivexFunctionsAction> onComplete) {
  J2OBJC_NEW_IMPL(IoReactivexInternalSubscribersForEachWhileSubscriber, initWithIoReactivexFunctionsPredicate_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_, onNext, onError, onComplete)
}

IoReactivexInternalSubscribersForEachWhileSubscriber *create_IoReactivexInternalSubscribersForEachWhileSubscriber_initWithIoReactivexFunctionsPredicate_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_(id<IoReactivexFunctionsPredicate> onNext, id<IoReactivexFunctionsConsumer> onError, id<IoReactivexFunctionsAction> onComplete) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalSubscribersForEachWhileSubscriber, initWithIoReactivexFunctionsPredicate_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_, onNext, onError, onComplete)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSubscribersForEachWhileSubscriber)
