//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/subscribers/BasicFuseableSubscriber.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/internal/fuseable/QueueFuseable.h"
#include "io/reactivex/internal/fuseable/QueueSubscription.h"
#include "io/reactivex/internal/subscribers/BasicFuseableSubscriber.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Throwable.h"
#include "java/lang/UnsupportedOperationException.h"
#include "org/reactivestreams/Subscriber.h"
#include "org/reactivestreams/Subscription.h"

#pragma clang diagnostic ignored "-Wprotocol"

@implementation IoReactivexInternalSubscribersBasicFuseableSubscriber

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream {
  IoReactivexInternalSubscribersBasicFuseableSubscriber_initWithOrgReactivestreamsSubscriber_(self, downstream);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_validateWithOrgReactivestreamsSubscription_withOrgReactivestreamsSubscription_(self->upstream_, s)) {
    JreStrongAssign(&self->upstream_, s);
    if ([IoReactivexInternalFuseableQueueSubscription_class_() isInstance:s]) {
      JreStrongAssign(&self->qs_, (id<IoReactivexInternalFuseableQueueSubscription>) cast_check(s, IoReactivexInternalFuseableQueueSubscription_class_()));
    }
    if ([self beforeDownstream]) {
      [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onSubscribeWithOrgReactivestreamsSubscription:self];
      [self afterDownstream];
    }
  }
}

- (jboolean)beforeDownstream {
  return true;
}

- (void)afterDownstream {
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if (done_) {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
    return;
  }
  done_ = true;
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
}

- (void)failWithJavaLangThrowable:(JavaLangThrowable *)t {
  IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(t);
  [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
  [self onErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  if (done_) {
    return;
  }
  done_ = true;
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onComplete];
}

- (jint)transitiveBoundaryFusionWithInt:(jint)mode {
  id<IoReactivexInternalFuseableQueueSubscription> qs = self->qs_;
  if (qs != nil) {
    if ((mode & IoReactivexInternalFuseableQueueFuseable_BOUNDARY) == 0) {
      jint m = [qs requestFusionWithInt:mode];
      if (m != IoReactivexInternalFuseableQueueFuseable_NONE) {
        sourceMode_ = m;
      }
      return m;
    }
  }
  return IoReactivexInternalFuseableQueueFuseable_NONE;
}

- (void)requestWithLong:(jlong)n {
  [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) requestWithLong:n];
}

- (void)cancel {
  [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
}

- (jboolean)isEmpty {
  return [((id<IoReactivexInternalFuseableQueueSubscription>) nil_chk(qs_)) isEmpty];
}

- (void)clear {
  [((id<IoReactivexInternalFuseableQueueSubscription>) nil_chk(qs_)) clear];
}

- (jboolean)offerWithId:(id)e {
  @throw create_JavaLangUnsupportedOperationException_initWithNSString_(@"Should not be called!");
}

- (jboolean)offerWithId:(id)v1
                 withId:(id)v2 {
  @throw create_JavaLangUnsupportedOperationException_initWithNSString_(@"Should not be called!");
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(upstream_);
  RELEASE_(qs_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x11, 2, 3, -1, -1, -1, -1 },
    { NULL, "Z", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x14, 6, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x14, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x11, 11, 12, -1, 13, -1, -1 },
    { NULL, "Z", 0x11, 11, 14, -1, 15, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(beforeDownstream);
  methods[3].selector = @selector(afterDownstream);
  methods[4].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[5].selector = @selector(failWithJavaLangThrowable:);
  methods[6].selector = @selector(onComplete);
  methods[7].selector = @selector(transitiveBoundaryFusionWithInt:);
  methods[8].selector = @selector(requestWithLong:);
  methods[9].selector = @selector(cancel);
  methods[10].selector = @selector(isEmpty);
  methods[11].selector = @selector(clear);
  methods[12].selector = @selector(offerWithId:);
  methods[13].selector = @selector(offerWithId:withId:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "downstream_", "LOrgReactivestreamsSubscriber;", .constantValue.asLong = 0, 0x14, -1, -1, 16, -1 },
    { "upstream_", "LOrgReactivestreamsSubscription;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "qs_", "LIoReactivexInternalFuseableQueueSubscription;", .constantValue.asLong = 0, 0x4, -1, -1, 17, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "sourceMode_", "I", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TR;>;)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onError", "LJavaLangThrowable;", "fail", "transitiveBoundaryFusion", "I", "request", "J", "offer", "LNSObject;", "(TR;)Z", "LNSObject;LNSObject;", "(TR;TR;)Z", "Lorg/reactivestreams/Subscriber<-TR;>;", "Lio/reactivex/internal/fuseable/QueueSubscription<TT;>;", "<T:Ljava/lang/Object;R:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/FlowableSubscriber<TT;>;Lio/reactivex/internal/fuseable/QueueSubscription<TR;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalSubscribersBasicFuseableSubscriber = { "BasicFuseableSubscriber", "io.reactivex.internal.subscribers", ptrTable, methods, fields, 7, 0x401, 14, 5, -1, -1, -1, 18, -1 };
  return &_IoReactivexInternalSubscribersBasicFuseableSubscriber;
}

@end

void IoReactivexInternalSubscribersBasicFuseableSubscriber_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalSubscribersBasicFuseableSubscriber *self, id<OrgReactivestreamsSubscriber> downstream) {
  NSObject_init(self);
  JreStrongAssign(&self->downstream_, downstream);
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalSubscribersBasicFuseableSubscriber)
