//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/subscribers/BlockingBaseSubscriber.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalSubscribersBlockingBaseSubscriber")
#ifdef RESTRICT_IoReactivexInternalSubscribersBlockingBaseSubscriber
#define INCLUDE_ALL_IoReactivexInternalSubscribersBlockingBaseSubscriber 0
#else
#define INCLUDE_ALL_IoReactivexInternalSubscribersBlockingBaseSubscriber 1
#endif
#undef RESTRICT_IoReactivexInternalSubscribersBlockingBaseSubscriber

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalSubscribersBlockingBaseSubscriber_) && (INCLUDE_ALL_IoReactivexInternalSubscribersBlockingBaseSubscriber || defined(INCLUDE_IoReactivexInternalSubscribersBlockingBaseSubscriber))
#define IoReactivexInternalSubscribersBlockingBaseSubscriber_

#define RESTRICT_JavaUtilConcurrentCountDownLatch 1
#define INCLUDE_JavaUtilConcurrentCountDownLatch 1
#include "java/util/concurrent/CountDownLatch.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

@class JavaLangThrowable;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalSubscribersBlockingBaseSubscriber : JavaUtilConcurrentCountDownLatch < IoReactivexFlowableSubscriber > {
 @public
  id value_;
  JavaLangThrowable *error_;
  id<OrgReactivestreamsSubscription> upstream_;
  volatile_jboolean cancelled_;
}

#pragma mark Public

- (instancetype __nonnull)init;

- (id)blockingGet;

- (void)onComplete;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalSubscribersBlockingBaseSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalSubscribersBlockingBaseSubscriber, value_, id)
J2OBJC_FIELD_SETTER(IoReactivexInternalSubscribersBlockingBaseSubscriber, error_, JavaLangThrowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalSubscribersBlockingBaseSubscriber, upstream_, id<OrgReactivestreamsSubscription>)

FOUNDATION_EXPORT void IoReactivexInternalSubscribersBlockingBaseSubscriber_init(IoReactivexInternalSubscribersBlockingBaseSubscriber *self);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalSubscribersBlockingBaseSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalSubscribersBlockingBaseSubscriber")
