//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/fuseable/FuseToMaybe.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/internal/fuseable/FuseToMaybe.h"

@interface IoReactivexInternalFuseableFuseToMaybe : NSObject

@end

@implementation IoReactivexInternalFuseableFuseToMaybe

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LIoReactivexMaybe;", 0x401, -1, -1, -1, 0, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(fuseToMaybe);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "()Lio/reactivex/Maybe<TT;>;", "<T:Ljava/lang/Object;>Ljava/lang/Object;" };
  static const J2ObjcClassInfo _IoReactivexInternalFuseableFuseToMaybe = { "FuseToMaybe", "io.reactivex.internal.fuseable", ptrTable, methods, NULL, 7, 0x609, 1, 0, -1, -1, -1, 1, -1 };
  return &_IoReactivexInternalFuseableFuseToMaybe;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(IoReactivexInternalFuseableFuseToMaybe)
