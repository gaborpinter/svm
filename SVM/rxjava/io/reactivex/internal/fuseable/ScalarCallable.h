//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/fuseable/ScalarCallable.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalFuseableScalarCallable")
#ifdef RESTRICT_IoReactivexInternalFuseableScalarCallable
#define INCLUDE_ALL_IoReactivexInternalFuseableScalarCallable 0
#else
#define INCLUDE_ALL_IoReactivexInternalFuseableScalarCallable 1
#endif
#undef RESTRICT_IoReactivexInternalFuseableScalarCallable

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalFuseableScalarCallable_) && (INCLUDE_ALL_IoReactivexInternalFuseableScalarCallable || defined(INCLUDE_IoReactivexInternalFuseableScalarCallable))
#define IoReactivexInternalFuseableScalarCallable_

#define RESTRICT_JavaUtilConcurrentCallable 1
#define INCLUDE_JavaUtilConcurrentCallable 1
#include "java/util/concurrent/Callable.h"

@protocol IoReactivexInternalFuseableScalarCallable < JavaUtilConcurrentCallable, JavaObject >

- (id)call;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalFuseableScalarCallable)

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalFuseableScalarCallable)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalFuseableScalarCallable")
