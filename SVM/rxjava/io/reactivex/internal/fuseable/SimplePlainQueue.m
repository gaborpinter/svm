//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/fuseable/SimplePlainQueue.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/internal/fuseable/SimplePlainQueue.h"

@interface IoReactivexInternalFuseableSimplePlainQueue : NSObject

@end

@implementation IoReactivexInternalFuseableSimplePlainQueue

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LNSObject;", 0x401, -1, -1, -1, 0, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(poll);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "()TT;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/internal/fuseable/SimpleQueue<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalFuseableSimplePlainQueue = { "SimplePlainQueue", "io.reactivex.internal.fuseable", ptrTable, methods, NULL, 7, 0x609, 1, 0, -1, -1, -1, 1, -1 };
  return &_IoReactivexInternalFuseableSimplePlainQueue;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(IoReactivexInternalFuseableSimplePlainQueue)
