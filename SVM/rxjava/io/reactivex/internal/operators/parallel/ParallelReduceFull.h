//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/parallel/ParallelReduceFull.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsParallelParallelReduceFull")
#ifdef RESTRICT_IoReactivexInternalOperatorsParallelParallelReduceFull
#define INCLUDE_ALL_IoReactivexInternalOperatorsParallelParallelReduceFull 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsParallelParallelReduceFull 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsParallelParallelReduceFull

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsParallelParallelReduceFull_) && (INCLUDE_ALL_IoReactivexInternalOperatorsParallelParallelReduceFull || defined(INCLUDE_IoReactivexInternalOperatorsParallelParallelReduceFull))
#define IoReactivexInternalOperatorsParallelParallelReduceFull_

#define RESTRICT_IoReactivexFlowable 1
#define INCLUDE_IoReactivexFlowable 1
#include "io/reactivex/Flowable.h"

@class IoReactivexParallelParallelFlowable;
@protocol IoReactivexFunctionsBiFunction;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsParallelParallelReduceFull : IoReactivexFlowable {
 @public
  IoReactivexParallelParallelFlowable *source_;
  id<IoReactivexFunctionsBiFunction> reducer_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexParallelParallelFlowable:(IoReactivexParallelParallelFlowable *)source
                                   withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)reducer;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsParallelParallelReduceFull)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull, source_, IoReactivexParallelParallelFlowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull, reducer_, id<IoReactivexFunctionsBiFunction>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsParallelParallelReduceFull_initWithIoReactivexParallelParallelFlowable_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsParallelParallelReduceFull *self, IoReactivexParallelParallelFlowable *source, id<IoReactivexFunctionsBiFunction> reducer);

FOUNDATION_EXPORT IoReactivexInternalOperatorsParallelParallelReduceFull *new_IoReactivexInternalOperatorsParallelParallelReduceFull_initWithIoReactivexParallelParallelFlowable_withIoReactivexFunctionsBiFunction_(IoReactivexParallelParallelFlowable *source, id<IoReactivexFunctionsBiFunction> reducer) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsParallelParallelReduceFull *create_IoReactivexInternalOperatorsParallelParallelReduceFull_initWithIoReactivexParallelParallelFlowable_withIoReactivexFunctionsBiFunction_(IoReactivexParallelParallelFlowable *source, id<IoReactivexFunctionsBiFunction> reducer);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsParallelParallelReduceFull)

#endif

#if !defined (IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsParallelParallelReduceFull || defined(INCLUDE_IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber))
#define IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber_

#define RESTRICT_IoReactivexInternalSubscriptionsDeferredScalarSubscription 1
#define INCLUDE_IoReactivexInternalSubscriptionsDeferredScalarSubscription 1
#include "io/reactivex/internal/subscriptions/DeferredScalarSubscription.h"

@class IOSObjectArray;
@class IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicInteger;
@class JavaUtilConcurrentAtomicAtomicReference;
@protocol IoReactivexFunctionsBiFunction;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber : IoReactivexInternalSubscriptionsDeferredScalarSubscription {
 @public
  IOSObjectArray *subscribers_;
  id<IoReactivexFunctionsBiFunction> reducer_;
  JavaUtilConcurrentAtomicAtomicReference *current_;
  JavaUtilConcurrentAtomicAtomicInteger *remaining_;
  JavaUtilConcurrentAtomicAtomicReference *error_;
}

#pragma mark Public

- (void)cancel;

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)subscriber
                                                       withInt:(jint)n
                            withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)reducer;

- (IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair *)addValueWithId:(id)value;

- (void)innerCompleteWithId:(id)value;

- (void)innerErrorWithJavaLangThrowable:(JavaLangThrowable *)ex;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber, subscribers_, IOSObjectArray *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber, reducer_, id<IoReactivexFunctionsBiFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber, current_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber, remaining_, JavaUtilConcurrentAtomicAtomicInteger *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber, error_, JavaUtilConcurrentAtomicAtomicReference *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber *self, id<OrgReactivestreamsSubscriber> subscriber, jint n, id<IoReactivexFunctionsBiFunction> reducer);

FOUNDATION_EXPORT IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber *new_IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withIoReactivexFunctionsBiFunction_(id<OrgReactivestreamsSubscriber> subscriber, jint n, id<IoReactivexFunctionsBiFunction> reducer) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber *create_IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withIoReactivexFunctionsBiFunction_(id<OrgReactivestreamsSubscriber> subscriber, jint n, id<IoReactivexFunctionsBiFunction> reducer);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber)

#endif

#if !defined (IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsParallelParallelReduceFull || defined(INCLUDE_IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber))
#define IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

@class IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber;
@class JavaLangThrowable;
@protocol IoReactivexFunctionsBiFunction;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber : JavaUtilConcurrentAtomicAtomicReference < IoReactivexFlowableSubscriber > {
 @public
  IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber *parent_;
  id<IoReactivexFunctionsBiFunction> reducer_;
  id value_ParallelReduceFullInnerSubscriber_;
  jboolean done_;
}

#pragma mark Public

- (id<OrgReactivestreamsSubscription>)accumulateAndGetWithId:(id<OrgReactivestreamsSubscription>)arg0
                          withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<OrgReactivestreamsSubscription>)get;

- (id<OrgReactivestreamsSubscription>)getAndAccumulateWithId:(id<OrgReactivestreamsSubscription>)arg0
                          withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<OrgReactivestreamsSubscription>)getAndSetWithId:(id<OrgReactivestreamsSubscription>)arg0;

- (id<OrgReactivestreamsSubscription>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (id<OrgReactivestreamsSubscription>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber:(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber *)parent
                                                                                       withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)reducer;

- (void)cancel;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber, parent_, IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber, reducer_, id<IoReactivexFunctionsBiFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber, value_ParallelReduceFullInnerSubscriber_, id)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber_initWithIoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber *self, IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber *parent, id<IoReactivexFunctionsBiFunction> reducer);

FOUNDATION_EXPORT IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber *new_IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber_initWithIoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber *parent, id<IoReactivexFunctionsBiFunction> reducer) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber *create_IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber_initWithIoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullMainSubscriber *parent, id<IoReactivexFunctionsBiFunction> reducer);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsParallelParallelReduceFull_ParallelReduceFullInnerSubscriber)

#endif

#if !defined (IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair_) && (INCLUDE_ALL_IoReactivexInternalOperatorsParallelParallelReduceFull || defined(INCLUDE_IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair))
#define IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicInteger 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicInteger 1
#include "java/util/concurrent/atomic/AtomicInteger.h"

@interface IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair : JavaUtilConcurrentAtomicAtomicInteger {
 @public
  id first_;
  id second_;
  JavaUtilConcurrentAtomicAtomicInteger *releaseIndex_;
}

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

#pragma mark Package-Private

- (instancetype __nonnull)init;

- (jboolean)releaseSlot;

- (jint)tryAcquireSlot;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair, first_, id)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair, second_, id)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair, releaseIndex_, JavaUtilConcurrentAtomicAtomicInteger *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair_init(IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair *self);

FOUNDATION_EXPORT IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair *new_IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair *create_IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair_init(void);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsParallelParallelReduceFull_SlotPair)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsParallelParallelReduceFull")
