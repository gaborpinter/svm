//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/BlockingFlowableIterable.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable || defined(INCLUDE_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable))
#define IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_

#define RESTRICT_JavaLangIterable 1
#define INCLUDE_JavaLangIterable 1
#include "java/lang/Iterable.h"

@class IoReactivexFlowable;
@protocol JavaUtilFunctionConsumer;
@protocol JavaUtilIterator;
@protocol JavaUtilSpliterator;

@interface IoReactivexInternalOperatorsFlowableBlockingFlowableIterable : NSObject < JavaLangIterable > {
 @public
  IoReactivexFlowable *source_;
  jint bufferSize_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                                              withInt:(jint)bufferSize;

- (id<JavaUtilIterator>)iterator;

#pragma mark Package-Private

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable, source_, IoReactivexFlowable *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_initWithIoReactivexFlowable_withInt_(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable *self, IoReactivexFlowable *source, jint bufferSize);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableBlockingFlowableIterable *new_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_initWithIoReactivexFlowable_withInt_(IoReactivexFlowable *source, jint bufferSize) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableBlockingFlowableIterable *create_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_initWithIoReactivexFlowable_withInt_(IoReactivexFlowable *source, jint bufferSize);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable || defined(INCLUDE_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator))
#define IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_JavaUtilIterator 1
#define INCLUDE_JavaUtilIterator 1
#include "java/util/Iterator.h"

#define RESTRICT_JavaLangRunnable 1
#define INCLUDE_JavaLangRunnable 1
#include "java/lang/Runnable.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class IoReactivexInternalQueueSpscArrayQueue;
@class JavaLangThrowable;
@protocol JavaUtilConcurrentLocksCondition;
@protocol JavaUtilConcurrentLocksLock;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionConsumer;
@protocol JavaUtilFunctionUnaryOperator;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator : JavaUtilConcurrentAtomicAtomicReference < IoReactivexFlowableSubscriber, JavaUtilIterator, JavaLangRunnable, IoReactivexDisposablesDisposable > {
 @public
  IoReactivexInternalQueueSpscArrayQueue *queue_;
  jlong batchSize_;
  jlong limit_;
  id<JavaUtilConcurrentLocksLock> lock_;
  id<JavaUtilConcurrentLocksCondition> condition_;
  jlong produced_;
  volatile_jboolean done_;
  JavaLangThrowable *error_;
}

#pragma mark Public

- (id<OrgReactivestreamsSubscription>)accumulateAndGetWithId:(id<OrgReactivestreamsSubscription>)arg0
                          withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)dispose;

- (id<OrgReactivestreamsSubscription>)get;

- (id<OrgReactivestreamsSubscription>)getAndAccumulateWithId:(id<OrgReactivestreamsSubscription>)arg0
                          withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<OrgReactivestreamsSubscription>)getAndSetWithId:(id<OrgReactivestreamsSubscription>)arg0;

- (id<OrgReactivestreamsSubscription>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (jboolean)hasNext;

- (jboolean)isDisposed;

- (id)next;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (void)remove;

- (void)run;

- (id<OrgReactivestreamsSubscription>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithInt:(jint)batchSize;

- (void)signalConsumer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator, queue_, IoReactivexInternalQueueSpscArrayQueue *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator, lock_, id<JavaUtilConcurrentLocksLock>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator, condition_, id<JavaUtilConcurrentLocksCondition>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator, error_, JavaLangThrowable *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator_initWithInt_(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator *self, jint batchSize);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator *new_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator_initWithInt_(jint batchSize) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator *create_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator_initWithInt_(jint batchSize);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableBlockingFlowableIterable_BlockingFlowableIterator)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableBlockingFlowableIterable")
