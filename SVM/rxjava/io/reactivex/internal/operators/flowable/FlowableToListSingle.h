//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableToListSingle.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableToListSingle")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableToListSingle
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableToListSingle 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableToListSingle 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableToListSingle

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableToListSingle_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableToListSingle || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableToListSingle))
#define IoReactivexInternalOperatorsFlowableFlowableToListSingle_

#define RESTRICT_IoReactivexSingle 1
#define INCLUDE_IoReactivexSingle 1
#include "io/reactivex/Single.h"

#define RESTRICT_IoReactivexInternalFuseableFuseToFlowable 1
#define INCLUDE_IoReactivexInternalFuseableFuseToFlowable 1
#include "io/reactivex/internal/fuseable/FuseToFlowable.h"

@class IoReactivexFlowable;
@protocol IoReactivexSingleObserver;
@protocol JavaUtilCollection;
@protocol JavaUtilConcurrentCallable;

@interface IoReactivexInternalOperatorsFlowableFlowableToListSingle : IoReactivexSingle < IoReactivexInternalFuseableFuseToFlowable > {
 @public
  IoReactivexFlowable *source_;
  id<JavaUtilConcurrentCallable> collectionSupplier_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source;

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                       withJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)collectionSupplier;

- (id<JavaUtilCollection>)blockingGet;

- (IoReactivexFlowable *)fuseToFlowable;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableToListSingle)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableToListSingle, source_, IoReactivexFlowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableToListSingle, collectionSupplier_, id<JavaUtilConcurrentCallable>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableToListSingle_initWithIoReactivexFlowable_(IoReactivexInternalOperatorsFlowableFlowableToListSingle *self, IoReactivexFlowable *source);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableToListSingle *new_IoReactivexInternalOperatorsFlowableFlowableToListSingle_initWithIoReactivexFlowable_(IoReactivexFlowable *source) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableToListSingle *create_IoReactivexInternalOperatorsFlowableFlowableToListSingle_initWithIoReactivexFlowable_(IoReactivexFlowable *source);

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableToListSingle_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_(IoReactivexInternalOperatorsFlowableFlowableToListSingle *self, IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> collectionSupplier);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableToListSingle *new_IoReactivexInternalOperatorsFlowableFlowableToListSingle_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_(IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> collectionSupplier) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableToListSingle *create_IoReactivexInternalOperatorsFlowableFlowableToListSingle_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_(IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> collectionSupplier);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableToListSingle)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableToListSingle || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber_

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class JavaLangThrowable;
@protocol IoReactivexSingleObserver;
@protocol JavaUtilCollection;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber : NSObject < IoReactivexFlowableSubscriber, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexSingleObserver> downstream_;
  id<OrgReactivestreamsSubscription> upstream_;
  id<JavaUtilCollection> value_;
}

#pragma mark Public

- (void)dispose;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)actual
                                     withJavaUtilCollection:(id<JavaUtilCollection>)collection;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber, downstream_, id<IoReactivexSingleObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber, upstream_, id<OrgReactivestreamsSubscription>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber, value_, id<JavaUtilCollection>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber_initWithIoReactivexSingleObserver_withJavaUtilCollection_(IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber *self, id<IoReactivexSingleObserver> actual, id<JavaUtilCollection> collection);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber_initWithIoReactivexSingleObserver_withJavaUtilCollection_(id<IoReactivexSingleObserver> actual, id<JavaUtilCollection> collection) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber_initWithIoReactivexSingleObserver_withJavaUtilCollection_(id<IoReactivexSingleObserver> actual, id<JavaUtilCollection> collection);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableToListSingle_ToListSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableToListSingle")
