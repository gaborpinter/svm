//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableGroupJoin.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableGroupJoin")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableGroupJoin
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableGroupJoin 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableGroupJoin 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableGroupJoin
#ifdef INCLUDE_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription
#define INCLUDE_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport 1
#endif

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableGroupJoin_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableGroupJoin || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableGroupJoin))
#define IoReactivexInternalOperatorsFlowableFlowableGroupJoin_

#define RESTRICT_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"

@class IoReactivexFlowable;
@protocol IoReactivexFunctionsBiFunction;
@protocol IoReactivexFunctionsFunction;
@protocol OrgReactivestreamsPublisher;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableGroupJoin : IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream {
 @public
  id<OrgReactivestreamsPublisher> other_;
  id<IoReactivexFunctionsFunction> leftEnd_;
  id<IoReactivexFunctionsFunction> rightEnd_;
  id<IoReactivexFunctionsBiFunction> resultSelector_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                      withOrgReactivestreamsPublisher:(id<OrgReactivestreamsPublisher>)other
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)leftEnd
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)rightEnd
                   withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)resultSelector;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableGroupJoin)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin, other_, id<OrgReactivestreamsPublisher>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin, leftEnd_, id<IoReactivexFunctionsFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin, rightEnd_, id<IoReactivexFunctionsFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin, resultSelector_, id<IoReactivexFunctionsBiFunction>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableGroupJoin_initWithIoReactivexFlowable_withOrgReactivestreamsPublisher_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsFlowableFlowableGroupJoin *self, IoReactivexFlowable *source, id<OrgReactivestreamsPublisher> other, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableGroupJoin *new_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_initWithIoReactivexFlowable_withOrgReactivestreamsPublisher_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(IoReactivexFlowable *source, id<OrgReactivestreamsPublisher> other, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableGroupJoin *create_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_initWithIoReactivexFlowable_withOrgReactivestreamsPublisher_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(IoReactivexFlowable *source, id<OrgReactivestreamsPublisher> other, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableGroupJoin || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport))
#define IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport_

@class IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber;
@class IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber;
@class JavaLangThrowable;

@protocol IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport < JavaObject >

- (void)innerErrorWithJavaLangThrowable:(JavaLangThrowable *)ex;

- (void)innerCompleteWithIoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber:(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber *)sender;

- (void)innerValueWithBoolean:(jboolean)isLeft
                       withId:(id)o;

- (void)innerCloseWithBoolean:(jboolean)isLeft
withIoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber:(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber *)index;

- (void)innerCloseErrorWithJavaLangThrowable:(JavaLangThrowable *)ex;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport)

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableGroupJoin || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription))
#define IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicInteger 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicInteger 1
#include "java/util/concurrent/atomic/AtomicInteger.h"

#define RESTRICT_OrgReactivestreamsSubscription 1
#define INCLUDE_OrgReactivestreamsSubscription 1
#include "org/reactivestreams/Subscription.h"

@class IoReactivexDisposablesCompositeDisposable;
@class IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber;
@class IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber;
@class IoReactivexInternalQueueSpscLinkedArrayQueue;
@class JavaLangInteger;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicLong;
@class JavaUtilConcurrentAtomicAtomicReference;
@protocol IoReactivexFunctionsBiFunction;
@protocol IoReactivexFunctionsFunction;
@protocol IoReactivexInternalFuseableSimpleQueue;
@protocol JavaUtilMap;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription : JavaUtilConcurrentAtomicAtomicInteger < OrgReactivestreamsSubscription, IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  JavaUtilConcurrentAtomicAtomicLong *requested_;
  IoReactivexInternalQueueSpscLinkedArrayQueue *queue_;
  IoReactivexDisposablesCompositeDisposable *disposables_;
  id<JavaUtilMap> lefts_;
  id<JavaUtilMap> rights_;
  JavaUtilConcurrentAtomicAtomicReference *error_;
  id<IoReactivexFunctionsFunction> leftEnd_;
  id<IoReactivexFunctionsFunction> rightEnd_;
  id<IoReactivexFunctionsBiFunction> resultSelector_;
  JavaUtilConcurrentAtomicAtomicInteger *active_;
  jint leftIndex_;
  jint rightIndex_;
  volatile_jboolean cancelled_;
}

+ (JavaLangInteger *)LEFT_VALUE;

+ (JavaLangInteger *)RIGHT_VALUE;

+ (JavaLangInteger *)LEFT_CLOSE;

+ (JavaLangInteger *)RIGHT_CLOSE;

#pragma mark Public

- (void)cancel;

- (NSUInteger)hash;

- (void)innerCloseWithBoolean:(jboolean)isLeft
withIoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber:(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber *)index;

- (void)innerCloseErrorWithJavaLangThrowable:(JavaLangThrowable *)ex;

- (void)innerCompleteWithIoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber:(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber *)sender;

- (void)innerErrorWithJavaLangThrowable:(JavaLangThrowable *)ex;

- (void)innerValueWithBoolean:(jboolean)isLeft
                       withId:(id)o;

- (jboolean)isEqual:(id)obj;

- (void)requestWithLong:(jlong)n;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                              withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)leftEnd
                              withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)rightEnd
                            withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)resultSelector;

- (void)cancelAll;

- (void)drain;

- (void)errorAllWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)a;

- (void)failWithJavaLangThrowable:(JavaLangThrowable *)exc
 withOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)a
withIoReactivexInternalFuseableSimpleQueue:(id<IoReactivexInternalFuseableSimpleQueue>)q;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, requested_, JavaUtilConcurrentAtomicAtomicLong *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, queue_, IoReactivexInternalQueueSpscLinkedArrayQueue *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, disposables_, IoReactivexDisposablesCompositeDisposable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, lefts_, id<JavaUtilMap>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, rights_, id<JavaUtilMap>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, error_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, leftEnd_, id<IoReactivexFunctionsFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, rightEnd_, id<IoReactivexFunctionsFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, resultSelector_, id<IoReactivexFunctionsBiFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, active_, JavaUtilConcurrentAtomicAtomicInteger *)

inline JavaLangInteger *IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_get_LEFT_VALUE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT JavaLangInteger *IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_LEFT_VALUE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, LEFT_VALUE, JavaLangInteger *)

inline JavaLangInteger *IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_get_RIGHT_VALUE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT JavaLangInteger *IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_RIGHT_VALUE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, RIGHT_VALUE, JavaLangInteger *)

inline JavaLangInteger *IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_get_LEFT_CLOSE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT JavaLangInteger *IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_LEFT_CLOSE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, LEFT_CLOSE, JavaLangInteger *)

inline JavaLangInteger *IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_get_RIGHT_CLOSE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT JavaLangInteger *IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_RIGHT_CLOSE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription, RIGHT_CLOSE, JavaLangInteger *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription *self, id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription *new_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription *create_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_GroupJoinSubscription)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableGroupJoin || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class JavaLangThrowable;
@protocol IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber : JavaUtilConcurrentAtomicAtomicReference < IoReactivexFlowableSubscriber, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport> parent_;
  jboolean isLeft_;
}

#pragma mark Public

- (id<OrgReactivestreamsSubscription>)accumulateAndGetWithId:(id<OrgReactivestreamsSubscription>)arg0
                          withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)dispose;

- (id<OrgReactivestreamsSubscription>)get;

- (id<OrgReactivestreamsSubscription>)getAndAccumulateWithId:(id<OrgReactivestreamsSubscription>)arg0
                          withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<OrgReactivestreamsSubscription>)getAndSetWithId:(id<OrgReactivestreamsSubscription>)arg0;

- (id<OrgReactivestreamsSubscription>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (id<OrgReactivestreamsSubscription>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport:(id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport>)parent
                                                                                        withBoolean:(jboolean)isLeft;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber, parent_, id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport_withBoolean_(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber *self, id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport> parent, jboolean isLeft);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport_withBoolean_(id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport> parent, jboolean isLeft) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport_withBoolean_(id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport> parent, jboolean isLeft);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightSubscriber)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableGroupJoin || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class JavaLangThrowable;
@protocol IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber : JavaUtilConcurrentAtomicAtomicReference < IoReactivexFlowableSubscriber, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport> parent_;
  jboolean isLeft_;
  jint index_;
}

#pragma mark Public

- (id<OrgReactivestreamsSubscription>)accumulateAndGetWithId:(id<OrgReactivestreamsSubscription>)arg0
                          withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)dispose;

- (id<OrgReactivestreamsSubscription>)get;

- (id<OrgReactivestreamsSubscription>)getAndAccumulateWithId:(id<OrgReactivestreamsSubscription>)arg0
                          withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<OrgReactivestreamsSubscription>)getAndSetWithId:(id<OrgReactivestreamsSubscription>)arg0;

- (id<OrgReactivestreamsSubscription>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (id<OrgReactivestreamsSubscription>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport:(id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport>)parent
                                                                                        withBoolean:(jboolean)isLeft
                                                                                            withInt:(jint)index;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber, parent_, id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport_withBoolean_withInt_(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber *self, id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport> parent, jboolean isLeft, jint index);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport_withBoolean_withInt_(id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport> parent, jboolean isLeft, jint index) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport_withBoolean_withInt_(id<IoReactivexInternalOperatorsFlowableFlowableGroupJoin_JoinSupport> parent, jboolean isLeft, jint index);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableGroupJoin_LeftRightEndSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableGroupJoin")
