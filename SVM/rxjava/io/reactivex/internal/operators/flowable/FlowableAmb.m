//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableAmb.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/internal/operators/flowable/FlowableAmb.h"
#include "io/reactivex/internal/subscriptions/EmptySubscription.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Iterable.h"
#include "java/lang/NullPointerException.h"
#include "java/lang/System.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"
#include "java/util/concurrent/atomic/AtomicLong.h"
#include "java/util/concurrent/atomic/AtomicReference.h"
#include "org/reactivestreams/Publisher.h"
#include "org/reactivestreams/Subscriber.h"
#include "org/reactivestreams/Subscription.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber_serialVersionUID -1185974347409665484LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsFlowableFlowableAmb

- (instancetype __nonnull)initWithOrgReactivestreamsPublisherArray:(IOSObjectArray *)sources
                                              withJavaLangIterable:(id<JavaLangIterable>)sourcesIterable {
  IoReactivexInternalOperatorsFlowableFlowableAmb_initWithOrgReactivestreamsPublisherArray_withJavaLangIterable_(self, sources, sourcesIterable);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  IOSObjectArray *sources = self->sources_;
  jint count = 0;
  if (sources == nil) {
    sources = [IOSObjectArray arrayWithLength:8 type:OrgReactivestreamsPublisher_class_()];
    @try {
      for (id<OrgReactivestreamsPublisher> __strong p in nil_chk(sourcesIterable_)) {
        if (p == nil) {
          IoReactivexInternalSubscriptionsEmptySubscription_errorWithJavaLangThrowable_withOrgReactivestreamsSubscriber_(create_JavaLangNullPointerException_initWithNSString_(@"One of the sources is null"), s);
          return;
        }
        if (count == sources->size_) {
          IOSObjectArray *b = [IOSObjectArray arrayWithLength:count + (JreRShift32(count, 2)) type:OrgReactivestreamsPublisher_class_()];
          JavaLangSystem_arraycopyWithId_withInt_withId_withInt_withInt_(sources, 0, b, 0, count);
          sources = b;
        }
        IOSObjectArray_Set(sources, count++, p);
      }
    }
    @catch (JavaLangThrowable *e) {
      IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(e);
      IoReactivexInternalSubscriptionsEmptySubscription_errorWithJavaLangThrowable_withOrgReactivestreamsSubscriber_(e, s);
      return;
    }
  }
  else {
    count = sources->size_;
  }
  if (count == 0) {
    IoReactivexInternalSubscriptionsEmptySubscription_completeWithOrgReactivestreamsSubscriber_(s);
    return;
  }
  else if (count == 1) {
    [((id<OrgReactivestreamsPublisher>) nil_chk(IOSObjectArray_Get(sources, 0))) subscribeWithOrgReactivestreamsSubscriber:s];
    return;
  }
  IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *ac = create_IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_initWithOrgReactivestreamsSubscriber_withInt_(s, count);
  [ac subscribeWithOrgReactivestreamsPublisherArray:sources];
}

- (void)dealloc {
  RELEASE_(sources_);
  RELEASE_(sourcesIterable_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsPublisherArray:withJavaLangIterable:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "sources_", "[LOrgReactivestreamsPublisher;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "sourcesIterable_", "LJavaLangIterable;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
  };
  static const void *ptrTable[] = { "[LOrgReactivestreamsPublisher;LJavaLangIterable;", "([Lorg/reactivestreams/Publisher<+TT;>;Ljava/lang/Iterable<+Lorg/reactivestreams/Publisher<+TT;>;>;)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "[Lorg/reactivestreams/Publisher<+TT;>;", "Ljava/lang/Iterable<+Lorg/reactivestreams/Publisher<+TT;>;>;", "LIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator;LIoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber;", "<T:Ljava/lang/Object;>Lio/reactivex/Flowable<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableAmb = { "FlowableAmb", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 2, 2, -1, 7, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableAmb;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableAmb_initWithOrgReactivestreamsPublisherArray_withJavaLangIterable_(IoReactivexInternalOperatorsFlowableFlowableAmb *self, IOSObjectArray *sources, id<JavaLangIterable> sourcesIterable) {
  IoReactivexFlowable_init(self);
  JreStrongAssign(&self->sources_, sources);
  JreStrongAssign(&self->sourcesIterable_, sourcesIterable);
}

IoReactivexInternalOperatorsFlowableFlowableAmb *new_IoReactivexInternalOperatorsFlowableFlowableAmb_initWithOrgReactivestreamsPublisherArray_withJavaLangIterable_(IOSObjectArray *sources, id<JavaLangIterable> sourcesIterable) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableAmb, initWithOrgReactivestreamsPublisherArray_withJavaLangIterable_, sources, sourcesIterable)
}

IoReactivexInternalOperatorsFlowableFlowableAmb *create_IoReactivexInternalOperatorsFlowableFlowableAmb_initWithOrgReactivestreamsPublisherArray_withJavaLangIterable_(IOSObjectArray *sources, id<JavaLangIterable> sourcesIterable) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableAmb, initWithOrgReactivestreamsPublisherArray_withJavaLangIterable_, sources, sourcesIterable)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableAmb)

@implementation IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                                                       withInt:(jint)count {
  IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_initWithOrgReactivestreamsSubscriber_withInt_(self, actual, count);
  return self;
}

- (void)subscribeWithOrgReactivestreamsPublisherArray:(IOSObjectArray *)sources {
  IOSObjectArray *as = subscribers_;
  jint len = ((IOSObjectArray *) nil_chk(as))->size_;
  for (jint i = 0; i < len; i++) {
    IOSObjectArray_SetAndConsume(as, i, new_IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_withInt_withOrgReactivestreamsSubscriber_(self, i + 1, downstream_));
  }
  [((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(winner_)) lazySetWithInt:0];
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onSubscribeWithOrgReactivestreamsSubscription:self];
  for (jint i = 0; i < len; i++) {
    if ([winner_ get] != 0) {
      return;
    }
    [((id<OrgReactivestreamsPublisher>) nil_chk(IOSObjectArray_Get(nil_chk(sources), i))) subscribeWithOrgReactivestreamsSubscriber:IOSObjectArray_Get(as, i)];
  }
}

- (void)requestWithLong:(jlong)n {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_validateWithLong_(n)) {
    jint w = [((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(winner_)) get];
    if (w > 0) {
      [((IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber *) nil_chk(IOSObjectArray_Get(nil_chk(subscribers_), w - 1))) requestWithLong:n];
    }
    else if (w == 0) {
      {
        IOSObjectArray *a__ = subscribers_;
        IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
        IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber * const *e__ = b__ + a__->size_;
        while (b__ < e__) {
          IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber *a = *b__++;
          [((IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber *) nil_chk(a)) requestWithLong:n];
        }
      }
    }
  }
}

- (jboolean)winWithInt:(jint)index {
  jint w = [((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(winner_)) get];
  if (w == 0) {
    if ([winner_ compareAndSetWithInt:0 withInt:index]) {
      IOSObjectArray *a = subscribers_;
      jint n = ((IOSObjectArray *) nil_chk(a))->size_;
      for (jint i = 0; i < n; i++) {
        if (i + 1 != index) {
          [((IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber *) nil_chk(IOSObjectArray_Get(a, i))) cancel];
        }
      }
      return true;
    }
  }
  return false;
}

- (void)cancel {
  if ([((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(winner_)) get] != -1) {
    [winner_ lazySetWithInt:-1];
    {
      IOSObjectArray *a__ = subscribers_;
      IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
      IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber * const *e__ = b__ + a__->size_;
      while (b__ < e__) {
        IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber *a = *b__++;
        [((IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber *) nil_chk(a)) cancel];
      }
    }
  }
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(subscribers_);
  RELEASE_(winner_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:withInt:);
  methods[1].selector = @selector(subscribeWithOrgReactivestreamsPublisherArray:);
  methods[2].selector = @selector(requestWithLong:);
  methods[3].selector = @selector(winWithInt:);
  methods[4].selector = @selector(cancel);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "downstream_", "LOrgReactivestreamsSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "subscribers_", "[LIoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "winner_", "LJavaUtilConcurrentAtomicAtomicInteger;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;I", "(Lorg/reactivestreams/Subscriber<-TT;>;I)V", "subscribe", "[LOrgReactivestreamsPublisher;", "([Lorg/reactivestreams/Publisher<+TT;>;)V", "request", "J", "win", "I", "Lorg/reactivestreams/Subscriber<-TT;>;", "[Lio/reactivex/internal/operators/flowable/FlowableAmb$AmbInnerSubscriber<TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableAmb;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Lorg/reactivestreams/Subscription;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator = { "AmbCoordinator", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 5, 3, 11, -1, -1, 12, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_initWithOrgReactivestreamsSubscriber_withInt_(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *self, id<OrgReactivestreamsSubscriber> actual, jint count) {
  NSObject_init(self);
  JreStrongAssignAndConsume(&self->winner_, new_JavaUtilConcurrentAtomicAtomicInteger_init());
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssignAndConsume(&self->subscribers_, [IOSObjectArray newArrayWithLength:count type:IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber_class_()]);
}

IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *new_IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_initWithOrgReactivestreamsSubscriber_withInt_(id<OrgReactivestreamsSubscriber> actual, jint count) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator, initWithOrgReactivestreamsSubscriber_withInt_, actual, count)
}

IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *create_IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_initWithOrgReactivestreamsSubscriber_withInt_(id<OrgReactivestreamsSubscriber> actual, jint count) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator, initWithOrgReactivestreamsSubscriber_withInt_, actual, count)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator)

@implementation IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator:(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *)parent
                                                                                         withInt:(jint)index
                                                                withOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream {
  IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_withInt_withOrgReactivestreamsSubscriber_(self, parent, index, downstream);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  IoReactivexInternalSubscriptionsSubscriptionHelper_deferredSetOnceWithJavaUtilConcurrentAtomicAtomicReference_withJavaUtilConcurrentAtomicAtomicLong_withOrgReactivestreamsSubscription_(self, missedRequested_, s);
}

- (void)requestWithLong:(jlong)n {
  IoReactivexInternalSubscriptionsSubscriptionHelper_deferredRequestWithJavaUtilConcurrentAtomicAtomicReference_withJavaUtilConcurrentAtomicAtomicLong_withLong_(self, missedRequested_, n);
}

- (void)onNextWithId:(id)t {
  if (won_) {
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:t];
  }
  else {
    if ([((IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *) nil_chk(parent_)) winWithInt:index_]) {
      won_ = true;
      [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:t];
    }
    else {
      [((id<OrgReactivestreamsSubscription>) nil_chk([self get])) cancel];
    }
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if (won_) {
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
  }
  else {
    if ([((IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *) nil_chk(parent_)) winWithInt:index_]) {
      won_ = true;
      [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
    }
    else {
      [((id<OrgReactivestreamsSubscription>) nil_chk([self get])) cancel];
      IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
    }
  }
}

- (void)onComplete {
  if (won_) {
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onComplete];
  }
  else {
    if ([((IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *) nil_chk(parent_)) winWithInt:index_]) {
      won_ = true;
      [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onComplete];
    }
    else {
      [((id<OrgReactivestreamsSubscription>) nil_chk([self get])) cancel];
    }
  }
}

- (void)cancel {
  IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(self);
}

- (void)dealloc {
  RELEASE_(parent_);
  RELEASE_(downstream_);
  RELEASE_(missedRequested_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, 8, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator:withInt:withOrgReactivestreamsSubscriber:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(requestWithLong:);
  methods[3].selector = @selector(onNextWithId:);
  methods[4].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[5].selector = @selector(onComplete);
  methods[6].selector = @selector(cancel);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "parent_", "LIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator;", .constantValue.asLong = 0, 0x10, -1, -1, 11, -1 },
    { "index_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "downstream_", "LOrgReactivestreamsSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 12, -1 },
    { "won_", "Z", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "missedRequested_", "LJavaUtilConcurrentAtomicAtomicLong;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator;ILOrgReactivestreamsSubscriber;", "(Lio/reactivex/internal/operators/flowable/FlowableAmb$AmbCoordinator<TT;>;ILorg/reactivestreams/Subscriber<-TT;>;)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "request", "J", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/internal/operators/flowable/FlowableAmb$AmbCoordinator<TT;>;", "Lorg/reactivestreams/Subscriber<-TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableAmb;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicReference<Lorg/reactivestreams/Subscription;>;Lio/reactivex/FlowableSubscriber<TT;>;Lorg/reactivestreams/Subscription;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber = { "AmbInnerSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 7, 6, 13, -1, -1, 14, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_withInt_withOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber *self, IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *parent, jint index, id<OrgReactivestreamsSubscriber> downstream) {
  JavaUtilConcurrentAtomicAtomicReference_init(self);
  JreStrongAssignAndConsume(&self->missedRequested_, new_JavaUtilConcurrentAtomicAtomicLong_init());
  JreStrongAssign(&self->parent_, parent);
  self->index_ = index;
  JreStrongAssign(&self->downstream_, downstream);
}

IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_withInt_withOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *parent, jint index, id<OrgReactivestreamsSubscriber> downstream) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber, initWithIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_withInt_withOrgReactivestreamsSubscriber_, parent, index, downstream)
}

IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_withInt_withOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator *parent, jint index, id<OrgReactivestreamsSubscriber> downstream) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber, initWithIoReactivexInternalOperatorsFlowableFlowableAmb_AmbCoordinator_withInt_withOrgReactivestreamsSubscriber_, parent, index, downstream)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableAmb_AmbInnerSubscriber)
