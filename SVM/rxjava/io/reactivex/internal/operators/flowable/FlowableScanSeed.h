//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableScanSeed.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableScanSeed")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableScanSeed
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableScanSeed 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableScanSeed 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableScanSeed

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableScanSeed_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableScanSeed || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableScanSeed))
#define IoReactivexInternalOperatorsFlowableFlowableScanSeed_

#define RESTRICT_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"

@class IoReactivexFlowable;
@protocol IoReactivexFunctionsBiFunction;
@protocol JavaUtilConcurrentCallable;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableScanSeed : IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream {
 @public
  id<IoReactivexFunctionsBiFunction> accumulator_;
  id<JavaUtilConcurrentCallable> seedSupplier_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                       withJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)seedSupplier
                   withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)accumulator;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableScanSeed)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableScanSeed, accumulator_, id<IoReactivexFunctionsBiFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableScanSeed, seedSupplier_, id<JavaUtilConcurrentCallable>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableScanSeed_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsFlowableFlowableScanSeed *self, IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> seedSupplier, id<IoReactivexFunctionsBiFunction> accumulator);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableScanSeed *new_IoReactivexInternalOperatorsFlowableFlowableScanSeed_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withIoReactivexFunctionsBiFunction_(IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> seedSupplier, id<IoReactivexFunctionsBiFunction> accumulator) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableScanSeed *create_IoReactivexInternalOperatorsFlowableFlowableScanSeed_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withIoReactivexFunctionsBiFunction_(IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> seedSupplier, id<IoReactivexFunctionsBiFunction> accumulator);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableScanSeed)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableScanSeed || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicInteger 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicInteger 1
#include "java/util/concurrent/atomic/AtomicInteger.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_OrgReactivestreamsSubscription 1
#define INCLUDE_OrgReactivestreamsSubscription 1
#include "org/reactivestreams/Subscription.h"

@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicLong;
@protocol IoReactivexFunctionsBiFunction;
@protocol IoReactivexInternalFuseableSimplePlainQueue;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber : JavaUtilConcurrentAtomicAtomicInteger < IoReactivexFlowableSubscriber, OrgReactivestreamsSubscription > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  id<IoReactivexFunctionsBiFunction> accumulator_;
  id<IoReactivexInternalFuseableSimplePlainQueue> queue_;
  JavaUtilConcurrentAtomicAtomicLong *requested_;
  jint prefetch_;
  jint limit_;
  volatile_jboolean cancelled_;
  volatile_jboolean done_;
  JavaLangThrowable *error_;
  id<OrgReactivestreamsSubscription> upstream_;
  id value_ScanSeedSubscriber_;
  jint consumed_;
}

#pragma mark Public

- (void)cancel;

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (void)requestWithLong:(jlong)n;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                            withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)accumulator
                                                        withId:(id)value
                                                       withInt:(jint)prefetch;

- (void)drain;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber, accumulator_, id<IoReactivexFunctionsBiFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber, queue_, id<IoReactivexInternalFuseableSimplePlainQueue>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber, requested_, JavaUtilConcurrentAtomicAtomicLong *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber, error_, JavaLangThrowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber, upstream_, id<OrgReactivestreamsSubscription>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber, value_ScanSeedSubscriber_, id)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsBiFunction_withId_withInt_(IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber *self, id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsBiFunction> accumulator, id value, jint prefetch);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsBiFunction_withId_withInt_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsBiFunction> accumulator, id value, jint prefetch) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsBiFunction_withId_withInt_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsBiFunction> accumulator, id value, jint prefetch);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableScanSeed_ScanSeedSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableScanSeed")
