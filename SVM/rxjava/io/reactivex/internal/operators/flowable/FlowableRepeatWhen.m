//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableRepeatWhen.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Function.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"
#include "io/reactivex/internal/operators/flowable/FlowableRepeatWhen.h"
#include "io/reactivex/internal/subscriptions/EmptySubscription.h"
#include "io/reactivex/internal/subscriptions/SubscriptionArbiter.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/processors/FlowableProcessor.h"
#include "io/reactivex/processors/UnicastProcessor.h"
#include "io/reactivex/subscribers/SerializedSubscriber.h"
#include "java/lang/Integer.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"
#include "java/util/concurrent/atomic/AtomicLong.h"
#include "java/util/concurrent/atomic/AtomicReference.h"
#include "org/reactivestreams/Publisher.h"
#include "org/reactivestreams/Subscriber.h"
#include "org/reactivestreams/Subscription.h"

#pragma clang diagnostic ignored "-Wprotocol"

inline jlong IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver_serialVersionUID 2827772011130406689LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver, serialVersionUID, jlong)

@interface IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber () {
 @public
  jlong produced_;
}

@end

inline jlong IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber_serialVersionUID -5604623027276966720LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber, serialVersionUID, jlong)

__attribute__((unused)) static void IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber_againWithId_(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber *self, id signal);

inline jlong IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber_serialVersionUID -2680129890138081029LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsFlowableFlowableRepeatWhen

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)handler {
  IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_(self, source, handler);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  IoReactivexSubscribersSerializedSubscriber *z = create_IoReactivexSubscribersSerializedSubscriber_initWithOrgReactivestreamsSubscriber_(s);
  IoReactivexProcessorsFlowableProcessor *processor = [((IoReactivexProcessorsUnicastProcessor *) nil_chk(IoReactivexProcessorsUnicastProcessor_createWithInt_(8))) toSerialized];
  id<OrgReactivestreamsPublisher> when;
  @try {
    when = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(handler_)) applyWithId:processor], @"handler returned a null Publisher");
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    IoReactivexInternalSubscriptionsEmptySubscription_errorWithJavaLangThrowable_withOrgReactivestreamsSubscriber_(ex, s);
    return;
  }
  IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver *receiver = create_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver_initWithOrgReactivestreamsPublisher_(source_);
  IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber *subscriber = create_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsFlowableProcessor_withOrgReactivestreamsSubscription_(z, processor, receiver);
  JreStrongAssign(&receiver->subscriber_, subscriber);
  [((id<OrgReactivestreamsSubscriber>) nil_chk(s)) onSubscribeWithOrgReactivestreamsSubscription:subscriber];
  [((id<OrgReactivestreamsPublisher>) nil_chk(when)) subscribeWithOrgReactivestreamsSubscriber:receiver];
  [receiver onNextWithId:JavaLangInteger_valueOfWithInt_(0)];
}

- (void)dealloc {
  RELEASE_(handler_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFlowable:withIoReactivexFunctionsFunction:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "handler_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexFlowable;LIoReactivexFunctionsFunction;", "(Lio/reactivex/Flowable<TT;>;Lio/reactivex/functions/Function<-Lio/reactivex/Flowable<Ljava/lang/Object;>;+Lorg/reactivestreams/Publisher<*>;>;)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "Lio/reactivex/functions/Function<-Lio/reactivex/Flowable<Ljava/lang/Object;>;+Lorg/reactivestreams/Publisher<*>;>;", "LIoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver;LIoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber;LIoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableRepeatWhen = { "FlowableRepeatWhen", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen *self, IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> handler) {
  IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream_initWithIoReactivexFlowable_(self, source);
  JreStrongAssign(&self->handler_, handler);
}

IoReactivexInternalOperatorsFlowableFlowableRepeatWhen *new_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_(IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> handler) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen, initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_, source, handler)
}

IoReactivexInternalOperatorsFlowableFlowableRepeatWhen *create_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_(IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> handler) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen, initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_, source, handler)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen)

@implementation IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver

- (instancetype __nonnull)initWithOrgReactivestreamsPublisher:(id<OrgReactivestreamsPublisher>)source {
  IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver_initWithOrgReactivestreamsPublisher_(self, source);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  IoReactivexInternalSubscriptionsSubscriptionHelper_deferredSetOnceWithJavaUtilConcurrentAtomicAtomicReference_withJavaUtilConcurrentAtomicAtomicLong_withOrgReactivestreamsSubscription_(upstream_, requested_, s);
}

- (void)onNextWithId:(id)t {
  if ([self getAndIncrement] == 0) {
    for (; ; ) {
      if ([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(upstream_)) get] == JreLoadEnum(IoReactivexInternalSubscriptionsSubscriptionHelper, CANCELLED)) {
        return;
      }
      [((id<OrgReactivestreamsPublisher>) nil_chk(source_)) subscribeWithOrgReactivestreamsSubscriber:subscriber_];
      if ([self decrementAndGet] == 0) {
        break;
      }
    }
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  [((IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber *) nil_chk(subscriber_)) cancel];
  [((id<OrgReactivestreamsSubscriber>) nil_chk(((IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber *) nil_chk(subscriber_))->downstream_)) onErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  [((IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber *) nil_chk(subscriber_)) cancel];
  [((id<OrgReactivestreamsSubscriber>) nil_chk(((IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber *) nil_chk(subscriber_))->downstream_)) onComplete];
}

- (void)requestWithLong:(jlong)n {
  IoReactivexInternalSubscriptionsSubscriptionHelper_deferredRequestWithJavaUtilConcurrentAtomicAtomicReference_withJavaUtilConcurrentAtomicAtomicLong_withLong_(upstream_, requested_, n);
}

- (void)cancel {
  IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(source_);
  RELEASE_(upstream_);
  RELEASE_(requested_);
  RELEASE_(subscriber_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsPublisher:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(requestWithLong:);
  methods[6].selector = @selector(cancel);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "source_", "LOrgReactivestreamsPublisher;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "upstream_", "LJavaUtilConcurrentAtomicAtomicReference;", .constantValue.asLong = 0, 0x10, -1, -1, 11, -1 },
    { "requested_", "LJavaUtilConcurrentAtomicAtomicLong;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "subscriber_", "LIoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber;", .constantValue.asLong = 0, 0x0, -1, -1, 12, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsPublisher;", "(Lorg/reactivestreams/Publisher<TT;>;)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "onError", "LJavaLangThrowable;", "request", "J", "Lorg/reactivestreams/Publisher<TT;>;", "Ljava/util/concurrent/atomic/AtomicReference<Lorg/reactivestreams/Subscription;>;", "Lio/reactivex/internal/operators/flowable/FlowableRepeatWhen$WhenSourceSubscriber<TT;TU;>;", "LIoReactivexInternalOperatorsFlowableFlowableRepeatWhen;", "<T:Ljava/lang/Object;U:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/FlowableSubscriber<Ljava/lang/Object;>;Lorg/reactivestreams/Subscription;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver = { "WhenReceiver", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 7, 5, 13, -1, -1, 14, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver_initWithOrgReactivestreamsPublisher_(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver *self, id<OrgReactivestreamsPublisher> source) {
  JavaUtilConcurrentAtomicAtomicInteger_init(self);
  JreStrongAssign(&self->source_, source);
  JreStrongAssignAndConsume(&self->upstream_, new_JavaUtilConcurrentAtomicAtomicReference_init());
  JreStrongAssignAndConsume(&self->requested_, new_JavaUtilConcurrentAtomicAtomicLong_init());
}

IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver *new_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver_initWithOrgReactivestreamsPublisher_(id<OrgReactivestreamsPublisher> source) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver, initWithOrgReactivestreamsPublisher_, source)
}

IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver *create_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver_initWithOrgReactivestreamsPublisher_(id<OrgReactivestreamsPublisher> source) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver, initWithOrgReactivestreamsPublisher_, source)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenReceiver)

@implementation IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                    withIoReactivexProcessorsFlowableProcessor:(IoReactivexProcessorsFlowableProcessor *)processor
                            withOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)receiver {
  IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsFlowableProcessor_withOrgReactivestreamsSubscription_(self, actual, processor, receiver);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  [self setSubscriptionWithOrgReactivestreamsSubscription:s];
}

- (void)onNextWithId:(id)t {
  produced_++;
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:t];
}

- (void)againWithId:(id)signal {
  IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber_againWithId_(self, signal);
}

- (void)cancel {
  [super cancel];
  [((id<OrgReactivestreamsSubscription>) nil_chk(receiver_)) cancel];
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(processor_);
  RELEASE_(receiver_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x11, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x11, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x14, 7, 5, -1, 8, -1, -1 },
    { NULL, "V", 0x11, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:withIoReactivexProcessorsFlowableProcessor:withOrgReactivestreamsSubscription:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(againWithId:);
  methods[4].selector = @selector(cancel);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LOrgReactivestreamsSubscriber;", .constantValue.asLong = 0, 0x14, -1, -1, 9, -1 },
    { "processor_", "LIoReactivexProcessorsFlowableProcessor;", .constantValue.asLong = 0, 0x14, -1, -1, 10, -1 },
    { "receiver_", "LOrgReactivestreamsSubscription;", .constantValue.asLong = 0, 0x14, -1, -1, -1, -1 },
    { "produced_", "J", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;LIoReactivexProcessorsFlowableProcessor;LOrgReactivestreamsSubscription;", "(Lorg/reactivestreams/Subscriber<-TT;>;Lio/reactivex/processors/FlowableProcessor<TU;>;Lorg/reactivestreams/Subscription;)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "(TT;)V", "again", "(TU;)V", "Lorg/reactivestreams/Subscriber<-TT;>;", "Lio/reactivex/processors/FlowableProcessor<TU;>;", "LIoReactivexInternalOperatorsFlowableFlowableRepeatWhen;", "<T:Ljava/lang/Object;U:Ljava/lang/Object;>Lio/reactivex/internal/subscriptions/SubscriptionArbiter;Lio/reactivex/FlowableSubscriber<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber = { "WhenSourceSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x408, 5, 5, 11, -1, -1, 12, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsFlowableProcessor_withOrgReactivestreamsSubscription_(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber *self, id<OrgReactivestreamsSubscriber> actual, IoReactivexProcessorsFlowableProcessor *processor, id<OrgReactivestreamsSubscription> receiver) {
  IoReactivexInternalSubscriptionsSubscriptionArbiter_initWithBoolean_(self, false);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->processor_, processor);
  JreStrongAssign(&self->receiver_, receiver);
}

void IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber_againWithId_(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber *self, id signal) {
  [self setSubscriptionWithOrgReactivestreamsSubscription:JreLoadEnum(IoReactivexInternalSubscriptionsEmptySubscription, INSTANCE)];
  jlong p = self->produced_;
  if (p != 0LL) {
    self->produced_ = 0LL;
    [self producedWithLong:p];
  }
  [((id<OrgReactivestreamsSubscription>) nil_chk(self->receiver_)) requestWithLong:1];
  [((IoReactivexProcessorsFlowableProcessor *) nil_chk(self->processor_)) onNextWithId:signal];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber)

@implementation IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                    withIoReactivexProcessorsFlowableProcessor:(IoReactivexProcessorsFlowableProcessor *)processor
                            withOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)receiver {
  IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsFlowableProcessor_withOrgReactivestreamsSubscription_(self, actual, processor, receiver);
  return self;
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  [((id<OrgReactivestreamsSubscription>) nil_chk(receiver_)) cancel];
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber_againWithId_(self, JavaLangInteger_valueOfWithInt_(0));
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:withIoReactivexProcessorsFlowableProcessor:withOrgReactivestreamsSubscription:);
  methods[1].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[2].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;LIoReactivexProcessorsFlowableProcessor;LOrgReactivestreamsSubscription;", "(Lorg/reactivestreams/Subscriber<-TT;>;Lio/reactivex/processors/FlowableProcessor<Ljava/lang/Object;>;Lorg/reactivestreams/Subscription;)V", "onError", "LJavaLangThrowable;", "LIoReactivexInternalOperatorsFlowableFlowableRepeatWhen;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/flowable/FlowableRepeatWhen$WhenSourceSubscriber<TT;Ljava/lang/Object;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber = { "RepeatWhenSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 3, 1, 4, -1, -1, 5, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsFlowableProcessor_withOrgReactivestreamsSubscription_(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber *self, id<OrgReactivestreamsSubscriber> actual, IoReactivexProcessorsFlowableProcessor *processor, id<OrgReactivestreamsSubscription> receiver) {
  IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_WhenSourceSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsFlowableProcessor_withOrgReactivestreamsSubscription_(self, actual, processor, receiver);
}

IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsFlowableProcessor_withOrgReactivestreamsSubscription_(id<OrgReactivestreamsSubscriber> actual, IoReactivexProcessorsFlowableProcessor *processor, id<OrgReactivestreamsSubscription> receiver) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber, initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsFlowableProcessor_withOrgReactivestreamsSubscription_, actual, processor, receiver)
}

IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsFlowableProcessor_withOrgReactivestreamsSubscription_(id<OrgReactivestreamsSubscriber> actual, IoReactivexProcessorsFlowableProcessor *processor, id<OrgReactivestreamsSubscription> receiver) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber, initWithOrgReactivestreamsSubscriber_withIoReactivexProcessorsFlowableProcessor_withOrgReactivestreamsSubscription_, actual, processor, receiver)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableRepeatWhen_RepeatWhenSubscriber)
