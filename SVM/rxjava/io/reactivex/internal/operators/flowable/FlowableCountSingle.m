//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableCountSingle.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/Single.h"
#include "io/reactivex/SingleObserver.h"
#include "io/reactivex/internal/operators/flowable/FlowableCount.h"
#include "io/reactivex/internal/operators/flowable/FlowableCountSingle.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Long.h"
#include "java/lang/Throwable.h"
#include "org/reactivestreams/Subscription.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation IoReactivexInternalOperatorsFlowableFlowableCountSingle

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source {
  IoReactivexInternalOperatorsFlowableFlowableCountSingle_initWithIoReactivexFlowable_(self, source);
  return self;
}

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer {
  [((IoReactivexFlowable *) nil_chk(source_)) subscribeWithIoReactivexFlowableSubscriber:create_IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber_initWithIoReactivexSingleObserver_(observer)];
}

- (IoReactivexFlowable *)fuseToFlowable {
  return IoReactivexPluginsRxJavaPlugins_onAssemblyWithIoReactivexFlowable_(create_IoReactivexInternalOperatorsFlowableFlowableCount_initWithIoReactivexFlowable_(source_));
}

- (void)dealloc {
  RELEASE_(source_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
    { NULL, "LIoReactivexFlowable;", 0x1, -1, -1, -1, 5, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFlowable:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexSingleObserver:);
  methods[2].selector = @selector(fuseToFlowable);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexFlowable;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexFlowable;", "(Lio/reactivex/Flowable<TT;>;)V", "subscribeActual", "LIoReactivexSingleObserver;", "(Lio/reactivex/SingleObserver<-Ljava/lang/Long;>;)V", "()Lio/reactivex/Flowable<Ljava/lang/Long;>;", "Lio/reactivex/Flowable<TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber;", "<T:Ljava/lang/Object;>Lio/reactivex/Single<Ljava/lang/Long;>;Lio/reactivex/internal/fuseable/FuseToFlowable<Ljava/lang/Long;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableCountSingle = { "FlowableCountSingle", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 3, 1, -1, 7, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableCountSingle;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableCountSingle_initWithIoReactivexFlowable_(IoReactivexInternalOperatorsFlowableFlowableCountSingle *self, IoReactivexFlowable *source) {
  IoReactivexSingle_init(self);
  JreStrongAssign(&self->source_, source);
}

IoReactivexInternalOperatorsFlowableFlowableCountSingle *new_IoReactivexInternalOperatorsFlowableFlowableCountSingle_initWithIoReactivexFlowable_(IoReactivexFlowable *source) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableCountSingle, initWithIoReactivexFlowable_, source)
}

IoReactivexInternalOperatorsFlowableFlowableCountSingle *create_IoReactivexInternalOperatorsFlowableFlowableCountSingle_initWithIoReactivexFlowable_(IoReactivexFlowable *source) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableCountSingle, initWithIoReactivexFlowable_, source)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableCountSingle)

@implementation IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber

- (instancetype __nonnull)initWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)downstream {
  IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber_initWithIoReactivexSingleObserver_(self, downstream);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_validateWithOrgReactivestreamsSubscription_withOrgReactivestreamsSubscription_(self->upstream_, s)) {
    JreStrongAssign(&self->upstream_, s);
    [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
    [((id<OrgReactivestreamsSubscription>) nil_chk(s)) requestWithLong:JavaLangLong_MAX_VALUE];
  }
}

- (void)onNextWithId:(id)t {
  count_++;
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  JreStrongAssign(&upstream_, JreLoadEnum(IoReactivexInternalSubscriptionsSubscriptionHelper, CANCELLED));
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  JreStrongAssign(&upstream_, JreLoadEnum(IoReactivexInternalSubscriptionsSubscriptionHelper, CANCELLED));
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSuccessWithId:JavaLangLong_valueOfWithLong_(count_)];
}

- (void)dispose {
  [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
  JreStrongAssign(&upstream_, JreLoadEnum(IoReactivexInternalSubscriptionsSubscriptionHelper, CANCELLED));
}

- (jboolean)isDisposed {
  return upstream_ == JreLoadEnum(IoReactivexInternalSubscriptionsSubscriptionHelper, CANCELLED);
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexSingleObserver:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(dispose);
  methods[6].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "downstream_", "LIoReactivexSingleObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 8, -1 },
    { "upstream_", "LOrgReactivestreamsSubscription;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "count_", "J", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexSingleObserver;", "(Lio/reactivex/SingleObserver<-Ljava/lang/Long;>;)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "onError", "LJavaLangThrowable;", "Lio/reactivex/SingleObserver<-Ljava/lang/Long;>;", "LIoReactivexInternalOperatorsFlowableFlowableCountSingle;", "Ljava/lang/Object;Lio/reactivex/FlowableSubscriber<Ljava/lang/Object;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber = { "CountSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 7, 3, 9, -1, -1, 10, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber_initWithIoReactivexSingleObserver_(IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber *self, id<IoReactivexSingleObserver> downstream) {
  NSObject_init(self);
  JreStrongAssign(&self->downstream_, downstream);
}

IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber_initWithIoReactivexSingleObserver_(id<IoReactivexSingleObserver> downstream) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber, initWithIoReactivexSingleObserver_, downstream)
}

IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber_initWithIoReactivexSingleObserver_(id<IoReactivexSingleObserver> downstream) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber, initWithIoReactivexSingleObserver_, downstream)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber)
