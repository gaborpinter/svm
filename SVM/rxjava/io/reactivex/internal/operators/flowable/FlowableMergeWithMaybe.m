//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableMergeWithMaybe.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/MaybeSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/fuseable/SimplePlainQueue.h"
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"
#include "io/reactivex/internal/operators/flowable/FlowableMergeWithMaybe.h"
#include "io/reactivex/internal/queue/SpscArrayQueue.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/internal/util/AtomicThrowable.h"
#include "io/reactivex/internal/util/BackpressureHelper.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"
#include "java/util/concurrent/atomic/AtomicLong.h"
#include "java/util/concurrent/atomic/AtomicReference.h"
#include "org/reactivestreams/Subscriber.h"
#include "org/reactivestreams/Subscription.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_serialVersionUID -4592979584110982903LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver, serialVersionUID, jlong)

inline jlong IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver_serialVersionUID -2935427570954647017LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                           withIoReactivexMaybeSource:(id<IoReactivexMaybeSource>)other {
  IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_initWithIoReactivexFlowable_withIoReactivexMaybeSource_(self, source, other);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)subscriber {
  IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *parent = create_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_initWithOrgReactivestreamsSubscriber_(subscriber);
  [((id<OrgReactivestreamsSubscriber>) nil_chk(subscriber)) onSubscribeWithOrgReactivestreamsSubscription:parent];
  [((IoReactivexFlowable *) nil_chk(source_)) subscribeWithIoReactivexFlowableSubscriber:parent];
  [((id<IoReactivexMaybeSource>) nil_chk(other_)) subscribeWithIoReactivexMaybeObserver:parent->otherObserver_];
}

- (void)dealloc {
  RELEASE_(other_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFlowable:withIoReactivexMaybeSource:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "other_", "LIoReactivexMaybeSource;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexFlowable;LIoReactivexMaybeSource;", "(Lio/reactivex/Flowable<TT;>;Lio/reactivex/MaybeSource<+TT;>;)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "Lio/reactivex/MaybeSource<+TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe = { "FlowableMergeWithMaybe", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_initWithIoReactivexFlowable_withIoReactivexMaybeSource_(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe *self, IoReactivexFlowable *source, id<IoReactivexMaybeSource> other) {
  IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream_initWithIoReactivexFlowable_(self, source);
  JreStrongAssign(&self->other_, other);
}

IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe *new_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_initWithIoReactivexFlowable_withIoReactivexMaybeSource_(IoReactivexFlowable *source, id<IoReactivexMaybeSource> other) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe, initWithIoReactivexFlowable_withIoReactivexMaybeSource_, source, other)
}

IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe *create_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_initWithIoReactivexFlowable_withIoReactivexMaybeSource_(IoReactivexFlowable *source, id<IoReactivexMaybeSource> other) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe, initWithIoReactivexFlowable_withIoReactivexMaybeSource_, source, other)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe)

@implementation IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver

+ (jint)OTHER_STATE_HAS_VALUE {
  return IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_HAS_VALUE;
}

+ (jint)OTHER_STATE_CONSUMED_OR_EMPTY {
  return IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_CONSUMED_OR_EMPTY;
}

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream {
  IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_initWithOrgReactivestreamsSubscriber_(self, downstream);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  IoReactivexInternalSubscriptionsSubscriptionHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withOrgReactivestreamsSubscription_withLong_(mainSubscription_, s, prefetch_);
}

- (void)onNextWithId:(id)t {
  if ([self compareAndSetWithInt:0 withInt:1]) {
    jlong e = emitted_;
    if ([((JavaUtilConcurrentAtomicAtomicLong *) nil_chk(requested_)) get] != e) {
      id<IoReactivexInternalFuseableSimplePlainQueue> q = JreLoadVolatileId(&queue_);
      if (q == nil || [q isEmpty]) {
        emitted_ = e + 1;
        [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:t];
        jint c = consumed_ + 1;
        if (c == limit_) {
          consumed_ = 0;
          [((id<OrgReactivestreamsSubscription>) nil_chk([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(mainSubscription_)) get])) requestWithLong:c];
        }
        else {
          consumed_ = c;
        }
      }
      else {
        [q offerWithId:t];
      }
    }
    else {
      id<IoReactivexInternalFuseableSimplePlainQueue> q = [self getOrCreateQueue];
      [((id<IoReactivexInternalFuseableSimplePlainQueue>) nil_chk(q)) offerWithId:t];
    }
    if ([self decrementAndGet] == 0) {
      return;
    }
  }
  else {
    id<IoReactivexInternalFuseableSimplePlainQueue> q = [self getOrCreateQueue];
    [((id<IoReactivexInternalFuseableSimplePlainQueue>) nil_chk(q)) offerWithId:t];
    if ([self getAndIncrement] != 0) {
      return;
    }
  }
  [self drainLoop];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)ex {
  if ([((IoReactivexInternalUtilAtomicThrowable *) nil_chk(error_)) addThrowableWithJavaLangThrowable:ex]) {
    IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(mainSubscription_);
    [self drain];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
  }
}

- (void)onComplete {
  JreAssignVolatileBoolean(&mainDone_, true);
  [self drain];
}

- (void)requestWithLong:(jlong)n {
  IoReactivexInternalUtilBackpressureHelper_addWithJavaUtilConcurrentAtomicAtomicLong_withLong_(requested_, n);
  [self drain];
}

- (void)cancel {
  JreAssignVolatileBoolean(&cancelled_, true);
  IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(mainSubscription_);
  IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(otherObserver_);
  if ([self getAndIncrement] == 0) {
    JreVolatileStrongAssign(&queue_, nil);
    JreStrongAssign(&singleItem_, nil);
  }
}

- (void)otherSuccessWithId:(id)value {
  if ([self compareAndSetWithInt:0 withInt:1]) {
    jlong e = emitted_;
    if ([((JavaUtilConcurrentAtomicAtomicLong *) nil_chk(requested_)) get] != e) {
      emitted_ = e + 1;
      [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:value];
      JreAssignVolatileInt(&otherState_, IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_CONSUMED_OR_EMPTY);
    }
    else {
      JreStrongAssign(&singleItem_, value);
      JreAssignVolatileInt(&otherState_, IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_HAS_VALUE);
      if ([self decrementAndGet] == 0) {
        return;
      }
    }
  }
  else {
    JreStrongAssign(&singleItem_, value);
    JreAssignVolatileInt(&otherState_, IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_HAS_VALUE);
    if ([self getAndIncrement] != 0) {
      return;
    }
  }
  [self drainLoop];
}

- (void)otherErrorWithJavaLangThrowable:(JavaLangThrowable *)ex {
  if ([((IoReactivexInternalUtilAtomicThrowable *) nil_chk(error_)) addThrowableWithJavaLangThrowable:ex]) {
    IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(mainSubscription_);
    [self drain];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
  }
}

- (void)otherComplete {
  JreAssignVolatileInt(&otherState_, IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_CONSUMED_OR_EMPTY);
  [self drain];
}

- (id<IoReactivexInternalFuseableSimplePlainQueue>)getOrCreateQueue {
  id<IoReactivexInternalFuseableSimplePlainQueue> q = JreLoadVolatileId(&queue_);
  if (q == nil) {
    q = create_IoReactivexInternalQueueSpscArrayQueue_initWithInt_(IoReactivexFlowable_bufferSize());
    JreVolatileStrongAssign(&queue_, q);
  }
  return q;
}

- (void)drain {
  if ([self getAndIncrement] == 0) {
    [self drainLoop];
  }
}

- (void)drainLoop {
  id<OrgReactivestreamsSubscriber> actual = self->downstream_;
  jint missed = 1;
  jlong e = emitted_;
  jint c = consumed_;
  jint lim = limit_;
  for (; ; ) {
    jlong r = [((JavaUtilConcurrentAtomicAtomicLong *) nil_chk(requested_)) get];
    while (e != r) {
      if (JreLoadVolatileBoolean(&cancelled_)) {
        JreStrongAssign(&singleItem_, nil);
        JreVolatileStrongAssign(&queue_, nil);
        return;
      }
      if ([((IoReactivexInternalUtilAtomicThrowable *) nil_chk(error_)) get] != nil) {
        JreStrongAssign(&singleItem_, nil);
        JreVolatileStrongAssign(&queue_, nil);
        [((id<OrgReactivestreamsSubscriber>) nil_chk(actual)) onErrorWithJavaLangThrowable:[error_ terminate]];
        return;
      }
      jint os = JreLoadVolatileInt(&otherState_);
      if (os == IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_HAS_VALUE) {
        id v = singleItem_;
        JreStrongAssign(&singleItem_, nil);
        JreAssignVolatileInt(&otherState_, IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_CONSUMED_OR_EMPTY);
        os = IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_CONSUMED_OR_EMPTY;
        [((id<OrgReactivestreamsSubscriber>) nil_chk(actual)) onNextWithId:v];
        e++;
        continue;
      }
      jboolean d = JreLoadVolatileBoolean(&mainDone_);
      id<IoReactivexInternalFuseableSimplePlainQueue> q = JreLoadVolatileId(&queue_);
      id v = q != nil ? [q poll] : nil;
      jboolean empty = v == nil;
      if (d && empty && os == IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_CONSUMED_OR_EMPTY) {
        JreVolatileStrongAssign(&queue_, nil);
        [((id<OrgReactivestreamsSubscriber>) nil_chk(actual)) onComplete];
        return;
      }
      if (empty) {
        break;
      }
      [((id<OrgReactivestreamsSubscriber>) nil_chk(actual)) onNextWithId:v];
      e++;
      if (++c == lim) {
        c = 0;
        [((id<OrgReactivestreamsSubscription>) nil_chk([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(mainSubscription_)) get])) requestWithLong:lim];
      }
    }
    if (e == r) {
      if (JreLoadVolatileBoolean(&cancelled_)) {
        JreStrongAssign(&singleItem_, nil);
        JreVolatileStrongAssign(&queue_, nil);
        return;
      }
      if ([((IoReactivexInternalUtilAtomicThrowable *) nil_chk(error_)) get] != nil) {
        JreStrongAssign(&singleItem_, nil);
        JreVolatileStrongAssign(&queue_, nil);
        [((id<OrgReactivestreamsSubscriber>) nil_chk(actual)) onErrorWithJavaLangThrowable:[error_ terminate]];
        return;
      }
      jboolean d = JreLoadVolatileBoolean(&mainDone_);
      id<IoReactivexInternalFuseableSimplePlainQueue> q = JreLoadVolatileId(&queue_);
      jboolean empty = q == nil || [q isEmpty];
      if (d && empty && JreLoadVolatileInt(&otherState_) == 2) {
        JreVolatileStrongAssign(&queue_, nil);
        [((id<OrgReactivestreamsSubscriber>) nil_chk(actual)) onComplete];
        return;
      }
    }
    emitted_ = e;
    consumed_ = c;
    missed = [self addAndGetWithInt:-missed];
    if (missed == 0) {
      break;
    }
  }
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)__javaClone:(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *)original {
  [super __javaClone:original];
  JreCloneVolatileStrong(&queue_, &original->queue_);
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(mainSubscription_);
  RELEASE_(otherObserver_);
  RELEASE_(error_);
  RELEASE_(requested_);
  JreReleaseVolatile(&queue_);
  RELEASE_(singleItem_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, 11, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x0, 12, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "LIoReactivexInternalFuseableSimplePlainQueue;", 0x0, -1, -1, -1, 13, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(requestWithLong:);
  methods[6].selector = @selector(cancel);
  methods[7].selector = @selector(otherSuccessWithId:);
  methods[8].selector = @selector(otherErrorWithJavaLangThrowable:);
  methods[9].selector = @selector(otherComplete);
  methods[10].selector = @selector(getOrCreateQueue);
  methods[11].selector = @selector(drain);
  methods[12].selector = @selector(drainLoop);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LOrgReactivestreamsSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 14, -1 },
    { "mainSubscription_", "LJavaUtilConcurrentAtomicAtomicReference;", .constantValue.asLong = 0, 0x10, -1, -1, 15, -1 },
    { "otherObserver_", "LIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 16, -1 },
    { "error_", "LIoReactivexInternalUtilAtomicThrowable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "requested_", "LJavaUtilConcurrentAtomicAtomicLong;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "prefetch_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "limit_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "queue_", "LIoReactivexInternalFuseableSimplePlainQueue;", .constantValue.asLong = 0, 0x40, -1, -1, 17, -1 },
    { "singleItem_", "LNSObject;", .constantValue.asLong = 0, 0x0, -1, -1, 18, -1 },
    { "cancelled_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "mainDone_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "otherState_", "I", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "emitted_", "J", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "consumed_", "I", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "OTHER_STATE_HAS_VALUE", "I", .constantValue.asInt = IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_HAS_VALUE, 0x18, -1, -1, -1, -1 },
    { "OTHER_STATE_CONSUMED_OR_EMPTY", "I", .constantValue.asInt = IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OTHER_STATE_CONSUMED_OR_EMPTY, 0x18, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "request", "J", "otherSuccess", "otherError", "()Lio/reactivex/internal/fuseable/SimplePlainQueue<TT;>;", "Lorg/reactivestreams/Subscriber<-TT;>;", "Ljava/util/concurrent/atomic/AtomicReference<Lorg/reactivestreams/Subscription;>;", "Lio/reactivex/internal/operators/flowable/FlowableMergeWithMaybe$MergeWithObserver$OtherObserver<TT;>;", "Lio/reactivex/internal/fuseable/SimplePlainQueue<TT;>;", "TT;", "LIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe;", "LIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/FlowableSubscriber<TT;>;Lorg/reactivestreams/Subscription;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver = { "MergeWithObserver", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 13, 17, 19, 20, -1, 21, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *self, id<OrgReactivestreamsSubscriber> downstream) {
  JavaUtilConcurrentAtomicAtomicInteger_init(self);
  JreStrongAssign(&self->downstream_, downstream);
  JreStrongAssignAndConsume(&self->mainSubscription_, new_JavaUtilConcurrentAtomicAtomicReference_init());
  JreStrongAssignAndConsume(&self->otherObserver_, new_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver_initWithIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_(self));
  JreStrongAssignAndConsume(&self->error_, new_IoReactivexInternalUtilAtomicThrowable_init());
  JreStrongAssignAndConsume(&self->requested_, new_JavaUtilConcurrentAtomicAtomicLong_init());
  self->prefetch_ = IoReactivexFlowable_bufferSize();
  self->limit_ = self->prefetch_ - (JreRShift32(self->prefetch_, 2));
}

IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *new_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver, initWithOrgReactivestreamsSubscriber_, downstream)
}

IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *create_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver, initWithOrgReactivestreamsSubscriber_, downstream)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver)

@implementation IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver:(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *)parent {
  IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver_initWithIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_(self, parent);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  IoReactivexInternalDisposablesDisposableHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexDisposablesDisposable_(self, d);
}

- (void)onSuccessWithId:(id)t {
  [((IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *) nil_chk(parent_)) otherSuccessWithId:t];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  [((IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *) nil_chk(parent_)) otherErrorWithJavaLangThrowable:e];
}

- (void)onComplete {
  [((IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *) nil_chk(parent_)) otherComplete];
}

- (void)dealloc {
  RELEASE_(parent_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onSuccessWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "parent_", "LIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver;", "(Lio/reactivex/internal/operators/flowable/FlowableMergeWithMaybe$MergeWithObserver<TT;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onSuccess", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/internal/operators/flowable/FlowableMergeWithMaybe$MergeWithObserver<TT;>;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;Lio/reactivex/MaybeObserver<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver = { "OtherObserver", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 5, 2, 0, -1, -1, 10, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver_initWithIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver *self, IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *parent) {
  JavaUtilConcurrentAtomicAtomicReference_init(self);
  JreStrongAssign(&self->parent_, parent);
}

IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver *new_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver_initWithIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *parent) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver, initWithIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_, parent)
}

IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver *create_IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver_initWithIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver *parent) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver, initWithIoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_, parent)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableMergeWithMaybe_MergeWithObserver_OtherObserver)
