//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableMapNotification.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/exceptions/CompositeException.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Function.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"
#include "io/reactivex/internal/operators/flowable/FlowableMapNotification.h"
#include "io/reactivex/internal/subscribers/SinglePostCompleteSubscriber.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/Callable.h"
#include "org/reactivestreams/Subscriber.h"

inline jlong IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber_serialVersionUID 2757120512858778108LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsFlowableFlowableMapNotification

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)onNextMapper
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)onErrorMapper
                       withJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)onCompleteSupplier {
  IoReactivexInternalOperatorsFlowableFlowableMapNotification_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_(self, source, onNextMapper, onErrorMapper, onCompleteSupplier);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  [((IoReactivexFlowable *) nil_chk(source_)) subscribeWithIoReactivexFlowableSubscriber:create_IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_(s, onNextMapper_, onErrorMapper_, onCompleteSupplier_)];
}

- (void)dealloc {
  RELEASE_(onNextMapper_);
  RELEASE_(onErrorMapper_);
  RELEASE_(onCompleteSupplier_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFlowable:withIoReactivexFunctionsFunction:withIoReactivexFunctionsFunction:withJavaUtilConcurrentCallable:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "onNextMapper_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "onErrorMapper_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
    { "onCompleteSupplier_", "LJavaUtilConcurrentCallable;", .constantValue.asLong = 0, 0x10, -1, -1, 7, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexFlowable;LIoReactivexFunctionsFunction;LIoReactivexFunctionsFunction;LJavaUtilConcurrentCallable;", "(Lio/reactivex/Flowable<TT;>;Lio/reactivex/functions/Function<-TT;+TR;>;Lio/reactivex/functions/Function<-Ljava/lang/Throwable;+TR;>;Ljava/util/concurrent/Callable<+TR;>;)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TR;>;)V", "Lio/reactivex/functions/Function<-TT;+TR;>;", "Lio/reactivex/functions/Function<-Ljava/lang/Throwable;+TR;>;", "Ljava/util/concurrent/Callable<+TR;>;", "LIoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber;", "<T:Ljava/lang/Object;R:Ljava/lang/Object;>Lio/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream<TT;TR;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableMapNotification = { "FlowableMapNotification", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 2, 3, -1, 8, -1, 9, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableMapNotification;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableMapNotification_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_(IoReactivexInternalOperatorsFlowableFlowableMapNotification *self, IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> onNextMapper, id<IoReactivexFunctionsFunction> onErrorMapper, id<JavaUtilConcurrentCallable> onCompleteSupplier) {
  IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream_initWithIoReactivexFlowable_(self, source);
  JreStrongAssign(&self->onNextMapper_, onNextMapper);
  JreStrongAssign(&self->onErrorMapper_, onErrorMapper);
  JreStrongAssign(&self->onCompleteSupplier_, onCompleteSupplier);
}

IoReactivexInternalOperatorsFlowableFlowableMapNotification *new_IoReactivexInternalOperatorsFlowableFlowableMapNotification_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_(IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> onNextMapper, id<IoReactivexFunctionsFunction> onErrorMapper, id<JavaUtilConcurrentCallable> onCompleteSupplier) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableMapNotification, initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_, source, onNextMapper, onErrorMapper, onCompleteSupplier)
}

IoReactivexInternalOperatorsFlowableFlowableMapNotification *create_IoReactivexInternalOperatorsFlowableFlowableMapNotification_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_(IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> onNextMapper, id<IoReactivexFunctionsFunction> onErrorMapper, id<JavaUtilConcurrentCallable> onCompleteSupplier) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableMapNotification, initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_, source, onNextMapper, onErrorMapper, onCompleteSupplier)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableMapNotification)

@implementation IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                              withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)onNextMapper
                              withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)onErrorMapper
                                withJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)onCompleteSupplier {
  IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_(self, actual, onNextMapper, onErrorMapper, onCompleteSupplier);
  return self;
}

- (void)onNextWithId:(id)t {
  id p;
  @try {
    p = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(onNextMapper_)) applyWithId:t], @"The onNext publisher returned is null");
  }
  @catch (JavaLangThrowable *e) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(e);
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
    return;
  }
  produced_++;
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:p];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  id p;
  @try {
    p = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(onErrorMapper_)) applyWithId:t], @"The onError publisher returned is null");
  }
  @catch (JavaLangThrowable *e) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(e);
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:create_IoReactivexExceptionsCompositeException_initWithJavaLangThrowableArray_([IOSObjectArray arrayWithObjects:(id[]){ t, e } count:2 type:JavaLangThrowable_class_()])];
    return;
  }
  [self completeWithId:p];
}

- (void)onComplete {
  id p;
  @try {
    p = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<JavaUtilConcurrentCallable>) nil_chk(onCompleteSupplier_)) call], @"The onComplete publisher returned is null");
  }
  @catch (JavaLangThrowable *e) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(e);
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
    return;
  }
  [self completeWithId:p];
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(onNextMapper_);
  RELEASE_(onErrorMapper_);
  RELEASE_(onCompleteSupplier_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:withIoReactivexFunctionsFunction:withIoReactivexFunctionsFunction:withJavaUtilConcurrentCallable:);
  methods[1].selector = @selector(onNextWithId:);
  methods[2].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[3].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "onNextMapper_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 7, -1 },
    { "onErrorMapper_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 8, -1 },
    { "onCompleteSupplier_", "LJavaUtilConcurrentCallable;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;LIoReactivexFunctionsFunction;LIoReactivexFunctionsFunction;LJavaUtilConcurrentCallable;", "(Lorg/reactivestreams/Subscriber<-TR;>;Lio/reactivex/functions/Function<-TT;+TR;>;Lio/reactivex/functions/Function<-Ljava/lang/Throwable;+TR;>;Ljava/util/concurrent/Callable<+TR;>;)V", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/functions/Function<-TT;+TR;>;", "Lio/reactivex/functions/Function<-Ljava/lang/Throwable;+TR;>;", "Ljava/util/concurrent/Callable<+TR;>;", "LIoReactivexInternalOperatorsFlowableFlowableMapNotification;", "<T:Ljava/lang/Object;R:Ljava/lang/Object;>Lio/reactivex/internal/subscribers/SinglePostCompleteSubscriber<TT;TR;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber = { "MapNotificationSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 4, 4, 10, -1, -1, 11, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_(IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber *self, id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> onNextMapper, id<IoReactivexFunctionsFunction> onErrorMapper, id<JavaUtilConcurrentCallable> onCompleteSupplier) {
  IoReactivexInternalSubscribersSinglePostCompleteSubscriber_initWithOrgReactivestreamsSubscriber_(self, actual);
  JreStrongAssign(&self->onNextMapper_, onNextMapper);
  JreStrongAssign(&self->onErrorMapper_, onErrorMapper);
  JreStrongAssign(&self->onCompleteSupplier_, onCompleteSupplier);
}

IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> onNextMapper, id<IoReactivexFunctionsFunction> onErrorMapper, id<JavaUtilConcurrentCallable> onCompleteSupplier) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber, initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_, actual, onNextMapper, onErrorMapper, onCompleteSupplier)
}

IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> onNextMapper, id<IoReactivexFunctionsFunction> onErrorMapper, id<JavaUtilConcurrentCallable> onCompleteSupplier) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber, initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withJavaUtilConcurrentCallable_, actual, onNextMapper, onErrorMapper, onCompleteSupplier)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableMapNotification_MapNotificationSubscriber)
