//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableOnBackpressureError.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/exceptions/MissingBackpressureException.h"
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"
#include "io/reactivex/internal/operators/flowable/FlowableOnBackpressureError.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/internal/util/BackpressureHelper.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Long.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/atomic/AtomicLong.h"
#include "org/reactivestreams/Subscriber.h"
#include "org/reactivestreams/Subscription.h"

inline jlong IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber_serialVersionUID -3176480756392482682LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source {
  IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_initWithIoReactivexFlowable_(self, source);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  [((IoReactivexFlowable *) nil_chk(self->source_)) subscribeWithIoReactivexFlowableSubscriber:create_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber_initWithOrgReactivestreamsSubscriber_(s)];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFlowable:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "LIoReactivexFlowable;", "(Lio/reactivex/Flowable<TT;>;)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "LIoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError = { "FlowableOnBackpressureError", "io.reactivex.internal.operators.flowable", ptrTable, methods, NULL, 7, 0x11, 2, 0, -1, 5, -1, 6, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_initWithIoReactivexFlowable_(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError *self, IoReactivexFlowable *source) {
  IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream_initWithIoReactivexFlowable_(self, source);
}

IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError *new_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_initWithIoReactivexFlowable_(IoReactivexFlowable *source) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError, initWithIoReactivexFlowable_, source)
}

IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError *create_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_initWithIoReactivexFlowable_(IoReactivexFlowable *source) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError, initWithIoReactivexFlowable_, source)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError)

@implementation IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream {
  IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber_initWithOrgReactivestreamsSubscriber_(self, downstream);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_validateWithOrgReactivestreamsSubscription_withOrgReactivestreamsSubscription_(self->upstream_, s)) {
    JreStrongAssign(&self->upstream_, s);
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onSubscribeWithOrgReactivestreamsSubscription:self];
    [((id<OrgReactivestreamsSubscription>) nil_chk(s)) requestWithLong:JavaLangLong_MAX_VALUE];
  }
}

- (void)onNextWithId:(id)t {
  if (done_) {
    return;
  }
  jlong r = [self get];
  if (r != 0LL) {
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:t];
    IoReactivexInternalUtilBackpressureHelper_producedWithJavaUtilConcurrentAtomicAtomicLong_withLong_(self, 1);
  }
  else {
    [self onErrorWithJavaLangThrowable:create_IoReactivexExceptionsMissingBackpressureException_initWithNSString_(@"could not emit value due to lack of requests")];
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if (done_) {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
    return;
  }
  done_ = true;
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  if (done_) {
    return;
  }
  done_ = true;
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onComplete];
}

- (void)requestWithLong:(jlong)n {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_validateWithLong_(n)) {
    IoReactivexInternalUtilBackpressureHelper_addWithJavaUtilConcurrentAtomicAtomicLong_withLong_(self, n);
  }
}

- (void)cancel {
  [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(requestWithLong:);
  methods[6].selector = @selector(cancel);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LOrgReactivestreamsSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 11, -1 },
    { "upstream_", "LOrgReactivestreamsSubscription;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "request", "J", "Lorg/reactivestreams/Subscriber<-TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableOnBackpressureError;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicLong;Lio/reactivex/FlowableSubscriber<TT;>;Lorg/reactivestreams/Subscription;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber = { "BackpressureErrorSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 7, 4, 12, -1, -1, 13, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber *self, id<OrgReactivestreamsSubscriber> downstream) {
  JavaUtilConcurrentAtomicAtomicLong_init(self);
  JreStrongAssign(&self->downstream_, downstream);
}

IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber, initWithOrgReactivestreamsSubscriber_, downstream)
}

IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber, initWithOrgReactivestreamsSubscriber_, downstream)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureError_BackpressureErrorSubscriber)
