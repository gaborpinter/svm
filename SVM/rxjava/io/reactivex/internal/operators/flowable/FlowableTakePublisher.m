//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableTakePublisher.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/internal/operators/flowable/FlowableTake.h"
#include "io/reactivex/internal/operators/flowable/FlowableTakePublisher.h"
#include "org/reactivestreams/Publisher.h"
#include "org/reactivestreams/Subscriber.h"

@implementation IoReactivexInternalOperatorsFlowableFlowableTakePublisher

- (instancetype __nonnull)initWithOrgReactivestreamsPublisher:(id<OrgReactivestreamsPublisher>)source
                                                     withLong:(jlong)limit {
  IoReactivexInternalOperatorsFlowableFlowableTakePublisher_initWithOrgReactivestreamsPublisher_withLong_(self, source, limit);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  [((id<OrgReactivestreamsPublisher>) nil_chk(source_)) subscribeWithOrgReactivestreamsSubscriber:create_IoReactivexInternalOperatorsFlowableFlowableTake_TakeSubscriber_initWithOrgReactivestreamsSubscriber_withLong_(s, limit_)];
}

- (void)dealloc {
  RELEASE_(source_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsPublisher:withLong:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LOrgReactivestreamsPublisher;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "limit_", "J", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsPublisher;J", "(Lorg/reactivestreams/Publisher<TT;>;J)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "Lorg/reactivestreams/Publisher<TT;>;", "<T:Ljava/lang/Object;>Lio/reactivex/Flowable<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableTakePublisher = { "FlowableTakePublisher", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 2, 2, -1, -1, -1, 6, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableTakePublisher;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableTakePublisher_initWithOrgReactivestreamsPublisher_withLong_(IoReactivexInternalOperatorsFlowableFlowableTakePublisher *self, id<OrgReactivestreamsPublisher> source, jlong limit) {
  IoReactivexFlowable_init(self);
  JreStrongAssign(&self->source_, source);
  self->limit_ = limit;
}

IoReactivexInternalOperatorsFlowableFlowableTakePublisher *new_IoReactivexInternalOperatorsFlowableFlowableTakePublisher_initWithOrgReactivestreamsPublisher_withLong_(id<OrgReactivestreamsPublisher> source, jlong limit) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableTakePublisher, initWithOrgReactivestreamsPublisher_withLong_, source, limit)
}

IoReactivexInternalOperatorsFlowableFlowableTakePublisher *create_IoReactivexInternalOperatorsFlowableFlowableTakePublisher_initWithOrgReactivestreamsPublisher_withLong_(id<OrgReactivestreamsPublisher> source, jlong limit) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableTakePublisher, initWithOrgReactivestreamsPublisher_withLong_, source, limit)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableTakePublisher)
