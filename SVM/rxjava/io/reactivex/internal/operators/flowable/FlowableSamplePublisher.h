//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableSamplePublisher.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher
#ifdef INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast
#define INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber 1
#endif
#ifdef INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast
#define INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber 1
#endif

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher))
#define IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_

#define RESTRICT_IoReactivexFlowable 1
#define INCLUDE_IoReactivexFlowable 1
#include "io/reactivex/Flowable.h"

@protocol OrgReactivestreamsPublisher;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableSamplePublisher : IoReactivexFlowable {
 @public
  id<OrgReactivestreamsPublisher> source_;
  id<OrgReactivestreamsPublisher> other_;
  jboolean emitLast_;
}

#pragma mark Public

- (instancetype __nonnull)initWithOrgReactivestreamsPublisher:(id<OrgReactivestreamsPublisher>)source
                              withOrgReactivestreamsPublisher:(id<OrgReactivestreamsPublisher>)other
                                                  withBoolean:(jboolean)emitLast;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher, source_, id<OrgReactivestreamsPublisher>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher, other_, id<OrgReactivestreamsPublisher>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_initWithOrgReactivestreamsPublisher_withOrgReactivestreamsPublisher_withBoolean_(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher *self, id<OrgReactivestreamsPublisher> source, id<OrgReactivestreamsPublisher> other, jboolean emitLast);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSamplePublisher *new_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_initWithOrgReactivestreamsPublisher_withOrgReactivestreamsPublisher_withBoolean_(id<OrgReactivestreamsPublisher> source, id<OrgReactivestreamsPublisher> other, jboolean emitLast) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSamplePublisher *create_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_initWithOrgReactivestreamsPublisher_withOrgReactivestreamsPublisher_withBoolean_(id<OrgReactivestreamsPublisher> source, id<OrgReactivestreamsPublisher> other, jboolean emitLast);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_OrgReactivestreamsSubscription 1
#define INCLUDE_OrgReactivestreamsSubscription 1
#include "org/reactivestreams/Subscription.h"

@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicLong;
@protocol OrgReactivestreamsPublisher;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber : JavaUtilConcurrentAtomicAtomicReference < IoReactivexFlowableSubscriber, OrgReactivestreamsSubscription > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  id<OrgReactivestreamsPublisher> sampler_;
  JavaUtilConcurrentAtomicAtomicLong *requested_;
  JavaUtilConcurrentAtomicAtomicReference *other_;
  id<OrgReactivestreamsSubscription> upstream_;
}

#pragma mark Public

- (void)cancel;

- (void)complete;

- (void)errorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (void)requestWithLong:(jlong)n;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                               withOrgReactivestreamsPublisher:(id<OrgReactivestreamsPublisher>)other;

- (void)completeMain;

- (void)completeOther;

- (void)emit;

- (void)run;

- (void)setOtherWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)o;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber, sampler_, id<OrgReactivestreamsPublisher>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber, requested_, JavaUtilConcurrentAtomicAtomicLong *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber, other_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber, upstream_, id<OrgReactivestreamsSubscription>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber_initWithOrgReactivestreamsSubscriber_withOrgReactivestreamsPublisher_(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber *self, id<OrgReactivestreamsSubscriber> actual, id<OrgReactivestreamsPublisher> other);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber_

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

@class IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber;
@class JavaLangThrowable;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber : NSObject < IoReactivexFlowableSubscriber > {
 @public
  IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber *parent_;
}

#pragma mark Public

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber:(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber *)parent;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber, parent_, IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber_(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber *self, IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber *parent);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber_(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber *parent) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber_(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber *parent);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplerSubscriber)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast))
#define IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast_

@protocol OrgReactivestreamsPublisher;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast : IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                               withOrgReactivestreamsPublisher:(id<OrgReactivestreamsPublisher>)other;

- (void)completeMain;

- (void)completeOther;

- (void)run;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast_initWithOrgReactivestreamsSubscriber_withOrgReactivestreamsPublisher_(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast *self, id<OrgReactivestreamsSubscriber> actual, id<OrgReactivestreamsPublisher> other);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast *new_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast_initWithOrgReactivestreamsSubscriber_withOrgReactivestreamsPublisher_(id<OrgReactivestreamsSubscriber> actual, id<OrgReactivestreamsPublisher> other) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast *create_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast_initWithOrgReactivestreamsSubscriber_withOrgReactivestreamsPublisher_(id<OrgReactivestreamsSubscriber> actual, id<OrgReactivestreamsPublisher> other);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainNoLast)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast))
#define IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast_

@class JavaUtilConcurrentAtomicAtomicInteger;
@protocol OrgReactivestreamsPublisher;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast : IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SamplePublisherSubscriber {
 @public
  JavaUtilConcurrentAtomicAtomicInteger *wip_;
  volatile_jboolean done_;
}

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                               withOrgReactivestreamsPublisher:(id<OrgReactivestreamsPublisher>)other;

- (void)completeMain;

- (void)completeOther;

- (void)run;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast, wip_, JavaUtilConcurrentAtomicAtomicInteger *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast_initWithOrgReactivestreamsSubscriber_withOrgReactivestreamsPublisher_(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast *self, id<OrgReactivestreamsSubscriber> actual, id<OrgReactivestreamsPublisher> other);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast *new_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast_initWithOrgReactivestreamsSubscriber_withOrgReactivestreamsPublisher_(id<OrgReactivestreamsSubscriber> actual, id<OrgReactivestreamsPublisher> other) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast *create_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast_initWithOrgReactivestreamsSubscriber_withOrgReactivestreamsPublisher_(id<OrgReactivestreamsSubscriber> actual, id<OrgReactivestreamsPublisher> other);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableSamplePublisher_SampleMainEmitLast)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSamplePublisher")
