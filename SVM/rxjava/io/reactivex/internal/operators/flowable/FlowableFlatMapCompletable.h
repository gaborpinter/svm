//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableFlatMapCompletable.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable))
#define IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_

#define RESTRICT_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"

@class IoReactivexFlowable;
@protocol IoReactivexFunctionsFunction;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable : IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream {
 @public
  id<IoReactivexFunctionsFunction> mapper_;
  jint maxConcurrency_;
  jboolean delayErrors_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)mapper
                                          withBoolean:(jboolean)delayErrors
                                              withInt:(jint)maxConcurrency;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)subscriber;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable, mapper_, id<IoReactivexFunctionsFunction>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_withBoolean_withInt_(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable *self, IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> mapper, jboolean delayErrors, jint maxConcurrency);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable *new_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_withBoolean_withInt_(IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> mapper, jboolean delayErrors, jint maxConcurrency) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable *create_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_withBoolean_withInt_(IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> mapper, jboolean delayErrors, jint maxConcurrency);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_

#define RESTRICT_IoReactivexInternalSubscriptionsBasicIntQueueSubscription 1
#define INCLUDE_IoReactivexInternalSubscriptionsBasicIntQueueSubscription 1
#include "io/reactivex/internal/subscriptions/BasicIntQueueSubscription.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

@class IoReactivexDisposablesCompositeDisposable;
@class IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer;
@class IoReactivexInternalUtilAtomicThrowable;
@class JavaLangThrowable;
@protocol IoReactivexFunctionsFunction;
@protocol OrgReactivestreamsSubscriber;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber : IoReactivexInternalSubscriptionsBasicIntQueueSubscription < IoReactivexFlowableSubscriber > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  IoReactivexInternalUtilAtomicThrowable *errors_;
  id<IoReactivexFunctionsFunction> mapper_;
  jboolean delayErrors_;
  IoReactivexDisposablesCompositeDisposable *set_;
  jint maxConcurrency_;
  id<OrgReactivestreamsSubscription> upstream_;
  volatile_jboolean cancelled_;
}

#pragma mark Public

- (void)cancel;

- (void)clear;

- (NSUInteger)hash;

- (jboolean)isEmpty;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onNextWithId:(id)value;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (id __nullable)poll;

- (void)requestWithLong:(jlong)n;

- (jint)requestFusionWithInt:(jint)mode;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)subscriber
                              withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)mapper
                                                   withBoolean:(jboolean)delayErrors
                                                       withInt:(jint)maxConcurrency;

- (void)innerCompleteWithIoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer:(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer *)inner;

- (void)innerErrorWithIoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer:(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer *)inner
                                                                                                              withJavaLangThrowable:(JavaLangThrowable *)e;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber, errors_, IoReactivexInternalUtilAtomicThrowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber, mapper_, id<IoReactivexFunctionsFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber, set_, IoReactivexDisposablesCompositeDisposable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber, upstream_, id<OrgReactivestreamsSubscription>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withBoolean_withInt_(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber *self, id<OrgReactivestreamsSubscriber> subscriber, id<IoReactivexFunctionsFunction> mapper, jboolean delayErrors, jint maxConcurrency);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withBoolean_withInt_(id<OrgReactivestreamsSubscriber> subscriber, id<IoReactivexFunctionsFunction> mapper, jboolean delayErrors, jint maxConcurrency) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withBoolean_withInt_(id<OrgReactivestreamsSubscriber> subscriber, id<IoReactivexFunctionsFunction> mapper, jboolean delayErrors, jint maxConcurrency);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer))
#define IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexCompletableObserver 1
#define INCLUDE_IoReactivexCompletableObserver 1
#include "io/reactivex/CompletableObserver.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber;
@class JavaLangThrowable;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;

@interface IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer : JavaUtilConcurrentAtomicAtomicReference < IoReactivexCompletableObserver, IoReactivexDisposablesDisposable >

#pragma mark Public

- (id<IoReactivexDisposablesDisposable>)accumulateAndGetWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)dispose;

- (id<IoReactivexDisposablesDisposable>)get;

- (id<IoReactivexDisposablesDisposable>)getAndAccumulateWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)getAndSetWithId:(id<IoReactivexDisposablesDisposable>)arg0;

- (id<IoReactivexDisposablesDisposable>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (id<IoReactivexDisposablesDisposable>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber:(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber *)outer$;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer_initWithIoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer *self, IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber *outer$);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer *new_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer_initWithIoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber *outer$) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer *create_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer_initWithIoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber *outer$);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable_FlatMapCompletableMainSubscriber_InnerConsumer)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableFlatMapCompletable")
