//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableDebounceTimed.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed))
#define IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_

#define RESTRICT_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"

@class IoReactivexFlowable;
@class IoReactivexScheduler;
@class JavaUtilConcurrentTimeUnit;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableDebounceTimed : IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream {
 @public
  jlong timeout_;
  JavaUtilConcurrentTimeUnit *unit_;
  IoReactivexScheduler *scheduler_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                                             withLong:(jlong)timeout
                       withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit
                             withIoReactivexScheduler:(IoReactivexScheduler *)scheduler;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed, unit_, JavaUtilConcurrentTimeUnit *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed, scheduler_, IoReactivexScheduler *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_initWithIoReactivexFlowable_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed *self, IoReactivexFlowable *source, jlong timeout, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableDebounceTimed *new_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_initWithIoReactivexFlowable_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_(IoReactivexFlowable *source, jlong timeout, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableDebounceTimed *create_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_initWithIoReactivexFlowable_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_(IoReactivexFlowable *source, jlong timeout, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicLong 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicLong 1
#include "java/util/concurrent/atomic/AtomicLong.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_OrgReactivestreamsSubscription 1
#define INCLUDE_OrgReactivestreamsSubscription 1
#include "org/reactivestreams/Subscription.h"

@class IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter;
@class IoReactivexScheduler_Worker;
@class JavaLangThrowable;
@class JavaUtilConcurrentTimeUnit;
@protocol IoReactivexDisposablesDisposable;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber : JavaUtilConcurrentAtomicAtomicLong < IoReactivexFlowableSubscriber, OrgReactivestreamsSubscription > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  jlong timeout_;
  JavaUtilConcurrentTimeUnit *unit_;
  IoReactivexScheduler_Worker *worker_;
  id<OrgReactivestreamsSubscription> upstream_;
  id<IoReactivexDisposablesDisposable> timer_;
  volatile_jlong index_;
  jboolean done_;
}

#pragma mark Public

- (void)cancel;

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (void)requestWithLong:(jlong)n;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                                                      withLong:(jlong)timeout
                                withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit
                               withIoReactivexScheduler_Worker:(IoReactivexScheduler_Worker *)worker;

- (void)emitWithLong:(jlong)idx
              withId:(id)t
withIoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter:(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter *)emitter;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithLong:(jlong)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber, unit_, JavaUtilConcurrentTimeUnit *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber, worker_, IoReactivexScheduler_Worker *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber, upstream_, id<OrgReactivestreamsSubscription>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber, timer_, id<IoReactivexDisposablesDisposable>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber_initWithOrgReactivestreamsSubscriber_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_Worker_(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber *self, id<OrgReactivestreamsSubscriber> actual, jlong timeout, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler_Worker *worker);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber_initWithOrgReactivestreamsSubscriber_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_Worker_(id<OrgReactivestreamsSubscriber> actual, jlong timeout, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler_Worker *worker) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber_initWithOrgReactivestreamsSubscriber_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_Worker_(id<OrgReactivestreamsSubscriber> actual, jlong timeout, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler_Worker *worker);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter))
#define IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_JavaLangRunnable 1
#define INCLUDE_JavaLangRunnable 1
#include "java/lang/Runnable.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber;
@class JavaUtilConcurrentAtomicAtomicBoolean;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;

@interface IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter : JavaUtilConcurrentAtomicAtomicReference < JavaLangRunnable, IoReactivexDisposablesDisposable > {
 @public
  id value_DebounceEmitter_;
  jlong idx_;
  IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber *parent_;
  JavaUtilConcurrentAtomicAtomicBoolean *once_;
}

#pragma mark Public

- (id<IoReactivexDisposablesDisposable>)accumulateAndGetWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)dispose;

- (id<IoReactivexDisposablesDisposable>)get;

- (id<IoReactivexDisposablesDisposable>)getAndAccumulateWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)getAndSetWithId:(id<IoReactivexDisposablesDisposable>)arg0;

- (id<IoReactivexDisposablesDisposable>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (jboolean)isDisposed;

- (void)run;

- (void)setResourceWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (id<IoReactivexDisposablesDisposable>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithId:(id)value
                            withLong:(jlong)idx
withIoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber:(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber *)parent;

- (void)emit;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter, value_DebounceEmitter_, id)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter, parent_, IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter, once_, JavaUtilConcurrentAtomicAtomicBoolean *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter_initWithId_withLong_withIoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber_(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter *self, id value, jlong idx, IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber *parent);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter *new_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter_initWithId_withLong_withIoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber_(id value, jlong idx, IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber *parent) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter *create_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter_initWithId_withLong_withIoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber_(id value, jlong idx, IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceTimedSubscriber *parent);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableDebounceTimed_DebounceEmitter)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDebounceTimed")
