//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableWithLatestFromMany.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Function.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"
#include "io/reactivex/internal/operators/flowable/FlowableMap.h"
#include "io/reactivex/internal/operators/flowable/FlowableWithLatestFromMany.h"
#include "io/reactivex/internal/subscriptions/EmptySubscription.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/internal/util/AtomicThrowable.h"
#include "io/reactivex/internal/util/HalfSerializer.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Iterable.h"
#include "java/lang/Long.h"
#include "java/lang/Throwable.h"
#include "java/util/Arrays.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"
#include "java/util/concurrent/atomic/AtomicLong.h"
#include "java/util/concurrent/atomic/AtomicReference.h"
#include "java/util/concurrent/atomic/AtomicReferenceArray.h"
#include "org/reactivestreams/Publisher.h"
#include "org/reactivestreams/Subscriber.h"
#include "org/reactivestreams/Subscription.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_serialVersionUID 1577321883966341961LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber, serialVersionUID, jlong)

inline jlong IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber_serialVersionUID 3256684027868224024LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber, serialVersionUID, jlong)

@interface IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc () {
 @public
  IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *this$0_;
}

@end

@implementation IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable * __nonnull)source
                 withOrgReactivestreamsPublisherArray:(IOSObjectArray * __nonnull)otherArray
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)combiner {
  IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_initWithIoReactivexFlowable_withOrgReactivestreamsPublisherArray_withIoReactivexFunctionsFunction_(self, source, otherArray, combiner);
  return self;
}

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable * __nonnull)source
                                 withJavaLangIterable:(id<JavaLangIterable> __nonnull)otherIterable
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction> __nonnull)combiner {
  IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_initWithIoReactivexFlowable_withJavaLangIterable_withIoReactivexFunctionsFunction_(self, source, otherIterable, combiner);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  IOSObjectArray *others = otherArray_;
  jint n = 0;
  if (others == nil) {
    others = [IOSObjectArray arrayWithLength:8 type:OrgReactivestreamsPublisher_class_()];
    @try {
      for (id<OrgReactivestreamsPublisher> __strong p in nil_chk(otherIterable_)) {
        if (n == others->size_) {
          others = JavaUtilArrays_copyOfWithNSObjectArray_withInt_(others, n + (JreRShift32(n, 1)));
        }
        IOSObjectArray_Set(nil_chk(others), n++, p);
      }
    }
    @catch (JavaLangThrowable *ex) {
      IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
      IoReactivexInternalSubscriptionsEmptySubscription_errorWithJavaLangThrowable_withOrgReactivestreamsSubscriber_(ex, s);
      return;
    }
  }
  else {
    n = others->size_;
  }
  if (n == 0) {
    [create_IoReactivexInternalOperatorsFlowableFlowableMap_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_(source_, create_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc_initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_(self)) subscribeActualWithOrgReactivestreamsSubscriber:s];
    return;
  }
  IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *parent = create_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withInt_(s, combiner_, n);
  [((id<OrgReactivestreamsSubscriber>) nil_chk(s)) onSubscribeWithOrgReactivestreamsSubscription:parent];
  [parent subscribeWithOrgReactivestreamsPublisherArray:others withInt:n];
  [((IoReactivexFlowable *) nil_chk(source_)) subscribeWithIoReactivexFlowableSubscriber:parent];
}

- (void)dealloc {
  RELEASE_(otherArray_);
  RELEASE_(otherIterable_);
  RELEASE_(combiner_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, NULL, 0x1, -1, 2, -1, 3, -1, -1 },
    { NULL, "V", 0x4, 4, 5, -1, 6, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFlowable:withOrgReactivestreamsPublisherArray:withIoReactivexFunctionsFunction:);
  methods[1].selector = @selector(initWithIoReactivexFlowable:withJavaLangIterable:withIoReactivexFunctionsFunction:);
  methods[2].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "otherArray_", "[LOrgReactivestreamsPublisher;", .constantValue.asLong = 0, 0x10, -1, -1, 7, -1 },
    { "otherIterable_", "LJavaLangIterable;", .constantValue.asLong = 0, 0x10, -1, -1, 8, -1 },
    { "combiner_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexFlowable;[LOrgReactivestreamsPublisher;LIoReactivexFunctionsFunction;", "(Lio/reactivex/Flowable<TT;>;[Lorg/reactivestreams/Publisher<*>;Lio/reactivex/functions/Function<-[Ljava/lang/Object;TR;>;)V", "LIoReactivexFlowable;LJavaLangIterable;LIoReactivexFunctionsFunction;", "(Lio/reactivex/Flowable<TT;>;Ljava/lang/Iterable<+Lorg/reactivestreams/Publisher<*>;>;Lio/reactivex/functions/Function<-[Ljava/lang/Object;TR;>;)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TR;>;)V", "[Lorg/reactivestreams/Publisher<*>;", "Ljava/lang/Iterable<+Lorg/reactivestreams/Publisher<*>;>;", "Lio/reactivex/functions/Function<-[Ljava/lang/Object;TR;>;", "LIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber;LIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber;LIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc;", "<T:Ljava/lang/Object;R:Ljava/lang/Object;>Lio/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream<TT;TR;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany = { "FlowableWithLatestFromMany", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 3, 3, -1, 10, -1, 11, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_initWithIoReactivexFlowable_withOrgReactivestreamsPublisherArray_withIoReactivexFunctionsFunction_(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *self, IoReactivexFlowable *source, IOSObjectArray *otherArray, id<IoReactivexFunctionsFunction> combiner) {
  IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream_initWithIoReactivexFlowable_(self, source);
  JreStrongAssign(&self->otherArray_, otherArray);
  JreStrongAssign(&self->otherIterable_, nil);
  JreStrongAssign(&self->combiner_, combiner);
}

IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *new_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_initWithIoReactivexFlowable_withOrgReactivestreamsPublisherArray_withIoReactivexFunctionsFunction_(IoReactivexFlowable *source, IOSObjectArray *otherArray, id<IoReactivexFunctionsFunction> combiner) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany, initWithIoReactivexFlowable_withOrgReactivestreamsPublisherArray_withIoReactivexFunctionsFunction_, source, otherArray, combiner)
}

IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *create_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_initWithIoReactivexFlowable_withOrgReactivestreamsPublisherArray_withIoReactivexFunctionsFunction_(IoReactivexFlowable *source, IOSObjectArray *otherArray, id<IoReactivexFunctionsFunction> combiner) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany, initWithIoReactivexFlowable_withOrgReactivestreamsPublisherArray_withIoReactivexFunctionsFunction_, source, otherArray, combiner)
}

void IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_initWithIoReactivexFlowable_withJavaLangIterable_withIoReactivexFunctionsFunction_(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *self, IoReactivexFlowable *source, id<JavaLangIterable> otherIterable, id<IoReactivexFunctionsFunction> combiner) {
  IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream_initWithIoReactivexFlowable_(self, source);
  JreStrongAssign(&self->otherArray_, nil);
  JreStrongAssign(&self->otherIterable_, otherIterable);
  JreStrongAssign(&self->combiner_, combiner);
}

IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *new_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_initWithIoReactivexFlowable_withJavaLangIterable_withIoReactivexFunctionsFunction_(IoReactivexFlowable *source, id<JavaLangIterable> otherIterable, id<IoReactivexFunctionsFunction> combiner) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany, initWithIoReactivexFlowable_withJavaLangIterable_withIoReactivexFunctionsFunction_, source, otherIterable, combiner)
}

IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *create_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_initWithIoReactivexFlowable_withJavaLangIterable_withIoReactivexFunctionsFunction_(IoReactivexFlowable *source, id<JavaLangIterable> otherIterable, id<IoReactivexFunctionsFunction> combiner) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany, initWithIoReactivexFlowable_withJavaLangIterable_withIoReactivexFunctionsFunction_, source, otherIterable, combiner)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany)

@implementation IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                              withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)combiner
                                                       withInt:(jint)n {
  IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withInt_(self, actual, combiner, n);
  return self;
}

- (void)subscribeWithOrgReactivestreamsPublisherArray:(IOSObjectArray *)others
                                              withInt:(jint)n {
  IOSObjectArray *subscribers = self->subscribers_;
  JavaUtilConcurrentAtomicAtomicReference *upstream = self->upstream_;
  for (jint i = 0; i < n; i++) {
    if ([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(upstream)) get] == JreLoadEnum(IoReactivexInternalSubscriptionsSubscriptionHelper, CANCELLED)) {
      return;
    }
    [((id<OrgReactivestreamsPublisher>) nil_chk(IOSObjectArray_Get(nil_chk(others), i))) subscribeWithOrgReactivestreamsSubscriber:IOSObjectArray_Get(nil_chk(subscribers), i)];
  }
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  IoReactivexInternalSubscriptionsSubscriptionHelper_deferredSetOnceWithJavaUtilConcurrentAtomicAtomicReference_withJavaUtilConcurrentAtomicAtomicLong_withOrgReactivestreamsSubscription_(self->upstream_, requested_, s);
}

- (void)onNextWithId:(id)t {
  if (![self tryOnNextWithId:t] && !JreLoadVolatileBoolean(&done_)) {
    [((id<OrgReactivestreamsSubscription>) nil_chk([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(upstream_)) get])) requestWithLong:1];
  }
}

- (jboolean)tryOnNextWithId:(id)t {
  if (JreLoadVolatileBoolean(&done_)) {
    return false;
  }
  JavaUtilConcurrentAtomicAtomicReferenceArray *ara = values_;
  jint n = [((JavaUtilConcurrentAtomicAtomicReferenceArray *) nil_chk(ara)) length];
  IOSObjectArray *objects = [IOSObjectArray arrayWithLength:n + 1 type:NSObject_class_()];
  IOSObjectArray_Set(objects, 0, t);
  for (jint i = 0; i < n; i++) {
    id o = [ara getWithInt:i];
    if (o == nil) {
      return false;
    }
    IOSObjectArray_Set(objects, i + 1, o);
  }
  id v;
  @try {
    v = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(combiner_)) applyWithId:objects], @"The combiner returned a null value");
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    [self cancel];
    [self onErrorWithJavaLangThrowable:ex];
    return false;
  }
  IoReactivexInternalUtilHalfSerializer_onNextWithOrgReactivestreamsSubscriber_withId_withJavaUtilConcurrentAtomicAtomicInteger_withIoReactivexInternalUtilAtomicThrowable_(downstream_, v, self, error_);
  return true;
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if (JreLoadVolatileBoolean(&done_)) {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
    return;
  }
  JreAssignVolatileBoolean(&done_, true);
  [self cancelAllButWithInt:-1];
  IoReactivexInternalUtilHalfSerializer_onErrorWithOrgReactivestreamsSubscriber_withJavaLangThrowable_withJavaUtilConcurrentAtomicAtomicInteger_withIoReactivexInternalUtilAtomicThrowable_(downstream_, t, self, error_);
}

- (void)onComplete {
  if (!JreLoadVolatileBoolean(&done_)) {
    JreAssignVolatileBoolean(&done_, true);
    [self cancelAllButWithInt:-1];
    IoReactivexInternalUtilHalfSerializer_onCompleteWithOrgReactivestreamsSubscriber_withJavaUtilConcurrentAtomicAtomicInteger_withIoReactivexInternalUtilAtomicThrowable_(downstream_, self, error_);
  }
}

- (void)requestWithLong:(jlong)n {
  IoReactivexInternalSubscriptionsSubscriptionHelper_deferredRequestWithJavaUtilConcurrentAtomicAtomicReference_withJavaUtilConcurrentAtomicAtomicLong_withLong_(upstream_, requested_, n);
}

- (void)cancel {
  IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
  {
    IOSObjectArray *a__ = subscribers_;
    IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
    IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber * const *e__ = b__ + a__->size_;
    while (b__ < e__) {
      IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber *s = *b__++;
      [((IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber *) nil_chk(s)) dispose];
    }
  }
}

- (void)innerNextWithInt:(jint)index
                  withId:(id)o {
  [((JavaUtilConcurrentAtomicAtomicReferenceArray *) nil_chk(values_)) setWithInt:index withId:o];
}

- (void)innerErrorWithInt:(jint)index
    withJavaLangThrowable:(JavaLangThrowable *)t {
  JreAssignVolatileBoolean(&done_, true);
  IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
  [self cancelAllButWithInt:index];
  IoReactivexInternalUtilHalfSerializer_onErrorWithOrgReactivestreamsSubscriber_withJavaLangThrowable_withJavaUtilConcurrentAtomicAtomicInteger_withIoReactivexInternalUtilAtomicThrowable_(downstream_, t, self, error_);
}

- (void)innerCompleteWithInt:(jint)index
                 withBoolean:(jboolean)nonEmpty {
  if (!nonEmpty) {
    JreAssignVolatileBoolean(&done_, true);
    IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
    [self cancelAllButWithInt:index];
    IoReactivexInternalUtilHalfSerializer_onCompleteWithOrgReactivestreamsSubscriber_withJavaUtilConcurrentAtomicAtomicInteger_withIoReactivexInternalUtilAtomicThrowable_(downstream_, self, error_);
  }
}

- (void)cancelAllButWithInt:(jint)index {
  IOSObjectArray *subscribers = self->subscribers_;
  for (jint i = 0; i < ((IOSObjectArray *) nil_chk(subscribers))->size_; i++) {
    if (i != index) {
      [((IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber *) nil_chk(IOSObjectArray_Get(subscribers, i))) dispose];
    }
  }
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(combiner_);
  RELEASE_(subscribers_);
  RELEASE_(values_);
  RELEASE_(upstream_);
  RELEASE_(requested_);
  RELEASE_(error_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x0, 2, 3, -1, 4, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, 9, -1, -1 },
    { NULL, "Z", 0x1, 10, 8, -1, 11, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 15, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, 16, 17, -1, -1, -1, -1 },
    { NULL, "V", 0x0, 18, 19, -1, -1, -1, -1 },
    { NULL, "V", 0x0, 20, 21, -1, -1, -1, -1 },
    { NULL, "V", 0x0, 22, 23, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:withIoReactivexFunctionsFunction:withInt:);
  methods[1].selector = @selector(subscribeWithOrgReactivestreamsPublisherArray:withInt:);
  methods[2].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[3].selector = @selector(onNextWithId:);
  methods[4].selector = @selector(tryOnNextWithId:);
  methods[5].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[6].selector = @selector(onComplete);
  methods[7].selector = @selector(requestWithLong:);
  methods[8].selector = @selector(cancel);
  methods[9].selector = @selector(innerNextWithInt:withId:);
  methods[10].selector = @selector(innerErrorWithInt:withJavaLangThrowable:);
  methods[11].selector = @selector(innerCompleteWithInt:withBoolean:);
  methods[12].selector = @selector(cancelAllButWithInt:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LOrgReactivestreamsSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 24, -1 },
    { "combiner_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 25, -1 },
    { "subscribers_", "[LIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "values_", "LJavaUtilConcurrentAtomicAtomicReferenceArray;", .constantValue.asLong = 0, 0x10, -1, -1, 26, -1 },
    { "upstream_", "LJavaUtilConcurrentAtomicAtomicReference;", .constantValue.asLong = 0, 0x10, -1, -1, 27, -1 },
    { "requested_", "LJavaUtilConcurrentAtomicAtomicLong;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "error_", "LIoReactivexInternalUtilAtomicThrowable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;LIoReactivexFunctionsFunction;I", "(Lorg/reactivestreams/Subscriber<-TR;>;Lio/reactivex/functions/Function<-[Ljava/lang/Object;TR;>;I)V", "subscribe", "[LOrgReactivestreamsPublisher;I", "([Lorg/reactivestreams/Publisher<*>;I)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "(TT;)V", "tryOnNext", "(TT;)Z", "onError", "LJavaLangThrowable;", "request", "J", "innerNext", "ILNSObject;", "innerError", "ILJavaLangThrowable;", "innerComplete", "IZ", "cancelAllBut", "I", "Lorg/reactivestreams/Subscriber<-TR;>;", "Lio/reactivex/functions/Function<-[Ljava/lang/Object;TR;>;", "Ljava/util/concurrent/atomic/AtomicReferenceArray<Ljava/lang/Object;>;", "Ljava/util/concurrent/atomic/AtomicReference<Lorg/reactivestreams/Subscription;>;", "LIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany;", "<T:Ljava/lang/Object;R:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/internal/fuseable/ConditionalSubscriber<TT;>;Lorg/reactivestreams/Subscription;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber = { "WithLatestFromSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 13, 9, 28, -1, -1, 29, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withInt_(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *self, id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> combiner, jint n) {
  JavaUtilConcurrentAtomicAtomicInteger_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->combiner_, combiner);
  IOSObjectArray *s = [IOSObjectArray arrayWithLength:n type:IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber_class_()];
  for (jint i = 0; i < n; i++) {
    IOSObjectArray_SetAndConsume(s, i, new_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_withInt_(self, i));
  }
  JreStrongAssign(&self->subscribers_, s);
  JreStrongAssignAndConsume(&self->values_, new_JavaUtilConcurrentAtomicAtomicReferenceArray_initWithInt_(n));
  JreStrongAssignAndConsume(&self->upstream_, new_JavaUtilConcurrentAtomicAtomicReference_init());
  JreStrongAssignAndConsume(&self->requested_, new_JavaUtilConcurrentAtomicAtomicLong_init());
  JreStrongAssignAndConsume(&self->error_, new_IoReactivexInternalUtilAtomicThrowable_init());
}

IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withInt_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> combiner, jint n) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber, initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withInt_, actual, combiner, n)
}

IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withInt_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> combiner, jint n) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber, initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_withInt_, actual, combiner, n)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber)

@implementation IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber:(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *)parent
                                                                                                                  withInt:(jint)index {
  IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_withInt_(self, parent, index);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  IoReactivexInternalSubscriptionsSubscriptionHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withOrgReactivestreamsSubscription_withLong_(self, s, JavaLangLong_MAX_VALUE);
}

- (void)onNextWithId:(id)t {
  if (!hasValue_) {
    hasValue_ = true;
  }
  [((IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *) nil_chk(parent_)) innerNextWithInt:index_ withId:t];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  [((IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *) nil_chk(parent_)) innerErrorWithInt:index_ withJavaLangThrowable:t];
}

- (void)onComplete {
  [((IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *) nil_chk(parent_)) innerCompleteWithInt:index_ withBoolean:hasValue_];
}

- (void)dispose {
  IoReactivexInternalSubscriptionsSubscriptionHelper_cancelWithJavaUtilConcurrentAtomicAtomicReference_(self);
}

- (void)dealloc {
  RELEASE_(parent_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber:withInt:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(dispose);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "parent_", "LIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 8, -1 },
    { "index_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "hasValue_", "Z", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber;I", "(Lio/reactivex/internal/operators/flowable/FlowableWithLatestFromMany$WithLatestFromSubscriber<**>;I)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "onError", "LJavaLangThrowable;", "Lio/reactivex/internal/operators/flowable/FlowableWithLatestFromMany$WithLatestFromSubscriber<**>;", "LIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany;", "Ljava/util/concurrent/atomic/AtomicReference<Lorg/reactivestreams/Subscription;>;Lio/reactivex/FlowableSubscriber<Ljava/lang/Object;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber = { "WithLatestInnerSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 6, 4, 9, -1, -1, 10, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_withInt_(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber *self, IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *parent, jint index) {
  JavaUtilConcurrentAtomicAtomicReference_init(self);
  JreStrongAssign(&self->parent_, parent);
  self->index_ = index;
}

IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_withInt_(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *parent, jint index) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber, initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_withInt_, parent, index)
}

IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_withInt_(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber *parent, jint index) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber, initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestFromSubscriber_withInt_, parent, index)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_WithLatestInnerSubscriber)

@implementation IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany:(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *)outer$ {
  IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc_initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_(self, outer$);
  return self;
}

- (id)applyWithId:(id)t {
  return IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(this$0_->combiner_)) applyWithId:[IOSObjectArray arrayWithObjects:(id[]){ t } count:1 type:NSObject_class_()]], @"The combiner returned a null value");
}

- (void)dealloc {
  RELEASE_(this$0_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, 0, 1, 2, 3, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany:);
  methods[1].selector = @selector(applyWithId:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "this$0_", "LIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany;", .constantValue.asLong = 0, 0x1012, -1, -1, 4, -1 },
  };
  static const void *ptrTable[] = { "apply", "LNSObject;", "LJavaLangException;", "(TT;)TR;", "Lio/reactivex/internal/operators/flowable/FlowableWithLatestFromMany<TT;TR;>;", "LIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany;", "Ljava/lang/Object;Lio/reactivex/functions/Function<TT;TR;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc = { "SingletonArrayFunc", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x10, 2, 1, 5, -1, -1, 6, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc_initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc *self, IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *outer$) {
  JreStrongAssign(&self->this$0_, outer$);
  NSObject_init(self);
}

IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc *new_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc_initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *outer$) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc, initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_, outer$)
}

IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc *create_IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc_initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany *outer$) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc, initWithIoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_, outer$)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableWithLatestFromMany_SingletonArrayFunc)
