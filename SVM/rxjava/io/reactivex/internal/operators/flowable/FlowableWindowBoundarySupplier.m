//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableWindowBoundarySupplier.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/exceptions/MissingBackpressureException.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"
#include "io/reactivex/internal/operators/flowable/FlowableWindowBoundarySupplier.h"
#include "io/reactivex/internal/queue/MpscLinkedQueue.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/internal/util/AtomicThrowable.h"
#include "io/reactivex/internal/util/BackpressureHelper.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "io/reactivex/processors/UnicastProcessor.h"
#include "io/reactivex/subscribers/DisposableSubscriber.h"
#include "java/lang/Long.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/Callable.h"
#include "java/util/concurrent/atomic/AtomicBoolean.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"
#include "java/util/concurrent/atomic/AtomicLong.h"
#include "java/util/concurrent/atomic/AtomicReference.h"
#include "org/reactivestreams/Publisher.h"
#include "org/reactivestreams/Subscriber.h"
#include "org/reactivestreams/Subscription.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_serialVersionUID 2233020065421370272LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                       withJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)other
                                              withInt:(jint)capacityHint {
  IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withInt_(self, source, other, capacityHint);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)subscriber {
  IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *parent = create_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withJavaUtilConcurrentCallable_(subscriber, capacityHint_, other_);
  [((IoReactivexFlowable *) nil_chk(source_)) subscribeWithIoReactivexFlowableSubscriber:parent];
}

- (void)dealloc {
  RELEASE_(other_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFlowable:withJavaUtilConcurrentCallable:withInt:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "other_", "LJavaUtilConcurrentCallable;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "capacityHint_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexFlowable;LJavaUtilConcurrentCallable;I", "(Lio/reactivex/Flowable<TT;>;Ljava/util/concurrent/Callable<+Lorg/reactivestreams/Publisher<TB;>;>;I)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-Lio/reactivex/Flowable<TT;>;>;)V", "Ljava/util/concurrent/Callable<+Lorg/reactivestreams/Publisher<TB;>;>;", "LIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber;LIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber;", "<T:Ljava/lang/Object;B:Ljava/lang/Object;>Lio/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream<TT;Lio/reactivex/Flowable<TT;>;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier = { "FlowableWindowBoundarySupplier", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 2, 2, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withInt_(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier *self, IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> other, jint capacityHint) {
  IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream_initWithIoReactivexFlowable_(self, source);
  JreStrongAssign(&self->other_, other);
  self->capacityHint_ = capacityHint;
}

IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier *new_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withInt_(IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> other, jint capacityHint) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier, initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withInt_, source, other, capacityHint)
}

IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier *create_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withInt_(IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> other, jint capacityHint) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier, initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withInt_, source, other, capacityHint)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier)

J2OBJC_INITIALIZED_DEFN(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber)

IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber *IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_BOUNDARY_DISPOSED;
id IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_NEXT_WINDOW;

@implementation IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber

+ (IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber *)BOUNDARY_DISPOSED {
  return IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_BOUNDARY_DISPOSED;
}

+ (id)NEXT_WINDOW {
  return IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_NEXT_WINDOW;
}

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream
                                                       withInt:(jint)capacityHint
                                withJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)other {
  IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withJavaUtilConcurrentCallable_(self, downstream, capacityHint, other);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_validateWithOrgReactivestreamsSubscription_withOrgReactivestreamsSubscription_(upstream_, s)) {
    JreStrongAssign(&upstream_, s);
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onSubscribeWithOrgReactivestreamsSubscription:self];
    [((IoReactivexInternalQueueMpscLinkedQueue *) nil_chk(queue_)) offerWithId:IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_NEXT_WINDOW];
    [self drain];
    [((id<OrgReactivestreamsSubscription>) nil_chk(s)) requestWithLong:JavaLangLong_MAX_VALUE];
  }
}

- (void)onNextWithId:(id)t {
  [((IoReactivexInternalQueueMpscLinkedQueue *) nil_chk(queue_)) offerWithId:t];
  [self drain];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  [self disposeBoundary];
  if ([((IoReactivexInternalUtilAtomicThrowable *) nil_chk(errors_)) addThrowableWithJavaLangThrowable:e]) {
    JreAssignVolatileBoolean(&done_, true);
    [self drain];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(e);
  }
}

- (void)onComplete {
  [self disposeBoundary];
  JreAssignVolatileBoolean(&done_, true);
  [self drain];
}

- (void)cancel {
  if ([((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(stopWindows_)) compareAndSetWithBoolean:false withBoolean:true]) {
    [self disposeBoundary];
    if ([((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(windows_)) decrementAndGet] == 0) {
      [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
    }
  }
}

- (void)requestWithLong:(jlong)n {
  IoReactivexInternalUtilBackpressureHelper_addWithJavaUtilConcurrentAtomicAtomicLong_withLong_(requested_, n);
}

- (void)disposeBoundary {
  id<IoReactivexDisposablesDisposable> d = [((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(boundarySubscriber_)) getAndSetWithId:IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_BOUNDARY_DISPOSED];
  if (d != nil && d != IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_BOUNDARY_DISPOSED) {
    [d dispose];
  }
}

- (void)run {
  if ([((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(windows_)) decrementAndGet] == 0) {
    [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
  }
}

- (void)innerNextWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber:(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber *)sender {
  [((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(boundarySubscriber_)) compareAndSetWithId:sender withId:nil];
  [((IoReactivexInternalQueueMpscLinkedQueue *) nil_chk(queue_)) offerWithId:IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_NEXT_WINDOW];
  [self drain];
}

- (void)innerErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
  if ([((IoReactivexInternalUtilAtomicThrowable *) nil_chk(errors_)) addThrowableWithJavaLangThrowable:e]) {
    JreAssignVolatileBoolean(&done_, true);
    [self drain];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(e);
  }
}

- (void)innerComplete {
  [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
  JreAssignVolatileBoolean(&done_, true);
  [self drain];
}

- (void)drain {
  if ([self getAndIncrement] != 0) {
    return;
  }
  jint missed = 1;
  id<OrgReactivestreamsSubscriber> downstream = self->downstream_;
  IoReactivexInternalQueueMpscLinkedQueue *queue = self->queue_;
  IoReactivexInternalUtilAtomicThrowable *errors = self->errors_;
  jlong emitted = self->emitted_;
  for (; ; ) {
    for (; ; ) {
      if ([((JavaUtilConcurrentAtomicAtomicInteger *) nil_chk(windows_)) get] == 0) {
        [((IoReactivexInternalQueueMpscLinkedQueue *) nil_chk(queue)) clear];
        JreStrongAssign(&window_, nil);
        return;
      }
      IoReactivexProcessorsUnicastProcessor *w = window_;
      jboolean d = JreLoadVolatileBoolean(&done_);
      if (d && [((IoReactivexInternalUtilAtomicThrowable *) nil_chk(errors)) get] != nil) {
        [((IoReactivexInternalQueueMpscLinkedQueue *) nil_chk(queue)) clear];
        JavaLangThrowable *ex = [((IoReactivexInternalUtilAtomicThrowable *) nil_chk(errors)) terminate];
        if (w != nil) {
          JreStrongAssign(&window_, nil);
          [w onErrorWithJavaLangThrowable:ex];
        }
        [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream)) onErrorWithJavaLangThrowable:ex];
        return;
      }
      id v = [((IoReactivexInternalQueueMpscLinkedQueue *) nil_chk(queue)) poll];
      jboolean empty = v == nil;
      if (d && empty) {
        JavaLangThrowable *ex = [((IoReactivexInternalUtilAtomicThrowable *) nil_chk(errors)) terminate];
        if (ex == nil) {
          if (w != nil) {
            JreStrongAssign(&window_, nil);
            [w onComplete];
          }
          [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream)) onComplete];
        }
        else {
          if (w != nil) {
            JreStrongAssign(&window_, nil);
            [w onErrorWithJavaLangThrowable:ex];
          }
          [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream)) onErrorWithJavaLangThrowable:ex];
        }
        return;
      }
      if (empty) {
        break;
      }
      if (v != IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_NEXT_WINDOW) {
        [((IoReactivexProcessorsUnicastProcessor *) nil_chk(w)) onNextWithId:v];
        continue;
      }
      if (w != nil) {
        JreStrongAssign(&window_, nil);
        [w onComplete];
      }
      if (![((JavaUtilConcurrentAtomicAtomicBoolean *) nil_chk(stopWindows_)) get]) {
        if (emitted != [((JavaUtilConcurrentAtomicAtomicLong *) nil_chk(requested_)) get]) {
          w = IoReactivexProcessorsUnicastProcessor_createWithInt_withJavaLangRunnable_(capacityHint_, self);
          JreStrongAssign(&window_, w);
          [windows_ getAndIncrement];
          id<OrgReactivestreamsPublisher> otherSource;
          @try {
            otherSource = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<JavaUtilConcurrentCallable>) nil_chk(other_)) call], @"The other Callable returned a null Publisher");
          }
          @catch (JavaLangThrowable *ex) {
            IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
            [((IoReactivexInternalUtilAtomicThrowable *) nil_chk(errors)) addThrowableWithJavaLangThrowable:ex];
            JreAssignVolatileBoolean(&done_, true);
            continue;
          }
          IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber *bo = create_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_(self);
          if ([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(boundarySubscriber_)) compareAndSetWithId:nil withId:bo]) {
            [((id<OrgReactivestreamsPublisher>) nil_chk(otherSource)) subscribeWithOrgReactivestreamsSubscriber:bo];
            emitted++;
            [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream)) onNextWithId:w];
          }
        }
        else {
          [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
          [self disposeBoundary];
          [((IoReactivexInternalUtilAtomicThrowable *) nil_chk(errors)) addThrowableWithJavaLangThrowable:create_IoReactivexExceptionsMissingBackpressureException_initWithNSString_(@"Could not deliver a window due to lack of requests")];
          JreAssignVolatileBoolean(&done_, true);
        }
      }
    }
    self->emitted_ = emitted;
    missed = [self addAndGetWithInt:-missed];
    if (missed == 0) {
      break;
    }
  }
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(boundarySubscriber_);
  RELEASE_(windows_);
  RELEASE_(queue_);
  RELEASE_(errors_);
  RELEASE_(stopWindows_);
  RELEASE_(other_);
  RELEASE_(requested_);
  RELEASE_(upstream_);
  RELEASE_(window_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, 11, 12, -1, 13, -1, -1 },
    { NULL, "V", 0x0, 14, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:withInt:withJavaUtilConcurrentCallable:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(cancel);
  methods[6].selector = @selector(requestWithLong:);
  methods[7].selector = @selector(disposeBoundary);
  methods[8].selector = @selector(run);
  methods[9].selector = @selector(innerNextWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber:);
  methods[10].selector = @selector(innerErrorWithJavaLangThrowable:);
  methods[11].selector = @selector(innerComplete);
  methods[12].selector = @selector(drain);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LOrgReactivestreamsSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 15, -1 },
    { "capacityHint_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "boundarySubscriber_", "LJavaUtilConcurrentAtomicAtomicReference;", .constantValue.asLong = 0, 0x10, -1, -1, 16, -1 },
    { "BOUNDARY_DISPOSED", "LIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber;", .constantValue.asLong = 0, 0x18, -1, 17, 18, -1 },
    { "windows_", "LJavaUtilConcurrentAtomicAtomicInteger;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "queue_", "LIoReactivexInternalQueueMpscLinkedQueue;", .constantValue.asLong = 0, 0x10, -1, -1, 19, -1 },
    { "errors_", "LIoReactivexInternalUtilAtomicThrowable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "stopWindows_", "LJavaUtilConcurrentAtomicAtomicBoolean;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "other_", "LJavaUtilConcurrentCallable;", .constantValue.asLong = 0, 0x10, -1, -1, 20, -1 },
    { "NEXT_WINDOW", "LNSObject;", .constantValue.asLong = 0, 0x18, -1, 21, -1, -1 },
    { "requested_", "LJavaUtilConcurrentAtomicAtomicLong;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LOrgReactivestreamsSubscription;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "window_", "LIoReactivexProcessorsUnicastProcessor;", .constantValue.asLong = 0, 0x0, -1, -1, 22, -1 },
    { "emitted_", "J", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;ILJavaUtilConcurrentCallable;", "(Lorg/reactivestreams/Subscriber<-Lio/reactivex/Flowable<TT;>;>;ILjava/util/concurrent/Callable<+Lorg/reactivestreams/Publisher<TB;>;>;)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "request", "J", "innerNext", "LIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber;", "(Lio/reactivex/internal/operators/flowable/FlowableWindowBoundarySupplier$WindowBoundaryInnerSubscriber<TT;TB;>;)V", "innerError", "Lorg/reactivestreams/Subscriber<-Lio/reactivex/Flowable<TT;>;>;", "Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/internal/operators/flowable/FlowableWindowBoundarySupplier$WindowBoundaryInnerSubscriber<TT;TB;>;>;", &IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_BOUNDARY_DISPOSED, "Lio/reactivex/internal/operators/flowable/FlowableWindowBoundarySupplier$WindowBoundaryInnerSubscriber<Ljava/lang/Object;Ljava/lang/Object;>;", "Lio/reactivex/internal/queue/MpscLinkedQueue<Ljava/lang/Object;>;", "Ljava/util/concurrent/Callable<+Lorg/reactivestreams/Publisher<TB;>;>;", &IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_NEXT_WINDOW, "Lio/reactivex/processors/UnicastProcessor<TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier;", "<T:Ljava/lang/Object;B:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/FlowableSubscriber<TT;>;Lorg/reactivestreams/Subscription;Ljava/lang/Runnable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber = { "WindowBoundaryMainSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 13, 16, 23, -1, -1, 24, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber;
}

+ (void)initialize {
  if (self == [IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber class]) {
    JreStrongAssignAndConsume(&IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_BOUNDARY_DISPOSED, new_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_(nil));
    JreStrongAssignAndConsume(&IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_NEXT_WINDOW, new_NSObject_init());
    J2OBJC_SET_INITIALIZED(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber)
  }
}

@end

void IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withJavaUtilConcurrentCallable_(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *self, id<OrgReactivestreamsSubscriber> downstream, jint capacityHint, id<JavaUtilConcurrentCallable> other) {
  JavaUtilConcurrentAtomicAtomicInteger_init(self);
  JreStrongAssign(&self->downstream_, downstream);
  self->capacityHint_ = capacityHint;
  JreStrongAssignAndConsume(&self->boundarySubscriber_, new_JavaUtilConcurrentAtomicAtomicReference_init());
  JreStrongAssignAndConsume(&self->windows_, new_JavaUtilConcurrentAtomicAtomicInteger_initWithInt_(1));
  JreStrongAssignAndConsume(&self->queue_, new_IoReactivexInternalQueueMpscLinkedQueue_init());
  JreStrongAssignAndConsume(&self->errors_, new_IoReactivexInternalUtilAtomicThrowable_init());
  JreStrongAssignAndConsume(&self->stopWindows_, new_JavaUtilConcurrentAtomicAtomicBoolean_init());
  JreStrongAssign(&self->other_, other);
  JreStrongAssignAndConsume(&self->requested_, new_JavaUtilConcurrentAtomicAtomicLong_init());
}

IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withJavaUtilConcurrentCallable_(id<OrgReactivestreamsSubscriber> downstream, jint capacityHint, id<JavaUtilConcurrentCallable> other) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber, initWithOrgReactivestreamsSubscriber_withInt_withJavaUtilConcurrentCallable_, downstream, capacityHint, other)
}

IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withJavaUtilConcurrentCallable_(id<OrgReactivestreamsSubscriber> downstream, jint capacityHint, id<JavaUtilConcurrentCallable> other) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber, initWithOrgReactivestreamsSubscriber_withInt_withJavaUtilConcurrentCallable_, downstream, capacityHint, other)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber)

@implementation IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber:(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *)parent {
  IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_(self, parent);
  return self;
}

- (void)onNextWithId:(id)t {
  if (done_) {
    return;
  }
  done_ = true;
  [self dispose];
  [((IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *) nil_chk(parent_)) innerNextWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber:self];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if (done_) {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
    return;
  }
  done_ = true;
  [((IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *) nil_chk(parent_)) innerErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  if (done_) {
    return;
  }
  done_ = true;
  [((IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *) nil_chk(parent_)) innerComplete];
}

- (void)dealloc {
  RELEASE_(parent_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber:);
  methods[1].selector = @selector(onNextWithId:);
  methods[2].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[3].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "parent_", "LIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 7, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber;", "(Lio/reactivex/internal/operators/flowable/FlowableWindowBoundarySupplier$WindowBoundaryMainSubscriber<TT;TB;>;)V", "onNext", "LNSObject;", "(TB;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/internal/operators/flowable/FlowableWindowBoundarySupplier$WindowBoundaryMainSubscriber<TT;TB;>;", "LIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier;", "<T:Ljava/lang/Object;B:Ljava/lang/Object;>Lio/reactivex/subscribers/DisposableSubscriber<TB;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber = { "WindowBoundaryInnerSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 4, 2, 8, -1, -1, 9, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber *self, IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *parent) {
  IoReactivexSubscribersDisposableSubscriber_init(self);
  JreStrongAssign(&self->parent_, parent);
}

IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *parent) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber, initWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_, parent)
}

IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber_initWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber *parent) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber, initWithIoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryMainSubscriber_, parent)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableWindowBoundarySupplier_WindowBoundaryInnerSubscriber)
