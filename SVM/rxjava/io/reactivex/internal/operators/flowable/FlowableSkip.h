//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableSkip.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSkip")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableSkip
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSkip 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSkip 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableSkip

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableSkip_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSkip || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSkip))
#define IoReactivexInternalOperatorsFlowableFlowableSkip_

#define RESTRICT_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"

@class IoReactivexFlowable;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableSkip : IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream {
 @public
  jlong n_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                                             withLong:(jlong)n;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableSkip)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableSkip_initWithIoReactivexFlowable_withLong_(IoReactivexInternalOperatorsFlowableFlowableSkip *self, IoReactivexFlowable *source, jlong n);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSkip *new_IoReactivexInternalOperatorsFlowableFlowableSkip_initWithIoReactivexFlowable_withLong_(IoReactivexFlowable *source, jlong n) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSkip *create_IoReactivexInternalOperatorsFlowableFlowableSkip_initWithIoReactivexFlowable_withLong_(IoReactivexFlowable *source, jlong n);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableSkip)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSkip || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber_

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_OrgReactivestreamsSubscription 1
#define INCLUDE_OrgReactivestreamsSubscription 1
#include "org/reactivestreams/Subscription.h"

@class JavaLangThrowable;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber : NSObject < IoReactivexFlowableSubscriber, OrgReactivestreamsSubscription > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  jlong remaining_;
  id<OrgReactivestreamsSubscription> upstream_;
}

#pragma mark Public

- (void)cancel;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (void)requestWithLong:(jlong)n;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                                                      withLong:(jlong)n;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber, upstream_, id<OrgReactivestreamsSubscription>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber_initWithOrgReactivestreamsSubscriber_withLong_(IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber *self, id<OrgReactivestreamsSubscriber> actual, jlong n);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber_initWithOrgReactivestreamsSubscriber_withLong_(id<OrgReactivestreamsSubscriber> actual, jlong n) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber_initWithOrgReactivestreamsSubscriber_withLong_(id<OrgReactivestreamsSubscriber> actual, jlong n);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableSkip_SkipSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableSkip")
