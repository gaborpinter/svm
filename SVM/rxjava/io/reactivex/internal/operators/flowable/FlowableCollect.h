//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableCollect.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCollect")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableCollect
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCollect 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCollect 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableCollect

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCollect_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCollect || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCollect))
#define IoReactivexInternalOperatorsFlowableFlowableCollect_

#define RESTRICT_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"

@class IoReactivexFlowable;
@protocol IoReactivexFunctionsBiConsumer;
@protocol JavaUtilConcurrentCallable;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableCollect : IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream {
 @public
  id<JavaUtilConcurrentCallable> initialSupplier_;
  id<IoReactivexFunctionsBiConsumer> collector_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                       withJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)initialSupplier
                   withIoReactivexFunctionsBiConsumer:(id<IoReactivexFunctionsBiConsumer>)collector;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCollect)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCollect, initialSupplier_, id<JavaUtilConcurrentCallable>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCollect, collector_, id<IoReactivexFunctionsBiConsumer>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCollect_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withIoReactivexFunctionsBiConsumer_(IoReactivexInternalOperatorsFlowableFlowableCollect *self, IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> initialSupplier, id<IoReactivexFunctionsBiConsumer> collector);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCollect *new_IoReactivexInternalOperatorsFlowableFlowableCollect_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withIoReactivexFunctionsBiConsumer_(IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> initialSupplier, id<IoReactivexFunctionsBiConsumer> collector) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCollect *create_IoReactivexInternalOperatorsFlowableFlowableCollect_initWithIoReactivexFlowable_withJavaUtilConcurrentCallable_withIoReactivexFunctionsBiConsumer_(IoReactivexFlowable *source, id<JavaUtilConcurrentCallable> initialSupplier, id<IoReactivexFunctionsBiConsumer> collector);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCollect)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCollect || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber_

#define RESTRICT_IoReactivexInternalSubscriptionsDeferredScalarSubscription 1
#define INCLUDE_IoReactivexInternalSubscriptionsDeferredScalarSubscription 1
#include "io/reactivex/internal/subscriptions/DeferredScalarSubscription.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

@class JavaLangThrowable;
@protocol IoReactivexFunctionsBiConsumer;
@protocol OrgReactivestreamsSubscriber;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber : IoReactivexInternalSubscriptionsDeferredScalarSubscription < IoReactivexFlowableSubscriber > {
 @public
  id<IoReactivexFunctionsBiConsumer> collector_;
  id u_;
  id<OrgReactivestreamsSubscription> upstream_;
  jboolean done_;
}

#pragma mark Public

- (void)cancel;

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                                                        withId:(id)u
                            withIoReactivexFunctionsBiConsumer:(id<IoReactivexFunctionsBiConsumer>)collector;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber, collector_, id<IoReactivexFunctionsBiConsumer>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber, u_, id)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber, upstream_, id<OrgReactivestreamsSubscription>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber_initWithOrgReactivestreamsSubscriber_withId_withIoReactivexFunctionsBiConsumer_(IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber *self, id<OrgReactivestreamsSubscriber> actual, id u, id<IoReactivexFunctionsBiConsumer> collector);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber_initWithOrgReactivestreamsSubscriber_withId_withIoReactivexFunctionsBiConsumer_(id<OrgReactivestreamsSubscriber> actual, id u, id<IoReactivexFunctionsBiConsumer> collector) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber_initWithOrgReactivestreamsSubscriber_withId_withIoReactivexFunctionsBiConsumer_(id<OrgReactivestreamsSubscriber> actual, id u, id<IoReactivexFunctionsBiConsumer> collector);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCollect_CollectSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCollect")
