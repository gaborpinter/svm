//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableDoOnLifecycle.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle))
#define IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_

#define RESTRICT_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"

@class IoReactivexFlowable;
@protocol IoReactivexFunctionsAction;
@protocol IoReactivexFunctionsConsumer;
@protocol IoReactivexFunctionsLongConsumer;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle : IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                     withIoReactivexFunctionsConsumer:(id<IoReactivexFunctionsConsumer>)onSubscribe
                 withIoReactivexFunctionsLongConsumer:(id<IoReactivexFunctionsLongConsumer>)onRequest
                       withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onCancel;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_initWithIoReactivexFlowable_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsLongConsumer_withIoReactivexFunctionsAction_(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle *self, IoReactivexFlowable *source, id<IoReactivexFunctionsConsumer> onSubscribe, id<IoReactivexFunctionsLongConsumer> onRequest, id<IoReactivexFunctionsAction> onCancel);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle *new_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_initWithIoReactivexFlowable_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsLongConsumer_withIoReactivexFunctionsAction_(IoReactivexFlowable *source, id<IoReactivexFunctionsConsumer> onSubscribe, id<IoReactivexFunctionsLongConsumer> onRequest, id<IoReactivexFunctionsAction> onCancel) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle *create_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_initWithIoReactivexFlowable_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsLongConsumer_withIoReactivexFunctionsAction_(IoReactivexFlowable *source, id<IoReactivexFunctionsConsumer> onSubscribe, id<IoReactivexFunctionsLongConsumer> onRequest, id<IoReactivexFunctionsAction> onCancel);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber_

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_OrgReactivestreamsSubscription 1
#define INCLUDE_OrgReactivestreamsSubscription 1
#include "org/reactivestreams/Subscription.h"

@class JavaLangThrowable;
@protocol IoReactivexFunctionsAction;
@protocol IoReactivexFunctionsConsumer;
@protocol IoReactivexFunctionsLongConsumer;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber : NSObject < IoReactivexFlowableSubscriber, OrgReactivestreamsSubscription > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  id<IoReactivexFunctionsConsumer> onSubscribe_;
  id<IoReactivexFunctionsLongConsumer> onRequest_;
  id<IoReactivexFunctionsAction> onCancel_;
  id<OrgReactivestreamsSubscription> upstream_;
}

#pragma mark Public

- (void)cancel;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (void)requestWithLong:(jlong)n;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                              withIoReactivexFunctionsConsumer:(id<IoReactivexFunctionsConsumer>)onSubscribe
                          withIoReactivexFunctionsLongConsumer:(id<IoReactivexFunctionsLongConsumer>)onRequest
                                withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onCancel;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber, onSubscribe_, id<IoReactivexFunctionsConsumer>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber, onRequest_, id<IoReactivexFunctionsLongConsumer>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber, onCancel_, id<IoReactivexFunctionsAction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber, upstream_, id<OrgReactivestreamsSubscription>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsLongConsumer_withIoReactivexFunctionsAction_(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber *self, id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsConsumer> onSubscribe, id<IoReactivexFunctionsLongConsumer> onRequest, id<IoReactivexFunctionsAction> onCancel);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsLongConsumer_withIoReactivexFunctionsAction_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsConsumer> onSubscribe, id<IoReactivexFunctionsLongConsumer> onRequest, id<IoReactivexFunctionsAction> onCancel) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsLongConsumer_withIoReactivexFunctionsAction_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsConsumer> onSubscribe, id<IoReactivexFunctionsLongConsumer> onRequest, id<IoReactivexFunctionsAction> onCancel);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle_SubscriptionLambdaSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableDoOnLifecycle")
