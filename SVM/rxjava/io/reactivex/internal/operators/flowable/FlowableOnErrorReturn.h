//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableOnErrorReturn.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn))
#define IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_

#define RESTRICT_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"

@class IoReactivexFlowable;
@protocol IoReactivexFunctionsFunction;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn : IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream {
 @public
  id<IoReactivexFunctionsFunction> valueSupplier_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)valueSupplier;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn, valueSupplier_, id<IoReactivexFunctionsFunction>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_(IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn *self, IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> valueSupplier);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn *new_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_(IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> valueSupplier) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn *create_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_initWithIoReactivexFlowable_withIoReactivexFunctionsFunction_(IoReactivexFlowable *source, id<IoReactivexFunctionsFunction> valueSupplier);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber_

#define RESTRICT_IoReactivexInternalSubscribersSinglePostCompleteSubscriber 1
#define INCLUDE_IoReactivexInternalSubscribersSinglePostCompleteSubscriber 1
#include "io/reactivex/internal/subscribers/SinglePostCompleteSubscriber.h"

@class JavaLangThrowable;
@protocol IoReactivexFunctionsFunction;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber : IoReactivexInternalSubscribersSinglePostCompleteSubscriber {
 @public
  id<IoReactivexFunctionsFunction> valueSupplier_;
}

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                              withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)valueSupplier;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber, valueSupplier_, id<IoReactivexFunctionsFunction>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_(IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber *self, id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> valueSupplier);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> valueSupplier) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsFunction_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsFunction> valueSupplier);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn_OnErrorReturnSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnErrorReturn")
