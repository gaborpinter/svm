//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableCreate.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableCreate
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableCreate
#ifdef INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter
#define INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter 1
#endif
#ifdef INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter
#define INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter 1
#endif
#ifdef INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter
#define INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter 1
#endif
#ifdef INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter
#define INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter 1
#endif
#ifdef INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter
#define INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter 1
#endif
#ifdef INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter
#define INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter 1
#endif

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCreate_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate))
#define IoReactivexInternalOperatorsFlowableFlowableCreate_

#define RESTRICT_IoReactivexFlowable 1
#define INCLUDE_IoReactivexFlowable 1
#include "io/reactivex/Flowable.h"

@class IoReactivexBackpressureStrategy;
@protocol IoReactivexFlowableOnSubscribe;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableCreate : IoReactivexFlowable {
 @public
  id<IoReactivexFlowableOnSubscribe> source_;
  IoReactivexBackpressureStrategy *backpressure_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowableOnSubscribe:(id<IoReactivexFlowableOnSubscribe>)source
                             withIoReactivexBackpressureStrategy:(IoReactivexBackpressureStrategy *)backpressure;

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)t;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCreate)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate, source_, id<IoReactivexFlowableOnSubscribe>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate, backpressure_, IoReactivexBackpressureStrategy *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCreate_initWithIoReactivexFlowableOnSubscribe_withIoReactivexBackpressureStrategy_(IoReactivexInternalOperatorsFlowableFlowableCreate *self, id<IoReactivexFlowableOnSubscribe> source, IoReactivexBackpressureStrategy *backpressure);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate *new_IoReactivexInternalOperatorsFlowableFlowableCreate_initWithIoReactivexFlowableOnSubscribe_withIoReactivexBackpressureStrategy_(id<IoReactivexFlowableOnSubscribe> source, IoReactivexBackpressureStrategy *backpressure) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate *create_IoReactivexInternalOperatorsFlowableFlowableCreate_initWithIoReactivexFlowableOnSubscribe_withIoReactivexBackpressureStrategy_(id<IoReactivexFlowableOnSubscribe> source, IoReactivexBackpressureStrategy *backpressure);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCreate)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter))
#define IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicInteger 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicInteger 1
#include "java/util/concurrent/atomic/AtomicInteger.h"

#define RESTRICT_IoReactivexFlowableEmitter 1
#define INCLUDE_IoReactivexFlowableEmitter 1
#include "io/reactivex/FlowableEmitter.h"

@class IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter;
@class IoReactivexInternalUtilAtomicThrowable;
@class JavaLangThrowable;
@protocol IoReactivexDisposablesDisposable;
@protocol IoReactivexFunctionsCancellable;
@protocol IoReactivexInternalFuseableSimplePlainQueue;

@interface IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter : JavaUtilConcurrentAtomicAtomicInteger < IoReactivexFlowableEmitter > {
 @public
  IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter *emitter_;
  IoReactivexInternalUtilAtomicThrowable *error_;
  id<IoReactivexInternalFuseableSimplePlainQueue> queue_;
  volatile_jboolean done_;
}

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isCancelled;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (jlong)requested;

- (id<IoReactivexFlowableEmitter>)serialize;

- (void)setCancellableWithIoReactivexFunctionsCancellable:(id<IoReactivexFunctionsCancellable>)c;

- (void)setDisposableWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (NSString *)description;

- (jboolean)tryOnErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter:(IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter *)emitter;

- (void)drain;

- (void)drainLoop;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter, emitter_, IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter, error_, IoReactivexInternalUtilAtomicThrowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter, queue_, id<IoReactivexInternalFuseableSimplePlainQueue>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter_initWithIoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter_(IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter *self, IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter *emitter);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter *new_IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter_initWithIoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter_(IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter *emitter) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter *create_IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter_initWithIoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter_(IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter *emitter);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCreate_SerializedEmitter)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter))
#define IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicLong 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicLong 1
#include "java/util/concurrent/atomic/AtomicLong.h"

#define RESTRICT_IoReactivexFlowableEmitter 1
#define INCLUDE_IoReactivexFlowableEmitter 1
#include "io/reactivex/FlowableEmitter.h"

#define RESTRICT_OrgReactivestreamsSubscription 1
#define INCLUDE_OrgReactivestreamsSubscription 1
#include "org/reactivestreams/Subscription.h"

@class IoReactivexInternalDisposablesSequentialDisposable;
@class JavaLangThrowable;
@protocol IoReactivexDisposablesDisposable;
@protocol IoReactivexFunctionsCancellable;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter : JavaUtilConcurrentAtomicAtomicLong < IoReactivexFlowableEmitter, OrgReactivestreamsSubscription > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  IoReactivexInternalDisposablesSequentialDisposable *serial_;
}

#pragma mark Public

- (void)cancel;

- (NSUInteger)hash;

- (jboolean)isCancelled;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)requestWithLong:(jlong)n;

- (jlong)requested;

- (id<IoReactivexFlowableEmitter>)serialize;

- (void)setCancellableWithIoReactivexFunctionsCancellable:(id<IoReactivexFunctionsCancellable>)c;

- (void)setDisposableWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (NSString *)description;

- (jboolean)tryOnErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

#pragma mark Protected

- (void)complete;

- (jboolean)errorWithJavaLangThrowable:(JavaLangThrowable *)e;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream;

- (void)onRequested;

- (void)onUnsubscribed;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter, serial_, IoReactivexInternalDisposablesSequentialDisposable *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter *self, id<OrgReactivestreamsSubscriber> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter))
#define IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter_

@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter : IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onNextWithId:(id)t;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter *self, id<OrgReactivestreamsSubscriber> downstream);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter *new_IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter *create_IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCreate_MissingEmitter)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter))
#define IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter_

@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter : IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onNextWithId:(id)t;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream;

- (void)onOverflow;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter *self, id<OrgReactivestreamsSubscriber> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter))
#define IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter_

@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter : IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream;

- (void)onOverflow;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter *self, id<OrgReactivestreamsSubscriber> downstream);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter *new_IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter *create_IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCreate_DropAsyncEmitter)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter))
#define IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter_

@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter : IoReactivexInternalOperatorsFlowableFlowableCreate_NoOverflowBaseAsyncEmitter

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream;

- (void)onOverflow;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter *self, id<OrgReactivestreamsSubscriber> downstream);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter *new_IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter *create_IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCreate_ErrorAsyncEmitter)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter))
#define IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter_

@class IoReactivexInternalQueueSpscLinkedArrayQueue;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicInteger;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter : IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter {
 @public
  IoReactivexInternalQueueSpscLinkedArrayQueue *queue_;
  JavaLangThrowable *error_;
  volatile_jboolean done_;
  JavaUtilConcurrentAtomicAtomicInteger *wip_;
}

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onNextWithId:(id)t;

- (jboolean)tryOnErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                                                       withInt:(jint)capacityHint;

- (void)drain;

- (void)onRequested;

- (void)onUnsubscribed;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter, queue_, IoReactivexInternalQueueSpscLinkedArrayQueue *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter, error_, JavaLangThrowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter, wip_, JavaUtilConcurrentAtomicAtomicInteger *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter_initWithOrgReactivestreamsSubscriber_withInt_(IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter *self, id<OrgReactivestreamsSubscriber> actual, jint capacityHint);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter *new_IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter_initWithOrgReactivestreamsSubscriber_withInt_(id<OrgReactivestreamsSubscriber> actual, jint capacityHint) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter *create_IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter_initWithOrgReactivestreamsSubscriber_withInt_(id<OrgReactivestreamsSubscriber> actual, jint capacityHint);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCreate_BufferAsyncEmitter)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter))
#define IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter_

@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicInteger;
@class JavaUtilConcurrentAtomicAtomicReference;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter : IoReactivexInternalOperatorsFlowableFlowableCreate_BaseEmitter {
 @public
  JavaUtilConcurrentAtomicAtomicReference *queue_;
  JavaLangThrowable *error_;
  volatile_jboolean done_;
  JavaUtilConcurrentAtomicAtomicInteger *wip_;
}

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onNextWithId:(id)t;

- (jboolean)tryOnErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream;

- (void)drain;

- (void)onRequested;

- (void)onUnsubscribed;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter, queue_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter, error_, JavaLangThrowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter, wip_, JavaUtilConcurrentAtomicAtomicInteger *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter *self, id<OrgReactivestreamsSubscriber> downstream);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter *new_IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter *create_IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCreate_LatestAsyncEmitter)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCreate")
