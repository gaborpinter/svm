//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableFromObservable.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/Observable.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/internal/operators/flowable/FlowableFromObservable.h"
#include "java/lang/Throwable.h"
#include "org/reactivestreams/Subscriber.h"

@interface IoReactivexInternalOperatorsFlowableFlowableFromObservable () {
 @public
  IoReactivexObservable *upstream_;
}

@end

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableFromObservable, upstream_, IoReactivexObservable *)

@implementation IoReactivexInternalOperatorsFlowableFlowableFromObservable

- (instancetype __nonnull)initWithIoReactivexObservable:(IoReactivexObservable *)upstream {
  IoReactivexInternalOperatorsFlowableFlowableFromObservable_initWithIoReactivexObservable_(self, upstream);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  [((IoReactivexObservable *) nil_chk(upstream_)) subscribeWithIoReactivexObserver:create_IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver_initWithOrgReactivestreamsSubscriber_(s)];
}

- (void)dealloc {
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservable:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "upstream_", "LIoReactivexObservable;", .constantValue.asLong = 0, 0x12, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservable;", "(Lio/reactivex/Observable<TT;>;)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "Lio/reactivex/Observable<TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/Flowable<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableFromObservable = { "FlowableFromObservable", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableFromObservable;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableFromObservable_initWithIoReactivexObservable_(IoReactivexInternalOperatorsFlowableFlowableFromObservable *self, IoReactivexObservable *upstream) {
  IoReactivexFlowable_init(self);
  JreStrongAssign(&self->upstream_, upstream);
}

IoReactivexInternalOperatorsFlowableFlowableFromObservable *new_IoReactivexInternalOperatorsFlowableFlowableFromObservable_initWithIoReactivexObservable_(IoReactivexObservable *upstream) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableFromObservable, initWithIoReactivexObservable_, upstream)
}

IoReactivexInternalOperatorsFlowableFlowableFromObservable *create_IoReactivexInternalOperatorsFlowableFlowableFromObservable_initWithIoReactivexObservable_(IoReactivexObservable *upstream) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableFromObservable, initWithIoReactivexObservable_, upstream)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableFromObservable)

@implementation IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver_initWithOrgReactivestreamsSubscriber_(self, s);
  return self;
}

- (void)onComplete {
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onComplete];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
}

- (void)onNextWithId:(id)value {
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:value];
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  JreStrongAssign(&self->upstream_, d);
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onSubscribeWithOrgReactivestreamsSubscription:self];
}

- (void)cancel {
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
}

- (void)requestWithLong:(jlong)n {
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:);
  methods[1].selector = @selector(onComplete);
  methods[2].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[3].selector = @selector(onNextWithId:);
  methods[4].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[5].selector = @selector(cancel);
  methods[6].selector = @selector(requestWithLong:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "downstream_", "LOrgReactivestreamsSubscriber;", .constantValue.asLong = 0, 0x10, -1, -1, 11, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "onError", "LJavaLangThrowable;", "onNext", "LNSObject;", "(TT;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "request", "J", "Lorg/reactivestreams/Subscriber<-TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableFromObservable;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/Observer<TT;>;Lorg/reactivestreams/Subscription;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver = { "SubscriberObserver", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 7, 2, 12, -1, -1, 13, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver *self, id<OrgReactivestreamsSubscriber> s) {
  NSObject_init(self);
  JreStrongAssign(&self->downstream_, s);
}

IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver *new_IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> s) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver, initWithOrgReactivestreamsSubscriber_, s)
}

IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver *create_IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> s) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver, initWithOrgReactivestreamsSubscriber_, s)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableFromObservable_SubscriberObserver)
