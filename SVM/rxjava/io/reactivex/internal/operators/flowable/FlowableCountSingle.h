//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableCountSingle.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCountSingle")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableCountSingle
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCountSingle 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCountSingle 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableCountSingle

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCountSingle_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCountSingle || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCountSingle))
#define IoReactivexInternalOperatorsFlowableFlowableCountSingle_

#define RESTRICT_IoReactivexSingle 1
#define INCLUDE_IoReactivexSingle 1
#include "io/reactivex/Single.h"

#define RESTRICT_IoReactivexInternalFuseableFuseToFlowable 1
#define INCLUDE_IoReactivexInternalFuseableFuseToFlowable 1
#include "io/reactivex/internal/fuseable/FuseToFlowable.h"

@class IoReactivexFlowable;
@class JavaLangLong;
@protocol IoReactivexSingleObserver;

@interface IoReactivexInternalOperatorsFlowableFlowableCountSingle : IoReactivexSingle < IoReactivexInternalFuseableFuseToFlowable > {
 @public
  IoReactivexFlowable *source_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source;

- (JavaLangLong *)blockingGet;

- (IoReactivexFlowable *)fuseToFlowable;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCountSingle)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCountSingle, source_, IoReactivexFlowable *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCountSingle_initWithIoReactivexFlowable_(IoReactivexInternalOperatorsFlowableFlowableCountSingle *self, IoReactivexFlowable *source);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCountSingle *new_IoReactivexInternalOperatorsFlowableFlowableCountSingle_initWithIoReactivexFlowable_(IoReactivexFlowable *source) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCountSingle *create_IoReactivexInternalOperatorsFlowableFlowableCountSingle_initWithIoReactivexFlowable_(IoReactivexFlowable *source);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCountSingle)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCountSingle || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber_

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class JavaLangThrowable;
@protocol IoReactivexSingleObserver;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber : NSObject < IoReactivexFlowableSubscriber, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexSingleObserver> downstream_;
  id<OrgReactivestreamsSubscription> upstream_;
  jlong count_;
}

#pragma mark Public

- (void)dispose;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)downstream;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber, downstream_, id<IoReactivexSingleObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber, upstream_, id<OrgReactivestreamsSubscription>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber_initWithIoReactivexSingleObserver_(IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber *self, id<IoReactivexSingleObserver> downstream);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber_initWithIoReactivexSingleObserver_(id<IoReactivexSingleObserver> downstream) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber_initWithIoReactivexSingleObserver_(id<IoReactivexSingleObserver> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableCountSingle_CountSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableCountSingle")
