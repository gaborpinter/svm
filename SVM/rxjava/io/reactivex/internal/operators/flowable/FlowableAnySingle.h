//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableAnySingle.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableAnySingle")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableAnySingle
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableAnySingle 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableAnySingle 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableAnySingle

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableAnySingle_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableAnySingle || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableAnySingle))
#define IoReactivexInternalOperatorsFlowableFlowableAnySingle_

#define RESTRICT_IoReactivexSingle 1
#define INCLUDE_IoReactivexSingle 1
#include "io/reactivex/Single.h"

#define RESTRICT_IoReactivexInternalFuseableFuseToFlowable 1
#define INCLUDE_IoReactivexInternalFuseableFuseToFlowable 1
#include "io/reactivex/internal/fuseable/FuseToFlowable.h"

@class IoReactivexFlowable;
@class JavaLangBoolean;
@protocol IoReactivexFunctionsPredicate;
@protocol IoReactivexSingleObserver;

@interface IoReactivexInternalOperatorsFlowableFlowableAnySingle : IoReactivexSingle < IoReactivexInternalFuseableFuseToFlowable > {
 @public
  IoReactivexFlowable *source_;
  id<IoReactivexFunctionsPredicate> predicate_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                    withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)predicate;

- (JavaLangBoolean *)blockingGet;

- (IoReactivexFlowable *)fuseToFlowable;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableAnySingle)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableAnySingle, source_, IoReactivexFlowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableAnySingle, predicate_, id<IoReactivexFunctionsPredicate>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableAnySingle_initWithIoReactivexFlowable_withIoReactivexFunctionsPredicate_(IoReactivexInternalOperatorsFlowableFlowableAnySingle *self, IoReactivexFlowable *source, id<IoReactivexFunctionsPredicate> predicate);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableAnySingle *new_IoReactivexInternalOperatorsFlowableFlowableAnySingle_initWithIoReactivexFlowable_withIoReactivexFunctionsPredicate_(IoReactivexFlowable *source, id<IoReactivexFunctionsPredicate> predicate) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableAnySingle *create_IoReactivexInternalOperatorsFlowableFlowableAnySingle_initWithIoReactivexFlowable_withIoReactivexFunctionsPredicate_(IoReactivexFlowable *source, id<IoReactivexFunctionsPredicate> predicate);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableAnySingle)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableAnySingle || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber_

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class JavaLangThrowable;
@protocol IoReactivexFunctionsPredicate;
@protocol IoReactivexSingleObserver;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber : NSObject < IoReactivexFlowableSubscriber, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexSingleObserver> downstream_;
  id<IoReactivexFunctionsPredicate> predicate_;
  id<OrgReactivestreamsSubscription> upstream_;
  jboolean done_;
}

#pragma mark Public

- (void)dispose;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)actual
                          withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)predicate;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber, downstream_, id<IoReactivexSingleObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber, predicate_, id<IoReactivexFunctionsPredicate>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber, upstream_, id<OrgReactivestreamsSubscription>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber_initWithIoReactivexSingleObserver_withIoReactivexFunctionsPredicate_(IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber *self, id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsPredicate> predicate);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber *new_IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber_initWithIoReactivexSingleObserver_withIoReactivexFunctionsPredicate_(id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsPredicate> predicate) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber *create_IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber_initWithIoReactivexSingleObserver_withIoReactivexFunctionsPredicate_(id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsPredicate> predicate);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableAnySingle_AnySubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableAnySingle")
