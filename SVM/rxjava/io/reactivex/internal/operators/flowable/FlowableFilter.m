//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableFilter.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/functions/Predicate.h"
#include "io/reactivex/internal/fuseable/ConditionalSubscriber.h"
#include "io/reactivex/internal/fuseable/QueueFuseable.h"
#include "io/reactivex/internal/fuseable/QueueSubscription.h"
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"
#include "io/reactivex/internal/operators/flowable/FlowableFilter.h"
#include "io/reactivex/internal/subscribers/BasicFuseableConditionalSubscriber.h"
#include "io/reactivex/internal/subscribers/BasicFuseableSubscriber.h"
#include "java/lang/Throwable.h"
#include "org/reactivestreams/Subscriber.h"
#include "org/reactivestreams/Subscription.h"

@implementation IoReactivexInternalOperatorsFlowableFlowableFilter

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                    withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)predicate {
  IoReactivexInternalOperatorsFlowableFlowableFilter_initWithIoReactivexFlowable_withIoReactivexFunctionsPredicate_(self, source, predicate);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  if ([IoReactivexInternalFuseableConditionalSubscriber_class_() isInstance:s]) {
    [((IoReactivexFlowable *) nil_chk(source_)) subscribeWithIoReactivexFlowableSubscriber:create_IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber_initWithIoReactivexInternalFuseableConditionalSubscriber_withIoReactivexFunctionsPredicate_((id<IoReactivexInternalFuseableConditionalSubscriber>) cast_check(s, IoReactivexInternalFuseableConditionalSubscriber_class_()), predicate_)];
  }
  else {
    [((IoReactivexFlowable *) nil_chk(source_)) subscribeWithIoReactivexFlowableSubscriber:create_IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsPredicate_(s, predicate_)];
  }
}

- (void)dealloc {
  RELEASE_(predicate_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFlowable:withIoReactivexFunctionsPredicate:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "predicate_", "LIoReactivexFunctionsPredicate;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexFlowable;LIoReactivexFunctionsPredicate;", "(Lio/reactivex/Flowable<TT;>;Lio/reactivex/functions/Predicate<-TT;>;)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "Lio/reactivex/functions/Predicate<-TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber;LIoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableFilter = { "FlowableFilter", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableFilter;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableFilter_initWithIoReactivexFlowable_withIoReactivexFunctionsPredicate_(IoReactivexInternalOperatorsFlowableFlowableFilter *self, IoReactivexFlowable *source, id<IoReactivexFunctionsPredicate> predicate) {
  IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream_initWithIoReactivexFlowable_(self, source);
  JreStrongAssign(&self->predicate_, predicate);
}

IoReactivexInternalOperatorsFlowableFlowableFilter *new_IoReactivexInternalOperatorsFlowableFlowableFilter_initWithIoReactivexFlowable_withIoReactivexFunctionsPredicate_(IoReactivexFlowable *source, id<IoReactivexFunctionsPredicate> predicate) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableFilter, initWithIoReactivexFlowable_withIoReactivexFunctionsPredicate_, source, predicate)
}

IoReactivexInternalOperatorsFlowableFlowableFilter *create_IoReactivexInternalOperatorsFlowableFlowableFilter_initWithIoReactivexFlowable_withIoReactivexFunctionsPredicate_(IoReactivexFlowable *source, id<IoReactivexFunctionsPredicate> predicate) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableFilter, initWithIoReactivexFlowable_withIoReactivexFunctionsPredicate_, source, predicate)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableFilter)

@implementation IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                             withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)filter {
  IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsPredicate_(self, actual, filter);
  return self;
}

- (void)onNextWithId:(id)t {
  if (![self tryOnNextWithId:t]) {
    [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) requestWithLong:1];
  }
}

- (jboolean)tryOnNextWithId:(id)t {
  if (done_) {
    return false;
  }
  if (sourceMode_ != IoReactivexInternalFuseableQueueFuseable_NONE) {
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:nil];
    return true;
  }
  jboolean b;
  @try {
    b = [((id<IoReactivexFunctionsPredicate>) nil_chk(filter_)) testWithId:t];
  }
  @catch (JavaLangThrowable *e) {
    [self failWithJavaLangThrowable:e];
    return true;
  }
  if (b) {
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onNextWithId:t];
  }
  return b;
}

- (jint)requestFusionWithInt:(jint)mode {
  return [self transitiveBoundaryFusionWithInt:mode];
}

- (id __nullable)poll {
  id<IoReactivexInternalFuseableQueueSubscription> qs = self->qs_;
  id<IoReactivexFunctionsPredicate> f = filter_;
  for (; ; ) {
    id t = [((id<IoReactivexInternalFuseableQueueSubscription>) nil_chk(qs)) poll];
    if (t == nil) {
      return nil;
    }
    if ([((id<IoReactivexFunctionsPredicate>) nil_chk(f)) testWithId:t]) {
      return t;
    }
    if (sourceMode_ == IoReactivexInternalFuseableQueueFuseable_ASYNC) {
      [qs requestWithLong:1];
    }
  }
}

- (void)dealloc {
  RELEASE_(filter_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
    { NULL, "Z", 0x1, 5, 3, -1, 6, -1, -1 },
    { NULL, "I", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, 9, 10, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:withIoReactivexFunctionsPredicate:);
  methods[1].selector = @selector(onNextWithId:);
  methods[2].selector = @selector(tryOnNextWithId:);
  methods[3].selector = @selector(requestFusionWithInt:);
  methods[4].selector = @selector(poll);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "filter_", "LIoReactivexFunctionsPredicate;", .constantValue.asLong = 0, 0x10, -1, -1, 11, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;LIoReactivexFunctionsPredicate;", "(Lorg/reactivestreams/Subscriber<-TT;>;Lio/reactivex/functions/Predicate<-TT;>;)V", "onNext", "LNSObject;", "(TT;)V", "tryOnNext", "(TT;)Z", "requestFusion", "I", "LJavaLangException;", "()TT;", "Lio/reactivex/functions/Predicate<-TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableFilter;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/subscribers/BasicFuseableSubscriber<TT;TT;>;Lio/reactivex/internal/fuseable/ConditionalSubscriber<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber = { "FilterSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 5, 1, 12, -1, -1, 13, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsPredicate_(IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber *self, id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsPredicate> filter) {
  IoReactivexInternalSubscribersBasicFuseableSubscriber_initWithOrgReactivestreamsSubscriber_(self, actual);
  JreStrongAssign(&self->filter_, filter);
}

IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsPredicate_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsPredicate> filter) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber, initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsPredicate_, actual, filter)
}

IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber_initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsPredicate_(id<OrgReactivestreamsSubscriber> actual, id<IoReactivexFunctionsPredicate> filter) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber, initWithOrgReactivestreamsSubscriber_withIoReactivexFunctionsPredicate_, actual, filter)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableFilter_FilterSubscriber)

@implementation IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber

- (instancetype __nonnull)initWithIoReactivexInternalFuseableConditionalSubscriber:(id<IoReactivexInternalFuseableConditionalSubscriber>)actual
                                                 withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)filter {
  IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber_initWithIoReactivexInternalFuseableConditionalSubscriber_withIoReactivexFunctionsPredicate_(self, actual, filter);
  return self;
}

- (void)onNextWithId:(id)t {
  if (![self tryOnNextWithId:t]) {
    [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) requestWithLong:1];
  }
}

- (jboolean)tryOnNextWithId:(id)t {
  if (done_) {
    return false;
  }
  if (sourceMode_ != IoReactivexInternalFuseableQueueFuseable_NONE) {
    return [((id<IoReactivexInternalFuseableConditionalSubscriber>) nil_chk(downstream_)) tryOnNextWithId:nil];
  }
  jboolean b;
  @try {
    b = [((id<IoReactivexFunctionsPredicate>) nil_chk(filter_)) testWithId:t];
  }
  @catch (JavaLangThrowable *e) {
    [self failWithJavaLangThrowable:e];
    return true;
  }
  return b && [((id<IoReactivexInternalFuseableConditionalSubscriber>) nil_chk(downstream_)) tryOnNextWithId:t];
}

- (jint)requestFusionWithInt:(jint)mode {
  return [self transitiveBoundaryFusionWithInt:mode];
}

- (id __nullable)poll {
  id<IoReactivexInternalFuseableQueueSubscription> qs = self->qs_;
  id<IoReactivexFunctionsPredicate> f = filter_;
  for (; ; ) {
    id t = [((id<IoReactivexInternalFuseableQueueSubscription>) nil_chk(qs)) poll];
    if (t == nil) {
      return nil;
    }
    if ([((id<IoReactivexFunctionsPredicate>) nil_chk(f)) testWithId:t]) {
      return t;
    }
    if (sourceMode_ == IoReactivexInternalFuseableQueueFuseable_ASYNC) {
      [qs requestWithLong:1];
    }
  }
}

- (void)dealloc {
  RELEASE_(filter_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
    { NULL, "Z", 0x1, 5, 3, -1, 6, -1, -1 },
    { NULL, "I", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, 9, 10, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalFuseableConditionalSubscriber:withIoReactivexFunctionsPredicate:);
  methods[1].selector = @selector(onNextWithId:);
  methods[2].selector = @selector(tryOnNextWithId:);
  methods[3].selector = @selector(requestFusionWithInt:);
  methods[4].selector = @selector(poll);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "filter_", "LIoReactivexFunctionsPredicate;", .constantValue.asLong = 0, 0x10, -1, -1, 11, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexInternalFuseableConditionalSubscriber;LIoReactivexFunctionsPredicate;", "(Lio/reactivex/internal/fuseable/ConditionalSubscriber<-TT;>;Lio/reactivex/functions/Predicate<-TT;>;)V", "onNext", "LNSObject;", "(TT;)V", "tryOnNext", "(TT;)Z", "requestFusion", "I", "LJavaLangException;", "()TT;", "Lio/reactivex/functions/Predicate<-TT;>;", "LIoReactivexInternalOperatorsFlowableFlowableFilter;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/subscribers/BasicFuseableConditionalSubscriber<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber = { "FilterConditionalSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 5, 1, 12, -1, -1, 13, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber_initWithIoReactivexInternalFuseableConditionalSubscriber_withIoReactivexFunctionsPredicate_(IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber *self, id<IoReactivexInternalFuseableConditionalSubscriber> actual, id<IoReactivexFunctionsPredicate> filter) {
  IoReactivexInternalSubscribersBasicFuseableConditionalSubscriber_initWithIoReactivexInternalFuseableConditionalSubscriber_(self, actual);
  JreStrongAssign(&self->filter_, filter);
}

IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber_initWithIoReactivexInternalFuseableConditionalSubscriber_withIoReactivexFunctionsPredicate_(id<IoReactivexInternalFuseableConditionalSubscriber> actual, id<IoReactivexFunctionsPredicate> filter) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber, initWithIoReactivexInternalFuseableConditionalSubscriber_withIoReactivexFunctionsPredicate_, actual, filter)
}

IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber_initWithIoReactivexInternalFuseableConditionalSubscriber_withIoReactivexFunctionsPredicate_(id<IoReactivexInternalFuseableConditionalSubscriber> actual, id<IoReactivexFunctionsPredicate> filter) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber, initWithIoReactivexInternalFuseableConditionalSubscriber_withIoReactivexFunctionsPredicate_, actual, filter)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableFilter_FilterConditionalSubscriber)
