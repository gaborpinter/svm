//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableOnBackpressureBuffer.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer))
#define IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_

#define RESTRICT_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream 1
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"

@class IoReactivexFlowable;
@protocol IoReactivexFunctionsAction;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer : IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream {
 @public
  jint bufferSize_;
  jboolean unbounded_;
  jboolean delayError_;
  id<IoReactivexFunctionsAction> onOverflow_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                                              withInt:(jint)bufferSize
                                          withBoolean:(jboolean)unbounded
                                          withBoolean:(jboolean)delayError
                       withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onOverflow;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer, onOverflow_, id<IoReactivexFunctionsAction>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_initWithIoReactivexFlowable_withInt_withBoolean_withBoolean_withIoReactivexFunctionsAction_(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer *self, IoReactivexFlowable *source, jint bufferSize, jboolean unbounded, jboolean delayError, id<IoReactivexFunctionsAction> onOverflow);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer *new_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_initWithIoReactivexFlowable_withInt_withBoolean_withBoolean_withIoReactivexFunctionsAction_(IoReactivexFlowable *source, jint bufferSize, jboolean unbounded, jboolean delayError, id<IoReactivexFunctionsAction> onOverflow) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer *create_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_initWithIoReactivexFlowable_withInt_withBoolean_withBoolean_withIoReactivexFunctionsAction_(IoReactivexFlowable *source, jint bufferSize, jboolean unbounded, jboolean delayError, id<IoReactivexFunctionsAction> onOverflow);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber_

#define RESTRICT_IoReactivexInternalSubscriptionsBasicIntQueueSubscription 1
#define INCLUDE_IoReactivexInternalSubscriptionsBasicIntQueueSubscription 1
#include "io/reactivex/internal/subscriptions/BasicIntQueueSubscription.h"

#define RESTRICT_IoReactivexFlowableSubscriber 1
#define INCLUDE_IoReactivexFlowableSubscriber 1
#include "io/reactivex/FlowableSubscriber.h"

@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicLong;
@protocol IoReactivexFunctionsAction;
@protocol IoReactivexInternalFuseableSimplePlainQueue;
@protocol OrgReactivestreamsSubscriber;
@protocol OrgReactivestreamsSubscription;

@interface IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber : IoReactivexInternalSubscriptionsBasicIntQueueSubscription < IoReactivexFlowableSubscriber > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  id<IoReactivexInternalFuseableSimplePlainQueue> queue_;
  jboolean delayError_;
  id<IoReactivexFunctionsAction> onOverflow_;
  id<OrgReactivestreamsSubscription> upstream_;
  volatile_jboolean cancelled_;
  volatile_jboolean done_;
  JavaLangThrowable *error_;
  JavaUtilConcurrentAtomicAtomicLong *requested_;
  jboolean outputFused_;
}

#pragma mark Public

- (void)cancel;

- (void)clear;

- (NSUInteger)hash;

- (jboolean)isEmpty;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s;

- (id __nullable)poll;

- (void)requestWithLong:(jlong)n;

- (jint)requestFusionWithInt:(jint)mode;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                                                       withInt:(jint)bufferSize
                                                   withBoolean:(jboolean)unbounded
                                                   withBoolean:(jboolean)delayError
                                withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onOverflow;

- (jboolean)checkTerminatedWithBoolean:(jboolean)d
                           withBoolean:(jboolean)empty
      withOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)a;

- (void)drain;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber, queue_, id<IoReactivexInternalFuseableSimplePlainQueue>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber, onOverflow_, id<IoReactivexFunctionsAction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber, upstream_, id<OrgReactivestreamsSubscription>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber, error_, JavaLangThrowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber, requested_, JavaUtilConcurrentAtomicAtomicLong *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withBoolean_withBoolean_withIoReactivexFunctionsAction_(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber *self, id<OrgReactivestreamsSubscriber> actual, jint bufferSize, jboolean unbounded, jboolean delayError, id<IoReactivexFunctionsAction> onOverflow);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withBoolean_withBoolean_withIoReactivexFunctionsAction_(id<OrgReactivestreamsSubscriber> actual, jint bufferSize, jboolean unbounded, jboolean delayError, id<IoReactivexFunctionsAction> onOverflow) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber_initWithOrgReactivestreamsSubscriber_withInt_withBoolean_withBoolean_withIoReactivexFunctionsAction_(id<OrgReactivestreamsSubscriber> actual, jint bufferSize, jboolean unbounded, jboolean delayError, id<IoReactivexFunctionsAction> onOverflow);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer_BackpressureBufferSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableOnBackpressureBuffer")
