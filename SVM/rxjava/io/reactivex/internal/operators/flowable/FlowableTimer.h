//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableTimer.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableTimer")
#ifdef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableTimer
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableTimer 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableTimer 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsFlowableFlowableTimer

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableTimer_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableTimer || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableTimer))
#define IoReactivexInternalOperatorsFlowableFlowableTimer_

#define RESTRICT_IoReactivexFlowable 1
#define INCLUDE_IoReactivexFlowable 1
#include "io/reactivex/Flowable.h"

@class IoReactivexScheduler;
@class JavaLangLong;
@class JavaUtilConcurrentTimeUnit;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableTimer : IoReactivexFlowable {
 @public
  IoReactivexScheduler *scheduler_;
  jlong delay_;
  JavaUtilConcurrentTimeUnit *unit_;
}

#pragma mark Public

- (instancetype __nonnull)initWithLong:(jlong)delay
        withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit
              withIoReactivexScheduler:(IoReactivexScheduler *)scheduler;

- (JavaLangLong *)blockingFirst;

- (JavaLangLong *)blockingFirstWithId:(JavaLangLong *)arg0;

- (JavaLangLong *)blockingLast;

- (JavaLangLong *)blockingLastWithId:(JavaLangLong *)arg0;

- (JavaLangLong *)blockingSingle;

- (JavaLangLong *)blockingSingleWithId:(JavaLangLong *)arg0;

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableTimer)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableTimer, scheduler_, IoReactivexScheduler *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableTimer, unit_, JavaUtilConcurrentTimeUnit *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableTimer_initWithLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_(IoReactivexInternalOperatorsFlowableFlowableTimer *self, jlong delay, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableTimer *new_IoReactivexInternalOperatorsFlowableFlowableTimer_initWithLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_(jlong delay, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableTimer *create_IoReactivexInternalOperatorsFlowableFlowableTimer_initWithLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_(jlong delay, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableTimer)

#endif

#if !defined (IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber_) && (INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableTimer || defined(INCLUDE_IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber))
#define IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_OrgReactivestreamsSubscription 1
#define INCLUDE_OrgReactivestreamsSubscription 1
#include "org/reactivestreams/Subscription.h"

#define RESTRICT_JavaLangRunnable 1
#define INCLUDE_JavaLangRunnable 1
#include "java/lang/Runnable.h"

@protocol IoReactivexDisposablesDisposable;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber : JavaUtilConcurrentAtomicAtomicReference < OrgReactivestreamsSubscription, JavaLangRunnable > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  volatile_jboolean requested_;
}

#pragma mark Public

- (id<IoReactivexDisposablesDisposable>)accumulateAndGetWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)cancel;

- (id<IoReactivexDisposablesDisposable>)get;

- (id<IoReactivexDisposablesDisposable>)getAndAccumulateWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)getAndSetWithId:(id<IoReactivexDisposablesDisposable>)arg0;

- (id<IoReactivexDisposablesDisposable>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (void)requestWithLong:(jlong)n;

- (void)run;

- (void)setResourceWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (id<IoReactivexDisposablesDisposable>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber, downstream_, id<OrgReactivestreamsSubscriber>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber *self, id<OrgReactivestreamsSubscriber> downstream);

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsFlowableFlowableTimer_TimerSubscriber)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsFlowableFlowableTimer")
