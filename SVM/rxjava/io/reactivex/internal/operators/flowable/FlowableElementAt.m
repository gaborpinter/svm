//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/flowable/FlowableElementAt.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream.h"
#include "io/reactivex/internal/operators/flowable/FlowableElementAt.h"
#include "io/reactivex/internal/subscriptions/DeferredScalarSubscription.h"
#include "io/reactivex/internal/subscriptions/SubscriptionHelper.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Long.h"
#include "java/lang/Throwable.h"
#include "java/util/NoSuchElementException.h"
#include "org/reactivestreams/Subscriber.h"
#include "org/reactivestreams/Subscription.h"

inline jlong IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber_serialVersionUID 4066607327284737757LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsFlowableFlowableElementAt

- (instancetype __nonnull)initWithIoReactivexFlowable:(IoReactivexFlowable *)source
                                             withLong:(jlong)index
                                               withId:(id)defaultValue
                                          withBoolean:(jboolean)errorOnFewer {
  IoReactivexInternalOperatorsFlowableFlowableElementAt_initWithIoReactivexFlowable_withLong_withId_withBoolean_(self, source, index, defaultValue, errorOnFewer);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  [((IoReactivexFlowable *) nil_chk(source_)) subscribeWithIoReactivexFlowableSubscriber:create_IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber_initWithOrgReactivestreamsSubscriber_withLong_withId_withBoolean_(s, index_, defaultValue_, errorOnFewer_)];
}

- (void)dealloc {
  RELEASE_(defaultValue_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexFlowable:withLong:withId:withBoolean:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "index_", "J", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "defaultValue_", "LNSObject;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "errorOnFewer_", "Z", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexFlowable;JLNSObject;Z", "(Lio/reactivex/Flowable<TT;>;JTT;Z)V", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "TT;", "LIoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/flowable/AbstractFlowableWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableElementAt = { "FlowableElementAt", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x11, 2, 3, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableElementAt;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableElementAt_initWithIoReactivexFlowable_withLong_withId_withBoolean_(IoReactivexInternalOperatorsFlowableFlowableElementAt *self, IoReactivexFlowable *source, jlong index, id defaultValue, jboolean errorOnFewer) {
  IoReactivexInternalOperatorsFlowableAbstractFlowableWithUpstream_initWithIoReactivexFlowable_(self, source);
  self->index_ = index;
  JreStrongAssign(&self->defaultValue_, defaultValue);
  self->errorOnFewer_ = errorOnFewer;
}

IoReactivexInternalOperatorsFlowableFlowableElementAt *new_IoReactivexInternalOperatorsFlowableFlowableElementAt_initWithIoReactivexFlowable_withLong_withId_withBoolean_(IoReactivexFlowable *source, jlong index, id defaultValue, jboolean errorOnFewer) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableElementAt, initWithIoReactivexFlowable_withLong_withId_withBoolean_, source, index, defaultValue, errorOnFewer)
}

IoReactivexInternalOperatorsFlowableFlowableElementAt *create_IoReactivexInternalOperatorsFlowableFlowableElementAt_initWithIoReactivexFlowable_withLong_withId_withBoolean_(IoReactivexFlowable *source, jlong index, id defaultValue, jboolean errorOnFewer) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableElementAt, initWithIoReactivexFlowable_withLong_withId_withBoolean_, source, index, defaultValue, errorOnFewer)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableElementAt)

@implementation IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                                                      withLong:(jlong)index
                                                        withId:(id)defaultValue
                                                   withBoolean:(jboolean)errorOnFewer {
  IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber_initWithOrgReactivestreamsSubscriber_withLong_withId_withBoolean_(self, actual, index, defaultValue, errorOnFewer);
  return self;
}

- (void)onSubscribeWithOrgReactivestreamsSubscription:(id<OrgReactivestreamsSubscription>)s {
  if (IoReactivexInternalSubscriptionsSubscriptionHelper_validateWithOrgReactivestreamsSubscription_withOrgReactivestreamsSubscription_(self->upstream_, s)) {
    JreStrongAssign(&self->upstream_, s);
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onSubscribeWithOrgReactivestreamsSubscription:self];
    [((id<OrgReactivestreamsSubscription>) nil_chk(s)) requestWithLong:JavaLangLong_MAX_VALUE];
  }
}

- (void)onNextWithId:(id)t {
  if (done_) {
    return;
  }
  jlong c = count_;
  if (c == index_) {
    done_ = true;
    [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
    [self completeWithId:t];
    return;
  }
  count_ = c + 1;
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if (done_) {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
    return;
  }
  done_ = true;
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  if (!done_) {
    done_ = true;
    id v = defaultValue_;
    if (v == nil) {
      if (errorOnFewer_) {
        [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:create_JavaUtilNoSuchElementException_init()];
      }
      else {
        [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onComplete];
      }
    }
    else {
      [self completeWithId:v];
    }
  }
}

- (void)cancel {
  [super cancel];
  [((id<OrgReactivestreamsSubscription>) nil_chk(upstream_)) cancel];
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(defaultValue_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:withLong:withId:withBoolean:);
  methods[1].selector = @selector(onSubscribeWithOrgReactivestreamsSubscription:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(cancel);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "index_", "J", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "defaultValue_", "LNSObject;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "errorOnFewer_", "Z", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LOrgReactivestreamsSubscription;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "count_", "J", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;JLNSObject;Z", "(Lorg/reactivestreams/Subscriber<-TT;>;JTT;Z)V", "onSubscribe", "LOrgReactivestreamsSubscription;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "TT;", "LIoReactivexInternalOperatorsFlowableFlowableElementAt;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/subscriptions/DeferredScalarSubscription<TT;>;Lio/reactivex/FlowableSubscriber<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber = { "ElementAtSubscriber", "io.reactivex.internal.operators.flowable", ptrTable, methods, fields, 7, 0x18, 6, 7, 10, -1, -1, 11, -1 };
  return &_IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber;
}

@end

void IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber_initWithOrgReactivestreamsSubscriber_withLong_withId_withBoolean_(IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber *self, id<OrgReactivestreamsSubscriber> actual, jlong index, id defaultValue, jboolean errorOnFewer) {
  IoReactivexInternalSubscriptionsDeferredScalarSubscription_initWithOrgReactivestreamsSubscriber_(self, actual);
  self->index_ = index;
  JreStrongAssign(&self->defaultValue_, defaultValue);
  self->errorOnFewer_ = errorOnFewer;
}

IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber *new_IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber_initWithOrgReactivestreamsSubscriber_withLong_withId_withBoolean_(id<OrgReactivestreamsSubscriber> actual, jlong index, id defaultValue, jboolean errorOnFewer) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber, initWithOrgReactivestreamsSubscriber_withLong_withId_withBoolean_, actual, index, defaultValue, errorOnFewer)
}

IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber *create_IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber_initWithOrgReactivestreamsSubscriber_withLong_withId_withBoolean_(id<OrgReactivestreamsSubscriber> actual, jlong index, id defaultValue, jboolean errorOnFewer) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber, initWithOrgReactivestreamsSubscriber_withLong_withId_withBoolean_, actual, index, defaultValue, errorOnFewer)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsFlowableFlowableElementAt_ElementAtSubscriber)
