//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableTakeLastTimed.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeLastTimed")
#ifdef RESTRICT_IoReactivexInternalOperatorsObservableObservableTakeLastTimed
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeLastTimed 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeLastTimed 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsObservableObservableTakeLastTimed

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableTakeLastTimed_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeLastTimed || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableTakeLastTimed))
#define IoReactivexInternalOperatorsObservableObservableTakeLastTimed_

#define RESTRICT_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"

@class IoReactivexScheduler;
@class JavaUtilConcurrentTimeUnit;
@protocol IoReactivexObservableSource;
@protocol IoReactivexObserver;

@interface IoReactivexInternalOperatorsObservableObservableTakeLastTimed : IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream {
 @public
  jlong count_;
  jlong time_;
  JavaUtilConcurrentTimeUnit *unit_;
  IoReactivexScheduler *scheduler_;
  jint bufferSize_;
  jboolean delayError_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                                                     withLong:(jlong)count
                                                     withLong:(jlong)time
                               withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit
                                     withIoReactivexScheduler:(IoReactivexScheduler *)scheduler
                                                      withInt:(jint)bufferSize
                                                  withBoolean:(jboolean)delayError;

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)t;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableTakeLastTimed)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeLastTimed, unit_, JavaUtilConcurrentTimeUnit *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeLastTimed, scheduler_, IoReactivexScheduler *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableTakeLastTimed_initWithIoReactivexObservableSource_withLong_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(IoReactivexInternalOperatorsObservableObservableTakeLastTimed *self, id<IoReactivexObservableSource> source, jlong count, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableTakeLastTimed *new_IoReactivexInternalOperatorsObservableObservableTakeLastTimed_initWithIoReactivexObservableSource_withLong_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(id<IoReactivexObservableSource> source, jlong count, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableTakeLastTimed *create_IoReactivexInternalOperatorsObservableObservableTakeLastTimed_initWithIoReactivexObservableSource_withLong_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(id<IoReactivexObservableSource> source, jlong count, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableTakeLastTimed)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeLastTimed || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver))
#define IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicBoolean 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicBoolean 1
#include "java/util/concurrent/atomic/AtomicBoolean.h"

#define RESTRICT_IoReactivexObserver 1
#define INCLUDE_IoReactivexObserver 1
#include "io/reactivex/Observer.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class IoReactivexInternalQueueSpscLinkedArrayQueue;
@class IoReactivexScheduler;
@class JavaLangThrowable;
@class JavaUtilConcurrentTimeUnit;

@interface IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver : JavaUtilConcurrentAtomicAtomicBoolean < IoReactivexObserver, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexObserver> downstream_;
  jlong count_;
  jlong time_;
  JavaUtilConcurrentTimeUnit *unit_;
  IoReactivexScheduler *scheduler_;
  IoReactivexInternalQueueSpscLinkedArrayQueue *queue_;
  jboolean delayError_;
  id<IoReactivexDisposablesDisposable> upstream_;
  volatile_jboolean cancelled_;
  JavaLangThrowable *error_;
}

#pragma mark Public

- (void)dispose;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                                             withLong:(jlong)count
                                             withLong:(jlong)time
                       withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit
                             withIoReactivexScheduler:(IoReactivexScheduler *)scheduler
                                              withInt:(jint)bufferSize
                                          withBoolean:(jboolean)delayError;

- (void)drain;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithBoolean:(jboolean)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver, downstream_, id<IoReactivexObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver, unit_, JavaUtilConcurrentTimeUnit *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver, scheduler_, IoReactivexScheduler *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver, queue_, IoReactivexInternalQueueSpscLinkedArrayQueue *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver, upstream_, id<IoReactivexDisposablesDisposable>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver, error_, JavaLangThrowable *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver_initWithIoReactivexObserver_withLong_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver *self, id<IoReactivexObserver> actual, jlong count, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver *new_IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver_initWithIoReactivexObserver_withLong_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(id<IoReactivexObserver> actual, jlong count, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver *create_IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver_initWithIoReactivexObserver_withLong_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(id<IoReactivexObserver> actual, jlong count, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableTakeLastTimed_TakeLastTimedObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeLastTimed")
