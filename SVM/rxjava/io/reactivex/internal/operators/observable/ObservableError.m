//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableError.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Observable.h"
#include "io/reactivex/Observer.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/internal/disposables/EmptyDisposable.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/operators/observable/ObservableError.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/Callable.h"

@implementation IoReactivexInternalOperatorsObservableObservableError

- (instancetype __nonnull)initWithJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)errorSupplier {
  IoReactivexInternalOperatorsObservableObservableError_initWithJavaUtilConcurrentCallable_(self, errorSupplier);
  return self;
}

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer {
  JavaLangThrowable *error;
  @try {
    error = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<JavaUtilConcurrentCallable>) nil_chk(errorSupplier_)) call], @"Callable returned null throwable. Null values are generally not allowed in 2.x operators and sources.");
  }
  @catch (JavaLangThrowable *t) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(t);
    error = t;
  }
  IoReactivexInternalDisposablesEmptyDisposable_errorWithJavaLangThrowable_withIoReactivexObserver_(error, observer);
}

- (void)dealloc {
  RELEASE_(errorSupplier_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaUtilConcurrentCallable:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "errorSupplier_", "LJavaUtilConcurrentCallable;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LJavaUtilConcurrentCallable;", "(Ljava/util/concurrent/Callable<+Ljava/lang/Throwable;>;)V", "subscribeActual", "LIoReactivexObserver;", "(Lio/reactivex/Observer<-TT;>;)V", "Ljava/util/concurrent/Callable<+Ljava/lang/Throwable;>;", "<T:Ljava/lang/Object;>Lio/reactivex/Observable<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableError = { "ObservableError", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, -1, -1, 6, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableError;
}

@end

void IoReactivexInternalOperatorsObservableObservableError_initWithJavaUtilConcurrentCallable_(IoReactivexInternalOperatorsObservableObservableError *self, id<JavaUtilConcurrentCallable> errorSupplier) {
  IoReactivexObservable_init(self);
  JreStrongAssign(&self->errorSupplier_, errorSupplier);
}

IoReactivexInternalOperatorsObservableObservableError *new_IoReactivexInternalOperatorsObservableObservableError_initWithJavaUtilConcurrentCallable_(id<JavaUtilConcurrentCallable> errorSupplier) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableError, initWithJavaUtilConcurrentCallable_, errorSupplier)
}

IoReactivexInternalOperatorsObservableObservableError *create_IoReactivexInternalOperatorsObservableObservableError_initWithJavaUtilConcurrentCallable_(id<JavaUtilConcurrentCallable> errorSupplier) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableError, initWithJavaUtilConcurrentCallable_, errorSupplier)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableError)
