//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableSequenceEqual.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "io/reactivex/Observable.h"
#include "io/reactivex/ObservableSource.h"
#include "io/reactivex/Observer.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/BiPredicate.h"
#include "io/reactivex/internal/disposables/ArrayCompositeDisposable.h"
#include "io/reactivex/internal/operators/observable/ObservableSequenceEqual.h"
#include "io/reactivex/internal/queue/SpscLinkedArrayQueue.h"
#include "java/lang/Boolean.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_serialVersionUID -6178010334400373240LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsObservableObservableSequenceEqual

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)first
                              withIoReactivexObservableSource:(id<IoReactivexObservableSource>)second
                          withIoReactivexFunctionsBiPredicate:(id<IoReactivexFunctionsBiPredicate>)comparer
                                                      withInt:(jint)bufferSize {
  IoReactivexInternalOperatorsObservableObservableSequenceEqual_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_withInt_(self, first, second, comparer, bufferSize);
  return self;
}

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer {
  IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *ec = create_IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_initWithIoReactivexObserver_withInt_withIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_(observer, bufferSize_, first_, second_, comparer_);
  [((id<IoReactivexObserver>) nil_chk(observer)) onSubscribeWithIoReactivexDisposablesDisposable:ec];
  [ec subscribe];
}

- (void)dealloc {
  RELEASE_(first_);
  RELEASE_(second_);
  RELEASE_(comparer_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservableSource:withIoReactivexObservableSource:withIoReactivexFunctionsBiPredicate:withInt:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "first_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "second_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "comparer_", "LIoReactivexFunctionsBiPredicate;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
    { "bufferSize_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservableSource;LIoReactivexObservableSource;LIoReactivexFunctionsBiPredicate;I", "(Lio/reactivex/ObservableSource<+TT;>;Lio/reactivex/ObservableSource<+TT;>;Lio/reactivex/functions/BiPredicate<-TT;-TT;>;I)V", "subscribeActual", "LIoReactivexObserver;", "(Lio/reactivex/Observer<-Ljava/lang/Boolean;>;)V", "Lio/reactivex/ObservableSource<+TT;>;", "Lio/reactivex/functions/BiPredicate<-TT;-TT;>;", "LIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator;LIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/Observable<Ljava/lang/Boolean;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableSequenceEqual = { "ObservableSequenceEqual", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x11, 2, 4, -1, 7, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableSequenceEqual;
}

@end

void IoReactivexInternalOperatorsObservableObservableSequenceEqual_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_withInt_(IoReactivexInternalOperatorsObservableObservableSequenceEqual *self, id<IoReactivexObservableSource> first, id<IoReactivexObservableSource> second, id<IoReactivexFunctionsBiPredicate> comparer, jint bufferSize) {
  IoReactivexObservable_init(self);
  JreStrongAssign(&self->first_, first);
  JreStrongAssign(&self->second_, second);
  JreStrongAssign(&self->comparer_, comparer);
  self->bufferSize_ = bufferSize;
}

IoReactivexInternalOperatorsObservableObservableSequenceEqual *new_IoReactivexInternalOperatorsObservableObservableSequenceEqual_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_withInt_(id<IoReactivexObservableSource> first, id<IoReactivexObservableSource> second, id<IoReactivexFunctionsBiPredicate> comparer, jint bufferSize) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableSequenceEqual, initWithIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_withInt_, first, second, comparer, bufferSize)
}

IoReactivexInternalOperatorsObservableObservableSequenceEqual *create_IoReactivexInternalOperatorsObservableObservableSequenceEqual_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_withInt_(id<IoReactivexObservableSource> first, id<IoReactivexObservableSource> second, id<IoReactivexFunctionsBiPredicate> comparer, jint bufferSize) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableSequenceEqual, initWithIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_withInt_, first, second, comparer, bufferSize)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableSequenceEqual)

@implementation IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                                              withInt:(jint)bufferSize
                      withIoReactivexObservableSource:(id<IoReactivexObservableSource>)first
                      withIoReactivexObservableSource:(id<IoReactivexObservableSource>)second
                  withIoReactivexFunctionsBiPredicate:(id<IoReactivexFunctionsBiPredicate>)comparer {
  IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_initWithIoReactivexObserver_withInt_withIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_(self, actual, bufferSize, first, second, comparer);
  return self;
}

- (jboolean)setDisposableWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d
                                                      withInt:(jint)index {
  return [((IoReactivexInternalDisposablesArrayCompositeDisposable *) nil_chk(resources_)) setResourceWithInt:index withIoReactivexDisposablesDisposable:d];
}

- (void)subscribe {
  IOSObjectArray *as = observers_;
  [((id<IoReactivexObservableSource>) nil_chk(first_)) subscribeWithIoReactivexObserver:IOSObjectArray_Get(nil_chk(as), 0)];
  [((id<IoReactivexObservableSource>) nil_chk(second_)) subscribeWithIoReactivexObserver:IOSObjectArray_Get(as, 1)];
}

- (void)dispose {
  if (!JreLoadVolatileBoolean(&cancelled_)) {
    JreAssignVolatileBoolean(&cancelled_, true);
    [((IoReactivexInternalDisposablesArrayCompositeDisposable *) nil_chk(resources_)) dispose];
    if ([self getAndIncrement] == 0) {
      IOSObjectArray *as = observers_;
      [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(((IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver *) nil_chk(IOSObjectArray_Get(nil_chk(as), 0)))->queue_)) clear];
      [((IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver *) nil_chk(IOSObjectArray_Get(as, 1)))->queue_ clear];
    }
  }
}

- (jboolean)isDisposed {
  return JreLoadVolatileBoolean(&cancelled_);
}

- (void)cancelWithIoReactivexInternalQueueSpscLinkedArrayQueue:(IoReactivexInternalQueueSpscLinkedArrayQueue *)q1
              withIoReactivexInternalQueueSpscLinkedArrayQueue:(IoReactivexInternalQueueSpscLinkedArrayQueue *)q2 {
  JreAssignVolatileBoolean(&cancelled_, true);
  [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(q1)) clear];
  [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(q2)) clear];
}

- (void)drain {
  if ([self getAndIncrement] != 0) {
    return;
  }
  jint missed = 1;
  IOSObjectArray *as = observers_;
  IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver *observer1 = IOSObjectArray_Get(nil_chk(as), 0);
  IoReactivexInternalQueueSpscLinkedArrayQueue *q1 = ((IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver *) nil_chk(observer1))->queue_;
  IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver *observer2 = IOSObjectArray_Get(as, 1);
  IoReactivexInternalQueueSpscLinkedArrayQueue *q2 = ((IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver *) nil_chk(observer2))->queue_;
  for (; ; ) {
    for (; ; ) {
      if (JreLoadVolatileBoolean(&cancelled_)) {
        [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(q1)) clear];
        [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(q2)) clear];
        return;
      }
      jboolean d1 = JreLoadVolatileBoolean(&observer1->done_);
      if (d1) {
        JavaLangThrowable *e = observer1->error_;
        if (e != nil) {
          [self cancelWithIoReactivexInternalQueueSpscLinkedArrayQueue:q1 withIoReactivexInternalQueueSpscLinkedArrayQueue:q2];
          [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
          return;
        }
      }
      jboolean d2 = JreLoadVolatileBoolean(&observer2->done_);
      if (d2) {
        JavaLangThrowable *e = observer2->error_;
        if (e != nil) {
          [self cancelWithIoReactivexInternalQueueSpscLinkedArrayQueue:q1 withIoReactivexInternalQueueSpscLinkedArrayQueue:q2];
          [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
          return;
        }
      }
      if (v1_ == nil) {
        JreStrongAssign(&v1_, [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(q1)) poll]);
      }
      jboolean e1 = v1_ == nil;
      if (v2_ == nil) {
        JreStrongAssign(&v2_, [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(q2)) poll]);
      }
      jboolean e2 = v2_ == nil;
      if (d1 && d2 && e1 && e2) {
        [((id<IoReactivexObserver>) nil_chk(downstream_)) onNextWithId:JavaLangBoolean_valueOfWithBoolean_(true)];
        [downstream_ onComplete];
        return;
      }
      if ((d1 && d2) && (e1 != e2)) {
        [self cancelWithIoReactivexInternalQueueSpscLinkedArrayQueue:q1 withIoReactivexInternalQueueSpscLinkedArrayQueue:q2];
        [((id<IoReactivexObserver>) nil_chk(downstream_)) onNextWithId:JavaLangBoolean_valueOfWithBoolean_(false)];
        [downstream_ onComplete];
        return;
      }
      if (!e1 && !e2) {
        jboolean c;
        @try {
          c = [((id<IoReactivexFunctionsBiPredicate>) nil_chk(comparer_)) testWithId:v1_ withId:v2_];
        }
        @catch (JavaLangThrowable *ex) {
          IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
          [self cancelWithIoReactivexInternalQueueSpscLinkedArrayQueue:q1 withIoReactivexInternalQueueSpscLinkedArrayQueue:q2];
          [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:ex];
          return;
        }
        if (!c) {
          [self cancelWithIoReactivexInternalQueueSpscLinkedArrayQueue:q1 withIoReactivexInternalQueueSpscLinkedArrayQueue:q2];
          [((id<IoReactivexObserver>) nil_chk(downstream_)) onNextWithId:JavaLangBoolean_valueOfWithBoolean_(false)];
          [downstream_ onComplete];
          return;
        }
        JreStrongAssign(&v1_, nil);
        JreStrongAssign(&v2_, nil);
      }
      if (e1 || e2) {
        break;
      }
    }
    missed = [self addAndGetWithInt:-missed];
    if (missed == 0) {
      break;
    }
  }
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(comparer_);
  RELEASE_(resources_);
  RELEASE_(first_);
  RELEASE_(second_);
  RELEASE_(observers_);
  RELEASE_(v1_);
  RELEASE_(v2_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "Z", 0x0, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObserver:withInt:withIoReactivexObservableSource:withIoReactivexObservableSource:withIoReactivexFunctionsBiPredicate:);
  methods[1].selector = @selector(setDisposableWithIoReactivexDisposablesDisposable:withInt:);
  methods[2].selector = @selector(subscribe);
  methods[3].selector = @selector(dispose);
  methods[4].selector = @selector(isDisposed);
  methods[5].selector = @selector(cancelWithIoReactivexInternalQueueSpscLinkedArrayQueue:withIoReactivexInternalQueueSpscLinkedArrayQueue:);
  methods[6].selector = @selector(drain);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LIoReactivexObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 7, -1 },
    { "comparer_", "LIoReactivexFunctionsBiPredicate;", .constantValue.asLong = 0, 0x10, -1, -1, 8, -1 },
    { "resources_", "LIoReactivexInternalDisposablesArrayCompositeDisposable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "first_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "second_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "observers_", "[LIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "cancelled_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "v1_", "LNSObject;", .constantValue.asLong = 0, 0x0, -1, -1, 11, -1 },
    { "v2_", "LNSObject;", .constantValue.asLong = 0, 0x0, -1, -1, 11, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObserver;ILIoReactivexObservableSource;LIoReactivexObservableSource;LIoReactivexFunctionsBiPredicate;", "(Lio/reactivex/Observer<-Ljava/lang/Boolean;>;ILio/reactivex/ObservableSource<+TT;>;Lio/reactivex/ObservableSource<+TT;>;Lio/reactivex/functions/BiPredicate<-TT;-TT;>;)V", "setDisposable", "LIoReactivexDisposablesDisposable;I", "cancel", "LIoReactivexInternalQueueSpscLinkedArrayQueue;LIoReactivexInternalQueueSpscLinkedArrayQueue;", "(Lio/reactivex/internal/queue/SpscLinkedArrayQueue<TT;>;Lio/reactivex/internal/queue/SpscLinkedArrayQueue<TT;>;)V", "Lio/reactivex/Observer<-Ljava/lang/Boolean;>;", "Lio/reactivex/functions/BiPredicate<-TT;-TT;>;", "Lio/reactivex/ObservableSource<+TT;>;", "[Lio/reactivex/internal/operators/observable/ObservableSequenceEqual$EqualObserver<TT;>;", "TT;", "LIoReactivexInternalOperatorsObservableObservableSequenceEqual;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator = { "EqualCoordinator", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 7, 10, 12, -1, -1, 13, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator;
}

@end

void IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_initWithIoReactivexObserver_withInt_withIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *self, id<IoReactivexObserver> actual, jint bufferSize, id<IoReactivexObservableSource> first, id<IoReactivexObservableSource> second, id<IoReactivexFunctionsBiPredicate> comparer) {
  JavaUtilConcurrentAtomicAtomicInteger_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->first_, first);
  JreStrongAssign(&self->second_, second);
  JreStrongAssign(&self->comparer_, comparer);
  IOSObjectArray *as = [IOSObjectArray arrayWithLength:2 type:IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver_class_()];
  JreStrongAssign(&self->observers_, as);
  IOSObjectArray_SetAndConsume(as, 0, new_IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver_initWithIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_withInt_withInt_(self, 0, bufferSize));
  IOSObjectArray_SetAndConsume(as, 1, new_IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver_initWithIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_withInt_withInt_(self, 1, bufferSize));
  JreStrongAssignAndConsume(&self->resources_, new_IoReactivexInternalDisposablesArrayCompositeDisposable_initWithInt_(2));
}

IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *new_IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_initWithIoReactivexObserver_withInt_withIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_(id<IoReactivexObserver> actual, jint bufferSize, id<IoReactivexObservableSource> first, id<IoReactivexObservableSource> second, id<IoReactivexFunctionsBiPredicate> comparer) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator, initWithIoReactivexObserver_withInt_withIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_, actual, bufferSize, first, second, comparer)
}

IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *create_IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_initWithIoReactivexObserver_withInt_withIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_(id<IoReactivexObserver> actual, jint bufferSize, id<IoReactivexObservableSource> first, id<IoReactivexObservableSource> second, id<IoReactivexFunctionsBiPredicate> comparer) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator, initWithIoReactivexObserver_withInt_withIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsBiPredicate_, actual, bufferSize, first, second, comparer)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator)

@implementation IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator:(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *)parent
                                                                                                         withInt:(jint)index
                                                                                                         withInt:(jint)bufferSize {
  IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver_initWithIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_withInt_withInt_(self, parent, index, bufferSize);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  [((IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *) nil_chk(parent_)) setDisposableWithIoReactivexDisposablesDisposable:d withInt:index_];
}

- (void)onNextWithId:(id)t {
  [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(queue_)) offerWithId:t];
  [((IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *) nil_chk(parent_)) drain];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  JreStrongAssign(&error_, t);
  JreAssignVolatileBoolean(&done_, true);
  [((IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *) nil_chk(parent_)) drain];
}

- (void)onComplete {
  JreAssignVolatileBoolean(&done_, true);
  [((IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *) nil_chk(parent_)) drain];
}

- (void)dealloc {
  RELEASE_(parent_);
  RELEASE_(queue_);
  RELEASE_(error_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator:withInt:withInt:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "parent_", "LIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "queue_", "LIoReactivexInternalQueueSpscLinkedArrayQueue;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "index_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "error_", "LJavaLangThrowable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator;II", "(Lio/reactivex/internal/operators/observable/ObservableSequenceEqual$EqualCoordinator<TT;>;II)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/internal/operators/observable/ObservableSequenceEqual$EqualCoordinator<TT;>;", "Lio/reactivex/internal/queue/SpscLinkedArrayQueue<TT;>;", "LIoReactivexInternalOperatorsObservableObservableSequenceEqual;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/Observer<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver = { "EqualObserver", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 5, 5, 11, -1, -1, 12, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver;
}

@end

void IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver_initWithIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_withInt_withInt_(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver *self, IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *parent, jint index, jint bufferSize) {
  NSObject_init(self);
  JreStrongAssign(&self->parent_, parent);
  self->index_ = index;
  JreStrongAssignAndConsume(&self->queue_, new_IoReactivexInternalQueueSpscLinkedArrayQueue_initWithInt_(bufferSize));
}

IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver *new_IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver_initWithIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_withInt_withInt_(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *parent, jint index, jint bufferSize) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver, initWithIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_withInt_withInt_, parent, index, bufferSize)
}

IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver *create_IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver_initWithIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_withInt_withInt_(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator *parent, jint index, jint bufferSize) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver, initWithIoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualCoordinator_withInt_withInt_, parent, index, bufferSize)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableSequenceEqual_EqualObserver)
