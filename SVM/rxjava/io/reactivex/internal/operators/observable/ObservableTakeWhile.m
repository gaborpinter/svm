//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableTakeWhile.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/ObservableSource.h"
#include "io/reactivex/Observer.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Predicate.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"
#include "io/reactivex/internal/operators/observable/ObservableTakeWhile.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Throwable.h"

@implementation IoReactivexInternalOperatorsObservableObservableTakeWhile

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                            withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)predicate {
  IoReactivexInternalOperatorsObservableObservableTakeWhile_initWithIoReactivexObservableSource_withIoReactivexFunctionsPredicate_(self, source, predicate);
  return self;
}

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)t {
  [((id<IoReactivexObservableSource>) nil_chk(source_)) subscribeWithIoReactivexObserver:create_IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver_initWithIoReactivexObserver_withIoReactivexFunctionsPredicate_(t, predicate_)];
}

- (void)dealloc {
  RELEASE_(predicate_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservableSource:withIoReactivexFunctionsPredicate:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "predicate_", "LIoReactivexFunctionsPredicate;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservableSource;LIoReactivexFunctionsPredicate;", "(Lio/reactivex/ObservableSource<TT;>;Lio/reactivex/functions/Predicate<-TT;>;)V", "subscribeActual", "LIoReactivexObserver;", "(Lio/reactivex/Observer<-TT;>;)V", "Lio/reactivex/functions/Predicate<-TT;>;", "LIoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/observable/AbstractObservableWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableTakeWhile = { "ObservableTakeWhile", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableTakeWhile;
}

@end

void IoReactivexInternalOperatorsObservableObservableTakeWhile_initWithIoReactivexObservableSource_withIoReactivexFunctionsPredicate_(IoReactivexInternalOperatorsObservableObservableTakeWhile *self, id<IoReactivexObservableSource> source, id<IoReactivexFunctionsPredicate> predicate) {
  IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream_initWithIoReactivexObservableSource_(self, source);
  JreStrongAssign(&self->predicate_, predicate);
}

IoReactivexInternalOperatorsObservableObservableTakeWhile *new_IoReactivexInternalOperatorsObservableObservableTakeWhile_initWithIoReactivexObservableSource_withIoReactivexFunctionsPredicate_(id<IoReactivexObservableSource> source, id<IoReactivexFunctionsPredicate> predicate) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableTakeWhile, initWithIoReactivexObservableSource_withIoReactivexFunctionsPredicate_, source, predicate)
}

IoReactivexInternalOperatorsObservableObservableTakeWhile *create_IoReactivexInternalOperatorsObservableObservableTakeWhile_initWithIoReactivexObservableSource_withIoReactivexFunctionsPredicate_(id<IoReactivexObservableSource> source, id<IoReactivexFunctionsPredicate> predicate) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableTakeWhile, initWithIoReactivexObservableSource_withIoReactivexFunctionsPredicate_, source, predicate)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableTakeWhile)

@implementation IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                    withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)predicate {
  IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver_initWithIoReactivexObserver_withIoReactivexFunctionsPredicate_(self, actual, predicate);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)dispose {
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
}

- (jboolean)isDisposed {
  return [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) isDisposed];
}

- (void)onNextWithId:(id)t {
  if (done_) {
    return;
  }
  jboolean b;
  @try {
    b = [((id<IoReactivexFunctionsPredicate>) nil_chk(predicate_)) testWithId:t];
  }
  @catch (JavaLangThrowable *e) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(e);
    [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
    [self onErrorWithJavaLangThrowable:e];
    return;
  }
  if (!b) {
    done_ = true;
    [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onComplete];
    return;
  }
  [((id<IoReactivexObserver>) nil_chk(downstream_)) onNextWithId:t];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if (done_) {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
    return;
  }
  done_ = true;
  [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  if (done_) {
    return;
  }
  done_ = true;
  [((id<IoReactivexObserver>) nil_chk(downstream_)) onComplete];
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(predicate_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObserver:withIoReactivexFunctionsPredicate:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(dispose);
  methods[3].selector = @selector(isDisposed);
  methods[4].selector = @selector(onNextWithId:);
  methods[5].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[6].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "downstream_", "LIoReactivexObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "predicate_", "LIoReactivexFunctionsPredicate;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObserver;LIoReactivexFunctionsPredicate;", "(Lio/reactivex/Observer<-TT;>;Lio/reactivex/functions/Predicate<-TT;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/Observer<-TT;>;", "Lio/reactivex/functions/Predicate<-TT;>;", "LIoReactivexInternalOperatorsObservableObservableTakeWhile;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/Observer<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver = { "TakeWhileObserver", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 7, 4, 11, -1, -1, 12, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver;
}

@end

void IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver_initWithIoReactivexObserver_withIoReactivexFunctionsPredicate_(IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver *self, id<IoReactivexObserver> actual, id<IoReactivexFunctionsPredicate> predicate) {
  NSObject_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->predicate_, predicate);
}

IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver *new_IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver_initWithIoReactivexObserver_withIoReactivexFunctionsPredicate_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsPredicate> predicate) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver, initWithIoReactivexObserver_withIoReactivexFunctionsPredicate_, actual, predicate)
}

IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver *create_IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver_initWithIoReactivexObserver_withIoReactivexFunctionsPredicate_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsPredicate> predicate) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver, initWithIoReactivexObserver_withIoReactivexFunctionsPredicate_, actual, predicate)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableTakeWhile_TakeWhileObserver)
