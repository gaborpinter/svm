//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableSubscribeOn.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableSubscribeOn")
#ifdef RESTRICT_IoReactivexInternalOperatorsObservableObservableSubscribeOn
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableSubscribeOn 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableSubscribeOn 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsObservableObservableSubscribeOn

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableSubscribeOn_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableSubscribeOn || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableSubscribeOn))
#define IoReactivexInternalOperatorsObservableObservableSubscribeOn_

#define RESTRICT_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"

@class IoReactivexScheduler;
@protocol IoReactivexObservableSource;
@protocol IoReactivexObserver;

@interface IoReactivexInternalOperatorsObservableObservableSubscribeOn : IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream {
 @public
  IoReactivexScheduler *scheduler_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                                     withIoReactivexScheduler:(IoReactivexScheduler *)scheduler;

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableSubscribeOn)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableSubscribeOn, scheduler_, IoReactivexScheduler *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableSubscribeOn_initWithIoReactivexObservableSource_withIoReactivexScheduler_(IoReactivexInternalOperatorsObservableObservableSubscribeOn *self, id<IoReactivexObservableSource> source, IoReactivexScheduler *scheduler);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableSubscribeOn *new_IoReactivexInternalOperatorsObservableObservableSubscribeOn_initWithIoReactivexObservableSource_withIoReactivexScheduler_(id<IoReactivexObservableSource> source, IoReactivexScheduler *scheduler) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableSubscribeOn *create_IoReactivexInternalOperatorsObservableObservableSubscribeOn_initWithIoReactivexObservableSource_withIoReactivexScheduler_(id<IoReactivexObservableSource> source, IoReactivexScheduler *scheduler);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableSubscribeOn)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableSubscribeOn || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver))
#define IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexObserver 1
#define INCLUDE_IoReactivexObserver 1
#include "io/reactivex/Observer.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class JavaLangThrowable;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;

@interface IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver : JavaUtilConcurrentAtomicAtomicReference < IoReactivexObserver, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexObserver> downstream_;
  JavaUtilConcurrentAtomicAtomicReference *upstream_;
}

#pragma mark Public

- (id<IoReactivexDisposablesDisposable>)accumulateAndGetWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)dispose;

- (id<IoReactivexDisposablesDisposable>)get;

- (id<IoReactivexDisposablesDisposable>)getAndAccumulateWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)getAndSetWithId:(id<IoReactivexDisposablesDisposable>)arg0;

- (id<IoReactivexDisposablesDisposable>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (id<IoReactivexDisposablesDisposable>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)downstream;

- (void)setDisposableWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver, downstream_, id<IoReactivexObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver, upstream_, JavaUtilConcurrentAtomicAtomicReference *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver_initWithIoReactivexObserver_(IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver *self, id<IoReactivexObserver> downstream);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver *new_IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver_initWithIoReactivexObserver_(id<IoReactivexObserver> downstream) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver *create_IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver_initWithIoReactivexObserver_(id<IoReactivexObserver> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableSubscribeOn || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask))
#define IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask_

#define RESTRICT_JavaLangRunnable 1
#define INCLUDE_JavaLangRunnable 1
#include "java/lang/Runnable.h"

@class IoReactivexInternalOperatorsObservableObservableSubscribeOn;
@class IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver;

@interface IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask : NSObject < JavaLangRunnable >

#pragma mark Public

- (void)run;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsObservableObservableSubscribeOn:(IoReactivexInternalOperatorsObservableObservableSubscribeOn *)outer$
          withIoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver:(IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver *)parent;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask_initWithIoReactivexInternalOperatorsObservableObservableSubscribeOn_withIoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver_(IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask *self, IoReactivexInternalOperatorsObservableObservableSubscribeOn *outer$, IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver *parent);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask *new_IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask_initWithIoReactivexInternalOperatorsObservableObservableSubscribeOn_withIoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver_(IoReactivexInternalOperatorsObservableObservableSubscribeOn *outer$, IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver *parent) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask *create_IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask_initWithIoReactivexInternalOperatorsObservableObservableSubscribeOn_withIoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver_(IoReactivexInternalOperatorsObservableObservableSubscribeOn *outer$, IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeOnObserver *parent);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableSubscribeOn_SubscribeTask)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableSubscribeOn")
