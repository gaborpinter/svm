//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableDetach.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/ObservableSource.h"
#include "io/reactivex/Observer.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"
#include "io/reactivex/internal/operators/observable/ObservableDetach.h"
#include "io/reactivex/internal/util/EmptyComponent.h"
#include "java/lang/Throwable.h"

@implementation IoReactivexInternalOperatorsObservableObservableDetach

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source {
  IoReactivexInternalOperatorsObservableObservableDetach_initWithIoReactivexObservableSource_(self, source);
  return self;
}

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer {
  [((id<IoReactivexObservableSource>) nil_chk(source_)) subscribeWithIoReactivexObserver:create_IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver_initWithIoReactivexObserver_(observer)];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservableSource:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexObserver:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "LIoReactivexObservableSource;", "(Lio/reactivex/ObservableSource<TT;>;)V", "subscribeActual", "LIoReactivexObserver;", "(Lio/reactivex/Observer<-TT;>;)V", "LIoReactivexInternalOperatorsObservableObservableDetach_DetachObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/observable/AbstractObservableWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableDetach = { "ObservableDetach", "io.reactivex.internal.operators.observable", ptrTable, methods, NULL, 7, 0x11, 2, 0, -1, 5, -1, 6, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableDetach;
}

@end

void IoReactivexInternalOperatorsObservableObservableDetach_initWithIoReactivexObservableSource_(IoReactivexInternalOperatorsObservableObservableDetach *self, id<IoReactivexObservableSource> source) {
  IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream_initWithIoReactivexObservableSource_(self, source);
}

IoReactivexInternalOperatorsObservableObservableDetach *new_IoReactivexInternalOperatorsObservableObservableDetach_initWithIoReactivexObservableSource_(id<IoReactivexObservableSource> source) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableDetach, initWithIoReactivexObservableSource_, source)
}

IoReactivexInternalOperatorsObservableObservableDetach *create_IoReactivexInternalOperatorsObservableObservableDetach_initWithIoReactivexObservableSource_(id<IoReactivexObservableSource> source) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableDetach, initWithIoReactivexObservableSource_, source)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableDetach)

@implementation IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)downstream {
  IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver_initWithIoReactivexObserver_(self, downstream);
  return self;
}

- (void)dispose {
  id<IoReactivexDisposablesDisposable> d = self->upstream_;
  JreStrongAssign(&self->upstream_, JreLoadEnum(IoReactivexInternalUtilEmptyComponent, INSTANCE));
  JreStrongAssign(&self->downstream_, IoReactivexInternalUtilEmptyComponent_asObserver());
  [((id<IoReactivexDisposablesDisposable>) nil_chk(d)) dispose];
}

- (jboolean)isDisposed {
  return [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) isDisposed];
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onNextWithId:(id)t {
  [((id<IoReactivexObserver>) nil_chk(downstream_)) onNextWithId:t];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  id<IoReactivexObserver> a = downstream_;
  JreStrongAssign(&self->upstream_, JreLoadEnum(IoReactivexInternalUtilEmptyComponent, INSTANCE));
  JreStrongAssign(&self->downstream_, IoReactivexInternalUtilEmptyComponent_asObserver());
  [((id<IoReactivexObserver>) nil_chk(a)) onErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  id<IoReactivexObserver> a = downstream_;
  JreStrongAssign(&self->upstream_, JreLoadEnum(IoReactivexInternalUtilEmptyComponent, INSTANCE));
  JreStrongAssign(&self->downstream_, IoReactivexInternalUtilEmptyComponent_asObserver());
  [((id<IoReactivexObserver>) nil_chk(a)) onComplete];
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObserver:);
  methods[1].selector = @selector(dispose);
  methods[2].selector = @selector(isDisposed);
  methods[3].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[4].selector = @selector(onNextWithId:);
  methods[5].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[6].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "downstream_", "LIoReactivexObserver;", .constantValue.asLong = 0, 0x0, -1, -1, 9, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObserver;", "(Lio/reactivex/Observer<-TT;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/Observer<-TT;>;", "LIoReactivexInternalOperatorsObservableObservableDetach;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/Observer<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver = { "DetachObserver", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 7, 2, 10, -1, -1, 11, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver;
}

@end

void IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver_initWithIoReactivexObserver_(IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver *self, id<IoReactivexObserver> downstream) {
  NSObject_init(self);
  JreStrongAssign(&self->downstream_, downstream);
}

IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver *new_IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver_initWithIoReactivexObserver_(id<IoReactivexObserver> downstream) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver, initWithIoReactivexObserver_, downstream)
}

IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver *create_IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver_initWithIoReactivexObserver_(id<IoReactivexObserver> downstream) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver, initWithIoReactivexObserver_, downstream)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableDetach_DetachObserver)
