//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableSkipLastTimed.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/ObservableSource.h"
#include "io/reactivex/Observer.h"
#include "io/reactivex/Scheduler.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"
#include "io/reactivex/internal/operators/observable/ObservableSkipLastTimed.h"
#include "io/reactivex/internal/queue/SpscLinkedArrayQueue.h"
#include "java/lang/Long.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/TimeUnit.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"

inline jlong IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver_serialVersionUID -5677354903406201275LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsObservableObservableSkipLastTimed

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                                                     withLong:(jlong)time
                               withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit
                                     withIoReactivexScheduler:(IoReactivexScheduler *)scheduler
                                                      withInt:(jint)bufferSize
                                                  withBoolean:(jboolean)delayError {
  IoReactivexInternalOperatorsObservableObservableSkipLastTimed_initWithIoReactivexObservableSource_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(self, source, time, unit, scheduler, bufferSize, delayError);
  return self;
}

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)t {
  [((id<IoReactivexObservableSource>) nil_chk(source_)) subscribeWithIoReactivexObserver:create_IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver_initWithIoReactivexObserver_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(t, time_, unit_, scheduler_, bufferSize_, delayError_)];
}

- (void)dealloc {
  RELEASE_(unit_);
  RELEASE_(scheduler_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservableSource:withLong:withJavaUtilConcurrentTimeUnit:withIoReactivexScheduler:withInt:withBoolean:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "time_", "J", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "unit_", "LJavaUtilConcurrentTimeUnit;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "scheduler_", "LIoReactivexScheduler;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "bufferSize_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "delayError_", "Z", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservableSource;JLJavaUtilConcurrentTimeUnit;LIoReactivexScheduler;IZ", "(Lio/reactivex/ObservableSource<TT;>;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;IZ)V", "subscribeActual", "LIoReactivexObserver;", "(Lio/reactivex/Observer<-TT;>;)V", "LIoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/observable/AbstractObservableWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableSkipLastTimed = { "ObservableSkipLastTimed", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x11, 2, 5, -1, 5, -1, 6, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableSkipLastTimed;
}

@end

void IoReactivexInternalOperatorsObservableObservableSkipLastTimed_initWithIoReactivexObservableSource_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(IoReactivexInternalOperatorsObservableObservableSkipLastTimed *self, id<IoReactivexObservableSource> source, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError) {
  IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream_initWithIoReactivexObservableSource_(self, source);
  self->time_ = time;
  JreStrongAssign(&self->unit_, unit);
  JreStrongAssign(&self->scheduler_, scheduler);
  self->bufferSize_ = bufferSize;
  self->delayError_ = delayError;
}

IoReactivexInternalOperatorsObservableObservableSkipLastTimed *new_IoReactivexInternalOperatorsObservableObservableSkipLastTimed_initWithIoReactivexObservableSource_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(id<IoReactivexObservableSource> source, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableSkipLastTimed, initWithIoReactivexObservableSource_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_, source, time, unit, scheduler, bufferSize, delayError)
}

IoReactivexInternalOperatorsObservableObservableSkipLastTimed *create_IoReactivexInternalOperatorsObservableObservableSkipLastTimed_initWithIoReactivexObservableSource_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(id<IoReactivexObservableSource> source, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableSkipLastTimed, initWithIoReactivexObservableSource_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_, source, time, unit, scheduler, bufferSize, delayError)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableSkipLastTimed)

@implementation IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                                             withLong:(jlong)time
                       withJavaUtilConcurrentTimeUnit:(JavaUtilConcurrentTimeUnit *)unit
                             withIoReactivexScheduler:(IoReactivexScheduler *)scheduler
                                              withInt:(jint)bufferSize
                                          withBoolean:(jboolean)delayError {
  IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver_initWithIoReactivexObserver_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(self, actual, time, unit, scheduler, bufferSize, delayError);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onNextWithId:(id)t {
  IoReactivexInternalQueueSpscLinkedArrayQueue *q = queue_;
  jlong now = [((IoReactivexScheduler *) nil_chk(scheduler_)) nowWithJavaUtilConcurrentTimeUnit:unit_];
  [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(q)) offerWithId:JavaLangLong_valueOfWithLong_(now) withId:t];
  [self drain];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  JreStrongAssign(&error_, t);
  JreAssignVolatileBoolean(&done_, true);
  [self drain];
}

- (void)onComplete {
  JreAssignVolatileBoolean(&done_, true);
  [self drain];
}

- (void)dispose {
  if (!JreLoadVolatileBoolean(&cancelled_)) {
    JreAssignVolatileBoolean(&cancelled_, true);
    [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
    if ([self getAndIncrement] == 0) {
      [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(queue_)) clear];
    }
  }
}

- (jboolean)isDisposed {
  return JreLoadVolatileBoolean(&cancelled_);
}

- (void)drain {
  if ([self getAndIncrement] != 0) {
    return;
  }
  jint missed = 1;
  id<IoReactivexObserver> a = downstream_;
  IoReactivexInternalQueueSpscLinkedArrayQueue *q = queue_;
  jboolean delayError = self->delayError_;
  JavaUtilConcurrentTimeUnit *unit = self->unit_;
  IoReactivexScheduler *scheduler = self->scheduler_;
  jlong time = self->time_;
  for (; ; ) {
    for (; ; ) {
      if (JreLoadVolatileBoolean(&cancelled_)) {
        [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(queue_)) clear];
        return;
      }
      jboolean d = JreLoadVolatileBoolean(&done_);
      JavaLangLong *ts = (JavaLangLong *) cast_chk([((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(q)) peek], [JavaLangLong class]);
      jboolean empty = ts == nil;
      jlong now = [((IoReactivexScheduler *) nil_chk(scheduler)) nowWithJavaUtilConcurrentTimeUnit:unit];
      if (!empty && [((JavaLangLong *) nil_chk(ts)) longLongValue] > now - time) {
        empty = true;
      }
      if (d) {
        if (delayError) {
          if (empty) {
            JavaLangThrowable *e = error_;
            if (e != nil) {
              [((id<IoReactivexObserver>) nil_chk(a)) onErrorWithJavaLangThrowable:e];
            }
            else {
              [((id<IoReactivexObserver>) nil_chk(a)) onComplete];
            }
            return;
          }
        }
        else {
          JavaLangThrowable *e = error_;
          if (e != nil) {
            [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(queue_)) clear];
            [((id<IoReactivexObserver>) nil_chk(a)) onErrorWithJavaLangThrowable:e];
            return;
          }
          else if (empty) {
            [((id<IoReactivexObserver>) nil_chk(a)) onComplete];
            return;
          }
        }
      }
      if (empty) {
        break;
      }
      [q poll];
      id v = [q poll];
      [((id<IoReactivexObserver>) nil_chk(a)) onNextWithId:v];
    }
    missed = [self addAndGetWithInt:-missed];
    if (missed == 0) {
      break;
    }
  }
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(unit_);
  RELEASE_(scheduler_);
  RELEASE_(queue_);
  RELEASE_(upstream_);
  RELEASE_(error_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObserver:withLong:withJavaUtilConcurrentTimeUnit:withIoReactivexScheduler:withInt:withBoolean:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(dispose);
  methods[6].selector = @selector(isDisposed);
  methods[7].selector = @selector(drain);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LIoReactivexObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "time_", "J", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "unit_", "LJavaUtilConcurrentTimeUnit;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "scheduler_", "LIoReactivexScheduler;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "queue_", "LIoReactivexInternalQueueSpscLinkedArrayQueue;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "delayError_", "Z", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "cancelled_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "error_", "LJavaLangThrowable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObserver;JLJavaUtilConcurrentTimeUnit;LIoReactivexScheduler;IZ", "(Lio/reactivex/Observer<-TT;>;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;IZ)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/Observer<-TT;>;", "Lio/reactivex/internal/queue/SpscLinkedArrayQueue<Ljava/lang/Object;>;", "LIoReactivexInternalOperatorsObservableObservableSkipLastTimed;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/Observer<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver = { "SkipLastTimedObserver", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 8, 11, 11, -1, -1, 12, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver;
}

@end

void IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver_initWithIoReactivexObserver_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver *self, id<IoReactivexObserver> actual, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError) {
  JavaUtilConcurrentAtomicAtomicInteger_init(self);
  JreStrongAssign(&self->downstream_, actual);
  self->time_ = time;
  JreStrongAssign(&self->unit_, unit);
  JreStrongAssign(&self->scheduler_, scheduler);
  JreStrongAssignAndConsume(&self->queue_, new_IoReactivexInternalQueueSpscLinkedArrayQueue_initWithInt_(bufferSize));
  self->delayError_ = delayError;
}

IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver *new_IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver_initWithIoReactivexObserver_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(id<IoReactivexObserver> actual, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver, initWithIoReactivexObserver_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_, actual, time, unit, scheduler, bufferSize, delayError)
}

IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver *create_IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver_initWithIoReactivexObserver_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_(id<IoReactivexObserver> actual, jlong time, JavaUtilConcurrentTimeUnit *unit, IoReactivexScheduler *scheduler, jint bufferSize, jboolean delayError) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver, initWithIoReactivexObserver_withLong_withJavaUtilConcurrentTimeUnit_withIoReactivexScheduler_withInt_withBoolean_, actual, time, unit, scheduler, bufferSize, delayError)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableSkipLastTimed_SkipLastTimedObserver)
