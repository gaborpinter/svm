//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableWindowBoundary.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableWindowBoundary")
#ifdef RESTRICT_IoReactivexInternalOperatorsObservableObservableWindowBoundary
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableWindowBoundary 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableWindowBoundary 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsObservableObservableWindowBoundary

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableWindowBoundary_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableWindowBoundary || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableWindowBoundary))
#define IoReactivexInternalOperatorsObservableObservableWindowBoundary_

#define RESTRICT_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"

@class IoReactivexObservable;
@protocol IoReactivexObservableSource;
@protocol IoReactivexObserver;

@interface IoReactivexInternalOperatorsObservableObservableWindowBoundary : IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream {
 @public
  id<IoReactivexObservableSource> other_;
  jint capacityHint_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                              withIoReactivexObservableSource:(id<IoReactivexObservableSource>)other
                                                      withInt:(jint)capacityHint;

- (IoReactivexObservable *)blockingFirst;

- (IoReactivexObservable *)blockingFirstWithId:(IoReactivexObservable *)arg0;

- (IoReactivexObservable *)blockingLast;

- (IoReactivexObservable *)blockingLastWithId:(IoReactivexObservable *)arg0;

- (IoReactivexObservable *)blockingSingle;

- (IoReactivexObservable *)blockingSingleWithId:(IoReactivexObservable *)arg0;

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableWindowBoundary)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableWindowBoundary, other_, id<IoReactivexObservableSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableWindowBoundary_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withInt_(IoReactivexInternalOperatorsObservableObservableWindowBoundary *self, id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> other, jint capacityHint);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableWindowBoundary *new_IoReactivexInternalOperatorsObservableObservableWindowBoundary_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withInt_(id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> other, jint capacityHint) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableWindowBoundary *create_IoReactivexInternalOperatorsObservableObservableWindowBoundary_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withInt_(id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> other, jint capacityHint);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableWindowBoundary)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableWindowBoundary || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver))
#define IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicInteger 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicInteger 1
#include "java/util/concurrent/atomic/AtomicInteger.h"

#define RESTRICT_IoReactivexObserver 1
#define INCLUDE_IoReactivexObserver 1
#include "io/reactivex/Observer.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

#define RESTRICT_JavaLangRunnable 1
#define INCLUDE_JavaLangRunnable 1
#include "java/lang/Runnable.h"

@class IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver;
@class IoReactivexInternalQueueMpscLinkedQueue;
@class IoReactivexInternalUtilAtomicThrowable;
@class IoReactivexSubjectsUnicastSubject;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicBoolean;
@class JavaUtilConcurrentAtomicAtomicReference;

@interface IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver : JavaUtilConcurrentAtomicAtomicInteger < IoReactivexObserver, IoReactivexDisposablesDisposable, JavaLangRunnable > {
 @public
  id<IoReactivexObserver> downstream_;
  jint capacityHint_;
  IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver *boundaryObserver_;
  JavaUtilConcurrentAtomicAtomicReference *upstream_;
  JavaUtilConcurrentAtomicAtomicInteger *windows_;
  IoReactivexInternalQueueMpscLinkedQueue *queue_;
  IoReactivexInternalUtilAtomicThrowable *errors_;
  JavaUtilConcurrentAtomicAtomicBoolean *stopWindows_;
  volatile_jboolean done_;
  IoReactivexSubjectsUnicastSubject *window_;
}

+ (id)NEXT_WINDOW;

#pragma mark Public

- (void)dispose;

- (NSUInteger)hash;

- (jboolean)isDisposed;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (void)run;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)downstream
                                              withInt:(jint)capacityHint;

- (void)drain;

- (void)innerComplete;

- (void)innerErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)innerNext;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver, downstream_, id<IoReactivexObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver, boundaryObserver_, IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver, upstream_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver, windows_, JavaUtilConcurrentAtomicAtomicInteger *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver, queue_, IoReactivexInternalQueueMpscLinkedQueue *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver, errors_, IoReactivexInternalUtilAtomicThrowable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver, stopWindows_, JavaUtilConcurrentAtomicAtomicBoolean *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver, window_, IoReactivexSubjectsUnicastSubject *)

inline id IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver_get_NEXT_WINDOW(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT id IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver_NEXT_WINDOW;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver, NEXT_WINDOW, id)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver_initWithIoReactivexObserver_withInt_(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver *self, id<IoReactivexObserver> downstream, jint capacityHint);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver *new_IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver_initWithIoReactivexObserver_withInt_(id<IoReactivexObserver> downstream, jint capacityHint) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver *create_IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver_initWithIoReactivexObserver_withInt_(id<IoReactivexObserver> downstream, jint capacityHint);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableWindowBoundary || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver))
#define IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver_

#define RESTRICT_IoReactivexObserversDisposableObserver 1
#define INCLUDE_IoReactivexObserversDisposableObserver 1
#include "io/reactivex/observers/DisposableObserver.h"

@class IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver;
@class JavaLangThrowable;

@interface IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver : IoReactivexObserversDisposableObserver {
 @public
  IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver *parent_;
  jboolean done_;
}

#pragma mark Public

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver:(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver *)parent;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver, parent_, IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver_initWithIoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver_(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver *self, IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver *parent);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver *new_IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver_initWithIoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver_(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver *parent) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver *create_IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver_initWithIoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver_(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryMainObserver *parent);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableWindowBoundary_WindowBoundaryInnerObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableWindowBoundary")
