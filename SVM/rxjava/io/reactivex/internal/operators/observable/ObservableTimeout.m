//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableTimeout.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Observable.h"
#include "io/reactivex/ObservableSource.h"
#include "io/reactivex/Observer.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Function.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/disposables/SequentialDisposable.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"
#include "io/reactivex/internal/operators/observable/ObservableTimeout.h"
#include "io/reactivex/internal/operators/observable/ObservableTimeoutTimed.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Long.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/TimeoutException.h"
#include "java/util/concurrent/atomic/AtomicLong.h"
#include "java/util/concurrent/atomic/AtomicReference.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

@interface IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport : NSObject

@end

inline jlong IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver_serialVersionUID 3764492702657003550LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver, serialVersionUID, jlong)

inline jlong IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver_serialVersionUID -7508389464265974549LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver, serialVersionUID, jlong)

inline jlong IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_serialVersionUID 8708641127342403073LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsObservableObservableTimeout

- (instancetype __nonnull)initWithIoReactivexObservable:(IoReactivexObservable *)source
                        withIoReactivexObservableSource:(id<IoReactivexObservableSource>)firstTimeoutIndicator
                       withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)itemTimeoutIndicator
                        withIoReactivexObservableSource:(id<IoReactivexObservableSource>)other {
  IoReactivexInternalOperatorsObservableObservableTimeout_initWithIoReactivexObservable_withIoReactivexObservableSource_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_(self, source, firstTimeoutIndicator, itemTimeoutIndicator, other);
  return self;
}

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer {
  if (other_ == nil) {
    IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver *parent = create_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_(observer, itemTimeoutIndicator_);
    [((id<IoReactivexObserver>) nil_chk(observer)) onSubscribeWithIoReactivexDisposablesDisposable:parent];
    [parent startFirstTimeoutWithIoReactivexObservableSource:firstTimeoutIndicator_];
    [((id<IoReactivexObservableSource>) nil_chk(source_)) subscribeWithIoReactivexObserver:parent];
  }
  else {
    IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver *parent = create_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_(observer, itemTimeoutIndicator_, other_);
    [((id<IoReactivexObserver>) nil_chk(observer)) onSubscribeWithIoReactivexDisposablesDisposable:parent];
    [parent startFirstTimeoutWithIoReactivexObservableSource:firstTimeoutIndicator_];
    [((id<IoReactivexObservableSource>) nil_chk(source_)) subscribeWithIoReactivexObserver:parent];
  }
}

- (void)dealloc {
  RELEASE_(firstTimeoutIndicator_);
  RELEASE_(itemTimeoutIndicator_);
  RELEASE_(other_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservable:withIoReactivexObservableSource:withIoReactivexFunctionsFunction:withIoReactivexObservableSource:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "firstTimeoutIndicator_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "itemTimeoutIndicator_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
    { "other_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 7, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservable;LIoReactivexObservableSource;LIoReactivexFunctionsFunction;LIoReactivexObservableSource;", "(Lio/reactivex/Observable<TT;>;Lio/reactivex/ObservableSource<TU;>;Lio/reactivex/functions/Function<-TT;+Lio/reactivex/ObservableSource<TV;>;>;Lio/reactivex/ObservableSource<+TT;>;)V", "subscribeActual", "LIoReactivexObserver;", "(Lio/reactivex/Observer<-TT;>;)V", "Lio/reactivex/ObservableSource<TU;>;", "Lio/reactivex/functions/Function<-TT;+Lio/reactivex/ObservableSource<TV;>;>;", "Lio/reactivex/ObservableSource<+TT;>;", "LIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport;LIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver;LIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver;LIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer;", "<T:Ljava/lang/Object;U:Ljava/lang/Object;V:Ljava/lang/Object;>Lio/reactivex/internal/operators/observable/AbstractObservableWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableTimeout = { "ObservableTimeout", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x11, 2, 3, -1, 8, -1, 9, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableTimeout;
}

@end

void IoReactivexInternalOperatorsObservableObservableTimeout_initWithIoReactivexObservable_withIoReactivexObservableSource_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_(IoReactivexInternalOperatorsObservableObservableTimeout *self, IoReactivexObservable *source, id<IoReactivexObservableSource> firstTimeoutIndicator, id<IoReactivexFunctionsFunction> itemTimeoutIndicator, id<IoReactivexObservableSource> other) {
  IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream_initWithIoReactivexObservableSource_(self, source);
  JreStrongAssign(&self->firstTimeoutIndicator_, firstTimeoutIndicator);
  JreStrongAssign(&self->itemTimeoutIndicator_, itemTimeoutIndicator);
  JreStrongAssign(&self->other_, other);
}

IoReactivexInternalOperatorsObservableObservableTimeout *new_IoReactivexInternalOperatorsObservableObservableTimeout_initWithIoReactivexObservable_withIoReactivexObservableSource_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_(IoReactivexObservable *source, id<IoReactivexObservableSource> firstTimeoutIndicator, id<IoReactivexFunctionsFunction> itemTimeoutIndicator, id<IoReactivexObservableSource> other) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableTimeout, initWithIoReactivexObservable_withIoReactivexObservableSource_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_, source, firstTimeoutIndicator, itemTimeoutIndicator, other)
}

IoReactivexInternalOperatorsObservableObservableTimeout *create_IoReactivexInternalOperatorsObservableObservableTimeout_initWithIoReactivexObservable_withIoReactivexObservableSource_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_(IoReactivexObservable *source, id<IoReactivexObservableSource> firstTimeoutIndicator, id<IoReactivexFunctionsFunction> itemTimeoutIndicator, id<IoReactivexObservableSource> other) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableTimeout, initWithIoReactivexObservable_withIoReactivexObservableSource_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_, source, firstTimeoutIndicator, itemTimeoutIndicator, other)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableTimeout)

@implementation IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "V", 0x401, 0, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(onTimeoutErrorWithLong:withJavaLangThrowable:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "onTimeoutError", "JLJavaLangThrowable;", "LIoReactivexInternalOperatorsObservableObservableTimeout;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport = { "TimeoutSelectorSupport", "io.reactivex.internal.operators.observable", ptrTable, methods, NULL, 7, 0x608, 1, 0, 2, -1, -1, -1, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport)

@implementation IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)itemTimeoutIndicator {
  IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_(self, actual, itemTimeoutIndicator);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  IoReactivexInternalDisposablesDisposableHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexDisposablesDisposable_(upstream_, d);
}

- (void)onNextWithId:(id)t {
  jlong idx = [self get];
  if (idx == JavaLangLong_MAX_VALUE || ![self compareAndSetWithLong:idx withLong:idx + 1]) {
    return;
  }
  id<IoReactivexDisposablesDisposable> d = [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(task_)) get];
  if (d != nil) {
    [d dispose];
  }
  [((id<IoReactivexObserver>) nil_chk(downstream_)) onNextWithId:t];
  id<IoReactivexObservableSource> itemTimeoutObservableSource;
  @try {
    itemTimeoutObservableSource = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(itemTimeoutIndicator_)) applyWithId:t], @"The itemTimeoutIndicator returned a null ObservableSource.");
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    [((id<IoReactivexDisposablesDisposable>) nil_chk([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(upstream_)) get])) dispose];
    [self getAndSetWithLong:JavaLangLong_MAX_VALUE];
    [downstream_ onErrorWithJavaLangThrowable:ex];
    return;
  }
  IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer *consumer = create_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_initWithLong_withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport_(idx + 1, self);
  if ([task_ replaceWithIoReactivexDisposablesDisposable:consumer]) {
    [((id<IoReactivexObservableSource>) nil_chk(itemTimeoutObservableSource)) subscribeWithIoReactivexObserver:consumer];
  }
}

- (void)startFirstTimeoutWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)firstTimeoutIndicator {
  if (firstTimeoutIndicator != nil) {
    IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer *consumer = create_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_initWithLong_withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport_(0LL, self);
    if ([((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(task_)) replaceWithIoReactivexDisposablesDisposable:consumer]) {
      [firstTimeoutIndicator subscribeWithIoReactivexObserver:consumer];
    }
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if ([self getAndSetWithLong:JavaLangLong_MAX_VALUE] != JavaLangLong_MAX_VALUE) {
    [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(task_)) dispose];
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
  }
}

- (void)onComplete {
  if ([self getAndSetWithLong:JavaLangLong_MAX_VALUE] != JavaLangLong_MAX_VALUE) {
    [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(task_)) dispose];
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onComplete];
  }
}

- (void)onTimeoutWithLong:(jlong)idx {
  if ([self compareAndSetWithLong:idx withLong:JavaLangLong_MAX_VALUE]) {
    IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:create_JavaUtilConcurrentTimeoutException_init()];
  }
}

- (void)onTimeoutErrorWithLong:(jlong)idx
         withJavaLangThrowable:(JavaLangThrowable *)ex {
  if ([self compareAndSetWithLong:idx withLong:JavaLangLong_MAX_VALUE]) {
    IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:ex];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
  }
}

- (void)dispose {
  IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
  [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(task_)) dispose];
}

- (jboolean)isDisposed {
  return IoReactivexInternalDisposablesDisposableHelper_isDisposedWithIoReactivexDisposablesDisposable_([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(upstream_)) get]);
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(itemTimeoutIndicator_);
  RELEASE_(task_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x0, 7, 8, -1, 9, -1, -1 },
    { NULL, "V", 0x1, 10, 11, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 15, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObserver:withIoReactivexFunctionsFunction:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(startFirstTimeoutWithIoReactivexObservableSource:);
  methods[4].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[5].selector = @selector(onComplete);
  methods[6].selector = @selector(onTimeoutWithLong:);
  methods[7].selector = @selector(onTimeoutErrorWithLong:withJavaLangThrowable:);
  methods[8].selector = @selector(dispose);
  methods[9].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LIoReactivexObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 16, -1 },
    { "itemTimeoutIndicator_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 17, -1 },
    { "task_", "LIoReactivexInternalDisposablesSequentialDisposable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LJavaUtilConcurrentAtomicAtomicReference;", .constantValue.asLong = 0, 0x10, -1, -1, 18, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObserver;LIoReactivexFunctionsFunction;", "(Lio/reactivex/Observer<-TT;>;Lio/reactivex/functions/Function<-TT;+Lio/reactivex/ObservableSource<*>;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "startFirstTimeout", "LIoReactivexObservableSource;", "(Lio/reactivex/ObservableSource<*>;)V", "onError", "LJavaLangThrowable;", "onTimeout", "J", "onTimeoutError", "JLJavaLangThrowable;", "Lio/reactivex/Observer<-TT;>;", "Lio/reactivex/functions/Function<-TT;+Lio/reactivex/ObservableSource<*>;>;", "Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;", "LIoReactivexInternalOperatorsObservableObservableTimeout;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicLong;Lio/reactivex/Observer<TT;>;Lio/reactivex/disposables/Disposable;Lio/reactivex/internal/operators/observable/ObservableTimeout$TimeoutSelectorSupport;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver = { "TimeoutObserver", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 10, 5, 19, -1, -1, 20, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver;
}

@end

void IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver *self, id<IoReactivexObserver> actual, id<IoReactivexFunctionsFunction> itemTimeoutIndicator) {
  JavaUtilConcurrentAtomicAtomicLong_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->itemTimeoutIndicator_, itemTimeoutIndicator);
  JreStrongAssignAndConsume(&self->task_, new_IoReactivexInternalDisposablesSequentialDisposable_init());
  JreStrongAssignAndConsume(&self->upstream_, new_JavaUtilConcurrentAtomicAtomicReference_init());
}

IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver *new_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsFunction> itemTimeoutIndicator) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver, initWithIoReactivexObserver_withIoReactivexFunctionsFunction_, actual, itemTimeoutIndicator)
}

IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver *create_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsFunction> itemTimeoutIndicator) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver, initWithIoReactivexObserver_withIoReactivexFunctionsFunction_, actual, itemTimeoutIndicator)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutObserver)

@implementation IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)itemTimeoutIndicator
                      withIoReactivexObservableSource:(id<IoReactivexObservableSource>)fallback {
  IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_(self, actual, itemTimeoutIndicator, fallback);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  IoReactivexInternalDisposablesDisposableHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexDisposablesDisposable_(upstream_, d);
}

- (void)onNextWithId:(id)t {
  jlong idx = [((JavaUtilConcurrentAtomicAtomicLong *) nil_chk(index_)) get];
  if (idx == JavaLangLong_MAX_VALUE || ![index_ compareAndSetWithLong:idx withLong:idx + 1]) {
    return;
  }
  id<IoReactivexDisposablesDisposable> d = [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(task_)) get];
  if (d != nil) {
    [d dispose];
  }
  [((id<IoReactivexObserver>) nil_chk(downstream_)) onNextWithId:t];
  id<IoReactivexObservableSource> itemTimeoutObservableSource;
  @try {
    itemTimeoutObservableSource = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(itemTimeoutIndicator_)) applyWithId:t], @"The itemTimeoutIndicator returned a null ObservableSource.");
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    [((id<IoReactivexDisposablesDisposable>) nil_chk([((JavaUtilConcurrentAtomicAtomicReference *) nil_chk(upstream_)) get])) dispose];
    [index_ getAndSetWithLong:JavaLangLong_MAX_VALUE];
    [downstream_ onErrorWithJavaLangThrowable:ex];
    return;
  }
  IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer *consumer = create_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_initWithLong_withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport_(idx + 1, self);
  if ([task_ replaceWithIoReactivexDisposablesDisposable:consumer]) {
    [((id<IoReactivexObservableSource>) nil_chk(itemTimeoutObservableSource)) subscribeWithIoReactivexObserver:consumer];
  }
}

- (void)startFirstTimeoutWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)firstTimeoutIndicator {
  if (firstTimeoutIndicator != nil) {
    IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer *consumer = create_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_initWithLong_withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport_(0LL, self);
    if ([((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(task_)) replaceWithIoReactivexDisposablesDisposable:consumer]) {
      [firstTimeoutIndicator subscribeWithIoReactivexObserver:consumer];
    }
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if ([((JavaUtilConcurrentAtomicAtomicLong *) nil_chk(index_)) getAndSetWithLong:JavaLangLong_MAX_VALUE] != JavaLangLong_MAX_VALUE) {
    [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(task_)) dispose];
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
    [task_ dispose];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
  }
}

- (void)onComplete {
  if ([((JavaUtilConcurrentAtomicAtomicLong *) nil_chk(index_)) getAndSetWithLong:JavaLangLong_MAX_VALUE] != JavaLangLong_MAX_VALUE) {
    [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(task_)) dispose];
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onComplete];
    [task_ dispose];
  }
}

- (void)onTimeoutWithLong:(jlong)idx {
  if ([((JavaUtilConcurrentAtomicAtomicLong *) nil_chk(index_)) compareAndSetWithLong:idx withLong:JavaLangLong_MAX_VALUE]) {
    IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
    id<IoReactivexObservableSource> f = fallback_;
    JreStrongAssign(&fallback_, nil);
    [((id<IoReactivexObservableSource>) nil_chk(f)) subscribeWithIoReactivexObserver:create_IoReactivexInternalOperatorsObservableObservableTimeoutTimed_FallbackObserver_initWithIoReactivexObserver_withJavaUtilConcurrentAtomicAtomicReference_(downstream_, self)];
  }
}

- (void)onTimeoutErrorWithLong:(jlong)idx
         withJavaLangThrowable:(JavaLangThrowable *)ex {
  if ([((JavaUtilConcurrentAtomicAtomicLong *) nil_chk(index_)) compareAndSetWithLong:idx withLong:JavaLangLong_MAX_VALUE]) {
    IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(self);
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:ex];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
  }
}

- (void)dispose {
  IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(upstream_);
  IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(self);
  [((IoReactivexInternalDisposablesSequentialDisposable *) nil_chk(task_)) dispose];
}

- (jboolean)isDisposed {
  return IoReactivexInternalDisposablesDisposableHelper_isDisposedWithIoReactivexDisposablesDisposable_([self get]);
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(itemTimeoutIndicator_);
  RELEASE_(task_);
  RELEASE_(index_);
  RELEASE_(upstream_);
  RELEASE_(fallback_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x0, 7, 8, -1, 9, -1, -1 },
    { NULL, "V", 0x1, 10, 11, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 15, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObserver:withIoReactivexFunctionsFunction:withIoReactivexObservableSource:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(startFirstTimeoutWithIoReactivexObservableSource:);
  methods[4].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[5].selector = @selector(onComplete);
  methods[6].selector = @selector(onTimeoutWithLong:);
  methods[7].selector = @selector(onTimeoutErrorWithLong:withJavaLangThrowable:);
  methods[8].selector = @selector(dispose);
  methods[9].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LIoReactivexObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 16, -1 },
    { "itemTimeoutIndicator_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 17, -1 },
    { "task_", "LIoReactivexInternalDisposablesSequentialDisposable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "index_", "LJavaUtilConcurrentAtomicAtomicLong;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LJavaUtilConcurrentAtomicAtomicReference;", .constantValue.asLong = 0, 0x10, -1, -1, 18, -1 },
    { "fallback_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x0, -1, -1, 19, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObserver;LIoReactivexFunctionsFunction;LIoReactivexObservableSource;", "(Lio/reactivex/Observer<-TT;>;Lio/reactivex/functions/Function<-TT;+Lio/reactivex/ObservableSource<*>;>;Lio/reactivex/ObservableSource<+TT;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "startFirstTimeout", "LIoReactivexObservableSource;", "(Lio/reactivex/ObservableSource<*>;)V", "onError", "LJavaLangThrowable;", "onTimeout", "J", "onTimeoutError", "JLJavaLangThrowable;", "Lio/reactivex/Observer<-TT;>;", "Lio/reactivex/functions/Function<-TT;+Lio/reactivex/ObservableSource<*>;>;", "Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;", "Lio/reactivex/ObservableSource<+TT;>;", "LIoReactivexInternalOperatorsObservableObservableTimeout;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;Lio/reactivex/Observer<TT;>;Lio/reactivex/disposables/Disposable;Lio/reactivex/internal/operators/observable/ObservableTimeout$TimeoutSelectorSupport;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver = { "TimeoutFallbackObserver", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 10, 7, 20, -1, -1, 21, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver;
}

@end

void IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver *self, id<IoReactivexObserver> actual, id<IoReactivexFunctionsFunction> itemTimeoutIndicator, id<IoReactivexObservableSource> fallback) {
  JavaUtilConcurrentAtomicAtomicReference_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->itemTimeoutIndicator_, itemTimeoutIndicator);
  JreStrongAssignAndConsume(&self->task_, new_IoReactivexInternalDisposablesSequentialDisposable_init());
  JreStrongAssign(&self->fallback_, fallback);
  JreStrongAssignAndConsume(&self->index_, new_JavaUtilConcurrentAtomicAtomicLong_init());
  JreStrongAssignAndConsume(&self->upstream_, new_JavaUtilConcurrentAtomicAtomicReference_init());
}

IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver *new_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsFunction> itemTimeoutIndicator, id<IoReactivexObservableSource> fallback) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver, initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_, actual, itemTimeoutIndicator, fallback)
}

IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver *create_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsFunction> itemTimeoutIndicator, id<IoReactivexObservableSource> fallback) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver, initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withIoReactivexObservableSource_, actual, itemTimeoutIndicator, fallback)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutFallbackObserver)

@implementation IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer

- (instancetype __nonnull)initWithLong:(jlong)idx
withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport:(id<IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport>)parent {
  IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_initWithLong_withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport_(self, idx, parent);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  IoReactivexInternalDisposablesDisposableHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexDisposablesDisposable_(self, d);
}

- (void)onNextWithId:(id)t {
  id<IoReactivexDisposablesDisposable> upstream = [self get];
  if (upstream != JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED)) {
    [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream)) dispose];
    [self lazySetWithId:JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED)];
    [((id<IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport>) nil_chk(parent_)) onTimeoutWithLong:idx_];
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if ([self get] != JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED)) {
    [self lazySetWithId:JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED)];
    [((id<IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport>) nil_chk(parent_)) onTimeoutErrorWithLong:idx_ withJavaLangThrowable:t];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
  }
}

- (void)onComplete {
  if ([self get] != JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED)) {
    [self lazySetWithId:JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED)];
    [((id<IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport>) nil_chk(parent_)) onTimeoutWithLong:idx_];
  }
}

- (void)dispose {
  IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(self);
}

- (jboolean)isDisposed {
  return IoReactivexInternalDisposablesDisposableHelper_isDisposedWithIoReactivexDisposablesDisposable_([self get]);
}

- (void)dealloc {
  RELEASE_(parent_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithLong:withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(dispose);
  methods[6].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "parent_", "LIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "idx_", "J", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "JLIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport;", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "onError", "LJavaLangThrowable;", "LIoReactivexInternalOperatorsObservableObservableTimeout;", "Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;Lio/reactivex/Observer<Ljava/lang/Object;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer = { "TimeoutConsumer", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 7, 3, 7, -1, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer;
}

@end

void IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_initWithLong_withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport_(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer *self, jlong idx, id<IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport> parent) {
  JavaUtilConcurrentAtomicAtomicReference_init(self);
  self->idx_ = idx;
  JreStrongAssign(&self->parent_, parent);
}

IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer *new_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_initWithLong_withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport_(jlong idx, id<IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport> parent) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer, initWithLong_withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport_, idx, parent)
}

IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer *create_IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer_initWithLong_withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport_(jlong idx, id<IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport> parent) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer, initWithLong_withIoReactivexInternalOperatorsObservableObservableTimeout_TimeoutSelectorSupport_, idx, parent)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableTimeout_TimeoutConsumer)
