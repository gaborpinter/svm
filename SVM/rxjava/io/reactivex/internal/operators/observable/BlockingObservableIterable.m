//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/BlockingObservableIterable.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/ObservableSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/operators/observable/BlockingObservableIterable.h"
#include "io/reactivex/internal/queue/SpscLinkedArrayQueue.h"
#include "io/reactivex/internal/util/BlockingHelper.h"
#include "io/reactivex/internal/util/ExceptionHelper.h"
#include "java/lang/InterruptedException.h"
#include "java/lang/Iterable.h"
#include "java/lang/RuntimeException.h"
#include "java/lang/Throwable.h"
#include "java/lang/UnsupportedOperationException.h"
#include "java/util/Iterator.h"
#include "java/util/NoSuchElementException.h"
#include "java/util/Spliterator.h"
#include "java/util/concurrent/atomic/AtomicReference.h"
#include "java/util/concurrent/locks/Condition.h"
#include "java/util/concurrent/locks/Lock.h"
#include "java/util/concurrent/locks/ReentrantLock.h"
#include "java/util/function/Consumer.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator_serialVersionUID 6695226475494099826LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsObservableBlockingObservableIterable

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                                                      withInt:(jint)bufferSize {
  IoReactivexInternalOperatorsObservableBlockingObservableIterable_initWithIoReactivexObservableSource_withInt_(self, source, bufferSize);
  return self;
}

- (id<JavaUtilIterator>)iterator {
  IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator *it = create_IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator_initWithInt_(bufferSize_);
  [((id<IoReactivexObservableSource>) nil_chk(source_)) subscribeWithIoReactivexObserver:it];
  return it;
}

- (void)forEachWithJavaUtilFunctionConsumer:(id<JavaUtilFunctionConsumer>)arg0 {
  JavaLangIterable_forEachWithJavaUtilFunctionConsumer_(self, arg0);
}

- (id<JavaUtilSpliterator>)spliterator {
  return JavaLangIterable_spliterator(self);
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id *)stackbuf count:(NSUInteger)len {
  return JreDefaultFastEnumeration(self, state, stackbuf);
}

- (void)dealloc {
  RELEASE_(source_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "LJavaUtilIterator;", 0x1, -1, -1, -1, 2, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservableSource:withInt:);
  methods[1].selector = @selector(iterator);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 3, -1 },
    { "bufferSize_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservableSource;I", "(Lio/reactivex/ObservableSource<+TT;>;I)V", "()Ljava/util/Iterator<TT;>;", "Lio/reactivex/ObservableSource<+TT;>;", "LIoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Ljava/lang/Iterable<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableBlockingObservableIterable = { "BlockingObservableIterable", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x11, 2, 2, -1, 4, -1, 5, -1 };
  return &_IoReactivexInternalOperatorsObservableBlockingObservableIterable;
}

@end

void IoReactivexInternalOperatorsObservableBlockingObservableIterable_initWithIoReactivexObservableSource_withInt_(IoReactivexInternalOperatorsObservableBlockingObservableIterable *self, id<IoReactivexObservableSource> source, jint bufferSize) {
  NSObject_init(self);
  JreStrongAssign(&self->source_, source);
  self->bufferSize_ = bufferSize;
}

IoReactivexInternalOperatorsObservableBlockingObservableIterable *new_IoReactivexInternalOperatorsObservableBlockingObservableIterable_initWithIoReactivexObservableSource_withInt_(id<IoReactivexObservableSource> source, jint bufferSize) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableBlockingObservableIterable, initWithIoReactivexObservableSource_withInt_, source, bufferSize)
}

IoReactivexInternalOperatorsObservableBlockingObservableIterable *create_IoReactivexInternalOperatorsObservableBlockingObservableIterable_initWithIoReactivexObservableSource_withInt_(id<IoReactivexObservableSource> source, jint bufferSize) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableBlockingObservableIterable, initWithIoReactivexObservableSource_withInt_, source, bufferSize)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableBlockingObservableIterable)

@implementation IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator

- (instancetype __nonnull)initWithInt:(jint)batchSize {
  IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator_initWithInt_(self, batchSize);
  return self;
}

- (jboolean)hasNext {
  for (; ; ) {
    jboolean d = JreLoadVolatileBoolean(&done_);
    jboolean empty = [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(queue_)) isEmpty];
    if (d) {
      JavaLangThrowable *e = error_;
      if (e != nil) {
        @throw nil_chk(IoReactivexInternalUtilExceptionHelper_wrapOrThrowWithJavaLangThrowable_(e));
      }
      else if (empty) {
        return false;
      }
    }
    if (empty) {
      @try {
        IoReactivexInternalUtilBlockingHelper_verifyNonBlocking();
        [((id<JavaUtilConcurrentLocksLock>) nil_chk(lock_)) lock];
        @try {
          while (!JreLoadVolatileBoolean(&done_) && [queue_ isEmpty]) {
            [((id<JavaUtilConcurrentLocksCondition>) nil_chk(condition_)) await];
          }
        }
        @finally {
          [lock_ unlock];
        }
      }
      @catch (JavaLangInterruptedException *ex) {
        IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(self);
        [self signalConsumer];
        @throw nil_chk(IoReactivexInternalUtilExceptionHelper_wrapOrThrowWithJavaLangThrowable_(ex));
      }
    }
    else {
      return true;
    }
  }
}

- (id)next {
  if ([self hasNext]) {
    return [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(queue_)) poll];
  }
  @throw create_JavaUtilNoSuchElementException_init();
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  IoReactivexInternalDisposablesDisposableHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexDisposablesDisposable_(self, d);
}

- (void)onNextWithId:(id)t {
  [((IoReactivexInternalQueueSpscLinkedArrayQueue *) nil_chk(queue_)) offerWithId:t];
  [self signalConsumer];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  JreStrongAssign(&error_, t);
  JreAssignVolatileBoolean(&done_, true);
  [self signalConsumer];
}

- (void)onComplete {
  JreAssignVolatileBoolean(&done_, true);
  [self signalConsumer];
}

- (void)signalConsumer {
  [((id<JavaUtilConcurrentLocksLock>) nil_chk(lock_)) lock];
  @try {
    [((id<JavaUtilConcurrentLocksCondition>) nil_chk(condition_)) signalAll];
  }
  @finally {
    [lock_ unlock];
  }
}

- (void)remove {
  @throw create_JavaLangUnsupportedOperationException_initWithNSString_(@"remove");
}

- (void)dispose {
  IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(self);
}

- (jboolean)isDisposed {
  return IoReactivexInternalDisposablesDisposableHelper_isDisposedWithIoReactivexDisposablesDisposable_([self get]);
}

- (void)forEachRemainingWithJavaUtilFunctionConsumer:(id<JavaUtilFunctionConsumer>)arg0 {
  JavaUtilIterator_forEachRemainingWithJavaUtilFunctionConsumer_(self, arg0);
}

- (void)dealloc {
  RELEASE_(queue_);
  RELEASE_(lock_);
  RELEASE_(condition_);
  RELEASE_(error_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithInt:);
  methods[1].selector = @selector(hasNext);
  methods[2].selector = @selector(next);
  methods[3].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[4].selector = @selector(onNextWithId:);
  methods[5].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[6].selector = @selector(onComplete);
  methods[7].selector = @selector(signalConsumer);
  methods[8].selector = @selector(remove);
  methods[9].selector = @selector(dispose);
  methods[10].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "queue_", "LIoReactivexInternalQueueSpscLinkedArrayQueue;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "lock_", "LJavaUtilConcurrentLocksLock;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "condition_", "LJavaUtilConcurrentLocksCondition;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "error_", "LJavaLangThrowable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "I", "()TT;", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/internal/queue/SpscLinkedArrayQueue<TT;>;", "LIoReactivexInternalOperatorsObservableBlockingObservableIterable;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;Lio/reactivex/Observer<TT;>;Ljava/util/Iterator<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator = { "BlockingObservableIterator", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 11, 6, 10, -1, -1, 11, -1 };
  return &_IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator;
}

@end

void IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator_initWithInt_(IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator *self, jint batchSize) {
  JavaUtilConcurrentAtomicAtomicReference_init(self);
  JreStrongAssignAndConsume(&self->queue_, new_IoReactivexInternalQueueSpscLinkedArrayQueue_initWithInt_(batchSize));
  JreStrongAssignAndConsume(&self->lock_, new_JavaUtilConcurrentLocksReentrantLock_init());
  JreStrongAssign(&self->condition_, [self->lock_ newCondition]);
}

IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator *new_IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator_initWithInt_(jint batchSize) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator, initWithInt_, batchSize)
}

IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator *create_IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator_initWithInt_(jint batchSize) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator, initWithInt_, batchSize)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableBlockingObservableIterable_BlockingObservableIterator)
