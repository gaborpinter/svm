//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/BlockingObservableLatest.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableBlockingObservableLatest")
#ifdef RESTRICT_IoReactivexInternalOperatorsObservableBlockingObservableLatest
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableBlockingObservableLatest 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableBlockingObservableLatest 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsObservableBlockingObservableLatest

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsObservableBlockingObservableLatest_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableBlockingObservableLatest || defined(INCLUDE_IoReactivexInternalOperatorsObservableBlockingObservableLatest))
#define IoReactivexInternalOperatorsObservableBlockingObservableLatest_

#define RESTRICT_JavaLangIterable 1
#define INCLUDE_JavaLangIterable 1
#include "java/lang/Iterable.h"

@protocol IoReactivexObservableSource;
@protocol JavaUtilFunctionConsumer;
@protocol JavaUtilIterator;
@protocol JavaUtilSpliterator;

@interface IoReactivexInternalOperatorsObservableBlockingObservableLatest : NSObject < JavaLangIterable > {
 @public
  id<IoReactivexObservableSource> source_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source;

- (id<JavaUtilIterator>)iterator;

#pragma mark Package-Private

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableBlockingObservableLatest)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableBlockingObservableLatest, source_, id<IoReactivexObservableSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableBlockingObservableLatest_initWithIoReactivexObservableSource_(IoReactivexInternalOperatorsObservableBlockingObservableLatest *self, id<IoReactivexObservableSource> source);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableBlockingObservableLatest *new_IoReactivexInternalOperatorsObservableBlockingObservableLatest_initWithIoReactivexObservableSource_(id<IoReactivexObservableSource> source) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableBlockingObservableLatest *create_IoReactivexInternalOperatorsObservableBlockingObservableLatest_initWithIoReactivexObservableSource_(id<IoReactivexObservableSource> source);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableBlockingObservableLatest)

#endif

#if !defined (IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableBlockingObservableLatest || defined(INCLUDE_IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator))
#define IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator_

#define RESTRICT_IoReactivexObserversDisposableObserver 1
#define INCLUDE_IoReactivexObserversDisposableObserver 1
#include "io/reactivex/observers/DisposableObserver.h"

#define RESTRICT_JavaUtilIterator 1
#define INCLUDE_JavaUtilIterator 1
#include "java/util/Iterator.h"

@class IoReactivexNotification;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicReference;
@class JavaUtilConcurrentSemaphore;
@protocol JavaUtilFunctionConsumer;

@interface IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator : IoReactivexObserversDisposableObserver < JavaUtilIterator > {
 @public
  IoReactivexNotification *iteratorNotification_;
  JavaUtilConcurrentSemaphore *notify_;
  JavaUtilConcurrentAtomicAtomicReference *value_;
}

#pragma mark Public

- (jboolean)hasNext;

- (id)next;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onNextWithId:(IoReactivexNotification *)args;

- (void)remove;

#pragma mark Package-Private

- (instancetype __nonnull)init;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator, iteratorNotification_, IoReactivexNotification *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator, notify_, JavaUtilConcurrentSemaphore *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator, value_, JavaUtilConcurrentAtomicAtomicReference *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator_init(IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator *self);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator *new_IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator *create_IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator_init(void);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableBlockingObservableLatest_BlockingObservableLatestIterator)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableBlockingObservableLatest")
