//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableScan.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableScan")
#ifdef RESTRICT_IoReactivexInternalOperatorsObservableObservableScan
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableScan 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableScan 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsObservableObservableScan

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableScan_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableScan || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableScan))
#define IoReactivexInternalOperatorsObservableObservableScan_

#define RESTRICT_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"

@protocol IoReactivexFunctionsBiFunction;
@protocol IoReactivexObservableSource;
@protocol IoReactivexObserver;

@interface IoReactivexInternalOperatorsObservableObservableScan : IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream {
 @public
  id<IoReactivexFunctionsBiFunction> accumulator_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                           withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)accumulator;

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)t;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableScan)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableScan, accumulator_, id<IoReactivexFunctionsBiFunction>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableScan_initWithIoReactivexObservableSource_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsObservableObservableScan *self, id<IoReactivexObservableSource> source, id<IoReactivexFunctionsBiFunction> accumulator);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableScan *new_IoReactivexInternalOperatorsObservableObservableScan_initWithIoReactivexObservableSource_withIoReactivexFunctionsBiFunction_(id<IoReactivexObservableSource> source, id<IoReactivexFunctionsBiFunction> accumulator) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableScan *create_IoReactivexInternalOperatorsObservableObservableScan_initWithIoReactivexObservableSource_withIoReactivexFunctionsBiFunction_(id<IoReactivexObservableSource> source, id<IoReactivexFunctionsBiFunction> accumulator);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableScan)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableScan_ScanObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableScan || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableScan_ScanObserver))
#define IoReactivexInternalOperatorsObservableObservableScan_ScanObserver_

#define RESTRICT_IoReactivexObserver 1
#define INCLUDE_IoReactivexObserver 1
#include "io/reactivex/Observer.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class JavaLangThrowable;
@protocol IoReactivexFunctionsBiFunction;

@interface IoReactivexInternalOperatorsObservableObservableScan_ScanObserver : NSObject < IoReactivexObserver, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexObserver> downstream_;
  id<IoReactivexFunctionsBiFunction> accumulator_;
  id<IoReactivexDisposablesDisposable> upstream_;
  id value_;
  jboolean done_;
}

#pragma mark Public

- (void)dispose;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                   withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)accumulator;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableScan_ScanObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableScan_ScanObserver, downstream_, id<IoReactivexObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableScan_ScanObserver, accumulator_, id<IoReactivexFunctionsBiFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableScan_ScanObserver, upstream_, id<IoReactivexDisposablesDisposable>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableScan_ScanObserver, value_, id)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableScan_ScanObserver_initWithIoReactivexObserver_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsObservableObservableScan_ScanObserver *self, id<IoReactivexObserver> actual, id<IoReactivexFunctionsBiFunction> accumulator);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableScan_ScanObserver *new_IoReactivexInternalOperatorsObservableObservableScan_ScanObserver_initWithIoReactivexObserver_withIoReactivexFunctionsBiFunction_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsBiFunction> accumulator) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableScan_ScanObserver *create_IoReactivexInternalOperatorsObservableObservableScan_ScanObserver_initWithIoReactivexObserver_withIoReactivexFunctionsBiFunction_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsBiFunction> accumulator);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableScan_ScanObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableScan")
