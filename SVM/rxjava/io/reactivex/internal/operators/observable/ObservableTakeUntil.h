//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableTakeUntil.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeUntil")
#ifdef RESTRICT_IoReactivexInternalOperatorsObservableObservableTakeUntil
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeUntil 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeUntil 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsObservableObservableTakeUntil

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableTakeUntil_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeUntil || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableTakeUntil))
#define IoReactivexInternalOperatorsObservableObservableTakeUntil_

#define RESTRICT_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"

@protocol IoReactivexObservableSource;
@protocol IoReactivexObserver;

@interface IoReactivexInternalOperatorsObservableObservableTakeUntil : IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream {
 @public
  id<IoReactivexObservableSource> other_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                              withIoReactivexObservableSource:(id<IoReactivexObservableSource>)other;

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)child;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableTakeUntil)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeUntil, other_, id<IoReactivexObservableSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableTakeUntil_initWithIoReactivexObservableSource_withIoReactivexObservableSource_(IoReactivexInternalOperatorsObservableObservableTakeUntil *self, id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> other);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableTakeUntil *new_IoReactivexInternalOperatorsObservableObservableTakeUntil_initWithIoReactivexObservableSource_withIoReactivexObservableSource_(id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> other) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableTakeUntil *create_IoReactivexInternalOperatorsObservableObservableTakeUntil_initWithIoReactivexObservableSource_withIoReactivexObservableSource_(id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> other);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableTakeUntil)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeUntil || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver))
#define IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicInteger 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicInteger 1
#include "java/util/concurrent/atomic/AtomicInteger.h"

#define RESTRICT_IoReactivexObserver 1
#define INCLUDE_IoReactivexObserver 1
#include "io/reactivex/Observer.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver;
@class IoReactivexInternalUtilAtomicThrowable;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicReference;

@interface IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver : JavaUtilConcurrentAtomicAtomicInteger < IoReactivexObserver, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexObserver> downstream_;
  JavaUtilConcurrentAtomicAtomicReference *upstream_;
  IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver *otherObserver_;
  IoReactivexInternalUtilAtomicThrowable *error_;
}

#pragma mark Public

- (void)dispose;

- (NSUInteger)hash;

- (jboolean)isDisposed;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)downstream;

- (void)otherComplete;

- (void)otherErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver, downstream_, id<IoReactivexObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver, upstream_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver, otherObserver_, IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver, error_, IoReactivexInternalUtilAtomicThrowable *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_initWithIoReactivexObserver_(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver *self, id<IoReactivexObserver> downstream);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver *new_IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_initWithIoReactivexObserver_(id<IoReactivexObserver> downstream) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver *create_IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_initWithIoReactivexObserver_(id<IoReactivexObserver> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeUntil || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver))
#define IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexObserver 1
#define INCLUDE_IoReactivexObserver 1
#include "io/reactivex/Observer.h"

@class IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver;
@class JavaLangThrowable;
@protocol IoReactivexDisposablesDisposable;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;

@interface IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver : JavaUtilConcurrentAtomicAtomicReference < IoReactivexObserver >

#pragma mark Public

- (id<IoReactivexDisposablesDisposable>)accumulateAndGetWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)get;

- (id<IoReactivexDisposablesDisposable>)getAndAccumulateWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)getAndSetWithId:(id<IoReactivexDisposablesDisposable>)arg0;

- (id<IoReactivexDisposablesDisposable>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (id<IoReactivexDisposablesDisposable>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver:(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver *)outer$;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver_initWithIoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver *self, IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver *outer$);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver *new_IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver_initWithIoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver *outer$) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver *create_IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver_initWithIoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver *outer$);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableTakeUntil_TakeUntilMainObserver_OtherObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableTakeUntil")
