//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableConcatWithSingle.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableConcatWithSingle")
#ifdef RESTRICT_IoReactivexInternalOperatorsObservableObservableConcatWithSingle
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableConcatWithSingle 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableConcatWithSingle 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsObservableObservableConcatWithSingle

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableConcatWithSingle_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableConcatWithSingle || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableConcatWithSingle))
#define IoReactivexInternalOperatorsObservableObservableConcatWithSingle_

#define RESTRICT_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"

@class IoReactivexObservable;
@protocol IoReactivexObservableSource;
@protocol IoReactivexObserver;
@protocol IoReactivexSingleSource;

@interface IoReactivexInternalOperatorsObservableObservableConcatWithSingle : IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream {
 @public
  id<IoReactivexSingleSource> other_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObservable:(IoReactivexObservable *)source
                            withIoReactivexSingleSource:(id<IoReactivexSingleSource>)other;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableConcatWithSingle)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableConcatWithSingle, other_, id<IoReactivexSingleSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableConcatWithSingle_initWithIoReactivexObservable_withIoReactivexSingleSource_(IoReactivexInternalOperatorsObservableObservableConcatWithSingle *self, IoReactivexObservable *source, id<IoReactivexSingleSource> other);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableConcatWithSingle *new_IoReactivexInternalOperatorsObservableObservableConcatWithSingle_initWithIoReactivexObservable_withIoReactivexSingleSource_(IoReactivexObservable *source, id<IoReactivexSingleSource> other) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableConcatWithSingle *create_IoReactivexInternalOperatorsObservableObservableConcatWithSingle_initWithIoReactivexObservable_withIoReactivexSingleSource_(IoReactivexObservable *source, id<IoReactivexSingleSource> other);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableConcatWithSingle)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableConcatWithSingle || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver))
#define IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexObserver 1
#define INCLUDE_IoReactivexObserver 1
#include "io/reactivex/Observer.h"

#define RESTRICT_IoReactivexSingleObserver 1
#define INCLUDE_IoReactivexSingleObserver 1
#include "io/reactivex/SingleObserver.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class JavaLangThrowable;
@protocol IoReactivexSingleSource;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;

@interface IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver : JavaUtilConcurrentAtomicAtomicReference < IoReactivexObserver, IoReactivexSingleObserver, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexObserver> downstream_;
  id<IoReactivexSingleSource> other_;
  jboolean inSingle_;
}

#pragma mark Public

- (id<IoReactivexDisposablesDisposable>)accumulateAndGetWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)dispose;

- (id<IoReactivexDisposablesDisposable>)get;

- (id<IoReactivexDisposablesDisposable>)getAndAccumulateWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)getAndSetWithId:(id<IoReactivexDisposablesDisposable>)arg0;

- (id<IoReactivexDisposablesDisposable>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (void)onSuccessWithId:(id)t;

- (id<IoReactivexDisposablesDisposable>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                          withIoReactivexSingleSource:(id<IoReactivexSingleSource>)other;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver, downstream_, id<IoReactivexObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver, other_, id<IoReactivexSingleSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver_initWithIoReactivexObserver_withIoReactivexSingleSource_(IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver *self, id<IoReactivexObserver> actual, id<IoReactivexSingleSource> other);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver *new_IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver_initWithIoReactivexObserver_withIoReactivexSingleSource_(id<IoReactivexObserver> actual, id<IoReactivexSingleSource> other) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver *create_IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver_initWithIoReactivexObserver_withIoReactivexSingleSource_(id<IoReactivexObserver> actual, id<IoReactivexSingleSource> other);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableConcatWithSingle_ConcatWithObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableConcatWithSingle")
