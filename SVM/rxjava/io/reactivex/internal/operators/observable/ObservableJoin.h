//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableJoin.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableJoin")
#ifdef RESTRICT_IoReactivexInternalOperatorsObservableObservableJoin
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableJoin 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableJoin 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsObservableObservableJoin

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableJoin_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableJoin || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableJoin))
#define IoReactivexInternalOperatorsObservableObservableJoin_

#define RESTRICT_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"

@protocol IoReactivexFunctionsBiFunction;
@protocol IoReactivexFunctionsFunction;
@protocol IoReactivexObservableSource;
@protocol IoReactivexObserver;

@interface IoReactivexInternalOperatorsObservableObservableJoin : IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream {
 @public
  id<IoReactivexObservableSource> other_;
  id<IoReactivexFunctionsFunction> leftEnd_;
  id<IoReactivexFunctionsFunction> rightEnd_;
  id<IoReactivexFunctionsBiFunction> resultSelector_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                              withIoReactivexObservableSource:(id<IoReactivexObservableSource>)other
                             withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)leftEnd
                             withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)rightEnd
                           withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)resultSelector;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableJoin)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin, other_, id<IoReactivexObservableSource>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin, leftEnd_, id<IoReactivexFunctionsFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin, rightEnd_, id<IoReactivexFunctionsFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin, resultSelector_, id<IoReactivexFunctionsBiFunction>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableJoin_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsObservableObservableJoin *self, id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> other, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableJoin *new_IoReactivexInternalOperatorsObservableObservableJoin_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> other, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableJoin *create_IoReactivexInternalOperatorsObservableObservableJoin_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> other, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableJoin)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableJoin || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable))
#define IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicInteger 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicInteger 1
#include "java/util/concurrent/atomic/AtomicInteger.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

#define RESTRICT_IoReactivexInternalOperatorsObservableObservableGroupJoin 1
#define INCLUDE_IoReactivexInternalOperatorsObservableObservableGroupJoin_JoinSupport 1
#include "io/reactivex/internal/operators/observable/ObservableGroupJoin.h"

@class IoReactivexDisposablesCompositeDisposable;
@class IoReactivexInternalOperatorsObservableObservableGroupJoin_LeftRightEndObserver;
@class IoReactivexInternalOperatorsObservableObservableGroupJoin_LeftRightObserver;
@class IoReactivexInternalQueueSpscLinkedArrayQueue;
@class JavaLangInteger;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicReference;
@protocol IoReactivexFunctionsBiFunction;
@protocol IoReactivexFunctionsFunction;
@protocol IoReactivexObserver;
@protocol JavaUtilMap;

@interface IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable : JavaUtilConcurrentAtomicAtomicInteger < IoReactivexDisposablesDisposable, IoReactivexInternalOperatorsObservableObservableGroupJoin_JoinSupport > {
 @public
  id<IoReactivexObserver> downstream_;
  IoReactivexInternalQueueSpscLinkedArrayQueue *queue_;
  IoReactivexDisposablesCompositeDisposable *disposables_;
  id<JavaUtilMap> lefts_;
  id<JavaUtilMap> rights_;
  JavaUtilConcurrentAtomicAtomicReference *error_;
  id<IoReactivexFunctionsFunction> leftEnd_;
  id<IoReactivexFunctionsFunction> rightEnd_;
  id<IoReactivexFunctionsBiFunction> resultSelector_;
  JavaUtilConcurrentAtomicAtomicInteger *active_;
  jint leftIndex_;
  jint rightIndex_;
  volatile_jboolean cancelled_;
}

+ (JavaLangInteger *)LEFT_VALUE;

+ (JavaLangInteger *)RIGHT_VALUE;

+ (JavaLangInteger *)LEFT_CLOSE;

+ (JavaLangInteger *)RIGHT_CLOSE;

#pragma mark Public

- (void)dispose;

- (NSUInteger)hash;

- (void)innerCloseWithBoolean:(jboolean)isLeft
withIoReactivexInternalOperatorsObservableObservableGroupJoin_LeftRightEndObserver:(IoReactivexInternalOperatorsObservableObservableGroupJoin_LeftRightEndObserver *)index;

- (void)innerCloseErrorWithJavaLangThrowable:(JavaLangThrowable *)ex;

- (void)innerCompleteWithIoReactivexInternalOperatorsObservableObservableGroupJoin_LeftRightObserver:(IoReactivexInternalOperatorsObservableObservableGroupJoin_LeftRightObserver *)sender;

- (void)innerErrorWithJavaLangThrowable:(JavaLangThrowable *)ex;

- (void)innerValueWithBoolean:(jboolean)isLeft
                       withId:(id)o;

- (jboolean)isDisposed;

- (jboolean)isEqual:(id)obj;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)leftEnd
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)rightEnd
                   withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)resultSelector;

- (void)cancelAll;

- (void)drain;

- (void)errorAllWithIoReactivexObserver:(id<IoReactivexObserver>)a;

- (void)failWithJavaLangThrowable:(JavaLangThrowable *)exc
          withIoReactivexObserver:(id<IoReactivexObserver>)a
withIoReactivexInternalQueueSpscLinkedArrayQueue:(IoReactivexInternalQueueSpscLinkedArrayQueue *)q;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, downstream_, id<IoReactivexObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, queue_, IoReactivexInternalQueueSpscLinkedArrayQueue *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, disposables_, IoReactivexDisposablesCompositeDisposable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, lefts_, id<JavaUtilMap>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, rights_, id<JavaUtilMap>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, error_, JavaUtilConcurrentAtomicAtomicReference *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, leftEnd_, id<IoReactivexFunctionsFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, rightEnd_, id<IoReactivexFunctionsFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, resultSelector_, id<IoReactivexFunctionsBiFunction>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, active_, JavaUtilConcurrentAtomicAtomicInteger *)

inline JavaLangInteger *IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_get_LEFT_VALUE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT JavaLangInteger *IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_LEFT_VALUE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, LEFT_VALUE, JavaLangInteger *)

inline JavaLangInteger *IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_get_RIGHT_VALUE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT JavaLangInteger *IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_RIGHT_VALUE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, RIGHT_VALUE, JavaLangInteger *)

inline JavaLangInteger *IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_get_LEFT_CLOSE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT JavaLangInteger *IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_LEFT_CLOSE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, LEFT_CLOSE, JavaLangInteger *)

inline JavaLangInteger *IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_get_RIGHT_CLOSE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT JavaLangInteger *IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_RIGHT_CLOSE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable, RIGHT_CLOSE, JavaLangInteger *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable *self, id<IoReactivexObserver> actual, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable *new_IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable *create_IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withIoReactivexFunctionsFunction_withIoReactivexFunctionsBiFunction_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsFunction> leftEnd, id<IoReactivexFunctionsFunction> rightEnd, id<IoReactivexFunctionsBiFunction> resultSelector);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableJoin_JoinDisposable)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableJoin")
