//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableReduceMaybe.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Maybe.h"
#include "io/reactivex/MaybeObserver.h"
#include "io/reactivex/ObservableSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/BiFunction.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/operators/observable/ObservableReduceMaybe.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Throwable.h"

@implementation IoReactivexInternalOperatorsObservableObservableReduceMaybe

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                           withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)reducer {
  IoReactivexInternalOperatorsObservableObservableReduceMaybe_initWithIoReactivexObservableSource_withIoReactivexFunctionsBiFunction_(self, source, reducer);
  return self;
}

- (void)subscribeActualWithIoReactivexMaybeObserver:(id<IoReactivexMaybeObserver>)observer {
  [((id<IoReactivexObservableSource>) nil_chk(source_)) subscribeWithIoReactivexObserver:create_IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver_initWithIoReactivexMaybeObserver_withIoReactivexFunctionsBiFunction_(observer, reducer_)];
}

- (void)dealloc {
  RELEASE_(source_);
  RELEASE_(reducer_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservableSource:withIoReactivexFunctionsBiFunction:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexMaybeObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "reducer_", "LIoReactivexFunctionsBiFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservableSource;LIoReactivexFunctionsBiFunction;", "(Lio/reactivex/ObservableSource<TT;>;Lio/reactivex/functions/BiFunction<TT;TT;TT;>;)V", "subscribeActual", "LIoReactivexMaybeObserver;", "(Lio/reactivex/MaybeObserver<-TT;>;)V", "Lio/reactivex/ObservableSource<TT;>;", "Lio/reactivex/functions/BiFunction<TT;TT;TT;>;", "LIoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/Maybe<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableReduceMaybe = { "ObservableReduceMaybe", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x11, 2, 2, -1, 7, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableReduceMaybe;
}

@end

void IoReactivexInternalOperatorsObservableObservableReduceMaybe_initWithIoReactivexObservableSource_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsObservableObservableReduceMaybe *self, id<IoReactivexObservableSource> source, id<IoReactivexFunctionsBiFunction> reducer) {
  IoReactivexMaybe_init(self);
  JreStrongAssign(&self->source_, source);
  JreStrongAssign(&self->reducer_, reducer);
}

IoReactivexInternalOperatorsObservableObservableReduceMaybe *new_IoReactivexInternalOperatorsObservableObservableReduceMaybe_initWithIoReactivexObservableSource_withIoReactivexFunctionsBiFunction_(id<IoReactivexObservableSource> source, id<IoReactivexFunctionsBiFunction> reducer) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableReduceMaybe, initWithIoReactivexObservableSource_withIoReactivexFunctionsBiFunction_, source, reducer)
}

IoReactivexInternalOperatorsObservableObservableReduceMaybe *create_IoReactivexInternalOperatorsObservableObservableReduceMaybe_initWithIoReactivexObservableSource_withIoReactivexFunctionsBiFunction_(id<IoReactivexObservableSource> source, id<IoReactivexFunctionsBiFunction> reducer) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableReduceMaybe, initWithIoReactivexObservableSource_withIoReactivexFunctionsBiFunction_, source, reducer)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableReduceMaybe)

@implementation IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver

- (instancetype __nonnull)initWithIoReactivexMaybeObserver:(id<IoReactivexMaybeObserver>)observer
                        withIoReactivexFunctionsBiFunction:(id<IoReactivexFunctionsBiFunction>)reducer {
  IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver_initWithIoReactivexMaybeObserver_withIoReactivexFunctionsBiFunction_(self, observer, reducer);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    [((id<IoReactivexMaybeObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onNextWithId:(id)value {
  if (!done_) {
    id v = self->value_;
    if (v == nil) {
      JreStrongAssign(&self->value_, value);
    }
    else {
      @try {
        JreStrongAssign(&self->value_, IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsBiFunction>) nil_chk(reducer_)) applyWithId:v withId:value], @"The reducer returned a null value"));
      }
      @catch (JavaLangThrowable *ex) {
        IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
        [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
        [self onErrorWithJavaLangThrowable:ex];
      }
    }
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  if (done_) {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(e);
    return;
  }
  done_ = true;
  JreStrongAssign(&value_, nil);
  [((id<IoReactivexMaybeObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
}

- (void)onComplete {
  if (done_) {
    return;
  }
  done_ = true;
  id v = value_;
  JreStrongAssign(&value_, nil);
  if (v != nil) {
    [((id<IoReactivexMaybeObserver>) nil_chk(downstream_)) onSuccessWithId:v];
  }
  else {
    [((id<IoReactivexMaybeObserver>) nil_chk(downstream_)) onComplete];
  }
}

- (void)dispose {
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
}

- (jboolean)isDisposed {
  return [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) isDisposed];
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(reducer_);
  RELEASE_(value_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexMaybeObserver:withIoReactivexFunctionsBiFunction:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(dispose);
  methods[6].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "downstream_", "LIoReactivexMaybeObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "reducer_", "LIoReactivexFunctionsBiFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "value_", "LNSObject;", .constantValue.asLong = 0, 0x0, -1, -1, 11, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexMaybeObserver;LIoReactivexFunctionsBiFunction;", "(Lio/reactivex/MaybeObserver<-TT;>;Lio/reactivex/functions/BiFunction<TT;TT;TT;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/MaybeObserver<-TT;>;", "Lio/reactivex/functions/BiFunction<TT;TT;TT;>;", "TT;", "LIoReactivexInternalOperatorsObservableObservableReduceMaybe;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/Observer<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver = { "ReduceObserver", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 7, 5, 12, -1, -1, 13, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver;
}

@end

void IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver_initWithIoReactivexMaybeObserver_withIoReactivexFunctionsBiFunction_(IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver *self, id<IoReactivexMaybeObserver> observer, id<IoReactivexFunctionsBiFunction> reducer) {
  NSObject_init(self);
  JreStrongAssign(&self->downstream_, observer);
  JreStrongAssign(&self->reducer_, reducer);
}

IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver *new_IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver_initWithIoReactivexMaybeObserver_withIoReactivexFunctionsBiFunction_(id<IoReactivexMaybeObserver> observer, id<IoReactivexFunctionsBiFunction> reducer) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver, initWithIoReactivexMaybeObserver_withIoReactivexFunctionsBiFunction_, observer, reducer)
}

IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver *create_IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver_initWithIoReactivexMaybeObserver_withIoReactivexFunctionsBiFunction_(id<IoReactivexMaybeObserver> observer, id<IoReactivexFunctionsBiFunction> reducer) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver, initWithIoReactivexMaybeObserver_withIoReactivexFunctionsBiFunction_, observer, reducer)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableReduceMaybe_ReduceObserver)
