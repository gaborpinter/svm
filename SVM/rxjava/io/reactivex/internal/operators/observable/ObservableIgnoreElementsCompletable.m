//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableIgnoreElementsCompletable.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Completable.h"
#include "io/reactivex/CompletableObserver.h"
#include "io/reactivex/Observable.h"
#include "io/reactivex/ObservableSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/internal/operators/observable/ObservableIgnoreElements.h"
#include "io/reactivex/internal/operators/observable/ObservableIgnoreElementsCompletable.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Throwable.h"

@implementation IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source {
  IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_initWithIoReactivexObservableSource_(self, source);
  return self;
}

- (void)subscribeActualWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)t {
  [((id<IoReactivexObservableSource>) nil_chk(source_)) subscribeWithIoReactivexObserver:create_IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable_initWithIoReactivexCompletableObserver_(t)];
}

- (IoReactivexObservable *)fuseToObservable {
  return IoReactivexPluginsRxJavaPlugins_onAssemblyWithIoReactivexObservable_(create_IoReactivexInternalOperatorsObservableObservableIgnoreElements_initWithIoReactivexObservableSource_(source_));
}

- (void)dealloc {
  RELEASE_(source_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "LIoReactivexObservable;", 0x1, -1, -1, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservableSource:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexCompletableObserver:);
  methods[2].selector = @selector(fuseToObservable);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservableSource;", "(Lio/reactivex/ObservableSource<TT;>;)V", "subscribeActual", "LIoReactivexCompletableObserver;", "()Lio/reactivex/Observable<TT;>;", "Lio/reactivex/ObservableSource<TT;>;", "LIoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable;", "<T:Ljava/lang/Object;>Lio/reactivex/Completable;Lio/reactivex/internal/fuseable/FuseToObservable<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable = { "ObservableIgnoreElementsCompletable", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x11, 3, 1, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable;
}

@end

void IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_initWithIoReactivexObservableSource_(IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable *self, id<IoReactivexObservableSource> source) {
  IoReactivexCompletable_init(self);
  JreStrongAssign(&self->source_, source);
}

IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable *new_IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_initWithIoReactivexObservableSource_(id<IoReactivexObservableSource> source) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable, initWithIoReactivexObservableSource_, source)
}

IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable *create_IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_initWithIoReactivexObservableSource_(id<IoReactivexObservableSource> source) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable, initWithIoReactivexObservableSource_, source)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable)

@implementation IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable

- (instancetype __nonnull)initWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)t {
  IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable_initWithIoReactivexCompletableObserver_(self, t);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  JreStrongAssign(&self->upstream_, d);
  [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
}

- (void)onNextWithId:(id)v {
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
}

- (void)onComplete {
  [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onComplete];
}

- (void)dispose {
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
}

- (jboolean)isDisposed {
  return [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) isDisposed];
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, 5, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexCompletableObserver:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(dispose);
  methods[6].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "downstream_", "LIoReactivexCompletableObserver;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexCompletableObserver;", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "LIoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/Observer<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable = { "IgnoreObservable", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 7, 2, 8, -1, -1, 9, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable;
}

@end

void IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable_initWithIoReactivexCompletableObserver_(IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable *self, id<IoReactivexCompletableObserver> t) {
  NSObject_init(self);
  JreStrongAssign(&self->downstream_, t);
}

IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable *new_IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable_initWithIoReactivexCompletableObserver_(id<IoReactivexCompletableObserver> t) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable, initWithIoReactivexCompletableObserver_, t)
}

IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable *create_IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable_initWithIoReactivexCompletableObserver_(id<IoReactivexCompletableObserver> t) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable, initWithIoReactivexCompletableObserver_, t)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableIgnoreElementsCompletable_IgnoreObservable)
