//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableDoAfterNext.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/ObservableSource.h"
#include "io/reactivex/Observer.h"
#include "io/reactivex/functions/Consumer.h"
#include "io/reactivex/internal/fuseable/QueueDisposable.h"
#include "io/reactivex/internal/fuseable/QueueFuseable.h"
#include "io/reactivex/internal/observers/BasicFuseableObserver.h"
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"
#include "io/reactivex/internal/operators/observable/ObservableDoAfterNext.h"
#include "java/lang/Throwable.h"

@implementation IoReactivexInternalOperatorsObservableObservableDoAfterNext

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                             withIoReactivexFunctionsConsumer:(id<IoReactivexFunctionsConsumer>)onAfterNext {
  IoReactivexInternalOperatorsObservableObservableDoAfterNext_initWithIoReactivexObservableSource_withIoReactivexFunctionsConsumer_(self, source, onAfterNext);
  return self;
}

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer {
  [((id<IoReactivexObservableSource>) nil_chk(source_)) subscribeWithIoReactivexObserver:create_IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver_initWithIoReactivexObserver_withIoReactivexFunctionsConsumer_(observer, onAfterNext_)];
}

- (void)dealloc {
  RELEASE_(onAfterNext_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservableSource:withIoReactivexFunctionsConsumer:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "onAfterNext_", "LIoReactivexFunctionsConsumer;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservableSource;LIoReactivexFunctionsConsumer;", "(Lio/reactivex/ObservableSource<TT;>;Lio/reactivex/functions/Consumer<-TT;>;)V", "subscribeActual", "LIoReactivexObserver;", "(Lio/reactivex/Observer<-TT;>;)V", "Lio/reactivex/functions/Consumer<-TT;>;", "LIoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/observable/AbstractObservableWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableDoAfterNext = { "ObservableDoAfterNext", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableDoAfterNext;
}

@end

void IoReactivexInternalOperatorsObservableObservableDoAfterNext_initWithIoReactivexObservableSource_withIoReactivexFunctionsConsumer_(IoReactivexInternalOperatorsObservableObservableDoAfterNext *self, id<IoReactivexObservableSource> source, id<IoReactivexFunctionsConsumer> onAfterNext) {
  IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream_initWithIoReactivexObservableSource_(self, source);
  JreStrongAssign(&self->onAfterNext_, onAfterNext);
}

IoReactivexInternalOperatorsObservableObservableDoAfterNext *new_IoReactivexInternalOperatorsObservableObservableDoAfterNext_initWithIoReactivexObservableSource_withIoReactivexFunctionsConsumer_(id<IoReactivexObservableSource> source, id<IoReactivexFunctionsConsumer> onAfterNext) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableDoAfterNext, initWithIoReactivexObservableSource_withIoReactivexFunctionsConsumer_, source, onAfterNext)
}

IoReactivexInternalOperatorsObservableObservableDoAfterNext *create_IoReactivexInternalOperatorsObservableObservableDoAfterNext_initWithIoReactivexObservableSource_withIoReactivexFunctionsConsumer_(id<IoReactivexObservableSource> source, id<IoReactivexFunctionsConsumer> onAfterNext) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableDoAfterNext, initWithIoReactivexObservableSource_withIoReactivexFunctionsConsumer_, source, onAfterNext)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableDoAfterNext)

@implementation IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                     withIoReactivexFunctionsConsumer:(id<IoReactivexFunctionsConsumer>)onAfterNext {
  IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver_initWithIoReactivexObserver_withIoReactivexFunctionsConsumer_(self, actual, onAfterNext);
  return self;
}

- (void)onNextWithId:(id)t {
  [((id<IoReactivexObserver>) nil_chk(downstream_)) onNextWithId:t];
  if (sourceMode_ == IoReactivexInternalFuseableQueueFuseable_NONE) {
    @try {
      [((id<IoReactivexFunctionsConsumer>) nil_chk(onAfterNext_)) acceptWithId:t];
    }
    @catch (JavaLangThrowable *ex) {
      [self failWithJavaLangThrowable:ex];
    }
  }
}

- (jint)requestFusionWithInt:(jint)mode {
  return [self transitiveBoundaryFusionWithInt:mode];
}

- (id __nullable)poll {
  id v = [((id<IoReactivexInternalFuseableQueueDisposable>) nil_chk(qd_)) poll];
  if (v != nil) {
    [((id<IoReactivexFunctionsConsumer>) nil_chk(onAfterNext_)) acceptWithId:v];
  }
  return v;
}

- (void)dealloc {
  RELEASE_(onAfterNext_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
    { NULL, "I", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, 7, 8, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObserver:withIoReactivexFunctionsConsumer:);
  methods[1].selector = @selector(onNextWithId:);
  methods[2].selector = @selector(requestFusionWithInt:);
  methods[3].selector = @selector(poll);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "onAfterNext_", "LIoReactivexFunctionsConsumer;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObserver;LIoReactivexFunctionsConsumer;", "(Lio/reactivex/Observer<-TT;>;Lio/reactivex/functions/Consumer<-TT;>;)V", "onNext", "LNSObject;", "(TT;)V", "requestFusion", "I", "LJavaLangException;", "()TT;", "Lio/reactivex/functions/Consumer<-TT;>;", "LIoReactivexInternalOperatorsObservableObservableDoAfterNext;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/observers/BasicFuseableObserver<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver = { "DoAfterObserver", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 4, 1, 10, -1, -1, 11, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver;
}

@end

void IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver_initWithIoReactivexObserver_withIoReactivexFunctionsConsumer_(IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver *self, id<IoReactivexObserver> actual, id<IoReactivexFunctionsConsumer> onAfterNext) {
  IoReactivexInternalObserversBasicFuseableObserver_initWithIoReactivexObserver_(self, actual);
  JreStrongAssign(&self->onAfterNext_, onAfterNext);
}

IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver *new_IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver_initWithIoReactivexObserver_withIoReactivexFunctionsConsumer_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsConsumer> onAfterNext) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver, initWithIoReactivexObserver_withIoReactivexFunctionsConsumer_, actual, onAfterNext)
}

IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver *create_IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver_initWithIoReactivexObserver_withIoReactivexFunctionsConsumer_(id<IoReactivexObserver> actual, id<IoReactivexFunctionsConsumer> onAfterNext) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver, initWithIoReactivexObserver_withIoReactivexFunctionsConsumer_, actual, onAfterNext)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableDoAfterNext_DoAfterObserver)
