//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableBufferExactBoundary.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/ObservableSource.h"
#include "io/reactivex/Observer.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/disposables/EmptyDisposable.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/fuseable/SimplePlainQueue.h"
#include "io/reactivex/internal/observers/QueueDrainObserver.h"
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"
#include "io/reactivex/internal/operators/observable/ObservableBufferExactBoundary.h"
#include "io/reactivex/internal/queue/MpscLinkedQueue.h"
#include "io/reactivex/internal/util/QueueDrainHelper.h"
#include "io/reactivex/observers/DisposableObserver.h"
#include "io/reactivex/observers/SerializedObserver.h"
#include "java/lang/Throwable.h"
#include "java/util/Collection.h"
#include "java/util/concurrent/Callable.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation IoReactivexInternalOperatorsObservableObservableBufferExactBoundary

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)source
                              withIoReactivexObservableSource:(id<IoReactivexObservableSource>)boundary
                               withJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)bufferSupplier {
  IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withJavaUtilConcurrentCallable_(self, source, boundary, bufferSupplier);
  return self;
}

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)t {
  [((id<IoReactivexObservableSource>) nil_chk(source_)) subscribeWithIoReactivexObserver:create_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_initWithIoReactivexObserver_withJavaUtilConcurrentCallable_withIoReactivexObservableSource_(create_IoReactivexObserversSerializedObserver_initWithIoReactivexObserver_(t), bufferSupplier_, boundary_)];
}

- (void)dealloc {
  RELEASE_(boundary_);
  RELEASE_(bufferSupplier_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservableSource:withIoReactivexObservableSource:withJavaUtilConcurrentCallable:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "boundary_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "bufferSupplier_", "LJavaUtilConcurrentCallable;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservableSource;LIoReactivexObservableSource;LJavaUtilConcurrentCallable;", "(Lio/reactivex/ObservableSource<TT;>;Lio/reactivex/ObservableSource<TB;>;Ljava/util/concurrent/Callable<TU;>;)V", "subscribeActual", "LIoReactivexObserver;", "(Lio/reactivex/Observer<-TU;>;)V", "Lio/reactivex/ObservableSource<TB;>;", "Ljava/util/concurrent/Callable<TU;>;", "LIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver;LIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver;", "<T:Ljava/lang/Object;U::Ljava/util/Collection<-TT;>;B:Ljava/lang/Object;>Lio/reactivex/internal/operators/observable/AbstractObservableWithUpstream<TT;TU;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableBufferExactBoundary = { "ObservableBufferExactBoundary", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x11, 2, 2, -1, 7, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary;
}

@end

void IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withJavaUtilConcurrentCallable_(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary *self, id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> boundary, id<JavaUtilConcurrentCallable> bufferSupplier) {
  IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream_initWithIoReactivexObservableSource_(self, source);
  JreStrongAssign(&self->boundary_, boundary);
  JreStrongAssign(&self->bufferSupplier_, bufferSupplier);
}

IoReactivexInternalOperatorsObservableObservableBufferExactBoundary *new_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withJavaUtilConcurrentCallable_(id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> boundary, id<JavaUtilConcurrentCallable> bufferSupplier) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary, initWithIoReactivexObservableSource_withIoReactivexObservableSource_withJavaUtilConcurrentCallable_, source, boundary, bufferSupplier)
}

IoReactivexInternalOperatorsObservableObservableBufferExactBoundary *create_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_initWithIoReactivexObservableSource_withIoReactivexObservableSource_withJavaUtilConcurrentCallable_(id<IoReactivexObservableSource> source, id<IoReactivexObservableSource> boundary, id<JavaUtilConcurrentCallable> bufferSupplier) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary, initWithIoReactivexObservableSource_withIoReactivexObservableSource_withJavaUtilConcurrentCallable_, source, boundary, bufferSupplier)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary)

@implementation IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                       withJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)bufferSupplier
                      withIoReactivexObservableSource:(id<IoReactivexObservableSource>)boundary {
  IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_initWithIoReactivexObserver_withJavaUtilConcurrentCallable_withIoReactivexObservableSource_(self, actual, bufferSupplier, boundary);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    id<JavaUtilCollection> b;
    @try {
      b = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<JavaUtilConcurrentCallable>) nil_chk(bufferSupplier_)) call], @"The buffer supplied is null");
    }
    @catch (JavaLangThrowable *e) {
      IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(e);
      JreAssignVolatileBoolean(&cancelled_, true);
      [((id<IoReactivexDisposablesDisposable>) nil_chk(d)) dispose];
      IoReactivexInternalDisposablesEmptyDisposable_errorWithJavaLangThrowable_withIoReactivexObserver_(e, downstream_);
      return;
    }
    JreStrongAssign(&buffer_, b);
    IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver *bs = create_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver_initWithIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_(self);
    JreStrongAssign(&other_, bs);
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
    if (!JreLoadVolatileBoolean(&cancelled_)) {
      [((id<IoReactivexObservableSource>) nil_chk(boundary_)) subscribeWithIoReactivexObserver:bs];
    }
  }
}

- (void)onNextWithId:(id)t {
  @synchronized(self) {
    id<JavaUtilCollection> b = buffer_;
    if (b == nil) {
      return;
    }
    [b addWithId:t];
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  [self dispose];
  [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  id<JavaUtilCollection> b;
  @synchronized(self) {
    b = JreRetainedLocalValue(buffer_);
    if (b == nil) {
      return;
    }
    JreStrongAssign(&buffer_, nil);
  }
  [((id<IoReactivexInternalFuseableSimplePlainQueue>) nil_chk(queue_)) offerWithId:b];
  JreAssignVolatileBoolean(&done_, true);
  if ([self enter]) {
    IoReactivexInternalUtilQueueDrainHelper_drainLoopWithIoReactivexInternalFuseableSimplePlainQueue_withIoReactivexObserver_withBoolean_withIoReactivexDisposablesDisposable_withIoReactivexInternalUtilObservableQueueDrain_(queue_, downstream_, false, self, self);
  }
}

- (void)dispose {
  if (!JreLoadVolatileBoolean(&cancelled_)) {
    JreAssignVolatileBoolean(&cancelled_, true);
    [((id<IoReactivexDisposablesDisposable>) nil_chk(other_)) dispose];
    [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
    if ([self enter]) {
      [((id<IoReactivexInternalFuseableSimplePlainQueue>) nil_chk(queue_)) clear];
    }
  }
}

- (jboolean)isDisposed {
  return JreLoadVolatileBoolean(&cancelled_);
}

- (void)next {
  id<JavaUtilCollection> next;
  @try {
    next = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<JavaUtilConcurrentCallable>) nil_chk(bufferSupplier_)) call], @"The buffer supplied is null");
  }
  @catch (JavaLangThrowable *e) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(e);
    [self dispose];
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
    return;
  }
  id<JavaUtilCollection> b;
  @synchronized(self) {
    b = JreRetainedLocalValue(buffer_);
    if (b == nil) {
      return;
    }
    JreStrongAssign(&buffer_, next);
  }
  [self fastPathEmitWithId:b withBoolean:false withIoReactivexDisposablesDisposable:self];
}

- (void)acceptWithIoReactivexObserver:(id<IoReactivexObserver>)a
                               withId:(id<JavaUtilCollection>)v {
  [((id<IoReactivexObserver>) nil_chk(downstream_)) onNextWithId:v];
}

- (void)dealloc {
  RELEASE_(bufferSupplier_);
  RELEASE_(boundary_);
  RELEASE_(upstream_);
  RELEASE_(other_);
  RELEASE_(buffer_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, 11, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObserver:withJavaUtilConcurrentCallable:withIoReactivexObservableSource:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(dispose);
  methods[6].selector = @selector(isDisposed);
  methods[7].selector = @selector(next);
  methods[8].selector = @selector(acceptWithIoReactivexObserver:withId:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "bufferSupplier_", "LJavaUtilConcurrentCallable;", .constantValue.asLong = 0, 0x10, -1, -1, 12, -1 },
    { "boundary_", "LIoReactivexObservableSource;", .constantValue.asLong = 0, 0x10, -1, -1, 13, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "other_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "buffer_", "LJavaUtilCollection;", .constantValue.asLong = 0, 0x0, -1, -1, 14, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObserver;LJavaUtilConcurrentCallable;LIoReactivexObservableSource;", "(Lio/reactivex/Observer<-TU;>;Ljava/util/concurrent/Callable<TU;>;Lio/reactivex/ObservableSource<TB;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "accept", "LIoReactivexObserver;LJavaUtilCollection;", "(Lio/reactivex/Observer<-TU;>;TU;)V", "Ljava/util/concurrent/Callable<TU;>;", "Lio/reactivex/ObservableSource<TB;>;", "TU;", "LIoReactivexInternalOperatorsObservableObservableBufferExactBoundary;", "<T:Ljava/lang/Object;U::Ljava/util/Collection<-TT;>;B:Ljava/lang/Object;>Lio/reactivex/internal/observers/QueueDrainObserver<TT;TU;TU;>;Lio/reactivex/Observer<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver = { "BufferExactBoundaryObserver", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 9, 5, 15, -1, -1, 16, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver;
}

@end

void IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_initWithIoReactivexObserver_withJavaUtilConcurrentCallable_withIoReactivexObservableSource_(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver *self, id<IoReactivexObserver> actual, id<JavaUtilConcurrentCallable> bufferSupplier, id<IoReactivexObservableSource> boundary) {
  IoReactivexInternalObserversQueueDrainObserver_initWithIoReactivexObserver_withIoReactivexInternalFuseableSimplePlainQueue_(self, actual, create_IoReactivexInternalQueueMpscLinkedQueue_init());
  JreStrongAssign(&self->bufferSupplier_, bufferSupplier);
  JreStrongAssign(&self->boundary_, boundary);
}

IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver *new_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_initWithIoReactivexObserver_withJavaUtilConcurrentCallable_withIoReactivexObservableSource_(id<IoReactivexObserver> actual, id<JavaUtilConcurrentCallable> bufferSupplier, id<IoReactivexObservableSource> boundary) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver, initWithIoReactivexObserver_withJavaUtilConcurrentCallable_withIoReactivexObservableSource_, actual, bufferSupplier, boundary)
}

IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver *create_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_initWithIoReactivexObserver_withJavaUtilConcurrentCallable_withIoReactivexObservableSource_(id<IoReactivexObserver> actual, id<JavaUtilConcurrentCallable> bufferSupplier, id<IoReactivexObservableSource> boundary) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver, initWithIoReactivexObserver_withJavaUtilConcurrentCallable_withIoReactivexObservableSource_, actual, bufferSupplier, boundary)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver)

@implementation IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver:(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver *)parent {
  IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver_initWithIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_(self, parent);
  return self;
}

- (void)onNextWithId:(id)t {
  [((IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver *) nil_chk(parent_)) next];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  [((IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver *) nil_chk(parent_)) onErrorWithJavaLangThrowable:t];
}

- (void)onComplete {
  [((IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver *) nil_chk(parent_)) onComplete];
}

- (void)dealloc {
  RELEASE_(parent_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, 4, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver:);
  methods[1].selector = @selector(onNextWithId:);
  methods[2].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[3].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "parent_", "LIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 7, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver;", "(Lio/reactivex/internal/operators/observable/ObservableBufferExactBoundary$BufferExactBoundaryObserver<TT;TU;TB;>;)V", "onNext", "LNSObject;", "(TB;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/internal/operators/observable/ObservableBufferExactBoundary$BufferExactBoundaryObserver<TT;TU;TB;>;", "LIoReactivexInternalOperatorsObservableObservableBufferExactBoundary;", "<T:Ljava/lang/Object;U::Ljava/util/Collection<-TT;>;B:Ljava/lang/Object;>Lio/reactivex/observers/DisposableObserver<TB;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver = { "BufferBoundaryObserver", "io.reactivex.internal.operators.observable", ptrTable, methods, fields, 7, 0x18, 4, 1, 8, -1, -1, 9, -1 };
  return &_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver;
}

@end

void IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver_initWithIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver *self, IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver *parent) {
  IoReactivexObserversDisposableObserver_init(self);
  JreStrongAssign(&self->parent_, parent);
}

IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver *new_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver_initWithIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver *parent) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver, initWithIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_, parent)
}

IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver *create_IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver_initWithIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver *parent) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver, initWithIoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferExactBoundaryObserver_, parent)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsObservableObservableBufferExactBoundary_BufferBoundaryObserver)
