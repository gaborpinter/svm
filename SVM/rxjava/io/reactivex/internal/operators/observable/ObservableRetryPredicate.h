//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/observable/ObservableRetryPredicate.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableRetryPredicate")
#ifdef RESTRICT_IoReactivexInternalOperatorsObservableObservableRetryPredicate
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableRetryPredicate 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableRetryPredicate 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsObservableObservableRetryPredicate

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableRetryPredicate_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableRetryPredicate || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableRetryPredicate))
#define IoReactivexInternalOperatorsObservableObservableRetryPredicate_

#define RESTRICT_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream 1
#include "io/reactivex/internal/operators/observable/AbstractObservableWithUpstream.h"

@class IoReactivexObservable;
@protocol IoReactivexFunctionsPredicate;
@protocol IoReactivexObservableSource;
@protocol IoReactivexObserver;

@interface IoReactivexInternalOperatorsObservableObservableRetryPredicate : IoReactivexInternalOperatorsObservableAbstractObservableWithUpstream {
 @public
  id<IoReactivexFunctionsPredicate> predicate_;
  jlong count_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObservable:(IoReactivexObservable *)source
                                               withLong:(jlong)count
                      withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)predicate;

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableRetryPredicate)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableRetryPredicate, predicate_, id<IoReactivexFunctionsPredicate>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableRetryPredicate_initWithIoReactivexObservable_withLong_withIoReactivexFunctionsPredicate_(IoReactivexInternalOperatorsObservableObservableRetryPredicate *self, IoReactivexObservable *source, jlong count, id<IoReactivexFunctionsPredicate> predicate);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableRetryPredicate *new_IoReactivexInternalOperatorsObservableObservableRetryPredicate_initWithIoReactivexObservable_withLong_withIoReactivexFunctionsPredicate_(IoReactivexObservable *source, jlong count, id<IoReactivexFunctionsPredicate> predicate) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableRetryPredicate *create_IoReactivexInternalOperatorsObservableObservableRetryPredicate_initWithIoReactivexObservable_withLong_withIoReactivexFunctionsPredicate_(IoReactivexObservable *source, jlong count, id<IoReactivexFunctionsPredicate> predicate);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableRetryPredicate)

#endif

#if !defined (IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableRetryPredicate || defined(INCLUDE_IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver))
#define IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicInteger 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicInteger 1
#include "java/util/concurrent/atomic/AtomicInteger.h"

#define RESTRICT_IoReactivexObserver 1
#define INCLUDE_IoReactivexObserver 1
#include "io/reactivex/Observer.h"

@class IoReactivexInternalDisposablesSequentialDisposable;
@class JavaLangThrowable;
@protocol IoReactivexDisposablesDisposable;
@protocol IoReactivexFunctionsPredicate;
@protocol IoReactivexObservableSource;

@interface IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver : JavaUtilConcurrentAtomicAtomicInteger < IoReactivexObserver > {
 @public
  id<IoReactivexObserver> downstream_;
  IoReactivexInternalDisposablesSequentialDisposable *upstream_;
  id<IoReactivexObservableSource> source_;
  id<IoReactivexFunctionsPredicate> predicate_;
  jlong remaining_;
}

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t;

- (void)onNextWithId:(id)t;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)actual
                                             withLong:(jlong)count
                    withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)predicate
withIoReactivexInternalDisposablesSequentialDisposable:(IoReactivexInternalDisposablesSequentialDisposable *)sa
                      withIoReactivexObservableSource:(id<IoReactivexObservableSource>)source;

- (void)subscribeNext;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver, downstream_, id<IoReactivexObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver, upstream_, IoReactivexInternalDisposablesSequentialDisposable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver, source_, id<IoReactivexObservableSource>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver, predicate_, id<IoReactivexFunctionsPredicate>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver_initWithIoReactivexObserver_withLong_withIoReactivexFunctionsPredicate_withIoReactivexInternalDisposablesSequentialDisposable_withIoReactivexObservableSource_(IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver *self, id<IoReactivexObserver> actual, jlong count, id<IoReactivexFunctionsPredicate> predicate, IoReactivexInternalDisposablesSequentialDisposable *sa, id<IoReactivexObservableSource> source);

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver *new_IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver_initWithIoReactivexObserver_withLong_withIoReactivexFunctionsPredicate_withIoReactivexInternalDisposablesSequentialDisposable_withIoReactivexObservableSource_(id<IoReactivexObserver> actual, jlong count, id<IoReactivexFunctionsPredicate> predicate, IoReactivexInternalDisposablesSequentialDisposable *sa, id<IoReactivexObservableSource> source) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver *create_IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver_initWithIoReactivexObserver_withLong_withIoReactivexFunctionsPredicate_withIoReactivexInternalDisposablesSequentialDisposable_withIoReactivexObservableSource_(id<IoReactivexObserver> actual, jlong count, id<IoReactivexFunctionsPredicate> predicate, IoReactivexInternalDisposablesSequentialDisposable *sa, id<IoReactivexObservableSource> source);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsObservableObservableRetryPredicate_RepeatObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsObservableObservableRetryPredicate")
