//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/completable/CompletableFromObservable.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromObservable")
#ifdef RESTRICT_IoReactivexInternalOperatorsCompletableCompletableFromObservable
#define INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromObservable 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromObservable 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsCompletableCompletableFromObservable

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsCompletableCompletableFromObservable_) && (INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromObservable || defined(INCLUDE_IoReactivexInternalOperatorsCompletableCompletableFromObservable))
#define IoReactivexInternalOperatorsCompletableCompletableFromObservable_

#define RESTRICT_IoReactivexCompletable 1
#define INCLUDE_IoReactivexCompletable 1
#include "io/reactivex/Completable.h"

@protocol IoReactivexCompletableObserver;
@protocol IoReactivexObservableSource;

@interface IoReactivexInternalOperatorsCompletableCompletableFromObservable : IoReactivexCompletable {
 @public
  id<IoReactivexObservableSource> observable_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexObservableSource:(id<IoReactivexObservableSource>)observable;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsCompletableCompletableFromObservable)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableFromObservable, observable_, id<IoReactivexObservableSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsCompletableCompletableFromObservable_initWithIoReactivexObservableSource_(IoReactivexInternalOperatorsCompletableCompletableFromObservable *self, id<IoReactivexObservableSource> observable);

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableFromObservable *new_IoReactivexInternalOperatorsCompletableCompletableFromObservable_initWithIoReactivexObservableSource_(id<IoReactivexObservableSource> observable) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableFromObservable *create_IoReactivexInternalOperatorsCompletableCompletableFromObservable_initWithIoReactivexObservableSource_(id<IoReactivexObservableSource> observable);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsCompletableCompletableFromObservable)

#endif

#if !defined (IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromObservable || defined(INCLUDE_IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver))
#define IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver_

#define RESTRICT_IoReactivexObserver 1
#define INCLUDE_IoReactivexObserver 1
#include "io/reactivex/Observer.h"

@class JavaLangThrowable;
@protocol IoReactivexCompletableObserver;
@protocol IoReactivexDisposablesDisposable;

@interface IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver : NSObject < IoReactivexObserver > {
 @public
  id<IoReactivexCompletableObserver> co_;
}

#pragma mark Public

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onNextWithId:(id)value;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)co;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver, co_, id<IoReactivexCompletableObserver>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver_initWithIoReactivexCompletableObserver_(IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver *self, id<IoReactivexCompletableObserver> co);

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver *new_IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver_initWithIoReactivexCompletableObserver_(id<IoReactivexCompletableObserver> co) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver *create_IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver_initWithIoReactivexCompletableObserver_(id<IoReactivexCompletableObserver> co);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsCompletableCompletableFromObservable_CompletableFromObservableObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromObservable")
