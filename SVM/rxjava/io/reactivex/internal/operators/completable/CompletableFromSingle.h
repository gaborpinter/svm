//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/completable/CompletableFromSingle.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromSingle")
#ifdef RESTRICT_IoReactivexInternalOperatorsCompletableCompletableFromSingle
#define INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromSingle 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromSingle 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsCompletableCompletableFromSingle

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsCompletableCompletableFromSingle_) && (INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromSingle || defined(INCLUDE_IoReactivexInternalOperatorsCompletableCompletableFromSingle))
#define IoReactivexInternalOperatorsCompletableCompletableFromSingle_

#define RESTRICT_IoReactivexCompletable 1
#define INCLUDE_IoReactivexCompletable 1
#include "io/reactivex/Completable.h"

@protocol IoReactivexCompletableObserver;
@protocol IoReactivexSingleSource;

@interface IoReactivexInternalOperatorsCompletableCompletableFromSingle : IoReactivexCompletable {
 @public
  id<IoReactivexSingleSource> single_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexSingleSource:(id<IoReactivexSingleSource>)single;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsCompletableCompletableFromSingle)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableFromSingle, single_, id<IoReactivexSingleSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsCompletableCompletableFromSingle_initWithIoReactivexSingleSource_(IoReactivexInternalOperatorsCompletableCompletableFromSingle *self, id<IoReactivexSingleSource> single);

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableFromSingle *new_IoReactivexInternalOperatorsCompletableCompletableFromSingle_initWithIoReactivexSingleSource_(id<IoReactivexSingleSource> single) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableFromSingle *create_IoReactivexInternalOperatorsCompletableCompletableFromSingle_initWithIoReactivexSingleSource_(id<IoReactivexSingleSource> single);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsCompletableCompletableFromSingle)

#endif

#if !defined (IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromSingle || defined(INCLUDE_IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver))
#define IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver_

#define RESTRICT_IoReactivexSingleObserver 1
#define INCLUDE_IoReactivexSingleObserver 1
#include "io/reactivex/SingleObserver.h"

@class JavaLangThrowable;
@protocol IoReactivexCompletableObserver;
@protocol IoReactivexDisposablesDisposable;

@interface IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver : NSObject < IoReactivexSingleObserver > {
 @public
  id<IoReactivexCompletableObserver> co_;
}

#pragma mark Public

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (void)onSuccessWithId:(id)value;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)co;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver, co_, id<IoReactivexCompletableObserver>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver_initWithIoReactivexCompletableObserver_(IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver *self, id<IoReactivexCompletableObserver> co);

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver *new_IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver_initWithIoReactivexCompletableObserver_(id<IoReactivexCompletableObserver> co) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver *create_IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver_initWithIoReactivexCompletableObserver_(id<IoReactivexCompletableObserver> co);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsCompletableCompletableFromSingle_CompletableFromSingleObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableFromSingle")
