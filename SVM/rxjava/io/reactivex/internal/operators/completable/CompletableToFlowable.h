//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/completable/CompletableToFlowable.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToFlowable")
#ifdef RESTRICT_IoReactivexInternalOperatorsCompletableCompletableToFlowable
#define INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToFlowable 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToFlowable 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsCompletableCompletableToFlowable

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsCompletableCompletableToFlowable_) && (INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToFlowable || defined(INCLUDE_IoReactivexInternalOperatorsCompletableCompletableToFlowable))
#define IoReactivexInternalOperatorsCompletableCompletableToFlowable_

#define RESTRICT_IoReactivexFlowable 1
#define INCLUDE_IoReactivexFlowable 1
#include "io/reactivex/Flowable.h"

@protocol IoReactivexCompletableSource;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsCompletableCompletableToFlowable : IoReactivexFlowable {
 @public
  id<IoReactivexCompletableSource> source_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexCompletableSource:(id<IoReactivexCompletableSource>)source;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsCompletableCompletableToFlowable)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableToFlowable, source_, id<IoReactivexCompletableSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsCompletableCompletableToFlowable_initWithIoReactivexCompletableSource_(IoReactivexInternalOperatorsCompletableCompletableToFlowable *self, id<IoReactivexCompletableSource> source);

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableToFlowable *new_IoReactivexInternalOperatorsCompletableCompletableToFlowable_initWithIoReactivexCompletableSource_(id<IoReactivexCompletableSource> source) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableToFlowable *create_IoReactivexInternalOperatorsCompletableCompletableToFlowable_initWithIoReactivexCompletableSource_(id<IoReactivexCompletableSource> source);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsCompletableCompletableToFlowable)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToFlowable")
