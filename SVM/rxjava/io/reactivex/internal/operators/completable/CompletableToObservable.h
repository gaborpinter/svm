//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/completable/CompletableToObservable.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToObservable")
#ifdef RESTRICT_IoReactivexInternalOperatorsCompletableCompletableToObservable
#define INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToObservable 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToObservable 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsCompletableCompletableToObservable

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsCompletableCompletableToObservable_) && (INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToObservable || defined(INCLUDE_IoReactivexInternalOperatorsCompletableCompletableToObservable))
#define IoReactivexInternalOperatorsCompletableCompletableToObservable_

#define RESTRICT_IoReactivexObservable 1
#define INCLUDE_IoReactivexObservable 1
#include "io/reactivex/Observable.h"

@protocol IoReactivexCompletableSource;
@protocol IoReactivexObserver;

@interface IoReactivexInternalOperatorsCompletableCompletableToObservable : IoReactivexObservable {
 @public
  id<IoReactivexCompletableSource> source_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexCompletableSource:(id<IoReactivexCompletableSource>)source;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsCompletableCompletableToObservable)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableToObservable, source_, id<IoReactivexCompletableSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsCompletableCompletableToObservable_initWithIoReactivexCompletableSource_(IoReactivexInternalOperatorsCompletableCompletableToObservable *self, id<IoReactivexCompletableSource> source);

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableToObservable *new_IoReactivexInternalOperatorsCompletableCompletableToObservable_initWithIoReactivexCompletableSource_(id<IoReactivexCompletableSource> source) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableToObservable *create_IoReactivexInternalOperatorsCompletableCompletableToObservable_initWithIoReactivexCompletableSource_(id<IoReactivexCompletableSource> source);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsCompletableCompletableToObservable)

#endif

#if !defined (IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToObservable || defined(INCLUDE_IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver))
#define IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver_

#define RESTRICT_IoReactivexInternalObserversBasicQueueDisposable 1
#define INCLUDE_IoReactivexInternalObserversBasicQueueDisposable 1
#include "io/reactivex/internal/observers/BasicQueueDisposable.h"

#define RESTRICT_IoReactivexCompletableObserver 1
#define INCLUDE_IoReactivexCompletableObserver 1
#include "io/reactivex/CompletableObserver.h"

@class JavaLangThrowable;
@class JavaLangVoid;
@protocol IoReactivexDisposablesDisposable;
@protocol IoReactivexObserver;

@interface IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver : IoReactivexInternalObserversBasicQueueDisposable < IoReactivexCompletableObserver > {
 @public
  id<IoReactivexObserver> observer_;
  id<IoReactivexDisposablesDisposable> upstream_;
}

#pragma mark Public

- (void)clear;

- (void)dispose;

- (jboolean)isDisposed;

- (jboolean)isEmpty;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (JavaLangVoid *)poll;

- (jint)requestFusionWithInt:(jint)mode;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver, observer_, id<IoReactivexObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver, upstream_, id<IoReactivexDisposablesDisposable>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver_initWithIoReactivexObserver_(IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver *self, id<IoReactivexObserver> observer);

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver *new_IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver_initWithIoReactivexObserver_(id<IoReactivexObserver> observer) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver *create_IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver_initWithIoReactivexObserver_(id<IoReactivexObserver> observer);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsCompletableCompletableToObservable_ObserverCompletableObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableToObservable")
