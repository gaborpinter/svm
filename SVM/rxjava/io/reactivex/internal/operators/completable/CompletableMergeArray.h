//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/completable/CompletableMergeArray.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableMergeArray")
#ifdef RESTRICT_IoReactivexInternalOperatorsCompletableCompletableMergeArray
#define INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableMergeArray 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableMergeArray 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsCompletableCompletableMergeArray

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsCompletableCompletableMergeArray_) && (INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableMergeArray || defined(INCLUDE_IoReactivexInternalOperatorsCompletableCompletableMergeArray))
#define IoReactivexInternalOperatorsCompletableCompletableMergeArray_

#define RESTRICT_IoReactivexCompletable 1
#define INCLUDE_IoReactivexCompletable 1
#include "io/reactivex/Completable.h"

@class IOSObjectArray;
@protocol IoReactivexCompletableObserver;

@interface IoReactivexInternalOperatorsCompletableCompletableMergeArray : IoReactivexCompletable {
 @public
  IOSObjectArray *sources_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexCompletableSourceArray:(IOSObjectArray *)sources;

- (void)subscribeActualWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsCompletableCompletableMergeArray)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableMergeArray, sources_, IOSObjectArray *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsCompletableCompletableMergeArray_initWithIoReactivexCompletableSourceArray_(IoReactivexInternalOperatorsCompletableCompletableMergeArray *self, IOSObjectArray *sources);

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableMergeArray *new_IoReactivexInternalOperatorsCompletableCompletableMergeArray_initWithIoReactivexCompletableSourceArray_(IOSObjectArray *sources) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableMergeArray *create_IoReactivexInternalOperatorsCompletableCompletableMergeArray_initWithIoReactivexCompletableSourceArray_(IOSObjectArray *sources);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsCompletableCompletableMergeArray)

#endif

#if !defined (IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableMergeArray || defined(INCLUDE_IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver))
#define IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicInteger 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicInteger 1
#include "java/util/concurrent/atomic/AtomicInteger.h"

#define RESTRICT_IoReactivexCompletableObserver 1
#define INCLUDE_IoReactivexCompletableObserver 1
#include "io/reactivex/CompletableObserver.h"

@class IoReactivexDisposablesCompositeDisposable;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicBoolean;
@protocol IoReactivexDisposablesDisposable;

@interface IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver : JavaUtilConcurrentAtomicAtomicInteger < IoReactivexCompletableObserver > {
 @public
  id<IoReactivexCompletableObserver> downstream_;
  JavaUtilConcurrentAtomicAtomicBoolean *once_;
  IoReactivexDisposablesCompositeDisposable *set_;
}

#pragma mark Public

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)actual
                       withJavaUtilConcurrentAtomicAtomicBoolean:(JavaUtilConcurrentAtomicAtomicBoolean *)once
                   withIoReactivexDisposablesCompositeDisposable:(IoReactivexDisposablesCompositeDisposable *)set
                                                         withInt:(jint)n;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithInt:(jint)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver, downstream_, id<IoReactivexCompletableObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver, once_, JavaUtilConcurrentAtomicAtomicBoolean *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver, set_, IoReactivexDisposablesCompositeDisposable *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver_initWithIoReactivexCompletableObserver_withJavaUtilConcurrentAtomicAtomicBoolean_withIoReactivexDisposablesCompositeDisposable_withInt_(IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver *self, id<IoReactivexCompletableObserver> actual, JavaUtilConcurrentAtomicAtomicBoolean *once, IoReactivexDisposablesCompositeDisposable *set, jint n);

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver *new_IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver_initWithIoReactivexCompletableObserver_withJavaUtilConcurrentAtomicAtomicBoolean_withIoReactivexDisposablesCompositeDisposable_withInt_(id<IoReactivexCompletableObserver> actual, JavaUtilConcurrentAtomicAtomicBoolean *once, IoReactivexDisposablesCompositeDisposable *set, jint n) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver *create_IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver_initWithIoReactivexCompletableObserver_withJavaUtilConcurrentAtomicAtomicBoolean_withIoReactivexDisposablesCompositeDisposable_withInt_(id<IoReactivexCompletableObserver> actual, JavaUtilConcurrentAtomicAtomicBoolean *once, IoReactivexDisposablesCompositeDisposable *set, jint n);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsCompletableCompletableMergeArray_InnerCompletableObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsCompletableCompletableMergeArray")
