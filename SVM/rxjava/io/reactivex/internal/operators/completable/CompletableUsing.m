//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/completable/CompletableUsing.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "io/reactivex/Completable.h"
#include "io/reactivex/CompletableObserver.h"
#include "io/reactivex/CompletableSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/CompositeException.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Consumer.h"
#include "io/reactivex/functions/Function.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/disposables/EmptyDisposable.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/operators/completable/CompletableUsing.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/Callable.h"
#include "java/util/concurrent/atomic/AtomicReference.h"

inline jlong IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver_serialVersionUID -674404550052917487LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsCompletableCompletableUsing

- (instancetype __nonnull)initWithJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)resourceSupplier
                            withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)completableFunction
                            withIoReactivexFunctionsConsumer:(id<IoReactivexFunctionsConsumer>)disposer
                                                 withBoolean:(jboolean)eager {
  IoReactivexInternalOperatorsCompletableCompletableUsing_initWithJavaUtilConcurrentCallable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsConsumer_withBoolean_(self, resourceSupplier, completableFunction, disposer, eager);
  return self;
}

- (void)subscribeActualWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)observer {
  id resource;
  @try {
    resource = [((id<JavaUtilConcurrentCallable>) nil_chk(resourceSupplier_)) call];
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    IoReactivexInternalDisposablesEmptyDisposable_errorWithJavaLangThrowable_withIoReactivexCompletableObserver_(ex, observer);
    return;
  }
  id<IoReactivexCompletableSource> source;
  @try {
    source = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(completableFunction_)) applyWithId:resource], @"The completableFunction returned a null CompletableSource");
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    if (eager_) {
      @try {
        [((id<IoReactivexFunctionsConsumer>) nil_chk(disposer_)) acceptWithId:resource];
      }
      @catch (JavaLangThrowable *exc) {
        IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(exc);
        IoReactivexInternalDisposablesEmptyDisposable_errorWithJavaLangThrowable_withIoReactivexCompletableObserver_(create_IoReactivexExceptionsCompositeException_initWithJavaLangThrowableArray_([IOSObjectArray arrayWithObjects:(id[]){ ex, exc } count:2 type:JavaLangThrowable_class_()]), observer);
        return;
      }
    }
    IoReactivexInternalDisposablesEmptyDisposable_errorWithJavaLangThrowable_withIoReactivexCompletableObserver_(ex, observer);
    if (!eager_) {
      @try {
        [((id<IoReactivexFunctionsConsumer>) nil_chk(disposer_)) acceptWithId:resource];
      }
      @catch (JavaLangThrowable *exc) {
        IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(exc);
        IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(exc);
      }
    }
    return;
  }
  [((id<IoReactivexCompletableSource>) nil_chk(source)) subscribeWithIoReactivexCompletableObserver:create_IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver_initWithIoReactivexCompletableObserver_withId_withIoReactivexFunctionsConsumer_withBoolean_(observer, resource, disposer_, eager_)];
}

- (void)dealloc {
  RELEASE_(resourceSupplier_);
  RELEASE_(completableFunction_);
  RELEASE_(disposer_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaUtilConcurrentCallable:withIoReactivexFunctionsFunction:withIoReactivexFunctionsConsumer:withBoolean:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexCompletableObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "resourceSupplier_", "LJavaUtilConcurrentCallable;", .constantValue.asLong = 0, 0x10, -1, -1, 4, -1 },
    { "completableFunction_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "disposer_", "LIoReactivexFunctionsConsumer;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
    { "eager_", "Z", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LJavaUtilConcurrentCallable;LIoReactivexFunctionsFunction;LIoReactivexFunctionsConsumer;Z", "(Ljava/util/concurrent/Callable<TR;>;Lio/reactivex/functions/Function<-TR;+Lio/reactivex/CompletableSource;>;Lio/reactivex/functions/Consumer<-TR;>;Z)V", "subscribeActual", "LIoReactivexCompletableObserver;", "Ljava/util/concurrent/Callable<TR;>;", "Lio/reactivex/functions/Function<-TR;+Lio/reactivex/CompletableSource;>;", "Lio/reactivex/functions/Consumer<-TR;>;", "LIoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver;", "<R:Ljava/lang/Object;>Lio/reactivex/Completable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsCompletableCompletableUsing = { "CompletableUsing", "io.reactivex.internal.operators.completable", ptrTable, methods, fields, 7, 0x11, 2, 4, -1, 7, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsCompletableCompletableUsing;
}

@end

void IoReactivexInternalOperatorsCompletableCompletableUsing_initWithJavaUtilConcurrentCallable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsConsumer_withBoolean_(IoReactivexInternalOperatorsCompletableCompletableUsing *self, id<JavaUtilConcurrentCallable> resourceSupplier, id<IoReactivexFunctionsFunction> completableFunction, id<IoReactivexFunctionsConsumer> disposer, jboolean eager) {
  IoReactivexCompletable_init(self);
  JreStrongAssign(&self->resourceSupplier_, resourceSupplier);
  JreStrongAssign(&self->completableFunction_, completableFunction);
  JreStrongAssign(&self->disposer_, disposer);
  self->eager_ = eager;
}

IoReactivexInternalOperatorsCompletableCompletableUsing *new_IoReactivexInternalOperatorsCompletableCompletableUsing_initWithJavaUtilConcurrentCallable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsConsumer_withBoolean_(id<JavaUtilConcurrentCallable> resourceSupplier, id<IoReactivexFunctionsFunction> completableFunction, id<IoReactivexFunctionsConsumer> disposer, jboolean eager) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsCompletableCompletableUsing, initWithJavaUtilConcurrentCallable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsConsumer_withBoolean_, resourceSupplier, completableFunction, disposer, eager)
}

IoReactivexInternalOperatorsCompletableCompletableUsing *create_IoReactivexInternalOperatorsCompletableCompletableUsing_initWithJavaUtilConcurrentCallable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsConsumer_withBoolean_(id<JavaUtilConcurrentCallable> resourceSupplier, id<IoReactivexFunctionsFunction> completableFunction, id<IoReactivexFunctionsConsumer> disposer, jboolean eager) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsCompletableCompletableUsing, initWithJavaUtilConcurrentCallable_withIoReactivexFunctionsFunction_withIoReactivexFunctionsConsumer_withBoolean_, resourceSupplier, completableFunction, disposer, eager)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsCompletableCompletableUsing)

@implementation IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver

- (instancetype __nonnull)initWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)actual
                                                          withId:(id)resource
                                withIoReactivexFunctionsConsumer:(id<IoReactivexFunctionsConsumer>)disposer
                                                     withBoolean:(jboolean)eager {
  IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver_initWithIoReactivexCompletableObserver_withId_withIoReactivexFunctionsConsumer_withBoolean_(self, actual, resource, disposer, eager);
  return self;
}

- (void)dispose {
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
  JreStrongAssign(&upstream_, JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED));
  [self disposeResourceAfter];
}

- (void)disposeResourceAfter {
  id resource = [self getAndSetWithId:self];
  if (resource != self) {
    @try {
      [((id<IoReactivexFunctionsConsumer>) nil_chk(disposer_)) acceptWithId:resource];
    }
    @catch (JavaLangThrowable *ex) {
      IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
      IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
    }
  }
}

- (jboolean)isDisposed {
  return [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) isDisposed];
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  JreStrongAssign(&upstream_, JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED));
  if (eager_) {
    id resource = [self getAndSetWithId:self];
    if (resource != self) {
      @try {
        [((id<IoReactivexFunctionsConsumer>) nil_chk(disposer_)) acceptWithId:resource];
      }
      @catch (JavaLangThrowable *ex) {
        IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
        e = create_IoReactivexExceptionsCompositeException_initWithJavaLangThrowableArray_([IOSObjectArray arrayWithObjects:(id[]){ e, ex } count:2 type:JavaLangThrowable_class_()]);
      }
    }
    else {
      return;
    }
  }
  [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
  if (!eager_) {
    [self disposeResourceAfter];
  }
}

- (void)onComplete {
  JreStrongAssign(&upstream_, JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED));
  if (eager_) {
    id resource = [self getAndSetWithId:self];
    if (resource != self) {
      @try {
        [((id<IoReactivexFunctionsConsumer>) nil_chk(disposer_)) acceptWithId:resource];
      }
      @catch (JavaLangThrowable *ex) {
        IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
        [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:ex];
        return;
      }
    }
    else {
      return;
    }
  }
  [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onComplete];
  if (!eager_) {
    [self disposeResourceAfter];
  }
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(disposer_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexCompletableObserver:withId:withIoReactivexFunctionsConsumer:withBoolean:);
  methods[1].selector = @selector(dispose);
  methods[2].selector = @selector(disposeResourceAfter);
  methods[3].selector = @selector(isDisposed);
  methods[4].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[5].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[6].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LIoReactivexCompletableObserver;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "disposer_", "LIoReactivexFunctionsConsumer;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
    { "eager_", "Z", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexCompletableObserver;LNSObject;LIoReactivexFunctionsConsumer;Z", "(Lio/reactivex/CompletableObserver;TR;Lio/reactivex/functions/Consumer<-TR;>;Z)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onError", "LJavaLangThrowable;", "Lio/reactivex/functions/Consumer<-TR;>;", "LIoReactivexInternalOperatorsCompletableCompletableUsing;", "<R:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicReference<Ljava/lang/Object;>;Lio/reactivex/CompletableObserver;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver = { "UsingObserver", "io.reactivex.internal.operators.completable", ptrTable, methods, fields, 7, 0x18, 7, 5, 7, -1, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver;
}

@end

void IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver_initWithIoReactivexCompletableObserver_withId_withIoReactivexFunctionsConsumer_withBoolean_(IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver *self, id<IoReactivexCompletableObserver> actual, id resource, id<IoReactivexFunctionsConsumer> disposer, jboolean eager) {
  JavaUtilConcurrentAtomicAtomicReference_initWithId_(self, resource);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->disposer_, disposer);
  self->eager_ = eager;
}

IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver *new_IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver_initWithIoReactivexCompletableObserver_withId_withIoReactivexFunctionsConsumer_withBoolean_(id<IoReactivexCompletableObserver> actual, id resource, id<IoReactivexFunctionsConsumer> disposer, jboolean eager) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver, initWithIoReactivexCompletableObserver_withId_withIoReactivexFunctionsConsumer_withBoolean_, actual, resource, disposer, eager)
}

IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver *create_IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver_initWithIoReactivexCompletableObserver_withId_withIoReactivexFunctionsConsumer_withBoolean_(id<IoReactivexCompletableObserver> actual, id resource, id<IoReactivexFunctionsConsumer> disposer, jboolean eager) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver, initWithIoReactivexCompletableObserver_withId_withIoReactivexFunctionsConsumer_withBoolean_, actual, resource, disposer, eager)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsCompletableCompletableUsing_UsingObserver)
