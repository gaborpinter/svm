//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/completable/CompletableToFlowable.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/CompletableSource.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/internal/observers/SubscriberCompletableObserver.h"
#include "io/reactivex/internal/operators/completable/CompletableToFlowable.h"
#include "org/reactivestreams/Subscriber.h"

@implementation IoReactivexInternalOperatorsCompletableCompletableToFlowable

- (instancetype __nonnull)initWithIoReactivexCompletableSource:(id<IoReactivexCompletableSource>)source {
  IoReactivexInternalOperatorsCompletableCompletableToFlowable_initWithIoReactivexCompletableSource_(self, source);
  return self;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  IoReactivexInternalObserversSubscriberCompletableObserver *os = create_IoReactivexInternalObserversSubscriberCompletableObserver_initWithOrgReactivestreamsSubscriber_(s);
  [((id<IoReactivexCompletableSource>) nil_chk(source_)) subscribeWithIoReactivexCompletableObserver:os];
}

- (void)dealloc {
  RELEASE_(source_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 1, 2, -1, 3, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexCompletableSource:);
  methods[1].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexCompletableSource;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexCompletableSource;", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "<T:Ljava/lang/Object;>Lio/reactivex/Flowable<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsCompletableCompletableToFlowable = { "CompletableToFlowable", "io.reactivex.internal.operators.completable", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, -1, -1, 4, -1 };
  return &_IoReactivexInternalOperatorsCompletableCompletableToFlowable;
}

@end

void IoReactivexInternalOperatorsCompletableCompletableToFlowable_initWithIoReactivexCompletableSource_(IoReactivexInternalOperatorsCompletableCompletableToFlowable *self, id<IoReactivexCompletableSource> source) {
  IoReactivexFlowable_init(self);
  JreStrongAssign(&self->source_, source);
}

IoReactivexInternalOperatorsCompletableCompletableToFlowable *new_IoReactivexInternalOperatorsCompletableCompletableToFlowable_initWithIoReactivexCompletableSource_(id<IoReactivexCompletableSource> source) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsCompletableCompletableToFlowable, initWithIoReactivexCompletableSource_, source)
}

IoReactivexInternalOperatorsCompletableCompletableToFlowable *create_IoReactivexInternalOperatorsCompletableCompletableToFlowable_initWithIoReactivexCompletableSource_(id<IoReactivexCompletableSource> source) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsCompletableCompletableToFlowable, initWithIoReactivexCompletableSource_, source)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsCompletableCompletableToFlowable)
