//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/completable/CompletableMaterialize.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Completable.h"
#include "io/reactivex/Single.h"
#include "io/reactivex/SingleObserver.h"
#include "io/reactivex/internal/operators/completable/CompletableMaterialize.h"
#include "io/reactivex/internal/operators/mixed/MaterializeSingleObserver.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation IoReactivexInternalOperatorsCompletableCompletableMaterialize

- (instancetype __nonnull)initWithIoReactivexCompletable:(IoReactivexCompletable *)source {
  IoReactivexInternalOperatorsCompletableCompletableMaterialize_initWithIoReactivexCompletable_(self, source);
  return self;
}

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer {
  [((IoReactivexCompletable *) nil_chk(source_)) subscribeWithIoReactivexCompletableObserver:create_IoReactivexInternalOperatorsMixedMaterializeSingleObserver_initWithIoReactivexSingleObserver_(observer)];
}

- (void)dealloc {
  RELEASE_(source_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 1, 2, -1, 3, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexCompletable:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexSingleObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexCompletable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexCompletable;", "subscribeActual", "LIoReactivexSingleObserver;", "(Lio/reactivex/SingleObserver<-Lio/reactivex/Notification<TT;>;>;)V", "<T:Ljava/lang/Object;>Lio/reactivex/Single<Lio/reactivex/Notification<TT;>;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsCompletableCompletableMaterialize = { "CompletableMaterialize", "io.reactivex.internal.operators.completable", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, -1, -1, 4, -1 };
  return &_IoReactivexInternalOperatorsCompletableCompletableMaterialize;
}

@end

void IoReactivexInternalOperatorsCompletableCompletableMaterialize_initWithIoReactivexCompletable_(IoReactivexInternalOperatorsCompletableCompletableMaterialize *self, IoReactivexCompletable *source) {
  IoReactivexSingle_init(self);
  JreStrongAssign(&self->source_, source);
}

IoReactivexInternalOperatorsCompletableCompletableMaterialize *new_IoReactivexInternalOperatorsCompletableCompletableMaterialize_initWithIoReactivexCompletable_(IoReactivexCompletable *source) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsCompletableCompletableMaterialize, initWithIoReactivexCompletable_, source)
}

IoReactivexInternalOperatorsCompletableCompletableMaterialize *create_IoReactivexInternalOperatorsCompletableCompletableMaterialize_initWithIoReactivexCompletable_(IoReactivexCompletable *source) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsCompletableCompletableMaterialize, initWithIoReactivexCompletable_, source)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsCompletableCompletableMaterialize)
