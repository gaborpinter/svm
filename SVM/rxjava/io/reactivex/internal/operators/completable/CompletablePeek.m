//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/completable/CompletablePeek.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "io/reactivex/Completable.h"
#include "io/reactivex/CompletableObserver.h"
#include "io/reactivex/CompletableSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/CompositeException.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Action.h"
#include "io/reactivex/functions/Consumer.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/disposables/EmptyDisposable.h"
#include "io/reactivex/internal/operators/completable/CompletablePeek.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Throwable.h"

@interface IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation () {
 @public
  IoReactivexInternalOperatorsCompletableCompletablePeek *this$0_;
}

@end

@implementation IoReactivexInternalOperatorsCompletableCompletablePeek

- (instancetype __nonnull)initWithIoReactivexCompletableSource:(id<IoReactivexCompletableSource>)source
                              withIoReactivexFunctionsConsumer:(id<IoReactivexFunctionsConsumer>)onSubscribe
                              withIoReactivexFunctionsConsumer:(id<IoReactivexFunctionsConsumer>)onError
                                withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onComplete
                                withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onTerminate
                                withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onAfterTerminate
                                withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onDispose {
  IoReactivexInternalOperatorsCompletableCompletablePeek_initWithIoReactivexCompletableSource_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_(self, source, onSubscribe, onError, onComplete, onTerminate, onAfterTerminate, onDispose);
  return self;
}

- (void)subscribeActualWithIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)observer {
  [((id<IoReactivexCompletableSource>) nil_chk(source_)) subscribeWithIoReactivexCompletableObserver:create_IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation_initWithIoReactivexInternalOperatorsCompletableCompletablePeek_withIoReactivexCompletableObserver_(self, observer)];
}

- (void)dealloc {
  RELEASE_(source_);
  RELEASE_(onSubscribe_);
  RELEASE_(onError_);
  RELEASE_(onComplete_);
  RELEASE_(onTerminate_);
  RELEASE_(onAfterTerminate_);
  RELEASE_(onDispose_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexCompletableSource:withIoReactivexFunctionsConsumer:withIoReactivexFunctionsConsumer:withIoReactivexFunctionsAction:withIoReactivexFunctionsAction:withIoReactivexFunctionsAction:withIoReactivexFunctionsAction:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexCompletableObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexCompletableSource;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "onSubscribe_", "LIoReactivexFunctionsConsumer;", .constantValue.asLong = 0, 0x10, -1, -1, 4, -1 },
    { "onError_", "LIoReactivexFunctionsConsumer;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "onComplete_", "LIoReactivexFunctionsAction;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "onTerminate_", "LIoReactivexFunctionsAction;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "onAfterTerminate_", "LIoReactivexFunctionsAction;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "onDispose_", "LIoReactivexFunctionsAction;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexCompletableSource;LIoReactivexFunctionsConsumer;LIoReactivexFunctionsConsumer;LIoReactivexFunctionsAction;LIoReactivexFunctionsAction;LIoReactivexFunctionsAction;LIoReactivexFunctionsAction;", "(Lio/reactivex/CompletableSource;Lio/reactivex/functions/Consumer<-Lio/reactivex/disposables/Disposable;>;Lio/reactivex/functions/Consumer<-Ljava/lang/Throwable;>;Lio/reactivex/functions/Action;Lio/reactivex/functions/Action;Lio/reactivex/functions/Action;Lio/reactivex/functions/Action;)V", "subscribeActual", "LIoReactivexCompletableObserver;", "Lio/reactivex/functions/Consumer<-Lio/reactivex/disposables/Disposable;>;", "Lio/reactivex/functions/Consumer<-Ljava/lang/Throwable;>;", "LIoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsCompletableCompletablePeek = { "CompletablePeek", "io.reactivex.internal.operators.completable", ptrTable, methods, fields, 7, 0x11, 2, 7, -1, 6, -1, -1, -1 };
  return &_IoReactivexInternalOperatorsCompletableCompletablePeek;
}

@end

void IoReactivexInternalOperatorsCompletableCompletablePeek_initWithIoReactivexCompletableSource_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_(IoReactivexInternalOperatorsCompletableCompletablePeek *self, id<IoReactivexCompletableSource> source, id<IoReactivexFunctionsConsumer> onSubscribe, id<IoReactivexFunctionsConsumer> onError, id<IoReactivexFunctionsAction> onComplete, id<IoReactivexFunctionsAction> onTerminate, id<IoReactivexFunctionsAction> onAfterTerminate, id<IoReactivexFunctionsAction> onDispose) {
  IoReactivexCompletable_init(self);
  JreStrongAssign(&self->source_, source);
  JreStrongAssign(&self->onSubscribe_, onSubscribe);
  JreStrongAssign(&self->onError_, onError);
  JreStrongAssign(&self->onComplete_, onComplete);
  JreStrongAssign(&self->onTerminate_, onTerminate);
  JreStrongAssign(&self->onAfterTerminate_, onAfterTerminate);
  JreStrongAssign(&self->onDispose_, onDispose);
}

IoReactivexInternalOperatorsCompletableCompletablePeek *new_IoReactivexInternalOperatorsCompletableCompletablePeek_initWithIoReactivexCompletableSource_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_(id<IoReactivexCompletableSource> source, id<IoReactivexFunctionsConsumer> onSubscribe, id<IoReactivexFunctionsConsumer> onError, id<IoReactivexFunctionsAction> onComplete, id<IoReactivexFunctionsAction> onTerminate, id<IoReactivexFunctionsAction> onAfterTerminate, id<IoReactivexFunctionsAction> onDispose) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsCompletableCompletablePeek, initWithIoReactivexCompletableSource_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_, source, onSubscribe, onError, onComplete, onTerminate, onAfterTerminate, onDispose)
}

IoReactivexInternalOperatorsCompletableCompletablePeek *create_IoReactivexInternalOperatorsCompletableCompletablePeek_initWithIoReactivexCompletableSource_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_(id<IoReactivexCompletableSource> source, id<IoReactivexFunctionsConsumer> onSubscribe, id<IoReactivexFunctionsConsumer> onError, id<IoReactivexFunctionsAction> onComplete, id<IoReactivexFunctionsAction> onTerminate, id<IoReactivexFunctionsAction> onAfterTerminate, id<IoReactivexFunctionsAction> onDispose) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsCompletableCompletablePeek, initWithIoReactivexCompletableSource_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsConsumer_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_withIoReactivexFunctionsAction_, source, onSubscribe, onError, onComplete, onTerminate, onAfterTerminate, onDispose)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsCompletableCompletablePeek)

@implementation IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsCompletableCompletablePeek:(IoReactivexInternalOperatorsCompletableCompletablePeek *)outer$
                                                      withIoReactivexCompletableObserver:(id<IoReactivexCompletableObserver>)downstream {
  IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation_initWithIoReactivexInternalOperatorsCompletableCompletablePeek_withIoReactivexCompletableObserver_(self, outer$, downstream);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  @try {
    [((id<IoReactivexFunctionsConsumer>) nil_chk(this$0_->onSubscribe_)) acceptWithId:d];
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    [((id<IoReactivexDisposablesDisposable>) nil_chk(d)) dispose];
    JreStrongAssign(&self->upstream_, JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED));
    IoReactivexInternalDisposablesEmptyDisposable_errorWithJavaLangThrowable_withIoReactivexCompletableObserver_(ex, downstream_);
    return;
  }
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  if (upstream_ == JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED)) {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(e);
    return;
  }
  @try {
    [((id<IoReactivexFunctionsConsumer>) nil_chk(this$0_->onError_)) acceptWithId:e];
    [((id<IoReactivexFunctionsAction>) nil_chk(this$0_->onTerminate_)) run];
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    e = create_IoReactivexExceptionsCompositeException_initWithJavaLangThrowableArray_([IOSObjectArray arrayWithObjects:(id[]){ e, ex } count:2 type:JavaLangThrowable_class_()]);
  }
  [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
  [self doAfter];
}

- (void)onComplete {
  if (upstream_ == JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED)) {
    return;
  }
  @try {
    [((id<IoReactivexFunctionsAction>) nil_chk(this$0_->onComplete_)) run];
    [((id<IoReactivexFunctionsAction>) nil_chk(this$0_->onTerminate_)) run];
  }
  @catch (JavaLangThrowable *e) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(e);
    [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
    return;
  }
  [((id<IoReactivexCompletableObserver>) nil_chk(downstream_)) onComplete];
  [self doAfter];
}

- (void)doAfter {
  @try {
    [((id<IoReactivexFunctionsAction>) nil_chk(this$0_->onAfterTerminate_)) run];
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
  }
}

- (void)dispose {
  @try {
    [((id<IoReactivexFunctionsAction>) nil_chk(this$0_->onDispose_)) run];
  }
  @catch (JavaLangThrowable *e) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(e);
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(e);
  }
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
}

- (jboolean)isDisposed {
  return [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) isDisposed];
}

- (void)dealloc {
  RELEASE_(this$0_);
  RELEASE_(downstream_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalOperatorsCompletableCompletablePeek:withIoReactivexCompletableObserver:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[3].selector = @selector(onComplete);
  methods[4].selector = @selector(doAfter);
  methods[5].selector = @selector(dispose);
  methods[6].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "this$0_", "LIoReactivexInternalOperatorsCompletableCompletablePeek;", .constantValue.asLong = 0, 0x1012, -1, -1, -1, -1 },
    { "downstream_", "LIoReactivexCompletableObserver;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexCompletableObserver;", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onError", "LJavaLangThrowable;", "LIoReactivexInternalOperatorsCompletableCompletablePeek;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation = { "CompletableObserverImplementation", "io.reactivex.internal.operators.completable", ptrTable, methods, fields, 7, 0x10, 7, 3, 5, -1, -1, -1, -1 };
  return &_IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation;
}

@end

void IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation_initWithIoReactivexInternalOperatorsCompletableCompletablePeek_withIoReactivexCompletableObserver_(IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation *self, IoReactivexInternalOperatorsCompletableCompletablePeek *outer$, id<IoReactivexCompletableObserver> downstream) {
  JreStrongAssign(&self->this$0_, outer$);
  NSObject_init(self);
  JreStrongAssign(&self->downstream_, downstream);
}

IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation *new_IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation_initWithIoReactivexInternalOperatorsCompletableCompletablePeek_withIoReactivexCompletableObserver_(IoReactivexInternalOperatorsCompletableCompletablePeek *outer$, id<IoReactivexCompletableObserver> downstream) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation, initWithIoReactivexInternalOperatorsCompletableCompletablePeek_withIoReactivexCompletableObserver_, outer$, downstream)
}

IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation *create_IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation_initWithIoReactivexInternalOperatorsCompletableCompletablePeek_withIoReactivexCompletableObserver_(IoReactivexInternalOperatorsCompletableCompletablePeek *outer$, id<IoReactivexCompletableObserver> downstream) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation, initWithIoReactivexInternalOperatorsCompletableCompletablePeek_withIoReactivexCompletableObserver_, outer$, downstream)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsCompletableCompletablePeek_CompletableObserverImplementation)
