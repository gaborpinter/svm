//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/mixed/ObservableConcatMapSingle.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Observable.h"
#include "io/reactivex/Observer.h"
#include "io/reactivex/SingleSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Function.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/fuseable/SimplePlainQueue.h"
#include "io/reactivex/internal/operators/mixed/ObservableConcatMapSingle.h"
#include "io/reactivex/internal/operators/mixed/ScalarXMapZHelper.h"
#include "io/reactivex/internal/queue/SpscLinkedArrayQueue.h"
#include "io/reactivex/internal/util/AtomicThrowable.h"
#include "io/reactivex/internal/util/ErrorMode.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"
#include "java/util/concurrent/atomic/AtomicReference.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_serialVersionUID -9140123220065488293LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver, serialVersionUID, jlong)

inline jlong IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver_serialVersionUID -3051469169682093892LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsMixedObservableConcatMapSingle

- (instancetype __nonnull)initWithIoReactivexObservable:(IoReactivexObservable *)source
                       withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)mapper
                   withIoReactivexInternalUtilErrorMode:(IoReactivexInternalUtilErrorMode *)errorMode
                                                withInt:(jint)prefetch {
  IoReactivexInternalOperatorsMixedObservableConcatMapSingle_initWithIoReactivexObservable_withIoReactivexFunctionsFunction_withIoReactivexInternalUtilErrorMode_withInt_(self, source, mapper, errorMode, prefetch);
  return self;
}

- (void)subscribeActualWithIoReactivexObserver:(id<IoReactivexObserver>)observer {
  if (!IoReactivexInternalOperatorsMixedScalarXMapZHelper_tryAsSingleWithId_withIoReactivexFunctionsFunction_withIoReactivexObserver_(source_, mapper_, observer)) {
    [((IoReactivexObservable *) nil_chk(source_)) subscribeWithIoReactivexObserver:create_IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withInt_withIoReactivexInternalUtilErrorMode_(observer, mapper_, prefetch_, errorMode_)];
  }
}

- (void)dealloc {
  RELEASE_(source_);
  RELEASE_(mapper_);
  RELEASE_(errorMode_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObservable:withIoReactivexFunctionsFunction:withIoReactivexInternalUtilErrorMode:withInt:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexObservable;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "mapper_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
    { "errorMode_", "LIoReactivexInternalUtilErrorMode;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "prefetch_", "I", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObservable;LIoReactivexFunctionsFunction;LIoReactivexInternalUtilErrorMode;I", "(Lio/reactivex/Observable<TT;>;Lio/reactivex/functions/Function<-TT;+Lio/reactivex/SingleSource<+TR;>;>;Lio/reactivex/internal/util/ErrorMode;I)V", "subscribeActual", "LIoReactivexObserver;", "(Lio/reactivex/Observer<-TR;>;)V", "Lio/reactivex/Observable<TT;>;", "Lio/reactivex/functions/Function<-TT;+Lio/reactivex/SingleSource<+TR;>;>;", "LIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver;", "<T:Ljava/lang/Object;R:Ljava/lang/Object;>Lio/reactivex/Observable<TR;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMixedObservableConcatMapSingle = { "ObservableConcatMapSingle", "io.reactivex.internal.operators.mixed", ptrTable, methods, fields, 7, 0x11, 2, 4, -1, 7, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsMixedObservableConcatMapSingle;
}

@end

void IoReactivexInternalOperatorsMixedObservableConcatMapSingle_initWithIoReactivexObservable_withIoReactivexFunctionsFunction_withIoReactivexInternalUtilErrorMode_withInt_(IoReactivexInternalOperatorsMixedObservableConcatMapSingle *self, IoReactivexObservable *source, id<IoReactivexFunctionsFunction> mapper, IoReactivexInternalUtilErrorMode *errorMode, jint prefetch) {
  IoReactivexObservable_init(self);
  JreStrongAssign(&self->source_, source);
  JreStrongAssign(&self->mapper_, mapper);
  JreStrongAssign(&self->errorMode_, errorMode);
  self->prefetch_ = prefetch;
}

IoReactivexInternalOperatorsMixedObservableConcatMapSingle *new_IoReactivexInternalOperatorsMixedObservableConcatMapSingle_initWithIoReactivexObservable_withIoReactivexFunctionsFunction_withIoReactivexInternalUtilErrorMode_withInt_(IoReactivexObservable *source, id<IoReactivexFunctionsFunction> mapper, IoReactivexInternalUtilErrorMode *errorMode, jint prefetch) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMixedObservableConcatMapSingle, initWithIoReactivexObservable_withIoReactivexFunctionsFunction_withIoReactivexInternalUtilErrorMode_withInt_, source, mapper, errorMode, prefetch)
}

IoReactivexInternalOperatorsMixedObservableConcatMapSingle *create_IoReactivexInternalOperatorsMixedObservableConcatMapSingle_initWithIoReactivexObservable_withIoReactivexFunctionsFunction_withIoReactivexInternalUtilErrorMode_withInt_(IoReactivexObservable *source, id<IoReactivexFunctionsFunction> mapper, IoReactivexInternalUtilErrorMode *errorMode, jint prefetch) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMixedObservableConcatMapSingle, initWithIoReactivexObservable_withIoReactivexFunctionsFunction_withIoReactivexInternalUtilErrorMode_withInt_, source, mapper, errorMode, prefetch)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMixedObservableConcatMapSingle)

@implementation IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver

+ (jint)STATE_INACTIVE {
  return IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_INACTIVE;
}

+ (jint)STATE_ACTIVE {
  return IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_ACTIVE;
}

+ (jint)STATE_RESULT_VALUE {
  return IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_RESULT_VALUE;
}

- (instancetype __nonnull)initWithIoReactivexObserver:(id<IoReactivexObserver>)downstream
                     withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)mapper
                                              withInt:(jint)prefetch
                 withIoReactivexInternalUtilErrorMode:(IoReactivexInternalUtilErrorMode *)errorMode {
  IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withInt_withIoReactivexInternalUtilErrorMode_(self, downstream, mapper, prefetch, errorMode);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(upstream_, d)) {
    JreStrongAssign(&upstream_, d);
    [((id<IoReactivexObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onNextWithId:(id)t {
  [((id<IoReactivexInternalFuseableSimplePlainQueue>) nil_chk(queue_)) offerWithId:t];
  [self drain];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  if ([((IoReactivexInternalUtilAtomicThrowable *) nil_chk(errors_)) addThrowableWithJavaLangThrowable:t]) {
    if (errorMode_ == JreLoadEnum(IoReactivexInternalUtilErrorMode, IMMEDIATE)) {
      [((IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver *) nil_chk(inner_)) dispose];
    }
    JreAssignVolatileBoolean(&done_, true);
    [self drain];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(t);
  }
}

- (void)onComplete {
  JreAssignVolatileBoolean(&done_, true);
  [self drain];
}

- (void)dispose {
  JreAssignVolatileBoolean(&cancelled_, true);
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
  [((IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver *) nil_chk(inner_)) dispose];
  if ([self getAndIncrement] == 0) {
    [((id<IoReactivexInternalFuseableSimplePlainQueue>) nil_chk(queue_)) clear];
    JreStrongAssign(&item_, nil);
  }
}

- (jboolean)isDisposed {
  return JreLoadVolatileBoolean(&cancelled_);
}

- (void)innerSuccessWithId:(id)item {
  JreStrongAssign(&self->item_, item);
  JreAssignVolatileInt(&self->state_, IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_RESULT_VALUE);
  [self drain];
}

- (void)innerErrorWithJavaLangThrowable:(JavaLangThrowable *)ex {
  if ([((IoReactivexInternalUtilAtomicThrowable *) nil_chk(errors_)) addThrowableWithJavaLangThrowable:ex]) {
    if (errorMode_ != JreLoadEnum(IoReactivexInternalUtilErrorMode, END)) {
      [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
    }
    JreAssignVolatileInt(&self->state_, IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_INACTIVE);
    [self drain];
  }
  else {
    IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
  }
}

- (void)drain {
  if ([self getAndIncrement] != 0) {
    return;
  }
  jint missed = 1;
  id<IoReactivexObserver> downstream = self->downstream_;
  IoReactivexInternalUtilErrorMode *errorMode = self->errorMode_;
  id<IoReactivexInternalFuseableSimplePlainQueue> queue = self->queue_;
  IoReactivexInternalUtilAtomicThrowable *errors = self->errors_;
  for (; ; ) {
    for (; ; ) {
      if (JreLoadVolatileBoolean(&cancelled_)) {
        [((id<IoReactivexInternalFuseableSimplePlainQueue>) nil_chk(queue)) clear];
        JreStrongAssign(&item_, nil);
        break;
      }
      jint s = JreLoadVolatileInt(&state_);
      if ([((IoReactivexInternalUtilAtomicThrowable *) nil_chk(errors)) get] != nil) {
        if (errorMode == JreLoadEnum(IoReactivexInternalUtilErrorMode, IMMEDIATE) || (errorMode == JreLoadEnum(IoReactivexInternalUtilErrorMode, BOUNDARY) && s == IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_INACTIVE)) {
          [((id<IoReactivexInternalFuseableSimplePlainQueue>) nil_chk(queue)) clear];
          JreStrongAssign(&item_, nil);
          JavaLangThrowable *ex = [errors terminate];
          [((id<IoReactivexObserver>) nil_chk(downstream)) onErrorWithJavaLangThrowable:ex];
          return;
        }
      }
      if (s == IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_INACTIVE) {
        jboolean d = JreLoadVolatileBoolean(&done_);
        id v = [((id<IoReactivexInternalFuseableSimplePlainQueue>) nil_chk(queue)) poll];
        jboolean empty = v == nil;
        if (d && empty) {
          JavaLangThrowable *ex = [errors terminate];
          if (ex == nil) {
            [((id<IoReactivexObserver>) nil_chk(downstream)) onComplete];
          }
          else {
            [((id<IoReactivexObserver>) nil_chk(downstream)) onErrorWithJavaLangThrowable:ex];
          }
          return;
        }
        if (empty) {
          break;
        }
        id<IoReactivexSingleSource> ss;
        @try {
          ss = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(mapper_)) applyWithId:v], @"The mapper returned a null SingleSource");
        }
        @catch (JavaLangThrowable *ex) {
          IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
          [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
          [queue clear];
          [errors addThrowableWithJavaLangThrowable:ex];
          ex = [errors terminate];
          [((id<IoReactivexObserver>) nil_chk(downstream)) onErrorWithJavaLangThrowable:ex];
          return;
        }
        JreAssignVolatileInt(&state_, IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_ACTIVE);
        [((id<IoReactivexSingleSource>) nil_chk(ss)) subscribeWithIoReactivexSingleObserver:inner_];
        break;
      }
      else if (s == IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_RESULT_VALUE) {
        id w = item_;
        JreStrongAssign(&item_, nil);
        [((id<IoReactivexObserver>) nil_chk(downstream)) onNextWithId:w];
        JreAssignVolatileInt(&state_, IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_INACTIVE);
      }
      else {
        break;
      }
    }
    missed = [self addAndGetWithInt:-missed];
    if (missed == 0) {
      break;
    }
  }
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(mapper_);
  RELEASE_(errors_);
  RELEASE_(inner_);
  RELEASE_(queue_);
  RELEASE_(errorMode_);
  RELEASE_(upstream_);
  RELEASE_(item_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, 9, 5, -1, 10, -1, -1 },
    { NULL, "V", 0x0, 11, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexObserver:withIoReactivexFunctionsFunction:withInt:withIoReactivexInternalUtilErrorMode:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onNextWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(dispose);
  methods[6].selector = @selector(isDisposed);
  methods[7].selector = @selector(innerSuccessWithId:);
  methods[8].selector = @selector(innerErrorWithJavaLangThrowable:);
  methods[9].selector = @selector(drain);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LIoReactivexObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 12, -1 },
    { "mapper_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 13, -1 },
    { "errors_", "LIoReactivexInternalUtilAtomicThrowable;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "inner_", "LIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 14, -1 },
    { "queue_", "LIoReactivexInternalFuseableSimplePlainQueue;", .constantValue.asLong = 0, 0x10, -1, -1, 15, -1 },
    { "errorMode_", "LIoReactivexInternalUtilErrorMode;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
    { "done_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "cancelled_", "Z", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "item_", "LNSObject;", .constantValue.asLong = 0, 0x0, -1, -1, 16, -1 },
    { "state_", "I", .constantValue.asLong = 0, 0x40, -1, -1, -1, -1 },
    { "STATE_INACTIVE", "I", .constantValue.asInt = IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_INACTIVE, 0x18, -1, -1, -1, -1 },
    { "STATE_ACTIVE", "I", .constantValue.asInt = IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_ACTIVE, 0x18, -1, -1, -1, -1 },
    { "STATE_RESULT_VALUE", "I", .constantValue.asInt = IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_STATE_RESULT_VALUE, 0x18, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexObserver;LIoReactivexFunctionsFunction;ILIoReactivexInternalUtilErrorMode;", "(Lio/reactivex/Observer<-TR;>;Lio/reactivex/functions/Function<-TT;+Lio/reactivex/SingleSource<+TR;>;>;ILio/reactivex/internal/util/ErrorMode;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onNext", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "innerSuccess", "(TR;)V", "innerError", "Lio/reactivex/Observer<-TR;>;", "Lio/reactivex/functions/Function<-TT;+Lio/reactivex/SingleSource<+TR;>;>;", "Lio/reactivex/internal/operators/mixed/ObservableConcatMapSingle$ConcatMapSingleMainObserver$ConcatMapSingleObserver<TR;>;", "Lio/reactivex/internal/fuseable/SimplePlainQueue<TT;>;", "TR;", "LIoReactivexInternalOperatorsMixedObservableConcatMapSingle;", "LIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver;", "<T:Ljava/lang/Object;R:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/Observer<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver = { "ConcatMapSingleMainObserver", "io.reactivex.internal.operators.mixed", ptrTable, methods, fields, 7, 0x18, 10, 15, 17, 18, -1, 19, -1 };
  return &_IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver;
}

@end

void IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withInt_withIoReactivexInternalUtilErrorMode_(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver *self, id<IoReactivexObserver> downstream, id<IoReactivexFunctionsFunction> mapper, jint prefetch, IoReactivexInternalUtilErrorMode *errorMode) {
  JavaUtilConcurrentAtomicAtomicInteger_init(self);
  JreStrongAssign(&self->downstream_, downstream);
  JreStrongAssign(&self->mapper_, mapper);
  JreStrongAssign(&self->errorMode_, errorMode);
  JreStrongAssignAndConsume(&self->errors_, new_IoReactivexInternalUtilAtomicThrowable_init());
  JreStrongAssignAndConsume(&self->inner_, new_IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver_initWithIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_(self));
  JreStrongAssignAndConsume(&self->queue_, new_IoReactivexInternalQueueSpscLinkedArrayQueue_initWithInt_(prefetch));
}

IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver *new_IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withInt_withIoReactivexInternalUtilErrorMode_(id<IoReactivexObserver> downstream, id<IoReactivexFunctionsFunction> mapper, jint prefetch, IoReactivexInternalUtilErrorMode *errorMode) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver, initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withInt_withIoReactivexInternalUtilErrorMode_, downstream, mapper, prefetch, errorMode)
}

IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver *create_IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withInt_withIoReactivexInternalUtilErrorMode_(id<IoReactivexObserver> downstream, id<IoReactivexFunctionsFunction> mapper, jint prefetch, IoReactivexInternalUtilErrorMode *errorMode) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver, initWithIoReactivexObserver_withIoReactivexFunctionsFunction_withInt_withIoReactivexInternalUtilErrorMode_, downstream, mapper, prefetch, errorMode)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver)

@implementation IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver:(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver *)parent {
  IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver_initWithIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_(self, parent);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  IoReactivexInternalDisposablesDisposableHelper_replaceWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexDisposablesDisposable_(self, d);
}

- (void)onSuccessWithId:(id)t {
  [((IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver *) nil_chk(parent_)) innerSuccessWithId:t];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  [((IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver *) nil_chk(parent_)) innerErrorWithJavaLangThrowable:e];
}

- (void)dispose {
  IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(self);
}

- (void)dealloc {
  RELEASE_(parent_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onSuccessWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(dispose);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "parent_", "LIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver;", "(Lio/reactivex/internal/operators/mixed/ObservableConcatMapSingle$ConcatMapSingleMainObserver<*TR;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onSuccess", "LNSObject;", "(TR;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/internal/operators/mixed/ObservableConcatMapSingle$ConcatMapSingleMainObserver<*TR;>;", "<R:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;Lio/reactivex/SingleObserver<TR;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver = { "ConcatMapSingleObserver", "io.reactivex.internal.operators.mixed", ptrTable, methods, fields, 7, 0x18, 5, 2, 0, -1, -1, 10, -1 };
  return &_IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver;
}

@end

void IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver_initWithIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver *self, IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver *parent) {
  JavaUtilConcurrentAtomicAtomicReference_init(self);
  JreStrongAssign(&self->parent_, parent);
}

IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver *new_IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver_initWithIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver *parent) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver, initWithIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_, parent)
}

IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver *create_IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver_initWithIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver *parent) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver, initWithIoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_, parent)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMixedObservableConcatMapSingle_ConcatMapSingleMainObserver_ConcatMapSingleObserver)
