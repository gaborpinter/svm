//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/maybe/MaybeOnErrorComplete.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "io/reactivex/MaybeObserver.h"
#include "io/reactivex/MaybeSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/CompositeException.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Predicate.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/operators/maybe/AbstractMaybeWithUpstream.h"
#include "io/reactivex/internal/operators/maybe/MaybeOnErrorComplete.h"
#include "java/lang/Throwable.h"

@implementation IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete

- (instancetype __nonnull)initWithIoReactivexMaybeSource:(id<IoReactivexMaybeSource>)source
                       withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)predicate {
  IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_initWithIoReactivexMaybeSource_withIoReactivexFunctionsPredicate_(self, source, predicate);
  return self;
}

- (void)subscribeActualWithIoReactivexMaybeObserver:(id<IoReactivexMaybeObserver>)observer {
  [((id<IoReactivexMaybeSource>) nil_chk(source_)) subscribeWithIoReactivexMaybeObserver:create_IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver_initWithIoReactivexMaybeObserver_withIoReactivexFunctionsPredicate_(observer, predicate_)];
}

- (void)dealloc {
  RELEASE_(predicate_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexMaybeSource:withIoReactivexFunctionsPredicate:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexMaybeObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "predicate_", "LIoReactivexFunctionsPredicate;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexMaybeSource;LIoReactivexFunctionsPredicate;", "(Lio/reactivex/MaybeSource<TT;>;Lio/reactivex/functions/Predicate<-Ljava/lang/Throwable;>;)V", "subscribeActual", "LIoReactivexMaybeObserver;", "(Lio/reactivex/MaybeObserver<-TT;>;)V", "Lio/reactivex/functions/Predicate<-Ljava/lang/Throwable;>;", "LIoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/operators/maybe/AbstractMaybeWithUpstream<TT;TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete = { "MaybeOnErrorComplete", "io.reactivex.internal.operators.maybe", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete;
}

@end

void IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_initWithIoReactivexMaybeSource_withIoReactivexFunctionsPredicate_(IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete *self, id<IoReactivexMaybeSource> source, id<IoReactivexFunctionsPredicate> predicate) {
  IoReactivexInternalOperatorsMaybeAbstractMaybeWithUpstream_initWithIoReactivexMaybeSource_(self, source);
  JreStrongAssign(&self->predicate_, predicate);
}

IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete *new_IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_initWithIoReactivexMaybeSource_withIoReactivexFunctionsPredicate_(id<IoReactivexMaybeSource> source, id<IoReactivexFunctionsPredicate> predicate) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete, initWithIoReactivexMaybeSource_withIoReactivexFunctionsPredicate_, source, predicate)
}

IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete *create_IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_initWithIoReactivexMaybeSource_withIoReactivexFunctionsPredicate_(id<IoReactivexMaybeSource> source, id<IoReactivexFunctionsPredicate> predicate) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete, initWithIoReactivexMaybeSource_withIoReactivexFunctionsPredicate_, source, predicate)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete)

@implementation IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver

- (instancetype __nonnull)initWithIoReactivexMaybeObserver:(id<IoReactivexMaybeObserver>)actual
                         withIoReactivexFunctionsPredicate:(id<IoReactivexFunctionsPredicate>)predicate {
  IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver_initWithIoReactivexMaybeObserver_withIoReactivexFunctionsPredicate_(self, actual, predicate);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    [((id<IoReactivexMaybeObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onSuccessWithId:(id)value {
  [((id<IoReactivexMaybeObserver>) nil_chk(downstream_)) onSuccessWithId:value];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  jboolean b;
  @try {
    b = [((id<IoReactivexFunctionsPredicate>) nil_chk(predicate_)) testWithId:e];
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    [((id<IoReactivexMaybeObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:create_IoReactivexExceptionsCompositeException_initWithJavaLangThrowableArray_([IOSObjectArray arrayWithObjects:(id[]){ e, ex } count:2 type:JavaLangThrowable_class_()])];
    return;
  }
  if (b) {
    [((id<IoReactivexMaybeObserver>) nil_chk(downstream_)) onComplete];
  }
  else {
    [((id<IoReactivexMaybeObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
  }
}

- (void)onComplete {
  [((id<IoReactivexMaybeObserver>) nil_chk(downstream_)) onComplete];
}

- (void)dispose {
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
}

- (jboolean)isDisposed {
  return [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) isDisposed];
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(predicate_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexMaybeObserver:withIoReactivexFunctionsPredicate:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onSuccessWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(dispose);
  methods[6].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "downstream_", "LIoReactivexMaybeObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "predicate_", "LIoReactivexFunctionsPredicate;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexMaybeObserver;LIoReactivexFunctionsPredicate;", "(Lio/reactivex/MaybeObserver<-TT;>;Lio/reactivex/functions/Predicate<-Ljava/lang/Throwable;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onSuccess", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/MaybeObserver<-TT;>;", "Lio/reactivex/functions/Predicate<-Ljava/lang/Throwable;>;", "LIoReactivexInternalOperatorsMaybeMaybeOnErrorComplete;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/MaybeObserver<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver = { "OnErrorCompleteMaybeObserver", "io.reactivex.internal.operators.maybe", ptrTable, methods, fields, 7, 0x18, 7, 3, 11, -1, -1, 12, -1 };
  return &_IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver;
}

@end

void IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver_initWithIoReactivexMaybeObserver_withIoReactivexFunctionsPredicate_(IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver *self, id<IoReactivexMaybeObserver> actual, id<IoReactivexFunctionsPredicate> predicate) {
  NSObject_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->predicate_, predicate);
}

IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver *new_IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver_initWithIoReactivexMaybeObserver_withIoReactivexFunctionsPredicate_(id<IoReactivexMaybeObserver> actual, id<IoReactivexFunctionsPredicate> predicate) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver, initWithIoReactivexMaybeObserver_withIoReactivexFunctionsPredicate_, actual, predicate)
}

IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver *create_IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver_initWithIoReactivexMaybeObserver_withIoReactivexFunctionsPredicate_(id<IoReactivexMaybeObserver> actual, id<IoReactivexFunctionsPredicate> predicate) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver, initWithIoReactivexMaybeObserver_withIoReactivexFunctionsPredicate_, actual, predicate)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMaybeMaybeOnErrorComplete_OnErrorCompleteMaybeObserver)
