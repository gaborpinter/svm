//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/maybe/MaybeToSingle.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/MaybeSource.h"
#include "io/reactivex/Single.h"
#include "io/reactivex/SingleObserver.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/operators/maybe/MaybeToSingle.h"
#include "java/lang/Throwable.h"
#include "java/util/NoSuchElementException.h"

@implementation IoReactivexInternalOperatorsMaybeMaybeToSingle

- (instancetype __nonnull)initWithIoReactivexMaybeSource:(id<IoReactivexMaybeSource>)source
                                                  withId:(id)defaultValue {
  IoReactivexInternalOperatorsMaybeMaybeToSingle_initWithIoReactivexMaybeSource_withId_(self, source, defaultValue);
  return self;
}

- (id<IoReactivexMaybeSource>)source {
  return source_;
}

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer {
  [((id<IoReactivexMaybeSource>) nil_chk(source_)) subscribeWithIoReactivexMaybeObserver:create_IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber_initWithIoReactivexSingleObserver_withId_(observer, defaultValue_)];
}

- (void)dealloc {
  RELEASE_(source_);
  RELEASE_(defaultValue_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "LIoReactivexMaybeSource;", 0x1, -1, -1, -1, 2, -1, -1 },
    { NULL, "V", 0x4, 3, 4, -1, 5, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexMaybeSource:withId:);
  methods[1].selector = @selector(source);
  methods[2].selector = @selector(subscribeActualWithIoReactivexSingleObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexMaybeSource;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
    { "defaultValue_", "LNSObject;", .constantValue.asLong = 0, 0x10, -1, -1, 7, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexMaybeSource;LNSObject;", "(Lio/reactivex/MaybeSource<TT;>;TT;)V", "()Lio/reactivex/MaybeSource<TT;>;", "subscribeActual", "LIoReactivexSingleObserver;", "(Lio/reactivex/SingleObserver<-TT;>;)V", "Lio/reactivex/MaybeSource<TT;>;", "TT;", "LIoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber;", "<T:Ljava/lang/Object;>Lio/reactivex/Single<TT;>;Lio/reactivex/internal/fuseable/HasUpstreamMaybeSource<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMaybeMaybeToSingle = { "MaybeToSingle", "io.reactivex.internal.operators.maybe", ptrTable, methods, fields, 7, 0x11, 3, 2, -1, 8, -1, 9, -1 };
  return &_IoReactivexInternalOperatorsMaybeMaybeToSingle;
}

@end

void IoReactivexInternalOperatorsMaybeMaybeToSingle_initWithIoReactivexMaybeSource_withId_(IoReactivexInternalOperatorsMaybeMaybeToSingle *self, id<IoReactivexMaybeSource> source, id defaultValue) {
  IoReactivexSingle_init(self);
  JreStrongAssign(&self->source_, source);
  JreStrongAssign(&self->defaultValue_, defaultValue);
}

IoReactivexInternalOperatorsMaybeMaybeToSingle *new_IoReactivexInternalOperatorsMaybeMaybeToSingle_initWithIoReactivexMaybeSource_withId_(id<IoReactivexMaybeSource> source, id defaultValue) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMaybeMaybeToSingle, initWithIoReactivexMaybeSource_withId_, source, defaultValue)
}

IoReactivexInternalOperatorsMaybeMaybeToSingle *create_IoReactivexInternalOperatorsMaybeMaybeToSingle_initWithIoReactivexMaybeSource_withId_(id<IoReactivexMaybeSource> source, id defaultValue) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMaybeMaybeToSingle, initWithIoReactivexMaybeSource_withId_, source, defaultValue)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMaybeMaybeToSingle)

@implementation IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber

- (instancetype __nonnull)initWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)actual
                                                     withId:(id)defaultValue {
  IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber_initWithIoReactivexSingleObserver_withId_(self, actual, defaultValue);
  return self;
}

- (void)dispose {
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
  JreStrongAssign(&upstream_, JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED));
}

- (jboolean)isDisposed {
  return [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) isDisposed];
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onSuccessWithId:(id)value {
  JreStrongAssign(&upstream_, JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED));
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSuccessWithId:value];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  JreStrongAssign(&upstream_, JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED));
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
}

- (void)onComplete {
  JreStrongAssign(&upstream_, JreLoadEnum(IoReactivexInternalDisposablesDisposableHelper, DISPOSED));
  if (defaultValue_ != nil) {
    [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSuccessWithId:defaultValue_];
  }
  else {
    [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:create_JavaUtilNoSuchElementException_initWithNSString_(@"The MaybeSource is empty")];
  }
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(defaultValue_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexSingleObserver:withId:);
  methods[1].selector = @selector(dispose);
  methods[2].selector = @selector(isDisposed);
  methods[3].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[4].selector = @selector(onSuccessWithId:);
  methods[5].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[6].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "downstream_", "LIoReactivexSingleObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "defaultValue_", "LNSObject;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexSingleObserver;LNSObject;", "(Lio/reactivex/SingleObserver<-TT;>;TT;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onSuccess", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/SingleObserver<-TT;>;", "TT;", "LIoReactivexInternalOperatorsMaybeMaybeToSingle;", "<T:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/MaybeObserver<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber = { "ToSingleMaybeSubscriber", "io.reactivex.internal.operators.maybe", ptrTable, methods, fields, 7, 0x18, 7, 3, 11, -1, -1, 12, -1 };
  return &_IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber;
}

@end

void IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber_initWithIoReactivexSingleObserver_withId_(IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber *self, id<IoReactivexSingleObserver> actual, id defaultValue) {
  NSObject_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->defaultValue_, defaultValue);
}

IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber *new_IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber_initWithIoReactivexSingleObserver_withId_(id<IoReactivexSingleObserver> actual, id defaultValue) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber, initWithIoReactivexSingleObserver_withId_, actual, defaultValue)
}

IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber *create_IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber_initWithIoReactivexSingleObserver_withId_(id<IoReactivexSingleObserver> actual, id defaultValue) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber, initWithIoReactivexSingleObserver_withId_, actual, defaultValue)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMaybeMaybeToSingle_ToSingleMaybeSubscriber)
