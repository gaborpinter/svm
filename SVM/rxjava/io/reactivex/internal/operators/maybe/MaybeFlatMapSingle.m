//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/maybe/MaybeFlatMapSingle.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/MaybeSource.h"
#include "io/reactivex/Single.h"
#include "io/reactivex/SingleObserver.h"
#include "io/reactivex/SingleSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Function.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/operators/maybe/MaybeFlatMapSingle.h"
#include "java/lang/Throwable.h"
#include "java/util/NoSuchElementException.h"
#include "java/util/concurrent/atomic/AtomicReference.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver_serialVersionUID 4827726964688405508LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle

- (instancetype __nonnull)initWithIoReactivexMaybeSource:(id<IoReactivexMaybeSource>)source
                        withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)mapper {
  IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_initWithIoReactivexMaybeSource_withIoReactivexFunctionsFunction_(self, source, mapper);
  return self;
}

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)downstream {
  [((id<IoReactivexMaybeSource>) nil_chk(source_)) subscribeWithIoReactivexMaybeObserver:create_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_(downstream, mapper_)];
}

- (void)dealloc {
  RELEASE_(source_);
  RELEASE_(mapper_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexMaybeSource:withIoReactivexFunctionsFunction:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexSingleObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexMaybeSource;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "mapper_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexMaybeSource;LIoReactivexFunctionsFunction;", "(Lio/reactivex/MaybeSource<TT;>;Lio/reactivex/functions/Function<-TT;+Lio/reactivex/SingleSource<+TR;>;>;)V", "subscribeActual", "LIoReactivexSingleObserver;", "(Lio/reactivex/SingleObserver<-TR;>;)V", "Lio/reactivex/MaybeSource<TT;>;", "Lio/reactivex/functions/Function<-TT;+Lio/reactivex/SingleSource<+TR;>;>;", "LIoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver;LIoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver;", "<T:Ljava/lang/Object;R:Ljava/lang/Object;>Lio/reactivex/Single<TR;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle = { "MaybeFlatMapSingle", "io.reactivex.internal.operators.maybe", ptrTable, methods, fields, 7, 0x11, 2, 2, -1, 7, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle;
}

@end

void IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_initWithIoReactivexMaybeSource_withIoReactivexFunctionsFunction_(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle *self, id<IoReactivexMaybeSource> source, id<IoReactivexFunctionsFunction> mapper) {
  IoReactivexSingle_init(self);
  JreStrongAssign(&self->source_, source);
  JreStrongAssign(&self->mapper_, mapper);
}

IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle *new_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_initWithIoReactivexMaybeSource_withIoReactivexFunctionsFunction_(id<IoReactivexMaybeSource> source, id<IoReactivexFunctionsFunction> mapper) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle, initWithIoReactivexMaybeSource_withIoReactivexFunctionsFunction_, source, mapper)
}

IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle *create_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_initWithIoReactivexMaybeSource_withIoReactivexFunctionsFunction_(id<IoReactivexMaybeSource> source, id<IoReactivexFunctionsFunction> mapper) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle, initWithIoReactivexMaybeSource_withIoReactivexFunctionsFunction_, source, mapper)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle)

@implementation IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver

- (instancetype __nonnull)initWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)actual
                           withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)mapper {
  IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_(self, actual, mapper);
  return self;
}

- (void)dispose {
  IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(self);
}

- (jboolean)isDisposed {
  return IoReactivexInternalDisposablesDisposableHelper_isDisposedWithIoReactivexDisposablesDisposable_([self get]);
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexDisposablesDisposable_(self, d)) {
    [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onSuccessWithId:(id)value {
  id<IoReactivexSingleSource> ss;
  @try {
    ss = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(mapper_)) applyWithId:value], @"The mapper returned a null SingleSource");
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    [self onErrorWithJavaLangThrowable:ex];
    return;
  }
  if (![self isDisposed]) {
    [((id<IoReactivexSingleSource>) nil_chk(ss)) subscribeWithIoReactivexSingleObserver:create_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver_initWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexSingleObserver_(self, downstream_)];
  }
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
}

- (void)onComplete {
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:create_JavaUtilNoSuchElementException_init()];
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(mapper_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexSingleObserver:withIoReactivexFunctionsFunction:);
  methods[1].selector = @selector(dispose);
  methods[2].selector = @selector(isDisposed);
  methods[3].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[4].selector = @selector(onSuccessWithId:);
  methods[5].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[6].selector = @selector(onComplete);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LIoReactivexSingleObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "mapper_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexSingleObserver;LIoReactivexFunctionsFunction;", "(Lio/reactivex/SingleObserver<-TR;>;Lio/reactivex/functions/Function<-TT;+Lio/reactivex/SingleSource<+TR;>;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onSuccess", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/SingleObserver<-TR;>;", "Lio/reactivex/functions/Function<-TT;+Lio/reactivex/SingleSource<+TR;>;>;", "LIoReactivexInternalOperatorsMaybeMaybeFlatMapSingle;", "<T:Ljava/lang/Object;R:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;Lio/reactivex/MaybeObserver<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver = { "FlatMapMaybeObserver", "io.reactivex.internal.operators.maybe", ptrTable, methods, fields, 7, 0x18, 7, 3, 11, -1, -1, 12, -1 };
  return &_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver;
}

@end

void IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver *self, id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsFunction> mapper) {
  JavaUtilConcurrentAtomicAtomicReference_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->mapper_, mapper);
}

IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver *new_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_(id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsFunction> mapper) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver, initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_, actual, mapper)
}

IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver *create_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_(id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsFunction> mapper) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver, initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_, actual, mapper)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapMaybeObserver)

@implementation IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver

- (instancetype __nonnull)initWithJavaUtilConcurrentAtomicAtomicReference:(JavaUtilConcurrentAtomicAtomicReference *)parent
                                            withIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)downstream {
  IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver_initWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexSingleObserver_(self, parent, downstream);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  IoReactivexInternalDisposablesDisposableHelper_replaceWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexDisposablesDisposable_(parent_, d);
}

- (void)onSuccessWithId:(id)value {
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSuccessWithId:value];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
}

- (void)dealloc {
  RELEASE_(parent_);
  RELEASE_(downstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaUtilConcurrentAtomicAtomicReference:withIoReactivexSingleObserver:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onSuccessWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "parent_", "LJavaUtilConcurrentAtomicAtomicReference;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "downstream_", "LIoReactivexSingleObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
  };
  static const void *ptrTable[] = { "LJavaUtilConcurrentAtomicAtomicReference;LIoReactivexSingleObserver;", "(Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;Lio/reactivex/SingleObserver<-TR;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onSuccess", "LNSObject;", "(TR;)V", "onError", "LJavaLangThrowable;", "Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;", "Lio/reactivex/SingleObserver<-TR;>;", "LIoReactivexInternalOperatorsMaybeMaybeFlatMapSingle;", "<R:Ljava/lang/Object;>Ljava/lang/Object;Lio/reactivex/SingleObserver<TR;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver = { "FlatMapSingleObserver", "io.reactivex.internal.operators.maybe", ptrTable, methods, fields, 7, 0x18, 4, 2, 11, -1, -1, 12, -1 };
  return &_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver;
}

@end

void IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver_initWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexSingleObserver_(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver *self, JavaUtilConcurrentAtomicAtomicReference *parent, id<IoReactivexSingleObserver> downstream) {
  NSObject_init(self);
  JreStrongAssign(&self->parent_, parent);
  JreStrongAssign(&self->downstream_, downstream);
}

IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver *new_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver_initWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexSingleObserver_(JavaUtilConcurrentAtomicAtomicReference *parent, id<IoReactivexSingleObserver> downstream) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver, initWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexSingleObserver_, parent, downstream)
}

IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver *create_IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver_initWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexSingleObserver_(JavaUtilConcurrentAtomicAtomicReference *parent, id<IoReactivexSingleObserver> downstream) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver, initWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexSingleObserver_, parent, downstream)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMaybeMaybeFlatMapSingle_FlatMapSingleObserver)
