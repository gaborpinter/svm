//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/maybe/MaybeMergeArray.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeMergeArray")
#ifdef RESTRICT_IoReactivexInternalOperatorsMaybeMaybeMergeArray
#define INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeMergeArray 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeMergeArray 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsMaybeMaybeMergeArray
#ifdef INCLUDE_IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue
#define INCLUDE_IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex 1
#endif
#ifdef INCLUDE_IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue
#define INCLUDE_IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex 1
#endif

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsMaybeMaybeMergeArray_) && (INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeMergeArray || defined(INCLUDE_IoReactivexInternalOperatorsMaybeMaybeMergeArray))
#define IoReactivexInternalOperatorsMaybeMaybeMergeArray_

#define RESTRICT_IoReactivexFlowable 1
#define INCLUDE_IoReactivexFlowable 1
#include "io/reactivex/Flowable.h"

@class IOSObjectArray;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsMaybeMaybeMergeArray : IoReactivexFlowable {
 @public
  IOSObjectArray *sources_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexMaybeSourceArray:(IOSObjectArray *)sources;

#pragma mark Protected

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsMaybeMaybeMergeArray)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeMergeArray, sources_, IOSObjectArray *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsMaybeMaybeMergeArray_initWithIoReactivexMaybeSourceArray_(IoReactivexInternalOperatorsMaybeMaybeMergeArray *self, IOSObjectArray *sources);

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeMergeArray *new_IoReactivexInternalOperatorsMaybeMaybeMergeArray_initWithIoReactivexMaybeSourceArray_(IOSObjectArray *sources) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeMergeArray *create_IoReactivexInternalOperatorsMaybeMaybeMergeArray_initWithIoReactivexMaybeSourceArray_(IOSObjectArray *sources);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsMaybeMaybeMergeArray)

#endif

#if !defined (IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeMergeArray || defined(INCLUDE_IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver))
#define IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver_

#define RESTRICT_IoReactivexInternalSubscriptionsBasicIntQueueSubscription 1
#define INCLUDE_IoReactivexInternalSubscriptionsBasicIntQueueSubscription 1
#include "io/reactivex/internal/subscriptions/BasicIntQueueSubscription.h"

#define RESTRICT_IoReactivexMaybeObserver 1
#define INCLUDE_IoReactivexMaybeObserver 1
#include "io/reactivex/MaybeObserver.h"

@class IoReactivexDisposablesCompositeDisposable;
@class IoReactivexInternalUtilAtomicThrowable;
@class JavaLangThrowable;
@class JavaUtilConcurrentAtomicAtomicLong;
@protocol IoReactivexDisposablesDisposable;
@protocol IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver : IoReactivexInternalSubscriptionsBasicIntQueueSubscription < IoReactivexMaybeObserver > {
 @public
  id<OrgReactivestreamsSubscriber> downstream_;
  IoReactivexDisposablesCompositeDisposable *set_;
  JavaUtilConcurrentAtomicAtomicLong *requested_;
  id<IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex> queue_;
  IoReactivexInternalUtilAtomicThrowable *error_;
  jint sourceCount_;
  volatile_jboolean cancelled_;
  jboolean outputFused_;
  jlong consumed_;
}

#pragma mark Public

- (void)cancel;

- (void)clear;

- (NSUInteger)hash;

- (jboolean)isEmpty;

- (jboolean)isEqual:(id)obj;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (void)onSuccessWithId:(id)value;

- (id __nullable)poll;

- (void)requestWithLong:(jlong)n;

- (jint)requestFusionWithInt:(jint)mode;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)actual
                                                       withInt:(jint)sourceCount
withIoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex:(id<IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex>)queue;

- (void)drain;

- (void)drainFused;

- (void)drainNormal;

- (jboolean)isCancelled;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver, downstream_, id<OrgReactivestreamsSubscriber>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver, set_, IoReactivexDisposablesCompositeDisposable *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver, requested_, JavaUtilConcurrentAtomicAtomicLong *)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver, queue_, id<IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver, error_, IoReactivexInternalUtilAtomicThrowable *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver_initWithOrgReactivestreamsSubscriber_withInt_withIoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex_(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver *self, id<OrgReactivestreamsSubscriber> actual, jint sourceCount, id<IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex> queue);

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver *new_IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver_initWithOrgReactivestreamsSubscriber_withInt_withIoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex_(id<OrgReactivestreamsSubscriber> actual, jint sourceCount, id<IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex> queue) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver *create_IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver_initWithOrgReactivestreamsSubscriber_withInt_withIoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex_(id<OrgReactivestreamsSubscriber> actual, jint sourceCount, id<IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex> queue);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MergeMaybeObserver)

#endif

#if !defined (IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex_) && (INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeMergeArray || defined(INCLUDE_IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex))
#define IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex_

#define RESTRICT_IoReactivexInternalFuseableSimpleQueue 1
#define INCLUDE_IoReactivexInternalFuseableSimpleQueue 1
#include "io/reactivex/internal/fuseable/SimpleQueue.h"

@protocol IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex < IoReactivexInternalFuseableSimpleQueue, JavaObject >

- (id __nullable)poll;

- (id)peek;

- (void)drop;

- (jint)consumerIndex;

- (jint)producerIndex;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex)

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex)

#endif

#if !defined (IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue_) && (INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeMergeArray || defined(INCLUDE_IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue))
#define IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReferenceArray 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReferenceArray 1
#include "java/util/concurrent/atomic/AtomicReferenceArray.h"

@class IOSObjectArray;
@class JavaUtilConcurrentAtomicAtomicInteger;

@interface IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue : JavaUtilConcurrentAtomicAtomicReferenceArray < IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex > {
 @public
  JavaUtilConcurrentAtomicAtomicInteger *producerIndex_;
  jint consumerIndex_;
}

#pragma mark Public

- (void)clear;

- (jint)consumerIndex;

- (void)drop;

- (jboolean)isEmpty;

- (jboolean)offerWithId:(id)value;

- (jboolean)offerWithId:(id)v1
                 withId:(id)v2;

- (id)peek;

- (id __nullable)poll;

- (jint)producerIndex;

#pragma mark Package-Private

- (instancetype __nonnull)initWithInt:(jint)length;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithNSObjectArray:(IOSObjectArray *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue, producerIndex_, JavaUtilConcurrentAtomicAtomicInteger *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue_initWithInt_(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue *self, jint length);

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue *new_IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue_initWithInt_(jint length) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue *create_IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue_initWithInt_(jint length);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_MpscFillOnceSimpleQueue)

#endif

#if !defined (IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue_) && (INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeMergeArray || defined(INCLUDE_IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue))
#define IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue_

#define RESTRICT_JavaUtilConcurrentConcurrentLinkedQueue 1
#define INCLUDE_JavaUtilConcurrentConcurrentLinkedQueue 1
#include "java/util/concurrent/ConcurrentLinkedQueue.h"

@class JavaUtilConcurrentAtomicAtomicInteger;
@protocol JavaUtilCollection;

@interface IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue : JavaUtilConcurrentConcurrentLinkedQueue < IoReactivexInternalOperatorsMaybeMaybeMergeArray_SimpleQueueWithConsumerIndex > {
 @public
  jint consumerIndex_;
  JavaUtilConcurrentAtomicAtomicInteger *producerIndex_;
}

#pragma mark Public

- (jint)consumerIndex;

- (void)drop;

- (jboolean)offerWithId:(id)e;

- (jboolean)offerWithId:(id)v1
                 withId:(id)v2;

- (id __nullable)poll;

- (jint)producerIndex;

#pragma mark Package-Private

- (instancetype __nonnull)init;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithJavaUtilCollection:(id<JavaUtilCollection>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue, producerIndex_, JavaUtilConcurrentAtomicAtomicInteger *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue_init(IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue *self);

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue *new_IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue *create_IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue_init(void);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsMaybeMaybeMergeArray_ClqSimpleQueue)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeMergeArray")
