//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/maybe/MaybeMaterialize.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Maybe.h"
#include "io/reactivex/Single.h"
#include "io/reactivex/SingleObserver.h"
#include "io/reactivex/internal/operators/maybe/MaybeMaterialize.h"
#include "io/reactivex/internal/operators/mixed/MaterializeSingleObserver.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation IoReactivexInternalOperatorsMaybeMaybeMaterialize

- (instancetype __nonnull)initWithIoReactivexMaybe:(IoReactivexMaybe *)source {
  IoReactivexInternalOperatorsMaybeMaybeMaterialize_initWithIoReactivexMaybe_(self, source);
  return self;
}

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer {
  [((IoReactivexMaybe *) nil_chk(source_)) subscribeWithIoReactivexMaybeObserver:create_IoReactivexInternalOperatorsMixedMaterializeSingleObserver_initWithIoReactivexSingleObserver_(observer)];
}

- (void)dealloc {
  RELEASE_(source_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexMaybe:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexSingleObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexMaybe;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexMaybe;", "(Lio/reactivex/Maybe<TT;>;)V", "subscribeActual", "LIoReactivexSingleObserver;", "(Lio/reactivex/SingleObserver<-Lio/reactivex/Notification<TT;>;>;)V", "Lio/reactivex/Maybe<TT;>;", "<T:Ljava/lang/Object;>Lio/reactivex/Single<Lio/reactivex/Notification<TT;>;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMaybeMaybeMaterialize = { "MaybeMaterialize", "io.reactivex.internal.operators.maybe", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, -1, -1, 6, -1 };
  return &_IoReactivexInternalOperatorsMaybeMaybeMaterialize;
}

@end

void IoReactivexInternalOperatorsMaybeMaybeMaterialize_initWithIoReactivexMaybe_(IoReactivexInternalOperatorsMaybeMaybeMaterialize *self, IoReactivexMaybe *source) {
  IoReactivexSingle_init(self);
  JreStrongAssign(&self->source_, source);
}

IoReactivexInternalOperatorsMaybeMaybeMaterialize *new_IoReactivexInternalOperatorsMaybeMaybeMaterialize_initWithIoReactivexMaybe_(IoReactivexMaybe *source) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMaybeMaybeMaterialize, initWithIoReactivexMaybe_, source)
}

IoReactivexInternalOperatorsMaybeMaybeMaterialize *create_IoReactivexInternalOperatorsMaybeMaybeMaterialize_initWithIoReactivexMaybe_(IoReactivexMaybe *source) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMaybeMaybeMaterialize, initWithIoReactivexMaybe_, source)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMaybeMaybeMaterialize)
