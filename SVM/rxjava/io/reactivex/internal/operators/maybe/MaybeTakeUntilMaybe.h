//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/maybe/MaybeTakeUntilMaybe.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe")
#ifdef RESTRICT_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe
#define INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_) && (INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe || defined(INCLUDE_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe))
#define IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_

#define RESTRICT_IoReactivexInternalOperatorsMaybeAbstractMaybeWithUpstream 1
#define INCLUDE_IoReactivexInternalOperatorsMaybeAbstractMaybeWithUpstream 1
#include "io/reactivex/internal/operators/maybe/AbstractMaybeWithUpstream.h"

@protocol IoReactivexMaybeObserver;
@protocol IoReactivexMaybeSource;

@interface IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe : IoReactivexInternalOperatorsMaybeAbstractMaybeWithUpstream {
 @public
  id<IoReactivexMaybeSource> other_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexMaybeSource:(id<IoReactivexMaybeSource>)source
                              withIoReactivexMaybeSource:(id<IoReactivexMaybeSource>)other;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexMaybeObserver:(id<IoReactivexMaybeObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)initWithIoReactivexMaybeSource:(id<IoReactivexMaybeSource>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe, other_, id<IoReactivexMaybeSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_initWithIoReactivexMaybeSource_withIoReactivexMaybeSource_(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe *self, id<IoReactivexMaybeSource> source, id<IoReactivexMaybeSource> other);

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe *new_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_initWithIoReactivexMaybeSource_withIoReactivexMaybeSource_(id<IoReactivexMaybeSource> source, id<IoReactivexMaybeSource> other) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe *create_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_initWithIoReactivexMaybeSource_withIoReactivexMaybeSource_(id<IoReactivexMaybeSource> source, id<IoReactivexMaybeSource> other);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe)

#endif

#if !defined (IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe || defined(INCLUDE_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver))
#define IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexMaybeObserver 1
#define INCLUDE_IoReactivexMaybeObserver 1
#include "io/reactivex/MaybeObserver.h"

#define RESTRICT_IoReactivexDisposablesDisposable 1
#define INCLUDE_IoReactivexDisposablesDisposable 1
#include "io/reactivex/disposables/Disposable.h"

@class IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver;
@class JavaLangThrowable;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;

@interface IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver : JavaUtilConcurrentAtomicAtomicReference < IoReactivexMaybeObserver, IoReactivexDisposablesDisposable > {
 @public
  id<IoReactivexMaybeObserver> downstream_;
  IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver *other_;
}

#pragma mark Public

- (id<IoReactivexDisposablesDisposable>)accumulateAndGetWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (void)dispose;

- (id<IoReactivexDisposablesDisposable>)get;

- (id<IoReactivexDisposablesDisposable>)getAndAccumulateWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)getAndSetWithId:(id<IoReactivexDisposablesDisposable>)arg0;

- (id<IoReactivexDisposablesDisposable>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (jboolean)isDisposed;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (void)onSuccessWithId:(id)value;

- (id<IoReactivexDisposablesDisposable>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexMaybeObserver:(id<IoReactivexMaybeObserver>)downstream;

- (void)otherComplete;

- (void)otherErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver, downstream_, id<IoReactivexMaybeObserver>)
J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver, other_, IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_initWithIoReactivexMaybeObserver_(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver *self, id<IoReactivexMaybeObserver> downstream);

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver *new_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_initWithIoReactivexMaybeObserver_(id<IoReactivexMaybeObserver> downstream) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver *create_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_initWithIoReactivexMaybeObserver_(id<IoReactivexMaybeObserver> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver)

#endif

#if !defined (IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe || defined(INCLUDE_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver))
#define IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver_

#define RESTRICT_JavaUtilConcurrentAtomicAtomicReference 1
#define INCLUDE_JavaUtilConcurrentAtomicAtomicReference 1
#include "java/util/concurrent/atomic/AtomicReference.h"

#define RESTRICT_IoReactivexMaybeObserver 1
#define INCLUDE_IoReactivexMaybeObserver 1
#include "io/reactivex/MaybeObserver.h"

@class IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver;
@class JavaLangThrowable;
@protocol IoReactivexDisposablesDisposable;
@protocol JavaUtilFunctionBinaryOperator;
@protocol JavaUtilFunctionUnaryOperator;

@interface IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver : JavaUtilConcurrentAtomicAtomicReference < IoReactivexMaybeObserver > {
 @public
  IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver *parent_;
}

#pragma mark Public

- (id<IoReactivexDisposablesDisposable>)accumulateAndGetWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)get;

- (id<IoReactivexDisposablesDisposable>)getAndAccumulateWithId:(id<IoReactivexDisposablesDisposable>)arg0
                            withJavaUtilFunctionBinaryOperator:(id<JavaUtilFunctionBinaryOperator>)arg1;

- (id<IoReactivexDisposablesDisposable>)getAndSetWithId:(id<IoReactivexDisposablesDisposable>)arg0;

- (id<IoReactivexDisposablesDisposable>)getAndUpdateWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

- (void)onComplete;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (void)onSuccessWithId:(id)value;

- (id<IoReactivexDisposablesDisposable>)updateAndGetWithJavaUtilFunctionUnaryOperator:(id<JavaUtilFunctionUnaryOperator>)arg0;

#pragma mark Package-Private

- (instancetype __nonnull)initWithIoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver:(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver *)parent;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

- (instancetype __nonnull)initWithId:(id)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver, parent_, IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver *)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver_initWithIoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver *self, IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver *parent);

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver *new_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver_initWithIoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver *parent) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver *create_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver_initWithIoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver *parent);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe_TakeUntilMainMaybeObserver_TakeUntilOtherMaybeObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsMaybeMaybeTakeUntilMaybe")
