//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/maybe/MaybeToFlowable.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Flowable.h"
#include "io/reactivex/MaybeSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/operators/maybe/MaybeToFlowable.h"
#include "io/reactivex/internal/subscriptions/DeferredScalarSubscription.h"
#include "java/lang/Throwable.h"
#include "org/reactivestreams/Subscriber.h"

inline jlong IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber_serialVersionUID 7603343402964826922LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsMaybeMaybeToFlowable

- (instancetype __nonnull)initWithIoReactivexMaybeSource:(id<IoReactivexMaybeSource>)source {
  IoReactivexInternalOperatorsMaybeMaybeToFlowable_initWithIoReactivexMaybeSource_(self, source);
  return self;
}

- (id<IoReactivexMaybeSource>)source {
  return source_;
}

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s {
  [((id<IoReactivexMaybeSource>) nil_chk(source_)) subscribeWithIoReactivexMaybeObserver:create_IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber_initWithOrgReactivestreamsSubscriber_(s)];
}

- (void)dealloc {
  RELEASE_(source_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "LIoReactivexMaybeSource;", 0x1, -1, -1, -1, 2, -1, -1 },
    { NULL, "V", 0x4, 3, 4, -1, 5, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexMaybeSource:);
  methods[1].selector = @selector(source);
  methods[2].selector = @selector(subscribeActualWithOrgReactivestreamsSubscriber:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexMaybeSource;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexMaybeSource;", "(Lio/reactivex/MaybeSource<TT;>;)V", "()Lio/reactivex/MaybeSource<TT;>;", "subscribeActual", "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "Lio/reactivex/MaybeSource<TT;>;", "LIoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber;", "<T:Ljava/lang/Object;>Lio/reactivex/Flowable<TT;>;Lio/reactivex/internal/fuseable/HasUpstreamMaybeSource<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMaybeMaybeToFlowable = { "MaybeToFlowable", "io.reactivex.internal.operators.maybe", ptrTable, methods, fields, 7, 0x11, 3, 1, -1, 7, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsMaybeMaybeToFlowable;
}

@end

void IoReactivexInternalOperatorsMaybeMaybeToFlowable_initWithIoReactivexMaybeSource_(IoReactivexInternalOperatorsMaybeMaybeToFlowable *self, id<IoReactivexMaybeSource> source) {
  IoReactivexFlowable_init(self);
  JreStrongAssign(&self->source_, source);
}

IoReactivexInternalOperatorsMaybeMaybeToFlowable *new_IoReactivexInternalOperatorsMaybeMaybeToFlowable_initWithIoReactivexMaybeSource_(id<IoReactivexMaybeSource> source) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMaybeMaybeToFlowable, initWithIoReactivexMaybeSource_, source)
}

IoReactivexInternalOperatorsMaybeMaybeToFlowable *create_IoReactivexInternalOperatorsMaybeMaybeToFlowable_initWithIoReactivexMaybeSource_(id<IoReactivexMaybeSource> source) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMaybeMaybeToFlowable, initWithIoReactivexMaybeSource_, source)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMaybeMaybeToFlowable)

@implementation IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream {
  IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber_initWithOrgReactivestreamsSubscriber_(self, downstream);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onSubscribeWithOrgReactivestreamsSubscription:self];
  }
}

- (void)onSuccessWithId:(id)value {
  [self completeWithId:value];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:e];
}

- (void)onComplete {
  [((id<OrgReactivestreamsSubscriber>) nil_chk(downstream_)) onComplete];
}

- (void)cancel {
  [super cancel];
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithOrgReactivestreamsSubscriber:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onSuccessWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(onComplete);
  methods[5].selector = @selector(cancel);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LOrgReactivestreamsSubscriber;", "(Lorg/reactivestreams/Subscriber<-TT;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onSuccess", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "LIoReactivexInternalOperatorsMaybeMaybeToFlowable;", "<T:Ljava/lang/Object;>Lio/reactivex/internal/subscriptions/DeferredScalarSubscription<TT;>;Lio/reactivex/MaybeObserver<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber = { "MaybeToFlowableSubscriber", "io.reactivex.internal.operators.maybe", ptrTable, methods, fields, 7, 0x18, 6, 2, 9, -1, -1, 10, -1 };
  return &_IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber;
}

@end

void IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber *self, id<OrgReactivestreamsSubscriber> downstream) {
  IoReactivexInternalSubscriptionsDeferredScalarSubscription_initWithOrgReactivestreamsSubscriber_(self, downstream);
}

IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber *new_IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber, initWithOrgReactivestreamsSubscriber_, downstream)
}

IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber *create_IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber, initWithOrgReactivestreamsSubscriber_, downstream)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsMaybeMaybeToFlowable_MaybeToFlowableSubscriber)
