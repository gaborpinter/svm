//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/single/SingleFromCallable.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Single.h"
#include "io/reactivex/SingleObserver.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/disposables/Disposables.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/operators/single/SingleFromCallable.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/Callable.h"

@implementation IoReactivexInternalOperatorsSingleSingleFromCallable

- (instancetype __nonnull)initWithJavaUtilConcurrentCallable:(id<JavaUtilConcurrentCallable>)callable {
  IoReactivexInternalOperatorsSingleSingleFromCallable_initWithJavaUtilConcurrentCallable_(self, callable);
  return self;
}

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer {
  id<IoReactivexDisposablesDisposable> d = IoReactivexDisposablesDisposables_empty();
  [((id<IoReactivexSingleObserver>) nil_chk(observer)) onSubscribeWithIoReactivexDisposablesDisposable:d];
  if ([((id<IoReactivexDisposablesDisposable>) nil_chk(d)) isDisposed]) {
    return;
  }
  id value;
  @try {
    value = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<JavaUtilConcurrentCallable>) nil_chk(callable_)) call], @"The callable returned a null value");
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    if (![d isDisposed]) {
      [observer onErrorWithJavaLangThrowable:ex];
    }
    else {
      IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
    }
    return;
  }
  if (![d isDisposed]) {
    [observer onSuccessWithId:value];
  }
}

- (void)dealloc {
  RELEASE_(callable_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaUtilConcurrentCallable:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexSingleObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "callable_", "LJavaUtilConcurrentCallable;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LJavaUtilConcurrentCallable;", "(Ljava/util/concurrent/Callable<+TT;>;)V", "subscribeActual", "LIoReactivexSingleObserver;", "(Lio/reactivex/SingleObserver<-TT;>;)V", "Ljava/util/concurrent/Callable<+TT;>;", "<T:Ljava/lang/Object;>Lio/reactivex/Single<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsSingleSingleFromCallable = { "SingleFromCallable", "io.reactivex.internal.operators.single", ptrTable, methods, fields, 7, 0x11, 2, 1, -1, -1, -1, 6, -1 };
  return &_IoReactivexInternalOperatorsSingleSingleFromCallable;
}

@end

void IoReactivexInternalOperatorsSingleSingleFromCallable_initWithJavaUtilConcurrentCallable_(IoReactivexInternalOperatorsSingleSingleFromCallable *self, id<JavaUtilConcurrentCallable> callable) {
  IoReactivexSingle_init(self);
  JreStrongAssign(&self->callable_, callable);
}

IoReactivexInternalOperatorsSingleSingleFromCallable *new_IoReactivexInternalOperatorsSingleSingleFromCallable_initWithJavaUtilConcurrentCallable_(id<JavaUtilConcurrentCallable> callable) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsSingleSingleFromCallable, initWithJavaUtilConcurrentCallable_, callable)
}

IoReactivexInternalOperatorsSingleSingleFromCallable *create_IoReactivexInternalOperatorsSingleSingleFromCallable_initWithJavaUtilConcurrentCallable_(id<JavaUtilConcurrentCallable> callable) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsSingleSingleFromCallable, initWithJavaUtilConcurrentCallable_, callable)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsSingleSingleFromCallable)
