//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/single/SingleDoFinally.java
//

#include "J2ObjC_source.h"
#include "io/reactivex/Single.h"
#include "io/reactivex/SingleObserver.h"
#include "io/reactivex/SingleSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Action.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/operators/single/SingleDoFinally.h"
#include "io/reactivex/plugins/RxJavaPlugins.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/atomic/AtomicInteger.h"

inline jlong IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver_serialVersionUID 4109457741734051389LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsSingleSingleDoFinally

- (instancetype __nonnull)initWithIoReactivexSingleSource:(id<IoReactivexSingleSource>)source
                           withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onFinally {
  IoReactivexInternalOperatorsSingleSingleDoFinally_initWithIoReactivexSingleSource_withIoReactivexFunctionsAction_(self, source, onFinally);
  return self;
}

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer {
  [((id<IoReactivexSingleSource>) nil_chk(source_)) subscribeWithIoReactivexSingleObserver:create_IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsAction_(observer, onFinally_)];
}

- (void)dealloc {
  RELEASE_(source_);
  RELEASE_(onFinally_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexSingleSource:withIoReactivexFunctionsAction:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexSingleObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexSingleSource;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "onFinally_", "LIoReactivexFunctionsAction;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexSingleSource;LIoReactivexFunctionsAction;", "(Lio/reactivex/SingleSource<TT;>;Lio/reactivex/functions/Action;)V", "subscribeActual", "LIoReactivexSingleObserver;", "(Lio/reactivex/SingleObserver<-TT;>;)V", "Lio/reactivex/SingleSource<TT;>;", "LIoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/Single<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsSingleSingleDoFinally = { "SingleDoFinally", "io.reactivex.internal.operators.single", ptrTable, methods, fields, 7, 0x11, 2, 2, -1, 6, -1, 7, -1 };
  return &_IoReactivexInternalOperatorsSingleSingleDoFinally;
}

@end

void IoReactivexInternalOperatorsSingleSingleDoFinally_initWithIoReactivexSingleSource_withIoReactivexFunctionsAction_(IoReactivexInternalOperatorsSingleSingleDoFinally *self, id<IoReactivexSingleSource> source, id<IoReactivexFunctionsAction> onFinally) {
  IoReactivexSingle_init(self);
  JreStrongAssign(&self->source_, source);
  JreStrongAssign(&self->onFinally_, onFinally);
}

IoReactivexInternalOperatorsSingleSingleDoFinally *new_IoReactivexInternalOperatorsSingleSingleDoFinally_initWithIoReactivexSingleSource_withIoReactivexFunctionsAction_(id<IoReactivexSingleSource> source, id<IoReactivexFunctionsAction> onFinally) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsSingleSingleDoFinally, initWithIoReactivexSingleSource_withIoReactivexFunctionsAction_, source, onFinally)
}

IoReactivexInternalOperatorsSingleSingleDoFinally *create_IoReactivexInternalOperatorsSingleSingleDoFinally_initWithIoReactivexSingleSource_withIoReactivexFunctionsAction_(id<IoReactivexSingleSource> source, id<IoReactivexFunctionsAction> onFinally) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsSingleSingleDoFinally, initWithIoReactivexSingleSource_withIoReactivexFunctionsAction_, source, onFinally)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsSingleSingleDoFinally)

@implementation IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver

- (instancetype __nonnull)initWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)actual
                             withIoReactivexFunctionsAction:(id<IoReactivexFunctionsAction>)onFinally {
  IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsAction_(self, actual, onFinally);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_validateWithIoReactivexDisposablesDisposable_withIoReactivexDisposablesDisposable_(self->upstream_, d)) {
    JreStrongAssign(&self->upstream_, d);
    [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onSuccessWithId:(id)t {
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSuccessWithId:t];
  [self runFinally];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)t {
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:t];
  [self runFinally];
}

- (void)dispose {
  [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) dispose];
  [self runFinally];
}

- (jboolean)isDisposed {
  return [((id<IoReactivexDisposablesDisposable>) nil_chk(upstream_)) isDisposed];
}

- (void)runFinally {
  if ([self compareAndSetWithInt:0 withInt:1]) {
    @try {
      [((id<IoReactivexFunctionsAction>) nil_chk(onFinally_)) run];
    }
    @catch (JavaLangThrowable *ex) {
      IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
      IoReactivexPluginsRxJavaPlugins_onErrorWithJavaLangThrowable_(ex);
    }
  }
}

- (jboolean)isEqual:(id)obj {
  return self == obj;
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(onFinally_);
  RELEASE_(upstream_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x0, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexSingleObserver:withIoReactivexFunctionsAction:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onSuccessWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(dispose);
  methods[5].selector = @selector(isDisposed);
  methods[6].selector = @selector(runFinally);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LIoReactivexSingleObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "onFinally_", "LIoReactivexFunctionsAction;", .constantValue.asLong = 0, 0x10, -1, -1, -1, -1 },
    { "upstream_", "LIoReactivexDisposablesDisposable;", .constantValue.asLong = 0, 0x0, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexSingleObserver;LIoReactivexFunctionsAction;", "(Lio/reactivex/SingleObserver<-TT;>;Lio/reactivex/functions/Action;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onSuccess", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/SingleObserver<-TT;>;", "LIoReactivexInternalOperatorsSingleSingleDoFinally;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicInteger;Lio/reactivex/SingleObserver<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver = { "DoFinallyObserver", "io.reactivex.internal.operators.single", ptrTable, methods, fields, 7, 0x18, 7, 4, 10, -1, -1, 11, -1 };
  return &_IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver;
}

@end

void IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsAction_(IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver *self, id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsAction> onFinally) {
  JavaUtilConcurrentAtomicAtomicInteger_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->onFinally_, onFinally);
}

IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver *new_IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsAction_(id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsAction> onFinally) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver, initWithIoReactivexSingleObserver_withIoReactivexFunctionsAction_, actual, onFinally)
}

IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver *create_IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsAction_(id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsAction> onFinally) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver, initWithIoReactivexSingleObserver_withIoReactivexFunctionsAction_, actual, onFinally)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsSingleSingleDoFinally_DoFinallyObserver)
