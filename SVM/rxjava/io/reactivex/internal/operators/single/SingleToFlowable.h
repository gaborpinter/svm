//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/single/SingleToFlowable.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleToFlowable")
#ifdef RESTRICT_IoReactivexInternalOperatorsSingleSingleToFlowable
#define INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleToFlowable 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleToFlowable 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsSingleSingleToFlowable

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsSingleSingleToFlowable_) && (INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleToFlowable || defined(INCLUDE_IoReactivexInternalOperatorsSingleSingleToFlowable))
#define IoReactivexInternalOperatorsSingleSingleToFlowable_

#define RESTRICT_IoReactivexFlowable 1
#define INCLUDE_IoReactivexFlowable 1
#include "io/reactivex/Flowable.h"

@protocol IoReactivexSingleSource;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsSingleSingleToFlowable : IoReactivexFlowable {
 @public
  id<IoReactivexSingleSource> source_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexSingleSource:(id<IoReactivexSingleSource>)source;

- (void)subscribeActualWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)s;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsSingleSingleToFlowable)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsSingleSingleToFlowable, source_, id<IoReactivexSingleSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsSingleSingleToFlowable_initWithIoReactivexSingleSource_(IoReactivexInternalOperatorsSingleSingleToFlowable *self, id<IoReactivexSingleSource> source);

FOUNDATION_EXPORT IoReactivexInternalOperatorsSingleSingleToFlowable *new_IoReactivexInternalOperatorsSingleSingleToFlowable_initWithIoReactivexSingleSource_(id<IoReactivexSingleSource> source) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsSingleSingleToFlowable *create_IoReactivexInternalOperatorsSingleSingleToFlowable_initWithIoReactivexSingleSource_(id<IoReactivexSingleSource> source);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsSingleSingleToFlowable)

#endif

#if !defined (IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver_) && (INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleToFlowable || defined(INCLUDE_IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver))
#define IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver_

#define RESTRICT_IoReactivexInternalSubscriptionsDeferredScalarSubscription 1
#define INCLUDE_IoReactivexInternalSubscriptionsDeferredScalarSubscription 1
#include "io/reactivex/internal/subscriptions/DeferredScalarSubscription.h"

#define RESTRICT_IoReactivexSingleObserver 1
#define INCLUDE_IoReactivexSingleObserver 1
#include "io/reactivex/SingleObserver.h"

@class JavaLangThrowable;
@protocol IoReactivexDisposablesDisposable;
@protocol OrgReactivestreamsSubscriber;

@interface IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver : IoReactivexInternalSubscriptionsDeferredScalarSubscription < IoReactivexSingleObserver > {
 @public
  id<IoReactivexDisposablesDisposable> upstream_;
}

#pragma mark Public

- (void)cancel;

- (NSUInteger)hash;

- (jboolean)isEqual:(id)obj;

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e;

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d;

- (void)onSuccessWithId:(id)value;

#pragma mark Package-Private

- (instancetype __nonnull)initWithOrgReactivestreamsSubscriber:(id<OrgReactivestreamsSubscriber>)downstream;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver, upstream_, id<IoReactivexDisposablesDisposable>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver_initWithOrgReactivestreamsSubscriber_(IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver *self, id<OrgReactivestreamsSubscriber> downstream);

FOUNDATION_EXPORT IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver *new_IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver *create_IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver_initWithOrgReactivestreamsSubscriber_(id<OrgReactivestreamsSubscriber> downstream);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsSingleSingleToFlowable_SingleToFlowableObserver)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleToFlowable")
