//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/single/SingleFromUnsafeSource.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleFromUnsafeSource")
#ifdef RESTRICT_IoReactivexInternalOperatorsSingleSingleFromUnsafeSource
#define INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleFromUnsafeSource 0
#else
#define INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleFromUnsafeSource 1
#endif
#undef RESTRICT_IoReactivexInternalOperatorsSingleSingleFromUnsafeSource

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexInternalOperatorsSingleSingleFromUnsafeSource_) && (INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleFromUnsafeSource || defined(INCLUDE_IoReactivexInternalOperatorsSingleSingleFromUnsafeSource))
#define IoReactivexInternalOperatorsSingleSingleFromUnsafeSource_

#define RESTRICT_IoReactivexSingle 1
#define INCLUDE_IoReactivexSingle 1
#include "io/reactivex/Single.h"

@protocol IoReactivexSingleObserver;
@protocol IoReactivexSingleSource;

@interface IoReactivexInternalOperatorsSingleSingleFromUnsafeSource : IoReactivexSingle {
 @public
  id<IoReactivexSingleSource> source_;
}

#pragma mark Public

- (instancetype __nonnull)initWithIoReactivexSingleSource:(id<IoReactivexSingleSource>)source;

#pragma mark Protected

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexInternalOperatorsSingleSingleFromUnsafeSource)

J2OBJC_FIELD_SETTER(IoReactivexInternalOperatorsSingleSingleFromUnsafeSource, source_, id<IoReactivexSingleSource>)

FOUNDATION_EXPORT void IoReactivexInternalOperatorsSingleSingleFromUnsafeSource_initWithIoReactivexSingleSource_(IoReactivexInternalOperatorsSingleSingleFromUnsafeSource *self, id<IoReactivexSingleSource> source);

FOUNDATION_EXPORT IoReactivexInternalOperatorsSingleSingleFromUnsafeSource *new_IoReactivexInternalOperatorsSingleSingleFromUnsafeSource_initWithIoReactivexSingleSource_(id<IoReactivexSingleSource> source) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT IoReactivexInternalOperatorsSingleSingleFromUnsafeSource *create_IoReactivexInternalOperatorsSingleSingleFromUnsafeSource_initWithIoReactivexSingleSource_(id<IoReactivexSingleSource> source);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexInternalOperatorsSingleSingleFromUnsafeSource)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexInternalOperatorsSingleSingleFromUnsafeSource")
