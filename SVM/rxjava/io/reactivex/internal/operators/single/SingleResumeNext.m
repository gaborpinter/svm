//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/internal/operators/single/SingleResumeNext.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "io/reactivex/Single.h"
#include "io/reactivex/SingleObserver.h"
#include "io/reactivex/SingleSource.h"
#include "io/reactivex/disposables/Disposable.h"
#include "io/reactivex/exceptions/CompositeException.h"
#include "io/reactivex/exceptions/Exceptions.h"
#include "io/reactivex/functions/Function.h"
#include "io/reactivex/internal/disposables/DisposableHelper.h"
#include "io/reactivex/internal/functions/ObjectHelper.h"
#include "io/reactivex/internal/observers/ResumeSingleObserver.h"
#include "io/reactivex/internal/operators/single/SingleResumeNext.h"
#include "java/lang/Throwable.h"
#include "java/util/concurrent/atomic/AtomicReference.h"

#pragma clang diagnostic ignored "-Wincomplete-implementation"

inline jlong IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver_get_serialVersionUID(void);
#define IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver_serialVersionUID -5314538511045349925LL
J2OBJC_STATIC_FIELD_CONSTANT(IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver, serialVersionUID, jlong)

@implementation IoReactivexInternalOperatorsSingleSingleResumeNext

- (instancetype __nonnull)initWithIoReactivexSingleSource:(id<IoReactivexSingleSource>)source
                         withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)nextFunction {
  IoReactivexInternalOperatorsSingleSingleResumeNext_initWithIoReactivexSingleSource_withIoReactivexFunctionsFunction_(self, source, nextFunction);
  return self;
}

- (void)subscribeActualWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)observer {
  [((id<IoReactivexSingleSource>) nil_chk(source_)) subscribeWithIoReactivexSingleObserver:create_IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_(observer, nextFunction_)];
}

- (void)dealloc {
  RELEASE_(source_);
  RELEASE_(nextFunction_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, 4, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexSingleSource:withIoReactivexFunctionsFunction:);
  methods[1].selector = @selector(subscribeActualWithIoReactivexSingleObserver:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "source_", "LIoReactivexSingleSource;", .constantValue.asLong = 0, 0x10, -1, -1, 5, -1 },
    { "nextFunction_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 6, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexSingleSource;LIoReactivexFunctionsFunction;", "(Lio/reactivex/SingleSource<+TT;>;Lio/reactivex/functions/Function<-Ljava/lang/Throwable;+Lio/reactivex/SingleSource<+TT;>;>;)V", "subscribeActual", "LIoReactivexSingleObserver;", "(Lio/reactivex/SingleObserver<-TT;>;)V", "Lio/reactivex/SingleSource<+TT;>;", "Lio/reactivex/functions/Function<-Ljava/lang/Throwable;+Lio/reactivex/SingleSource<+TT;>;>;", "LIoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver;", "<T:Ljava/lang/Object;>Lio/reactivex/Single<TT;>;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsSingleSingleResumeNext = { "SingleResumeNext", "io.reactivex.internal.operators.single", ptrTable, methods, fields, 7, 0x11, 2, 2, -1, 7, -1, 8, -1 };
  return &_IoReactivexInternalOperatorsSingleSingleResumeNext;
}

@end

void IoReactivexInternalOperatorsSingleSingleResumeNext_initWithIoReactivexSingleSource_withIoReactivexFunctionsFunction_(IoReactivexInternalOperatorsSingleSingleResumeNext *self, id<IoReactivexSingleSource> source, id<IoReactivexFunctionsFunction> nextFunction) {
  IoReactivexSingle_init(self);
  JreStrongAssign(&self->source_, source);
  JreStrongAssign(&self->nextFunction_, nextFunction);
}

IoReactivexInternalOperatorsSingleSingleResumeNext *new_IoReactivexInternalOperatorsSingleSingleResumeNext_initWithIoReactivexSingleSource_withIoReactivexFunctionsFunction_(id<IoReactivexSingleSource> source, id<IoReactivexFunctionsFunction> nextFunction) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsSingleSingleResumeNext, initWithIoReactivexSingleSource_withIoReactivexFunctionsFunction_, source, nextFunction)
}

IoReactivexInternalOperatorsSingleSingleResumeNext *create_IoReactivexInternalOperatorsSingleSingleResumeNext_initWithIoReactivexSingleSource_withIoReactivexFunctionsFunction_(id<IoReactivexSingleSource> source, id<IoReactivexFunctionsFunction> nextFunction) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsSingleSingleResumeNext, initWithIoReactivexSingleSource_withIoReactivexFunctionsFunction_, source, nextFunction)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsSingleSingleResumeNext)

@implementation IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver

- (instancetype __nonnull)initWithIoReactivexSingleObserver:(id<IoReactivexSingleObserver>)actual
                           withIoReactivexFunctionsFunction:(id<IoReactivexFunctionsFunction>)nextFunction {
  IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_(self, actual, nextFunction);
  return self;
}

- (void)onSubscribeWithIoReactivexDisposablesDisposable:(id<IoReactivexDisposablesDisposable>)d {
  if (IoReactivexInternalDisposablesDisposableHelper_setOnceWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexDisposablesDisposable_(self, d)) {
    [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSubscribeWithIoReactivexDisposablesDisposable:self];
  }
}

- (void)onSuccessWithId:(id)value {
  [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onSuccessWithId:value];
}

- (void)onErrorWithJavaLangThrowable:(JavaLangThrowable *)e {
  id<IoReactivexSingleSource> source;
  @try {
    source = IoReactivexInternalFunctionsObjectHelper_requireNonNullWithId_withNSString_([((id<IoReactivexFunctionsFunction>) nil_chk(nextFunction_)) applyWithId:e], @"The nextFunction returned a null SingleSource.");
  }
  @catch (JavaLangThrowable *ex) {
    IoReactivexExceptionsExceptions_throwIfFatalWithJavaLangThrowable_(ex);
    [((id<IoReactivexSingleObserver>) nil_chk(downstream_)) onErrorWithJavaLangThrowable:create_IoReactivexExceptionsCompositeException_initWithJavaLangThrowableArray_([IOSObjectArray arrayWithObjects:(id[]){ e, ex } count:2 type:JavaLangThrowable_class_()])];
    return;
  }
  [((id<IoReactivexSingleSource>) nil_chk(source)) subscribeWithIoReactivexSingleObserver:create_IoReactivexInternalObserversResumeSingleObserver_initWithJavaUtilConcurrentAtomicAtomicReference_withIoReactivexSingleObserver_(self, downstream_)];
}

- (void)dispose {
  IoReactivexInternalDisposablesDisposableHelper_disposeWithJavaUtilConcurrentAtomicAtomicReference_(self);
}

- (jboolean)isDisposed {
  return IoReactivexInternalDisposablesDisposableHelper_isDisposedWithIoReactivexDisposablesDisposable_([self get]);
}

- (void)dealloc {
  RELEASE_(downstream_);
  RELEASE_(nextFunction_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, 1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, 6, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithIoReactivexSingleObserver:withIoReactivexFunctionsFunction:);
  methods[1].selector = @selector(onSubscribeWithIoReactivexDisposablesDisposable:);
  methods[2].selector = @selector(onSuccessWithId:);
  methods[3].selector = @selector(onErrorWithJavaLangThrowable:);
  methods[4].selector = @selector(dispose);
  methods[5].selector = @selector(isDisposed);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "downstream_", "LIoReactivexSingleObserver;", .constantValue.asLong = 0, 0x10, -1, -1, 9, -1 },
    { "nextFunction_", "LIoReactivexFunctionsFunction;", .constantValue.asLong = 0, 0x10, -1, -1, 10, -1 },
  };
  static const void *ptrTable[] = { "LIoReactivexSingleObserver;LIoReactivexFunctionsFunction;", "(Lio/reactivex/SingleObserver<-TT;>;Lio/reactivex/functions/Function<-Ljava/lang/Throwable;+Lio/reactivex/SingleSource<+TT;>;>;)V", "onSubscribe", "LIoReactivexDisposablesDisposable;", "onSuccess", "LNSObject;", "(TT;)V", "onError", "LJavaLangThrowable;", "Lio/reactivex/SingleObserver<-TT;>;", "Lio/reactivex/functions/Function<-Ljava/lang/Throwable;+Lio/reactivex/SingleSource<+TT;>;>;", "LIoReactivexInternalOperatorsSingleSingleResumeNext;", "<T:Ljava/lang/Object;>Ljava/util/concurrent/atomic/AtomicReference<Lio/reactivex/disposables/Disposable;>;Lio/reactivex/SingleObserver<TT;>;Lio/reactivex/disposables/Disposable;" };
  static const J2ObjcClassInfo _IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver = { "ResumeMainSingleObserver", "io.reactivex.internal.operators.single", ptrTable, methods, fields, 7, 0x18, 6, 3, 11, -1, -1, 12, -1 };
  return &_IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver;
}

@end

void IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_(IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver *self, id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsFunction> nextFunction) {
  JavaUtilConcurrentAtomicAtomicReference_init(self);
  JreStrongAssign(&self->downstream_, actual);
  JreStrongAssign(&self->nextFunction_, nextFunction);
}

IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver *new_IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_(id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsFunction> nextFunction) {
  J2OBJC_NEW_IMPL(IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver, initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_, actual, nextFunction)
}

IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver *create_IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver_initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_(id<IoReactivexSingleObserver> actual, id<IoReactivexFunctionsFunction> nextFunction) {
  J2OBJC_CREATE_IMPL(IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver, initWithIoReactivexSingleObserver_withIoReactivexFunctionsFunction_, actual, nextFunction)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(IoReactivexInternalOperatorsSingleSingleResumeNext_ResumeMainSingleObserver)
