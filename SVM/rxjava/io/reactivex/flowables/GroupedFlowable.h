//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/rxjava/io/reactivex/flowables/GroupedFlowable.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_IoReactivexFlowablesGroupedFlowable")
#ifdef RESTRICT_IoReactivexFlowablesGroupedFlowable
#define INCLUDE_ALL_IoReactivexFlowablesGroupedFlowable 0
#else
#define INCLUDE_ALL_IoReactivexFlowablesGroupedFlowable 1
#endif
#undef RESTRICT_IoReactivexFlowablesGroupedFlowable

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (IoReactivexFlowablesGroupedFlowable_) && (INCLUDE_ALL_IoReactivexFlowablesGroupedFlowable || defined(INCLUDE_IoReactivexFlowablesGroupedFlowable))
#define IoReactivexFlowablesGroupedFlowable_

#define RESTRICT_IoReactivexFlowable 1
#define INCLUDE_IoReactivexFlowable 1
#include "io/reactivex/Flowable.h"

@interface IoReactivexFlowablesGroupedFlowable : IoReactivexFlowable {
 @public
  id key_;
}

#pragma mark Public

- (id __nullable)getKey;

#pragma mark Protected

- (instancetype __nonnull)initWithId:(id __nullable)key;

@end

J2OBJC_EMPTY_STATIC_INIT(IoReactivexFlowablesGroupedFlowable)

J2OBJC_FIELD_SETTER(IoReactivexFlowablesGroupedFlowable, key_, id)

FOUNDATION_EXPORT void IoReactivexFlowablesGroupedFlowable_initWithId_(IoReactivexFlowablesGroupedFlowable *self, id key);

J2OBJC_TYPE_LITERAL_HEADER(IoReactivexFlowablesGroupedFlowable)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_IoReactivexFlowablesGroupedFlowable")
