//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/src/main/java/com/scolvo/core/vm/plugin/AccessTokenHandler.java
//

#include "J2ObjC_source.h"
#include "com/google/gson/JsonElement.h"
#include "com/google/gson/JsonObject.h"
#include "com/google/gson/JsonParser.h"
#include "com/scolvo/core/plugin/http/HttpResponse.h"
#include "com/scolvo/core/plugin/http/RequestMethod.h"
#include "com/scolvo/core/vm/api/Authentication.h"
#include "com/scolvo/core/vm/api/Logger.h"
#include "com/scolvo/core/vm/model/AuthToken.h"
#include "com/scolvo/core/vm/plugin/AccessTokenHandler.h"
#include "com/scolvo/core/vm/plugin/HttpClientProvider.h"
#include "java/io/IOException.h"
#include "java/lang/NullPointerException.h"
#include "java/util/LinkedHashMap.h"
#include "java/util/Map.h"

@interface ComScolvoCoreVmPluginAccessTokenHandler () {
 @public
  id<ComScolvoCoreVmApiAuthentication> auth_;
  ComScolvoCoreVmApiLogger *logger_;
  ComScolvoCoreVmPluginHttpClientProvider *httpClient_;
}

- (jboolean)communicationWithJavaUtilMap:(id<JavaUtilMap>)params;

- (void)addBasicParamsWithJavaUtilMap:(id<JavaUtilMap>)params;

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreVmPluginAccessTokenHandler, auth_, id<ComScolvoCoreVmApiAuthentication>)
J2OBJC_FIELD_SETTER(ComScolvoCoreVmPluginAccessTokenHandler, logger_, ComScolvoCoreVmApiLogger *)
J2OBJC_FIELD_SETTER(ComScolvoCoreVmPluginAccessTokenHandler, httpClient_, ComScolvoCoreVmPluginHttpClientProvider *)

__attribute__((unused)) static jboolean ComScolvoCoreVmPluginAccessTokenHandler_communicationWithJavaUtilMap_(ComScolvoCoreVmPluginAccessTokenHandler *self, id<JavaUtilMap> params);

__attribute__((unused)) static void ComScolvoCoreVmPluginAccessTokenHandler_addBasicParamsWithJavaUtilMap_(ComScolvoCoreVmPluginAccessTokenHandler *self, id<JavaUtilMap> params);

@implementation ComScolvoCoreVmPluginAccessTokenHandler

- (instancetype __nonnull)initWithComScolvoCoreVmApiLogger:(ComScolvoCoreVmApiLogger *)logger
                      withComScolvoCoreVmApiAuthentication:(id<ComScolvoCoreVmApiAuthentication>)auth
               withComScolvoCoreVmPluginHttpClientProvider:(ComScolvoCoreVmPluginHttpClientProvider *)httpClient {
  ComScolvoCoreVmPluginAccessTokenHandler_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmApiAuthentication_withComScolvoCoreVmPluginHttpClientProvider_(self, logger, auth, httpClient);
  return self;
}

- (jboolean)grantAccessTokenWithNSString:(NSString *)username
                            withNSString:(NSString *)password {
  id<JavaUtilMap> params = create_JavaUtilLinkedHashMap_init();
  [params putWithId:@"grant_type" withId:@"password"];
  [params putWithId:@"username" withId:username];
  [params putWithId:@"password" withId:password];
  @try {
    return ComScolvoCoreVmPluginAccessTokenHandler_communicationWithJavaUtilMap_(self, params);
  }
  @catch (JavaIoIOException *e) {
    [e printStackTrace];
    return false;
  }
}

- (jboolean)refreshAccessTokenWithNSString:(NSString *)refreshToken {
  id<JavaUtilMap> params = create_JavaUtilLinkedHashMap_init();
  [params putWithId:@"grant_type" withId:@"refresh_token"];
  [params putWithId:@"refresh_token" withId:refreshToken];
  @try {
    return ComScolvoCoreVmPluginAccessTokenHandler_communicationWithJavaUtilMap_(self, params);
  }
  @catch (JavaIoIOException *e) {
    [e printStackTrace];
    return false;
  }
}

- (jboolean)communicationWithJavaUtilMap:(id<JavaUtilMap>)params {
  return ComScolvoCoreVmPluginAccessTokenHandler_communicationWithJavaUtilMap_(self, params);
}

- (void)addBasicParamsWithJavaUtilMap:(id<JavaUtilMap>)params {
  ComScolvoCoreVmPluginAccessTokenHandler_addBasicParamsWithJavaUtilMap_(self, params);
}

- (ComScolvoCoreVmModelAuthToken *)convertTokenStringToAuthWithNSString:(NSString *)json {
  ComGoogleGsonJsonObject *jobject = [((ComGoogleGsonJsonElement *) nil_chk([create_ComGoogleGsonJsonParser_init() parseWithNSString:json])) getAsJsonObject];
  @try {
    return create_ComScolvoCoreVmModelAuthToken_initWithNSString_withNSString_([((ComGoogleGsonJsonElement *) nil_chk([((ComGoogleGsonJsonObject *) nil_chk(jobject)) getWithNSString:@"access_token"])) getAsString], [((ComGoogleGsonJsonElement *) nil_chk([jobject getWithNSString:@"refresh_token"])) getAsString]);
  }
  @catch (JavaLangNullPointerException *e) {
    return nil;
  }
}

- (void)dealloc {
  RELEASE_(jks_path_);
  RELEASE_(auth_);
  RELEASE_(logger_);
  RELEASE_(httpClient_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "Z", 0x2, 5, 6, 7, 8, -1, -1 },
    { NULL, "V", 0x2, 9, 6, -1, 10, -1, -1 },
    { NULL, "LComScolvoCoreVmModelAuthToken;", 0x4, 11, 4, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithComScolvoCoreVmApiLogger:withComScolvoCoreVmApiAuthentication:withComScolvoCoreVmPluginHttpClientProvider:);
  methods[1].selector = @selector(grantAccessTokenWithNSString:withNSString:);
  methods[2].selector = @selector(refreshAccessTokenWithNSString:);
  methods[3].selector = @selector(communicationWithJavaUtilMap:);
  methods[4].selector = @selector(addBasicParamsWithJavaUtilMap:);
  methods[5].selector = @selector(convertTokenStringToAuthWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "jks_path_", "LNSString;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "auth_", "LComScolvoCoreVmApiAuthentication;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "logger_", "LComScolvoCoreVmApiLogger;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "httpClient_", "LComScolvoCoreVmPluginHttpClientProvider;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LComScolvoCoreVmApiLogger;LComScolvoCoreVmApiAuthentication;LComScolvoCoreVmPluginHttpClientProvider;", "grantAccessToken", "LNSString;LNSString;", "refreshAccessToken", "LNSString;", "communication", "LJavaUtilMap;", "LJavaIoIOException;", "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)Z", "addBasicParams", "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)V", "convertTokenStringToAuth" };
  static const J2ObjcClassInfo _ComScolvoCoreVmPluginAccessTokenHandler = { "AccessTokenHandler", "com.scolvo.core.vm.plugin", ptrTable, methods, fields, 7, 0x1, 6, 4, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreVmPluginAccessTokenHandler;
}

@end

void ComScolvoCoreVmPluginAccessTokenHandler_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmApiAuthentication_withComScolvoCoreVmPluginHttpClientProvider_(ComScolvoCoreVmPluginAccessTokenHandler *self, ComScolvoCoreVmApiLogger *logger, id<ComScolvoCoreVmApiAuthentication> auth, ComScolvoCoreVmPluginHttpClientProvider *httpClient) {
  NSObject_init(self);
  JreStrongAssign(&self->auth_, auth);
  JreStrongAssign(&self->httpClient_, httpClient);
  JreStrongAssign(&self->logger_, logger);
}

ComScolvoCoreVmPluginAccessTokenHandler *new_ComScolvoCoreVmPluginAccessTokenHandler_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmApiAuthentication_withComScolvoCoreVmPluginHttpClientProvider_(ComScolvoCoreVmApiLogger *logger, id<ComScolvoCoreVmApiAuthentication> auth, ComScolvoCoreVmPluginHttpClientProvider *httpClient) {
  J2OBJC_NEW_IMPL(ComScolvoCoreVmPluginAccessTokenHandler, initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmApiAuthentication_withComScolvoCoreVmPluginHttpClientProvider_, logger, auth, httpClient)
}

ComScolvoCoreVmPluginAccessTokenHandler *create_ComScolvoCoreVmPluginAccessTokenHandler_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmApiAuthentication_withComScolvoCoreVmPluginHttpClientProvider_(ComScolvoCoreVmApiLogger *logger, id<ComScolvoCoreVmApiAuthentication> auth, ComScolvoCoreVmPluginHttpClientProvider *httpClient) {
  J2OBJC_CREATE_IMPL(ComScolvoCoreVmPluginAccessTokenHandler, initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmApiAuthentication_withComScolvoCoreVmPluginHttpClientProvider_, logger, auth, httpClient)
}

jboolean ComScolvoCoreVmPluginAccessTokenHandler_communicationWithJavaUtilMap_(ComScolvoCoreVmPluginAccessTokenHandler *self, id<JavaUtilMap> params) {
  id<JavaUtilMap> config = create_JavaUtilLinkedHashMap_init();
  [config putWithId:@"url" withId:[((id<ComScolvoCoreVmApiAuthentication>) nil_chk(self->auth_)) getAuthUrl]];
  [config putWithId:@"requestMethod" withId:JreLoadEnum(ComScolvoCorePluginHttpRequestMethod, POST)];
  [config putWithId:@"contentType" withId:@"application/x-www-form-urlencoded"];
  ComScolvoCoreVmPluginAccessTokenHandler_addBasicParamsWithJavaUtilMap_(self, params);
  [config putWithId:@"postInput" withId:params];
  id<JavaUtilMap> headerParams = create_JavaUtilLinkedHashMap_init();
  [headerParams putWithId:@"User-Agent" withId:@"Mozilla/5.0 ( compatible ) "];
  [headerParams putWithId:@"Accept" withId:@"*/*"];
  [config putWithId:@"headerParams" withId:headerParams];
  ComScolvoCorePluginHttpHttpResponse *response = [((ComScolvoCoreVmPluginHttpClientProvider *) nil_chk(self->httpClient_)) httpCallWithJavaUtilMap:config];
  if ([((ComScolvoCorePluginHttpHttpResponse *) nil_chk(response)) getStatusCode] > 299) {
    [((ComScolvoCoreVmApiLogger *) nil_chk(self->logger_)) warnWithNSString:JreStrcat("$@", @"HTTP communication error: ", response)];
    return false;
  }
  ComScolvoCoreVmModelAuthToken *authToken = [self convertTokenStringToAuthWithNSString:(NSString *) cast_chk([response getData], [NSString class])];
  if (authToken != nil) {
    [((id<ComScolvoCoreVmApiAuthentication>) nil_chk(self->auth_)) storeTokenWithNSString:[authToken getAccessToken] withNSString:[authToken getRefreshToken]];
  }
  return authToken != nil;
}

void ComScolvoCoreVmPluginAccessTokenHandler_addBasicParamsWithJavaUtilMap_(ComScolvoCoreVmPluginAccessTokenHandler *self, id<JavaUtilMap> params) {
  [((id<JavaUtilMap>) nil_chk(params)) putWithId:@"client_id" withId:[((id<ComScolvoCoreVmApiAuthentication>) nil_chk(self->auth_)) getClientId]];
  [params putWithId:@"client_secret" withId:[((id<ComScolvoCoreVmApiAuthentication>) nil_chk(self->auth_)) getClientSecret]];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreVmPluginAccessTokenHandler)
