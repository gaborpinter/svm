//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/src/main/java/com/scolvo/core/vm/plugin/HttpClientProvider.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/interpreter/symbols/BuiltinFunctionSymbol.h"
#include "com/scolvo/core/interpreter/symbols/SymbolTable.h"
#include "com/scolvo/core/plugin/http/HttpClient.h"
#include "com/scolvo/core/plugin/http/HttpResponse.h"
#include "com/scolvo/core/vm/plugin/HttpClientProvider.h"
#include "java/lang/System.h"
#include "java/util/Map.h"

@interface ComScolvoCoreVmPluginHttpClientProvider () {
 @public
  ComScolvoCorePluginHttpHttpClient *httpClient_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreVmPluginHttpClientProvider, httpClient_, ComScolvoCorePluginHttpHttpClient *)

inline jlong ComScolvoCoreVmPluginHttpClientProvider_get_serialVersionUID(void);
#define ComScolvoCoreVmPluginHttpClientProvider_serialVersionUID -4619991343498912299LL
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCoreVmPluginHttpClientProvider, serialVersionUID, jlong)

@implementation ComScolvoCoreVmPluginHttpClientProvider

- (instancetype __nonnull)initWithComScolvoCorePluginHttpHttpClient:(ComScolvoCorePluginHttpHttpClient *)httpClient
                                                       withNSString:(NSString *)keystorePath {
  ComScolvoCoreVmPluginHttpClientProvider_initWithComScolvoCorePluginHttpHttpClient_withNSString_(self, httpClient, keystorePath);
  return self;
}

- (void)register__WithComScolvoCoreInterpreterSymbolsSymbolTable:(ComScolvoCoreInterpreterSymbolsSymbolTable *)symbolTable {
  [((ComScolvoCoreInterpreterSymbolsSymbolTable *) nil_chk(symbolTable)) defineWithComScolvoCoreInterpreterSymbolsSymbol:create_ComScolvoCoreInterpreterSymbolsBuiltinFunctionSymbol_initWithNSString_withId_withNSString_(@"httpCall", self, @"httpCall")];
}

- (ComScolvoCorePluginHttpHttpResponse *)httpCallWithJavaUtilMap:(id<JavaUtilMap>)configJson {
  return [((ComScolvoCorePluginHttpHttpClient *) nil_chk(httpClient_)) callWithJavaUtilMap:configJson];
}

- (void)dealloc {
  RELEASE_(httpClient_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "LComScolvoCorePluginHttpHttpResponse;", 0x1, 3, 4, -1, 5, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithComScolvoCorePluginHttpHttpClient:withNSString:);
  methods[1].selector = @selector(register__WithComScolvoCoreInterpreterSymbolsSymbolTable:);
  methods[2].selector = @selector(httpCallWithJavaUtilMap:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = ComScolvoCoreVmPluginHttpClientProvider_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "httpClient_", "LComScolvoCorePluginHttpHttpClient;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LComScolvoCorePluginHttpHttpClient;LNSString;", "register", "LComScolvoCoreInterpreterSymbolsSymbolTable;", "httpCall", "LJavaUtilMap;", "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)Lcom/scolvo/core/plugin/http/HttpResponse;" };
  static const J2ObjcClassInfo _ComScolvoCoreVmPluginHttpClientProvider = { "HttpClientProvider", "com.scolvo.core.vm.plugin", ptrTable, methods, fields, 7, 0x1, 3, 2, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreVmPluginHttpClientProvider;
}

@end

void ComScolvoCoreVmPluginHttpClientProvider_initWithComScolvoCorePluginHttpHttpClient_withNSString_(ComScolvoCoreVmPluginHttpClientProvider *self, ComScolvoCorePluginHttpHttpClient *httpClient, NSString *keystorePath) {
  NSObject_init(self);
  JavaLangSystem_setPropertyWithNSString_withNSString_(@"javax.net.ssl.trustStore", keystorePath);
  JavaLangSystem_setPropertyWithNSString_withNSString_(@"javax.net.ssl.trustStoreType", @"jks");
  JreStrongAssign(&self->httpClient_, httpClient);
}

ComScolvoCoreVmPluginHttpClientProvider *new_ComScolvoCoreVmPluginHttpClientProvider_initWithComScolvoCorePluginHttpHttpClient_withNSString_(ComScolvoCorePluginHttpHttpClient *httpClient, NSString *keystorePath) {
  J2OBJC_NEW_IMPL(ComScolvoCoreVmPluginHttpClientProvider, initWithComScolvoCorePluginHttpHttpClient_withNSString_, httpClient, keystorePath)
}

ComScolvoCoreVmPluginHttpClientProvider *create_ComScolvoCoreVmPluginHttpClientProvider_initWithComScolvoCorePluginHttpHttpClient_withNSString_(ComScolvoCorePluginHttpHttpClient *httpClient, NSString *keystorePath) {
  J2OBJC_CREATE_IMPL(ComScolvoCoreVmPluginHttpClientProvider, initWithComScolvoCorePluginHttpHttpClient_withNSString_, httpClient, keystorePath)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreVmPluginHttpClientProvider)
