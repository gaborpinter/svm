//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/src/main/java/com/scolvo/core/vm/plugin/AuthUserProvider.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "IOSPrimitiveArray.h"
#include "J2ObjC_source.h"
#include "com/google/gson/Gson.h"
#include "com/scolvo/core/interpreter/symbols/BuiltinFunctionSymbol.h"
#include "com/scolvo/core/interpreter/symbols/SymbolTable.h"
#include "com/scolvo/core/plugin/ScolvoGson.h"
#include "com/scolvo/core/plugin/base64/Base64.h"
#include "com/scolvo/core/vm/api/Authentication.h"
#include "com/scolvo/core/vm/api/Logger.h"
#include "com/scolvo/core/vm/plugin/AuthUserProvider.h"
#include "java/lang/Exception.h"
#include "java/util/HashMap.h"
#include "java/util/Map.h"

@interface ComScolvoCoreVmPluginAuthUserProvider () {
 @public
  id<ComScolvoCoreVmApiAuthentication> auth_;
  ComScolvoCoreVmApiLogger *logger_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreVmPluginAuthUserProvider, auth_, id<ComScolvoCoreVmApiAuthentication>)
J2OBJC_FIELD_SETTER(ComScolvoCoreVmPluginAuthUserProvider, logger_, ComScolvoCoreVmApiLogger *)

inline jlong ComScolvoCoreVmPluginAuthUserProvider_get_serialVersionUID(void);
#define ComScolvoCoreVmPluginAuthUserProvider_serialVersionUID -4619991343498912299LL
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCoreVmPluginAuthUserProvider, serialVersionUID, jlong)

@implementation ComScolvoCoreVmPluginAuthUserProvider

- (instancetype __nonnull)initWithComScolvoCoreVmApiAuthentication:(id<ComScolvoCoreVmApiAuthentication>)auth
                                      withComScolvoCoreVmApiLogger:(ComScolvoCoreVmApiLogger *)logger {
  ComScolvoCoreVmPluginAuthUserProvider_initWithComScolvoCoreVmApiAuthentication_withComScolvoCoreVmApiLogger_(self, auth, logger);
  return self;
}

- (void)register__WithComScolvoCoreInterpreterSymbolsSymbolTable:(ComScolvoCoreInterpreterSymbolsSymbolTable *)symbolTable {
  [((ComScolvoCoreInterpreterSymbolsSymbolTable *) nil_chk(symbolTable)) defineWithComScolvoCoreInterpreterSymbolsSymbol:create_ComScolvoCoreInterpreterSymbolsBuiltinFunctionSymbol_initWithNSString_withId_withNSString_(@"getCurrentUser", self, @"getCurrentUser")];
}

- (id<JavaUtilMap>)getCurrentUser {
  NSString *token = [((id<ComScolvoCoreVmApiAuthentication>) nil_chk(auth_)) getAccessToken];
  @try {
    NSString *decodedTokenBody = [NSString java_stringWithBytes:ComScolvoCorePluginBase64Base64_decodeBase64WithNSString_(IOSObjectArray_Get(nil_chk([((NSString *) nil_chk(token)) java_split:@"\\."]), 1))];
    return [((ComGoogleGsonGson *) nil_chk(JreLoadStatic(ComScolvoCorePluginScolvoGson, INSTANCE))) fromJsonWithNSString:decodedTokenBody withIOSClass:JavaUtilHashMap_class_()];
  }
  @catch (JavaLangException *e) {
    [((ComScolvoCoreVmApiLogger *) nil_chk(logger_)) warnWithNSString:JreStrcat("$$C$", @"Invalid token found ", token, ' ', [e getMessage])];
    return nil;
  }
}

- (void)dealloc {
  RELEASE_(auth_);
  RELEASE_(logger_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "LJavaUtilMap;", 0x1, -1, -1, -1, 3, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithComScolvoCoreVmApiAuthentication:withComScolvoCoreVmApiLogger:);
  methods[1].selector = @selector(register__WithComScolvoCoreInterpreterSymbolsSymbolTable:);
  methods[2].selector = @selector(getCurrentUser);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = ComScolvoCoreVmPluginAuthUserProvider_serialVersionUID, 0x1a, -1, -1, -1, -1 },
    { "auth_", "LComScolvoCoreVmApiAuthentication;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "logger_", "LComScolvoCoreVmApiLogger;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LComScolvoCoreVmApiAuthentication;LComScolvoCoreVmApiLogger;", "register", "LComScolvoCoreInterpreterSymbolsSymbolTable;", "()Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;" };
  static const J2ObjcClassInfo _ComScolvoCoreVmPluginAuthUserProvider = { "AuthUserProvider", "com.scolvo.core.vm.plugin", ptrTable, methods, fields, 7, 0x1, 3, 3, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreVmPluginAuthUserProvider;
}

@end

void ComScolvoCoreVmPluginAuthUserProvider_initWithComScolvoCoreVmApiAuthentication_withComScolvoCoreVmApiLogger_(ComScolvoCoreVmPluginAuthUserProvider *self, id<ComScolvoCoreVmApiAuthentication> auth, ComScolvoCoreVmApiLogger *logger) {
  NSObject_init(self);
  JreStrongAssign(&self->auth_, auth);
  JreStrongAssign(&self->logger_, logger);
}

ComScolvoCoreVmPluginAuthUserProvider *new_ComScolvoCoreVmPluginAuthUserProvider_initWithComScolvoCoreVmApiAuthentication_withComScolvoCoreVmApiLogger_(id<ComScolvoCoreVmApiAuthentication> auth, ComScolvoCoreVmApiLogger *logger) {
  J2OBJC_NEW_IMPL(ComScolvoCoreVmPluginAuthUserProvider, initWithComScolvoCoreVmApiAuthentication_withComScolvoCoreVmApiLogger_, auth, logger)
}

ComScolvoCoreVmPluginAuthUserProvider *create_ComScolvoCoreVmPluginAuthUserProvider_initWithComScolvoCoreVmApiAuthentication_withComScolvoCoreVmApiLogger_(id<ComScolvoCoreVmApiAuthentication> auth, ComScolvoCoreVmApiLogger *logger) {
  J2OBJC_CREATE_IMPL(ComScolvoCoreVmPluginAuthUserProvider, initWithComScolvoCoreVmApiAuthentication_withComScolvoCoreVmApiLogger_, auth, logger)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreVmPluginAuthUserProvider)
