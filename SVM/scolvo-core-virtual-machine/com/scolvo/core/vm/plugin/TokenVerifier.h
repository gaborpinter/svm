//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/src/main/java/com/scolvo/core/vm/plugin/TokenVerifier.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreVmPluginTokenVerifier")
#ifdef RESTRICT_ComScolvoCoreVmPluginTokenVerifier
#define INCLUDE_ALL_ComScolvoCoreVmPluginTokenVerifier 0
#else
#define INCLUDE_ALL_ComScolvoCoreVmPluginTokenVerifier 1
#endif
#undef RESTRICT_ComScolvoCoreVmPluginTokenVerifier

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreVmPluginTokenVerifier_) && (INCLUDE_ALL_ComScolvoCoreVmPluginTokenVerifier || defined(INCLUDE_ComScolvoCoreVmPluginTokenVerifier))
#define ComScolvoCoreVmPluginTokenVerifier_

@class IOSObjectArray;
@class JavaUtilDate;
@protocol JavaUtilList;

@interface ComScolvoCoreVmPluginTokenVerifier : NSObject

#pragma mark Public

- (instancetype __nonnull)init;

- (id<JavaUtilList>)getRolesWithNSString:(NSString *)token;

- (NSString *)getUsernameWithNSString:(NSString *)token;

- (void)isAllowedWithNSString:(NSString *)accessToken
            withNSStringArray:(IOSObjectArray *)roles;

- (jboolean)isTokenExpiredWithNSString:(NSString *)token
                      withJavaUtilDate:(JavaUtilDate *)date;

- (void)validateTokenWithNSString:(NSString *)token;

#pragma mark Protected

- (NSString *)decodeBase64WithNSString:(NSString *)string;

- (void)isAllowedWithNSString:(NSString *)accessToken
            withNSStringArray:(IOSObjectArray *)roles
             withJavaUtilDate:(JavaUtilDate *)date;

- (void)matchPermissionWithNSString:(NSString *)token
                  withNSStringArray:(IOSObjectArray *)roles;

- (void)validateTokenWithNSString:(NSString *)token
                 withJavaUtilDate:(JavaUtilDate *)date;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreVmPluginTokenVerifier)

FOUNDATION_EXPORT void ComScolvoCoreVmPluginTokenVerifier_init(ComScolvoCoreVmPluginTokenVerifier *self);

FOUNDATION_EXPORT ComScolvoCoreVmPluginTokenVerifier *new_ComScolvoCoreVmPluginTokenVerifier_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreVmPluginTokenVerifier *create_ComScolvoCoreVmPluginTokenVerifier_init(void);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreVmPluginTokenVerifier)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreVmPluginTokenVerifier")
