//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/src/main/java/com/scolvo/core/vm/plugin/AuthenticationException.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/vm/plugin/AuthenticationException.h"
#include "java/lang/Exception.h"

inline jlong ComScolvoCoreVmPluginAuthenticationException_get_serialVersionUID(void);
#define ComScolvoCoreVmPluginAuthenticationException_serialVersionUID 3870864468503864017LL
J2OBJC_STATIC_FIELD_CONSTANT(ComScolvoCoreVmPluginAuthenticationException, serialVersionUID, jlong)

@implementation ComScolvoCoreVmPluginAuthenticationException

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype __nonnull)init {
  ComScolvoCoreVmPluginAuthenticationException_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "serialVersionUID", "J", .constantValue.asLong = ComScolvoCoreVmPluginAuthenticationException_serialVersionUID, 0x1a, -1, -1, -1, -1 },
  };
  static const J2ObjcClassInfo _ComScolvoCoreVmPluginAuthenticationException = { "AuthenticationException", "com.scolvo.core.vm.plugin", NULL, methods, fields, 7, 0x401, 1, 1, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreVmPluginAuthenticationException;
}

@end

void ComScolvoCoreVmPluginAuthenticationException_init(ComScolvoCoreVmPluginAuthenticationException *self) {
  JavaLangException_init(self);
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreVmPluginAuthenticationException)
