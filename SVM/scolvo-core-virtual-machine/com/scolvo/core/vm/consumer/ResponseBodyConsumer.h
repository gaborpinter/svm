//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/src/main/java/com/scolvo/core/vm/consumer/ResponseBodyConsumer.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreVmConsumerResponseBodyConsumer")
#ifdef RESTRICT_ComScolvoCoreVmConsumerResponseBodyConsumer
#define INCLUDE_ALL_ComScolvoCoreVmConsumerResponseBodyConsumer 0
#else
#define INCLUDE_ALL_ComScolvoCoreVmConsumerResponseBodyConsumer 1
#endif
#undef RESTRICT_ComScolvoCoreVmConsumerResponseBodyConsumer

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreVmConsumerResponseBodyConsumer_) && (INCLUDE_ALL_ComScolvoCoreVmConsumerResponseBodyConsumer || defined(INCLUDE_ComScolvoCoreVmConsumerResponseBodyConsumer))
#define ComScolvoCoreVmConsumerResponseBodyConsumer_

#define RESTRICT_IoReactivexFunctionsConsumer 1
#define INCLUDE_IoReactivexFunctionsConsumer 1
#include "io/reactivex/functions/Consumer.h"

@class ComScolvoCoreVmApiLogger;
@class ComScolvoCoreVmApiTypeDefinitionRepository;
@class ComScolvoCoreVmPluginHttpClientProvider;
@class ComScolvoCoreVmResourceAwsS3Service;
@class ComScolvoCoreVmServiceBroadcastService;
@protocol ComScolvoCoreVmActionRpResponseBody;
@protocol ComScolvoCoreVmApiAuthentication;
@protocol ComScolvoCoreVmApiResourceRepository;
@protocol ComScolvoCoreVmInterpreterState;

@interface ComScolvoCoreVmConsumerResponseBodyConsumer : NSObject < IoReactivexFunctionsConsumer >

#pragma mark Public

- (instancetype __nonnull)initWithComScolvoCoreVmApiLogger:(ComScolvoCoreVmApiLogger *)logger
                      withComScolvoCoreVmApiAuthentication:(id<ComScolvoCoreVmApiAuthentication>)auth
                withComScolvoCoreVmServiceBroadcastService:(ComScolvoCoreVmServiceBroadcastService *)broadcastService
            withComScolvoCoreVmApiTypeDefinitionRepository:(ComScolvoCoreVmApiTypeDefinitionRepository *)dataRepo
               withComScolvoCoreVmPluginHttpClientProvider:(ComScolvoCoreVmPluginHttpClientProvider *)httpClient
                       withComScolvoCoreVmInterpreterState:(id<ComScolvoCoreVmInterpreterState>)interpreterState
                  withComScolvoCoreVmApiResourceRepository:(id<ComScolvoCoreVmApiResourceRepository>)repo
                   withComScolvoCoreVmResourceAwsS3Service:(ComScolvoCoreVmResourceAwsS3Service *)awsS3Service;

- (void)acceptWithId:(id<ComScolvoCoreVmActionRpResponseBody>)response;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreVmConsumerResponseBodyConsumer)

FOUNDATION_EXPORT void ComScolvoCoreVmConsumerResponseBodyConsumer_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmApiAuthentication_withComScolvoCoreVmServiceBroadcastService_withComScolvoCoreVmApiTypeDefinitionRepository_withComScolvoCoreVmPluginHttpClientProvider_withComScolvoCoreVmInterpreterState_withComScolvoCoreVmApiResourceRepository_withComScolvoCoreVmResourceAwsS3Service_(ComScolvoCoreVmConsumerResponseBodyConsumer *self, ComScolvoCoreVmApiLogger *logger, id<ComScolvoCoreVmApiAuthentication> auth, ComScolvoCoreVmServiceBroadcastService *broadcastService, ComScolvoCoreVmApiTypeDefinitionRepository *dataRepo, ComScolvoCoreVmPluginHttpClientProvider *httpClient, id<ComScolvoCoreVmInterpreterState> interpreterState, id<ComScolvoCoreVmApiResourceRepository> repo, ComScolvoCoreVmResourceAwsS3Service *awsS3Service);

FOUNDATION_EXPORT ComScolvoCoreVmConsumerResponseBodyConsumer *new_ComScolvoCoreVmConsumerResponseBodyConsumer_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmApiAuthentication_withComScolvoCoreVmServiceBroadcastService_withComScolvoCoreVmApiTypeDefinitionRepository_withComScolvoCoreVmPluginHttpClientProvider_withComScolvoCoreVmInterpreterState_withComScolvoCoreVmApiResourceRepository_withComScolvoCoreVmResourceAwsS3Service_(ComScolvoCoreVmApiLogger *logger, id<ComScolvoCoreVmApiAuthentication> auth, ComScolvoCoreVmServiceBroadcastService *broadcastService, ComScolvoCoreVmApiTypeDefinitionRepository *dataRepo, ComScolvoCoreVmPluginHttpClientProvider *httpClient, id<ComScolvoCoreVmInterpreterState> interpreterState, id<ComScolvoCoreVmApiResourceRepository> repo, ComScolvoCoreVmResourceAwsS3Service *awsS3Service) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreVmConsumerResponseBodyConsumer *create_ComScolvoCoreVmConsumerResponseBodyConsumer_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmApiAuthentication_withComScolvoCoreVmServiceBroadcastService_withComScolvoCoreVmApiTypeDefinitionRepository_withComScolvoCoreVmPluginHttpClientProvider_withComScolvoCoreVmInterpreterState_withComScolvoCoreVmApiResourceRepository_withComScolvoCoreVmResourceAwsS3Service_(ComScolvoCoreVmApiLogger *logger, id<ComScolvoCoreVmApiAuthentication> auth, ComScolvoCoreVmServiceBroadcastService *broadcastService, ComScolvoCoreVmApiTypeDefinitionRepository *dataRepo, ComScolvoCoreVmPluginHttpClientProvider *httpClient, id<ComScolvoCoreVmInterpreterState> interpreterState, id<ComScolvoCoreVmApiResourceRepository> repo, ComScolvoCoreVmResourceAwsS3Service *awsS3Service);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreVmConsumerResponseBodyConsumer)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreVmConsumerResponseBodyConsumer")
