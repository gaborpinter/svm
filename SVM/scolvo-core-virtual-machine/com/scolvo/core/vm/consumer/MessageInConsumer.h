//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/src/main/java/com/scolvo/core/vm/consumer/MessageInConsumer.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreVmConsumerMessageInConsumer")
#ifdef RESTRICT_ComScolvoCoreVmConsumerMessageInConsumer
#define INCLUDE_ALL_ComScolvoCoreVmConsumerMessageInConsumer 0
#else
#define INCLUDE_ALL_ComScolvoCoreVmConsumerMessageInConsumer 1
#endif
#undef RESTRICT_ComScolvoCoreVmConsumerMessageInConsumer

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreVmConsumerMessageInConsumer_) && (INCLUDE_ALL_ComScolvoCoreVmConsumerMessageInConsumer || defined(INCLUDE_ComScolvoCoreVmConsumerMessageInConsumer))
#define ComScolvoCoreVmConsumerMessageInConsumer_

#define RESTRICT_IoReactivexFunctionsConsumer 1
#define INCLUDE_IoReactivexFunctionsConsumer 1
#include "io/reactivex/functions/Consumer.h"

@class ComScolvoCoreVmActionMessageIn;
@class ComScolvoCoreVmServiceBroadcastService;

@interface ComScolvoCoreVmConsumerMessageInConsumer : NSObject < IoReactivexFunctionsConsumer >

#pragma mark Public

- (instancetype __nonnull)initWithComScolvoCoreVmServiceBroadcastService:(ComScolvoCoreVmServiceBroadcastService *)broadcast;

- (void)acceptWithId:(ComScolvoCoreVmActionMessageIn *)msg;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreVmConsumerMessageInConsumer)

FOUNDATION_EXPORT void ComScolvoCoreVmConsumerMessageInConsumer_initWithComScolvoCoreVmServiceBroadcastService_(ComScolvoCoreVmConsumerMessageInConsumer *self, ComScolvoCoreVmServiceBroadcastService *broadcast);

FOUNDATION_EXPORT ComScolvoCoreVmConsumerMessageInConsumer *new_ComScolvoCoreVmConsumerMessageInConsumer_initWithComScolvoCoreVmServiceBroadcastService_(ComScolvoCoreVmServiceBroadcastService *broadcast) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreVmConsumerMessageInConsumer *create_ComScolvoCoreVmConsumerMessageInConsumer_initWithComScolvoCoreVmServiceBroadcastService_(ComScolvoCoreVmServiceBroadcastService *broadcast);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreVmConsumerMessageInConsumer)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreVmConsumerMessageInConsumer")
