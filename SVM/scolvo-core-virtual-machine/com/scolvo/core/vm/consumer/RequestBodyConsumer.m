//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/src/main/java/com/scolvo/core/vm/consumer/RequestBodyConsumer.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "com/google/gson/Gson.h"
#include "com/scolvo/core/plugin/ScolvoGson.h"
#include "com/scolvo/core/vm/action/msg/MessageToStore.h"
#include "com/scolvo/core/vm/action/rq/Request.h"
#include "com/scolvo/core/vm/action/rq/RequestBody.h"
#include "com/scolvo/core/vm/action/rq/RequestHeader.h"
#include "com/scolvo/core/vm/api/Device.h"
#include "com/scolvo/core/vm/api/Logger.h"
#include "com/scolvo/core/vm/consumer/RequestBodyConsumer.h"
#include "com/scolvo/core/vm/service/BroadcastService.h"
#include "java/lang/Long.h"
#include "java/lang/System.h"

@interface ComScolvoCoreVmConsumerRequestBodyConsumer () {
 @public
  ComScolvoCoreVmApiLogger *logger_;
  id<ComScolvoCoreVmApiDevice> device_;
  ComScolvoCoreVmServiceBroadcastService *broadcastService_;
}

- (ComScolvoCoreVmActionRqRequest *)buildRequestHeadWithNSString:(NSString *)messageType
                                                    withNSString:(NSString *)originId;

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreVmConsumerRequestBodyConsumer, logger_, ComScolvoCoreVmApiLogger *)
J2OBJC_FIELD_SETTER(ComScolvoCoreVmConsumerRequestBodyConsumer, device_, id<ComScolvoCoreVmApiDevice>)
J2OBJC_FIELD_SETTER(ComScolvoCoreVmConsumerRequestBodyConsumer, broadcastService_, ComScolvoCoreVmServiceBroadcastService *)

__attribute__((unused)) static ComScolvoCoreVmActionRqRequest *ComScolvoCoreVmConsumerRequestBodyConsumer_buildRequestHeadWithNSString_withNSString_(ComScolvoCoreVmConsumerRequestBodyConsumer *self, NSString *messageType, NSString *originId);

@implementation ComScolvoCoreVmConsumerRequestBodyConsumer

- (instancetype __nonnull)initWithComScolvoCoreVmApiLogger:(ComScolvoCoreVmApiLogger *)logger
                withComScolvoCoreVmServiceBroadcastService:(ComScolvoCoreVmServiceBroadcastService *)broadcastService
                              withComScolvoCoreVmApiDevice:(id<ComScolvoCoreVmApiDevice>)device {
  ComScolvoCoreVmConsumerRequestBodyConsumer_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmServiceBroadcastService_withComScolvoCoreVmApiDevice_(self, logger, broadcastService, device);
  return self;
}

- (void)acceptWithId:(id<ComScolvoCoreVmActionRqRequestBody>)body {
  ComScolvoCoreVmActionRqRequest *request = ComScolvoCoreVmConsumerRequestBodyConsumer_buildRequestHeadWithNSString_withNSString_(self, [[((id<ComScolvoCoreVmActionRqRequestBody>) nil_chk(body)) java_getClass] getSimpleName], [body getOriginId]);
  [((ComScolvoCoreVmActionRqRequest *) nil_chk(request)) setBodyWithComScolvoCoreVmActionRqRequestBody:body];
  [((ComScolvoCoreVmApiLogger *) nil_chk(logger_)) infoWithNSString:JreStrcat("$$", @"Send message to central: ", [((ComScolvoCoreVmActionRqRequestHeader *) nil_chk([request getHeader])) getMessageType])];
  [((ComScolvoCoreVmServiceBroadcastService *) nil_chk(broadcastService_)) publishActionWithId:create_ComScolvoCoreVmActionMsgMessageToStore_initWithNSString_([((ComGoogleGsonGson *) nil_chk(JreLoadStatic(ComScolvoCorePluginScolvoGson, INSTANCE))) toJsonWithId:request])];
}

- (ComScolvoCoreVmActionRqRequest *)buildRequestHeadWithNSString:(NSString *)messageType
                                                    withNSString:(NSString *)originId {
  return ComScolvoCoreVmConsumerRequestBodyConsumer_buildRequestHeadWithNSString_withNSString_(self, messageType, originId);
}

- (void)dealloc {
  RELEASE_(logger_);
  RELEASE_(device_);
  RELEASE_(broadcastService_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, 3, -1, -1, -1 },
    { NULL, "LComScolvoCoreVmActionRqRequest;", 0x2, 4, 5, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithComScolvoCoreVmApiLogger:withComScolvoCoreVmServiceBroadcastService:withComScolvoCoreVmApiDevice:);
  methods[1].selector = @selector(acceptWithId:);
  methods[2].selector = @selector(buildRequestHeadWithNSString:withNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "logger_", "LComScolvoCoreVmApiLogger;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "device_", "LComScolvoCoreVmApiDevice;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "broadcastService_", "LComScolvoCoreVmServiceBroadcastService;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LComScolvoCoreVmApiLogger;LComScolvoCoreVmServiceBroadcastService;LComScolvoCoreVmApiDevice;", "accept", "LComScolvoCoreVmActionRqRequestBody;", "LJavaLangException;", "buildRequestHead", "LNSString;LNSString;", "Ljava/lang/Object;Lio/reactivex/functions/Consumer<Lcom/scolvo/core/vm/action/rq/RequestBody;>;" };
  static const J2ObjcClassInfo _ComScolvoCoreVmConsumerRequestBodyConsumer = { "RequestBodyConsumer", "com.scolvo.core.vm.consumer", ptrTable, methods, fields, 7, 0x1, 3, 3, -1, -1, -1, 6, -1 };
  return &_ComScolvoCoreVmConsumerRequestBodyConsumer;
}

@end

void ComScolvoCoreVmConsumerRequestBodyConsumer_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmServiceBroadcastService_withComScolvoCoreVmApiDevice_(ComScolvoCoreVmConsumerRequestBodyConsumer *self, ComScolvoCoreVmApiLogger *logger, ComScolvoCoreVmServiceBroadcastService *broadcastService, id<ComScolvoCoreVmApiDevice> device) {
  NSObject_init(self);
  JreStrongAssign(&self->logger_, logger);
  JreStrongAssign(&self->broadcastService_, broadcastService);
  JreStrongAssign(&self->device_, device);
}

ComScolvoCoreVmConsumerRequestBodyConsumer *new_ComScolvoCoreVmConsumerRequestBodyConsumer_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmServiceBroadcastService_withComScolvoCoreVmApiDevice_(ComScolvoCoreVmApiLogger *logger, ComScolvoCoreVmServiceBroadcastService *broadcastService, id<ComScolvoCoreVmApiDevice> device) {
  J2OBJC_NEW_IMPL(ComScolvoCoreVmConsumerRequestBodyConsumer, initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmServiceBroadcastService_withComScolvoCoreVmApiDevice_, logger, broadcastService, device)
}

ComScolvoCoreVmConsumerRequestBodyConsumer *create_ComScolvoCoreVmConsumerRequestBodyConsumer_initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmServiceBroadcastService_withComScolvoCoreVmApiDevice_(ComScolvoCoreVmApiLogger *logger, ComScolvoCoreVmServiceBroadcastService *broadcastService, id<ComScolvoCoreVmApiDevice> device) {
  J2OBJC_CREATE_IMPL(ComScolvoCoreVmConsumerRequestBodyConsumer, initWithComScolvoCoreVmApiLogger_withComScolvoCoreVmServiceBroadcastService_withComScolvoCoreVmApiDevice_, logger, broadcastService, device)
}

ComScolvoCoreVmActionRqRequest *ComScolvoCoreVmConsumerRequestBodyConsumer_buildRequestHeadWithNSString_withNSString_(ComScolvoCoreVmConsumerRequestBodyConsumer *self, NSString *messageType, NSString *originId) {
  ComScolvoCoreVmActionRqRequestHeader *header = create_ComScolvoCoreVmActionRqRequestHeader_init();
  [header setClientIdWithNSString:[((id<ComScolvoCoreVmApiDevice>) nil_chk(self->device_)) getDeviceId]];
  [header setCreatedWithJavaLangLong:JavaLangLong_valueOfWithLong_(JavaLangSystem_currentTimeMillis())];
  [header setIfModifiedSinceWithJavaLangLong:JavaLangLong_valueOfWithLong_(0LL)];
  [header setMessageIdWithNSString:originId];
  [header setMessageTypeWithNSString:messageType];
  ComScolvoCoreVmActionRqRequest *configRequest = create_ComScolvoCoreVmActionRqRequest_init();
  [configRequest setHeaderWithComScolvoCoreVmActionRqRequestHeader:header];
  return configRequest;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreVmConsumerRequestBodyConsumer)
