//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/symbols/TypeSymbol.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreInterpreterSymbolsTypeSymbol")
#ifdef RESTRICT_ComScolvoCoreInterpreterSymbolsTypeSymbol
#define INCLUDE_ALL_ComScolvoCoreInterpreterSymbolsTypeSymbol 0
#else
#define INCLUDE_ALL_ComScolvoCoreInterpreterSymbolsTypeSymbol 1
#endif
#undef RESTRICT_ComScolvoCoreInterpreterSymbolsTypeSymbol

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreInterpreterSymbolsTypeSymbol_) && (INCLUDE_ALL_ComScolvoCoreInterpreterSymbolsTypeSymbol || defined(INCLUDE_ComScolvoCoreInterpreterSymbolsTypeSymbol))
#define ComScolvoCoreInterpreterSymbolsTypeSymbol_

#define RESTRICT_ComScolvoCoreInterpreterSymbolsSymbol 1
#define INCLUDE_ComScolvoCoreInterpreterSymbolsSymbol 1
#include "com/scolvo/core/interpreter/symbols/Symbol.h"

@interface ComScolvoCoreInterpreterSymbolsTypeSymbol : NSObject < ComScolvoCoreInterpreterSymbolsSymbol >

#pragma mark Public

- (instancetype __nonnull)initWithNSString:(NSString *)name;

- (NSString *)getName;

- (NSString *)description;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreInterpreterSymbolsTypeSymbol)

FOUNDATION_EXPORT void ComScolvoCoreInterpreterSymbolsTypeSymbol_initWithNSString_(ComScolvoCoreInterpreterSymbolsTypeSymbol *self, NSString *name);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreInterpreterSymbolsTypeSymbol)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreInterpreterSymbolsTypeSymbol")
