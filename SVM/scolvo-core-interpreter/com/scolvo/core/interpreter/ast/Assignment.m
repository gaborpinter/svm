//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/Assignment.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/interpreter/Token.h"
#include "com/scolvo/core/interpreter/ast/ASTNode.h"
#include "com/scolvo/core/interpreter/ast/Assignment.h"
#include "com/scolvo/core/interpreter/ast/Variable.h"
#include "com/scolvo/core/interpreter/visitors/Visitor.h"

@interface ComScolvoCoreInterpreterAstAssignment () {
 @public
  ComScolvoCoreInterpreterAstVariable *variable_;
  ComScolvoCoreInterpreterAstASTNode *expression_;
  ComScolvoCoreInterpreterToken *assignOp_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreInterpreterAstAssignment, variable_, ComScolvoCoreInterpreterAstVariable *)
J2OBJC_FIELD_SETTER(ComScolvoCoreInterpreterAstAssignment, expression_, ComScolvoCoreInterpreterAstASTNode *)
J2OBJC_FIELD_SETTER(ComScolvoCoreInterpreterAstAssignment, assignOp_, ComScolvoCoreInterpreterToken *)

@implementation ComScolvoCoreInterpreterAstAssignment

- (instancetype __nonnull)initWithComScolvoCoreInterpreterAstVariable:(ComScolvoCoreInterpreterAstVariable *)variable
                                    withComScolvoCoreInterpreterToken:(ComScolvoCoreInterpreterToken *)assignOp
                               withComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)expression {
  ComScolvoCoreInterpreterAstAssignment_initWithComScolvoCoreInterpreterAstVariable_withComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterAstASTNode_(self, variable, assignOp, expression);
  return self;
}

- (ComScolvoCoreInterpreterAstVariable *)getVariable {
  return variable_;
}

- (ComScolvoCoreInterpreterToken *)getAssignOp {
  return assignOp_;
}

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor {
  [((id<ComScolvoCoreInterpreterVisitorsVisitor>) nil_chk(visitor)) visitWithComScolvoCoreInterpreterAstAssignment:self];
}

- (ComScolvoCoreInterpreterAstASTNode *)getExpression {
  return expression_;
}

- (void)dealloc {
  RELEASE_(variable_);
  RELEASE_(expression_);
  RELEASE_(assignOp_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreInterpreterAstVariable;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreInterpreterToken;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreInterpreterAstASTNode;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithComScolvoCoreInterpreterAstVariable:withComScolvoCoreInterpreterToken:withComScolvoCoreInterpreterAstASTNode:);
  methods[1].selector = @selector(getVariable);
  methods[2].selector = @selector(getAssignOp);
  methods[3].selector = @selector(acceptWithComScolvoCoreInterpreterVisitorsVisitor:);
  methods[4].selector = @selector(getExpression);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "variable_", "LComScolvoCoreInterpreterAstVariable;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "expression_", "LComScolvoCoreInterpreterAstASTNode;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "assignOp_", "LComScolvoCoreInterpreterToken;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LComScolvoCoreInterpreterAstVariable;LComScolvoCoreInterpreterToken;LComScolvoCoreInterpreterAstASTNode;", "accept", "LComScolvoCoreInterpreterVisitorsVisitor;" };
  static const J2ObjcClassInfo _ComScolvoCoreInterpreterAstAssignment = { "Assignment", "com.scolvo.core.interpreter.ast", ptrTable, methods, fields, 7, 0x1, 5, 3, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreInterpreterAstAssignment;
}

@end

void ComScolvoCoreInterpreterAstAssignment_initWithComScolvoCoreInterpreterAstVariable_withComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterAstAssignment *self, ComScolvoCoreInterpreterAstVariable *variable, ComScolvoCoreInterpreterToken *assignOp, ComScolvoCoreInterpreterAstASTNode *expression) {
  ComScolvoCoreInterpreterAstASTNode_init(self);
  JreStrongAssign(&self->variable_, variable);
  JreStrongAssign(&self->assignOp_, assignOp);
  JreStrongAssign(&self->expression_, expression);
}

ComScolvoCoreInterpreterAstAssignment *new_ComScolvoCoreInterpreterAstAssignment_initWithComScolvoCoreInterpreterAstVariable_withComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterAstVariable *variable, ComScolvoCoreInterpreterToken *assignOp, ComScolvoCoreInterpreterAstASTNode *expression) {
  J2OBJC_NEW_IMPL(ComScolvoCoreInterpreterAstAssignment, initWithComScolvoCoreInterpreterAstVariable_withComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterAstASTNode_, variable, assignOp, expression)
}

ComScolvoCoreInterpreterAstAssignment *create_ComScolvoCoreInterpreterAstAssignment_initWithComScolvoCoreInterpreterAstVariable_withComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterAstVariable *variable, ComScolvoCoreInterpreterToken *assignOp, ComScolvoCoreInterpreterAstASTNode *expression) {
  J2OBJC_CREATE_IMPL(ComScolvoCoreInterpreterAstAssignment, initWithComScolvoCoreInterpreterAstVariable_withComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterAstASTNode_, variable, assignOp, expression)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreInterpreterAstAssignment)
