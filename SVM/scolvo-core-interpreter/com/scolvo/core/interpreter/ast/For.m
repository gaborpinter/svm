//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/For.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/interpreter/ast/ASTNode.h"
#include "com/scolvo/core/interpreter/ast/For.h"
#include "com/scolvo/core/interpreter/visitors/Visitor.h"

@interface ComScolvoCoreInterpreterAstFor () {
 @public
  ComScolvoCoreInterpreterAstASTNode *init__;
  ComScolvoCoreInterpreterAstASTNode *condition_;
  ComScolvoCoreInterpreterAstASTNode *update_;
  ComScolvoCoreInterpreterAstASTNode *statement_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreInterpreterAstFor, init__, ComScolvoCoreInterpreterAstASTNode *)
J2OBJC_FIELD_SETTER(ComScolvoCoreInterpreterAstFor, condition_, ComScolvoCoreInterpreterAstASTNode *)
J2OBJC_FIELD_SETTER(ComScolvoCoreInterpreterAstFor, update_, ComScolvoCoreInterpreterAstASTNode *)
J2OBJC_FIELD_SETTER(ComScolvoCoreInterpreterAstFor, statement_, ComScolvoCoreInterpreterAstASTNode *)

@implementation ComScolvoCoreInterpreterAstFor

- (instancetype __nonnull)initWithComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)init_
                              withComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)condition
                              withComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)update
                              withComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)statement {
  ComScolvoCoreInterpreterAstFor_initWithComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_(self, init_, condition, update, statement);
  return self;
}

- (ComScolvoCoreInterpreterAstASTNode *)getInit {
  return init__;
}

- (ComScolvoCoreInterpreterAstASTNode *)getCondition {
  return condition_;
}

- (ComScolvoCoreInterpreterAstASTNode *)getUpdate {
  return update_;
}

- (ComScolvoCoreInterpreterAstASTNode *)getStatement {
  return statement_;
}

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor {
  [((id<ComScolvoCoreInterpreterVisitorsVisitor>) nil_chk(visitor)) visitWithComScolvoCoreInterpreterAstFor:self];
}

- (void)dealloc {
  RELEASE_(init__);
  RELEASE_(condition_);
  RELEASE_(update_);
  RELEASE_(statement_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreInterpreterAstASTNode;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreInterpreterAstASTNode;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreInterpreterAstASTNode;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreInterpreterAstASTNode;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithComScolvoCoreInterpreterAstASTNode:withComScolvoCoreInterpreterAstASTNode:withComScolvoCoreInterpreterAstASTNode:withComScolvoCoreInterpreterAstASTNode:);
  methods[1].selector = @selector(getInit);
  methods[2].selector = @selector(getCondition);
  methods[3].selector = @selector(getUpdate);
  methods[4].selector = @selector(getStatement);
  methods[5].selector = @selector(acceptWithComScolvoCoreInterpreterVisitorsVisitor:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "init__", "LComScolvoCoreInterpreterAstASTNode;", .constantValue.asLong = 0, 0x2, 3, -1, -1, -1 },
    { "condition_", "LComScolvoCoreInterpreterAstASTNode;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "update_", "LComScolvoCoreInterpreterAstASTNode;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "statement_", "LComScolvoCoreInterpreterAstASTNode;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LComScolvoCoreInterpreterAstASTNode;LComScolvoCoreInterpreterAstASTNode;LComScolvoCoreInterpreterAstASTNode;LComScolvoCoreInterpreterAstASTNode;", "accept", "LComScolvoCoreInterpreterVisitorsVisitor;", "init" };
  static const J2ObjcClassInfo _ComScolvoCoreInterpreterAstFor = { "For", "com.scolvo.core.interpreter.ast", ptrTable, methods, fields, 7, 0x1, 6, 4, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreInterpreterAstFor;
}

@end

void ComScolvoCoreInterpreterAstFor_initWithComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterAstFor *self, ComScolvoCoreInterpreterAstASTNode *init_, ComScolvoCoreInterpreterAstASTNode *condition, ComScolvoCoreInterpreterAstASTNode *update, ComScolvoCoreInterpreterAstASTNode *statement) {
  ComScolvoCoreInterpreterAstASTNode_init(self);
  JreStrongAssign(&self->init__, init_);
  JreStrongAssign(&self->condition_, condition);
  JreStrongAssign(&self->update_, update);
  JreStrongAssign(&self->statement_, statement);
}

ComScolvoCoreInterpreterAstFor *new_ComScolvoCoreInterpreterAstFor_initWithComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterAstASTNode *init_, ComScolvoCoreInterpreterAstASTNode *condition, ComScolvoCoreInterpreterAstASTNode *update, ComScolvoCoreInterpreterAstASTNode *statement) {
  J2OBJC_NEW_IMPL(ComScolvoCoreInterpreterAstFor, initWithComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_, init_, condition, update, statement)
}

ComScolvoCoreInterpreterAstFor *create_ComScolvoCoreInterpreterAstFor_initWithComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterAstASTNode *init_, ComScolvoCoreInterpreterAstASTNode *condition, ComScolvoCoreInterpreterAstASTNode *update, ComScolvoCoreInterpreterAstASTNode *statement) {
  J2OBJC_CREATE_IMPL(ComScolvoCoreInterpreterAstFor, initWithComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_, init_, condition, update, statement)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreInterpreterAstFor)
