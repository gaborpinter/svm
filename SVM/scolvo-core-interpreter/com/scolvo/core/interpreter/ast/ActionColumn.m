//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/ActionColumn.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/interpreter/ast/AbstractColumn.h"
#include "com/scolvo/core/interpreter/ast/ActionColumn.h"
#include "com/scolvo/core/interpreter/visitors/Visitor.h"
#include "java/util/List.h"

@interface ComScolvoCoreInterpreterAstActionColumn () {
 @public
  id<JavaUtilList> actions_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreInterpreterAstActionColumn, actions_, id<JavaUtilList>)

@implementation ComScolvoCoreInterpreterAstActionColumn

- (instancetype __nonnull)initWithJavaUtilList:(id<JavaUtilList>)actions {
  ComScolvoCoreInterpreterAstActionColumn_initWithJavaUtilList_(self, actions);
  return self;
}

- (id<JavaUtilList>)getActions {
  return actions_;
}

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor {
  [((id<ComScolvoCoreInterpreterVisitorsVisitor>) nil_chk(visitor)) visitWithComScolvoCoreInterpreterAstActionColumn:self];
}

- (void)dealloc {
  RELEASE_(actions_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "LJavaUtilList;", 0x1, -1, -1, -1, 2, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithJavaUtilList:);
  methods[1].selector = @selector(getActions);
  methods[2].selector = @selector(acceptWithComScolvoCoreInterpreterVisitorsVisitor:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "actions_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 5, -1 },
  };
  static const void *ptrTable[] = { "LJavaUtilList;", "(Ljava/util/List<Lcom/scolvo/core/interpreter/ast/ASTNode;>;)V", "()Ljava/util/List<Lcom/scolvo/core/interpreter/ast/ASTNode;>;", "accept", "LComScolvoCoreInterpreterVisitorsVisitor;", "Ljava/util/List<Lcom/scolvo/core/interpreter/ast/ASTNode;>;" };
  static const J2ObjcClassInfo _ComScolvoCoreInterpreterAstActionColumn = { "ActionColumn", "com.scolvo.core.interpreter.ast", ptrTable, methods, fields, 7, 0x1, 3, 1, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreInterpreterAstActionColumn;
}

@end

void ComScolvoCoreInterpreterAstActionColumn_initWithJavaUtilList_(ComScolvoCoreInterpreterAstActionColumn *self, id<JavaUtilList> actions) {
  ComScolvoCoreInterpreterAstAbstractColumn_init(self);
  JreStrongAssign(&self->actions_, actions);
}

ComScolvoCoreInterpreterAstActionColumn *new_ComScolvoCoreInterpreterAstActionColumn_initWithJavaUtilList_(id<JavaUtilList> actions) {
  J2OBJC_NEW_IMPL(ComScolvoCoreInterpreterAstActionColumn, initWithJavaUtilList_, actions)
}

ComScolvoCoreInterpreterAstActionColumn *create_ComScolvoCoreInterpreterAstActionColumn_initWithJavaUtilList_(id<JavaUtilList> actions) {
  J2OBJC_CREATE_IMPL(ComScolvoCoreInterpreterAstActionColumn, initWithJavaUtilList_, actions)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreInterpreterAstActionColumn)
