//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/ConstantValue.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstConstantValue")
#ifdef RESTRICT_ComScolvoCoreInterpreterAstConstantValue
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstConstantValue 0
#else
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstConstantValue 1
#endif
#undef RESTRICT_ComScolvoCoreInterpreterAstConstantValue

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreInterpreterAstConstantValue_) && (INCLUDE_ALL_ComScolvoCoreInterpreterAstConstantValue || defined(INCLUDE_ComScolvoCoreInterpreterAstConstantValue))
#define ComScolvoCoreInterpreterAstConstantValue_

#define RESTRICT_ComScolvoCoreInterpreterAstASTNode 1
#define INCLUDE_ComScolvoCoreInterpreterAstASTNode 1
#include "com/scolvo/core/interpreter/ast/ASTNode.h"

@class ComScolvoCoreInterpreterToken;
@class ComScolvoCoreInterpreterTokenType;
@protocol ComScolvoCoreInterpreterVisitorsVisitor;

@interface ComScolvoCoreInterpreterAstConstantValue : ComScolvoCoreInterpreterAstASTNode

#pragma mark Public

- (instancetype __nonnull)initWithComScolvoCoreInterpreterToken:(ComScolvoCoreInterpreterToken *)number
                          withComScolvoCoreInterpreterTokenType:(ComScolvoCoreInterpreterTokenType *)type;

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor;

- (ComScolvoCoreInterpreterTokenType *)getType;

- (id)getValue;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreInterpreterAstConstantValue)

FOUNDATION_EXPORT void ComScolvoCoreInterpreterAstConstantValue_initWithComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterTokenType_(ComScolvoCoreInterpreterAstConstantValue *self, ComScolvoCoreInterpreterToken *number, ComScolvoCoreInterpreterTokenType *type);

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstConstantValue *new_ComScolvoCoreInterpreterAstConstantValue_initWithComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterTokenType_(ComScolvoCoreInterpreterToken *number, ComScolvoCoreInterpreterTokenType *type) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstConstantValue *create_ComScolvoCoreInterpreterAstConstantValue_initWithComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterTokenType_(ComScolvoCoreInterpreterToken *number, ComScolvoCoreInterpreterTokenType *type);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreInterpreterAstConstantValue)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstConstantValue")
