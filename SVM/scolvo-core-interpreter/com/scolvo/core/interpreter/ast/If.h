//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/If.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstIf")
#ifdef RESTRICT_ComScolvoCoreInterpreterAstIf
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstIf 0
#else
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstIf 1
#endif
#undef RESTRICT_ComScolvoCoreInterpreterAstIf

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreInterpreterAstIf_) && (INCLUDE_ALL_ComScolvoCoreInterpreterAstIf || defined(INCLUDE_ComScolvoCoreInterpreterAstIf))
#define ComScolvoCoreInterpreterAstIf_

#define RESTRICT_ComScolvoCoreInterpreterAstASTNode 1
#define INCLUDE_ComScolvoCoreInterpreterAstASTNode 1
#include "com/scolvo/core/interpreter/ast/ASTNode.h"

@protocol ComScolvoCoreInterpreterVisitorsVisitor;

@interface ComScolvoCoreInterpreterAstIf : ComScolvoCoreInterpreterAstASTNode

#pragma mark Public

- (instancetype __nonnull)initWithComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)condition
                              withComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)thenStatement
                              withComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)elseStatement;

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor;

- (ComScolvoCoreInterpreterAstASTNode *)getCondition;

- (ComScolvoCoreInterpreterAstASTNode *)getElseStatement;

- (ComScolvoCoreInterpreterAstASTNode *)getThenStatement;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreInterpreterAstIf)

FOUNDATION_EXPORT void ComScolvoCoreInterpreterAstIf_initWithComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterAstIf *self, ComScolvoCoreInterpreterAstASTNode *condition, ComScolvoCoreInterpreterAstASTNode *thenStatement, ComScolvoCoreInterpreterAstASTNode *elseStatement);

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstIf *new_ComScolvoCoreInterpreterAstIf_initWithComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterAstASTNode *condition, ComScolvoCoreInterpreterAstASTNode *thenStatement, ComScolvoCoreInterpreterAstASTNode *elseStatement) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstIf *create_ComScolvoCoreInterpreterAstIf_initWithComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterAstASTNode *condition, ComScolvoCoreInterpreterAstASTNode *thenStatement, ComScolvoCoreInterpreterAstASTNode *elseStatement);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreInterpreterAstIf)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstIf")
