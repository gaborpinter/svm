//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/menu/Item.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstMenuItem")
#ifdef RESTRICT_ComScolvoCoreInterpreterAstMenuItem
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstMenuItem 0
#else
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstMenuItem 1
#endif
#undef RESTRICT_ComScolvoCoreInterpreterAstMenuItem

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreInterpreterAstMenuItem_) && (INCLUDE_ALL_ComScolvoCoreInterpreterAstMenuItem || defined(INCLUDE_ComScolvoCoreInterpreterAstMenuItem))
#define ComScolvoCoreInterpreterAstMenuItem_

#define RESTRICT_ComScolvoCoreInterpreterAstContent 1
#define INCLUDE_ComScolvoCoreInterpreterAstContent 1
#include "com/scolvo/core/interpreter/ast/Content.h"

@class ComScolvoCoreInterpreterToken;
@protocol ComScolvoCoreInterpreterVisitorsVisitor;
@protocol JavaUtilList;

@interface ComScolvoCoreInterpreterAstMenuItem : ComScolvoCoreInterpreterAstContent

#pragma mark Public

- (instancetype __nonnull)initWithComScolvoCoreInterpreterToken:(ComScolvoCoreInterpreterToken *)name;

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor;

- (NSString *)getName;

- (id<JavaUtilList>)getRoles;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreInterpreterAstMenuItem)

FOUNDATION_EXPORT void ComScolvoCoreInterpreterAstMenuItem_initWithComScolvoCoreInterpreterToken_(ComScolvoCoreInterpreterAstMenuItem *self, ComScolvoCoreInterpreterToken *name);

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstMenuItem *new_ComScolvoCoreInterpreterAstMenuItem_initWithComScolvoCoreInterpreterToken_(ComScolvoCoreInterpreterToken *name) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstMenuItem *create_ComScolvoCoreInterpreterAstMenuItem_initWithComScolvoCoreInterpreterToken_(ComScolvoCoreInterpreterToken *name);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreInterpreterAstMenuItem)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstMenuItem")
