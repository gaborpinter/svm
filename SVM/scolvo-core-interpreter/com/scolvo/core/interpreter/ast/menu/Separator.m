//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/menu/Separator.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/interpreter/Token.h"
#include "com/scolvo/core/interpreter/ast/menu/Item.h"
#include "com/scolvo/core/interpreter/ast/menu/Separator.h"
#include "com/scolvo/core/interpreter/visitors/Visitor.h"

@implementation ComScolvoCoreInterpreterAstMenuSeparator

- (instancetype __nonnull)initWithComScolvoCoreInterpreterToken:(ComScolvoCoreInterpreterToken *)name {
  ComScolvoCoreInterpreterAstMenuSeparator_initWithComScolvoCoreInterpreterToken_(self, name);
  return self;
}

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor {
  [((id<ComScolvoCoreInterpreterVisitorsVisitor>) nil_chk(visitor)) visitWithComScolvoCoreInterpreterAstMenuSeparator:self];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithComScolvoCoreInterpreterToken:);
  methods[1].selector = @selector(acceptWithComScolvoCoreInterpreterVisitorsVisitor:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "LComScolvoCoreInterpreterToken;", "accept", "LComScolvoCoreInterpreterVisitorsVisitor;" };
  static const J2ObjcClassInfo _ComScolvoCoreInterpreterAstMenuSeparator = { "Separator", "com.scolvo.core.interpreter.ast.menu", ptrTable, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreInterpreterAstMenuSeparator;
}

@end

void ComScolvoCoreInterpreterAstMenuSeparator_initWithComScolvoCoreInterpreterToken_(ComScolvoCoreInterpreterAstMenuSeparator *self, ComScolvoCoreInterpreterToken *name) {
  ComScolvoCoreInterpreterAstMenuItem_initWithComScolvoCoreInterpreterToken_(self, name);
}

ComScolvoCoreInterpreterAstMenuSeparator *new_ComScolvoCoreInterpreterAstMenuSeparator_initWithComScolvoCoreInterpreterToken_(ComScolvoCoreInterpreterToken *name) {
  J2OBJC_NEW_IMPL(ComScolvoCoreInterpreterAstMenuSeparator, initWithComScolvoCoreInterpreterToken_, name)
}

ComScolvoCoreInterpreterAstMenuSeparator *create_ComScolvoCoreInterpreterAstMenuSeparator_initWithComScolvoCoreInterpreterToken_(ComScolvoCoreInterpreterToken *name) {
  J2OBJC_CREATE_IMPL(ComScolvoCoreInterpreterAstMenuSeparator, initWithComScolvoCoreInterpreterToken_, name)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreInterpreterAstMenuSeparator)
