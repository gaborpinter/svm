//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/TypeProperty.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstTypeProperty")
#ifdef RESTRICT_ComScolvoCoreInterpreterAstTypeProperty
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstTypeProperty 0
#else
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstTypeProperty 1
#endif
#undef RESTRICT_ComScolvoCoreInterpreterAstTypeProperty

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreInterpreterAstTypeProperty_) && (INCLUDE_ALL_ComScolvoCoreInterpreterAstTypeProperty || defined(INCLUDE_ComScolvoCoreInterpreterAstTypeProperty))
#define ComScolvoCoreInterpreterAstTypeProperty_

#define RESTRICT_ComScolvoCoreInterpreterAstASTNode 1
#define INCLUDE_ComScolvoCoreInterpreterAstASTNode 1
#include "com/scolvo/core/interpreter/ast/ASTNode.h"

@class ComScolvoCoreInterpreterToken;
@protocol ComScolvoCoreInterpreterVisitorsVisitor;

@interface ComScolvoCoreInterpreterAstTypeProperty : ComScolvoCoreInterpreterAstASTNode

#pragma mark Public

- (instancetype __nonnull)initWithComScolvoCoreInterpreterToken:(ComScolvoCoreInterpreterToken *)propertyName
                              withComScolvoCoreInterpreterToken:(ComScolvoCoreInterpreterToken *)propertyType
                                                    withBoolean:(jboolean)required
                         withComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)propertyTypeParam1
                         withComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)propertyTypeParam2
                         withComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)minOccurs
                         withComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)maxOccurs;

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor;

- (ComScolvoCoreInterpreterAstASTNode *)getMaxOccurs;

- (ComScolvoCoreInterpreterAstASTNode *)getMinOccurs;

- (NSString *)getName;

- (ComScolvoCoreInterpreterToken *)getPropertyType;

- (ComScolvoCoreInterpreterAstASTNode *)getPropertyTypeParam1;

- (ComScolvoCoreInterpreterAstASTNode *)getPropertyTypeParam2;

- (jboolean)isRequired;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreInterpreterAstTypeProperty)

FOUNDATION_EXPORT void ComScolvoCoreInterpreterAstTypeProperty_initWithComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterToken_withBoolean_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterAstTypeProperty *self, ComScolvoCoreInterpreterToken *propertyName, ComScolvoCoreInterpreterToken *propertyType, jboolean required, ComScolvoCoreInterpreterAstASTNode *propertyTypeParam1, ComScolvoCoreInterpreterAstASTNode *propertyTypeParam2, ComScolvoCoreInterpreterAstASTNode *minOccurs, ComScolvoCoreInterpreterAstASTNode *maxOccurs);

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstTypeProperty *new_ComScolvoCoreInterpreterAstTypeProperty_initWithComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterToken_withBoolean_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterToken *propertyName, ComScolvoCoreInterpreterToken *propertyType, jboolean required, ComScolvoCoreInterpreterAstASTNode *propertyTypeParam1, ComScolvoCoreInterpreterAstASTNode *propertyTypeParam2, ComScolvoCoreInterpreterAstASTNode *minOccurs, ComScolvoCoreInterpreterAstASTNode *maxOccurs) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstTypeProperty *create_ComScolvoCoreInterpreterAstTypeProperty_initWithComScolvoCoreInterpreterToken_withComScolvoCoreInterpreterToken_withBoolean_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_withComScolvoCoreInterpreterAstASTNode_(ComScolvoCoreInterpreterToken *propertyName, ComScolvoCoreInterpreterToken *propertyType, jboolean required, ComScolvoCoreInterpreterAstASTNode *propertyTypeParam1, ComScolvoCoreInterpreterAstASTNode *propertyTypeParam2, ComScolvoCoreInterpreterAstASTNode *minOccurs, ComScolvoCoreInterpreterAstASTNode *maxOccurs);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreInterpreterAstTypeProperty)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstTypeProperty")
