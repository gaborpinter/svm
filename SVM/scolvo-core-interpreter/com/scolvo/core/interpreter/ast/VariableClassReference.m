//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/VariableClassReference.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/interpreter/ast/ASTNode.h"
#include "com/scolvo/core/interpreter/ast/QualifiedName.h"
#include "com/scolvo/core/interpreter/ast/VariableClassReference.h"
#include "com/scolvo/core/interpreter/visitors/Visitor.h"

@interface ComScolvoCoreInterpreterAstVariableClassReference () {
 @public
  ComScolvoCoreInterpreterAstQualifiedName *qualifiedName_;
}

@end

J2OBJC_FIELD_SETTER(ComScolvoCoreInterpreterAstVariableClassReference, qualifiedName_, ComScolvoCoreInterpreterAstQualifiedName *)

@implementation ComScolvoCoreInterpreterAstVariableClassReference

- (instancetype __nonnull)initWithComScolvoCoreInterpreterAstQualifiedName:(ComScolvoCoreInterpreterAstQualifiedName *)qualifiedName {
  ComScolvoCoreInterpreterAstVariableClassReference_initWithComScolvoCoreInterpreterAstQualifiedName_(self, qualifiedName);
  return self;
}

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor {
  [((id<ComScolvoCoreInterpreterVisitorsVisitor>) nil_chk(visitor)) visitWithComScolvoCoreInterpreterAstVariableClassReference:self];
}

- (ComScolvoCoreInterpreterAstQualifiedName *)getQualifiedName {
  return qualifiedName_;
}

- (NSString *)description {
  return JreStrcat("$@C", @"VariableClassReference [qualifiedName=", qualifiedName_, ']');
}

- (void)dealloc {
  RELEASE_(qualifiedName_);
  [super dealloc];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "LComScolvoCoreInterpreterAstQualifiedName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 3, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithComScolvoCoreInterpreterAstQualifiedName:);
  methods[1].selector = @selector(acceptWithComScolvoCoreInterpreterVisitorsVisitor:);
  methods[2].selector = @selector(getQualifiedName);
  methods[3].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "qualifiedName_", "LComScolvoCoreInterpreterAstQualifiedName;", .constantValue.asLong = 0, 0x12, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LComScolvoCoreInterpreterAstQualifiedName;", "accept", "LComScolvoCoreInterpreterVisitorsVisitor;", "toString" };
  static const J2ObjcClassInfo _ComScolvoCoreInterpreterAstVariableClassReference = { "VariableClassReference", "com.scolvo.core.interpreter.ast", ptrTable, methods, fields, 7, 0x1, 4, 1, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreInterpreterAstVariableClassReference;
}

@end

void ComScolvoCoreInterpreterAstVariableClassReference_initWithComScolvoCoreInterpreterAstQualifiedName_(ComScolvoCoreInterpreterAstVariableClassReference *self, ComScolvoCoreInterpreterAstQualifiedName *qualifiedName) {
  ComScolvoCoreInterpreterAstASTNode_init(self);
  JreStrongAssign(&self->qualifiedName_, qualifiedName);
}

ComScolvoCoreInterpreterAstVariableClassReference *new_ComScolvoCoreInterpreterAstVariableClassReference_initWithComScolvoCoreInterpreterAstQualifiedName_(ComScolvoCoreInterpreterAstQualifiedName *qualifiedName) {
  J2OBJC_NEW_IMPL(ComScolvoCoreInterpreterAstVariableClassReference, initWithComScolvoCoreInterpreterAstQualifiedName_, qualifiedName)
}

ComScolvoCoreInterpreterAstVariableClassReference *create_ComScolvoCoreInterpreterAstVariableClassReference_initWithComScolvoCoreInterpreterAstQualifiedName_(ComScolvoCoreInterpreterAstQualifiedName *qualifiedName) {
  J2OBJC_CREATE_IMPL(ComScolvoCoreInterpreterAstVariableClassReference, initWithComScolvoCoreInterpreterAstQualifiedName_, qualifiedName)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(ComScolvoCoreInterpreterAstVariableClassReference)
