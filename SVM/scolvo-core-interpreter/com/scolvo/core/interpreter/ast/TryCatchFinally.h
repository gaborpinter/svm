//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/TryCatchFinally.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstTryCatchFinally")
#ifdef RESTRICT_ComScolvoCoreInterpreterAstTryCatchFinally
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstTryCatchFinally 0
#else
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstTryCatchFinally 1
#endif
#undef RESTRICT_ComScolvoCoreInterpreterAstTryCatchFinally

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreInterpreterAstTryCatchFinally_) && (INCLUDE_ALL_ComScolvoCoreInterpreterAstTryCatchFinally || defined(INCLUDE_ComScolvoCoreInterpreterAstTryCatchFinally))
#define ComScolvoCoreInterpreterAstTryCatchFinally_

#define RESTRICT_ComScolvoCoreInterpreterAstASTNode 1
#define INCLUDE_ComScolvoCoreInterpreterAstASTNode 1
#include "com/scolvo/core/interpreter/ast/ASTNode.h"

@protocol ComScolvoCoreInterpreterVisitorsVisitor;
@protocol JavaUtilList;

@interface ComScolvoCoreInterpreterAstTryCatchFinally : ComScolvoCoreInterpreterAstASTNode

#pragma mark Public

- (instancetype __nonnull)init;

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor;

- (id<JavaUtilList>)getCatches;

- (ComScolvoCoreInterpreterAstASTNode *)getFinalStatement;

- (ComScolvoCoreInterpreterAstASTNode *)getStatement;

- (void)setCatchesWithJavaUtilList:(id<JavaUtilList>)catches;

- (void)setFinalStatementWithComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)finalStatement;

- (void)setStatementWithComScolvoCoreInterpreterAstASTNode:(ComScolvoCoreInterpreterAstASTNode *)statement;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreInterpreterAstTryCatchFinally)

FOUNDATION_EXPORT void ComScolvoCoreInterpreterAstTryCatchFinally_init(ComScolvoCoreInterpreterAstTryCatchFinally *self);

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstTryCatchFinally *new_ComScolvoCoreInterpreterAstTryCatchFinally_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstTryCatchFinally *create_ComScolvoCoreInterpreterAstTryCatchFinally_init(void);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreInterpreterAstTryCatchFinally)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstTryCatchFinally")
