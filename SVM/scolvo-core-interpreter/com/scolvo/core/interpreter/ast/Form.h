//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/Form.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstForm")
#ifdef RESTRICT_ComScolvoCoreInterpreterAstForm
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstForm 0
#else
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstForm 1
#endif
#undef RESTRICT_ComScolvoCoreInterpreterAstForm

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreInterpreterAstForm_) && (INCLUDE_ALL_ComScolvoCoreInterpreterAstForm || defined(INCLUDE_ComScolvoCoreInterpreterAstForm))
#define ComScolvoCoreInterpreterAstForm_

#define RESTRICT_ComScolvoCoreInterpreterAstContent 1
#define INCLUDE_ComScolvoCoreInterpreterAstContent 1
#include "com/scolvo/core/interpreter/ast/Content.h"

@class ComScolvoCoreInterpreterToken;
@protocol ComScolvoCoreInterpreterVisitorsVisitor;
@protocol JavaUtilList;

@interface ComScolvoCoreInterpreterAstForm : ComScolvoCoreInterpreterAstContent

#pragma mark Public

- (instancetype __nonnull)initWithComScolvoCoreInterpreterToken:(ComScolvoCoreInterpreterToken *)id_;

- (void)acceptWithComScolvoCoreInterpreterVisitorsVisitor:(id<ComScolvoCoreInterpreterVisitorsVisitor>)visitor;

- (id<JavaUtilList>)getActions;

- (id<JavaUtilList>)getFields;

- (void)setActionsWithJavaUtilList:(id<JavaUtilList>)actions;

- (void)setFieldsWithJavaUtilList:(id<JavaUtilList>)fields;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreInterpreterAstForm)

FOUNDATION_EXPORT void ComScolvoCoreInterpreterAstForm_initWithComScolvoCoreInterpreterToken_(ComScolvoCoreInterpreterAstForm *self, ComScolvoCoreInterpreterToken *id_);

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstForm *new_ComScolvoCoreInterpreterAstForm_initWithComScolvoCoreInterpreterToken_(ComScolvoCoreInterpreterToken *id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreInterpreterAstForm *create_ComScolvoCoreInterpreterAstForm_initWithComScolvoCoreInterpreterToken_(ComScolvoCoreInterpreterToken *id_);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreInterpreterAstForm)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstForm")
