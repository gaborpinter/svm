//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/ast/AbstractColumn.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstAbstractColumn")
#ifdef RESTRICT_ComScolvoCoreInterpreterAstAbstractColumn
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstAbstractColumn 0
#else
#define INCLUDE_ALL_ComScolvoCoreInterpreterAstAbstractColumn 1
#endif
#undef RESTRICT_ComScolvoCoreInterpreterAstAbstractColumn

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreInterpreterAstAbstractColumn_) && (INCLUDE_ALL_ComScolvoCoreInterpreterAstAbstractColumn || defined(INCLUDE_ComScolvoCoreInterpreterAstAbstractColumn))
#define ComScolvoCoreInterpreterAstAbstractColumn_

#define RESTRICT_ComScolvoCoreInterpreterAstASTNode 1
#define INCLUDE_ComScolvoCoreInterpreterAstASTNode 1
#include "com/scolvo/core/interpreter/ast/ASTNode.h"

@interface ComScolvoCoreInterpreterAstAbstractColumn : ComScolvoCoreInterpreterAstASTNode

#pragma mark Public

- (instancetype __nonnull)init;

@end

J2OBJC_EMPTY_STATIC_INIT(ComScolvoCoreInterpreterAstAbstractColumn)

FOUNDATION_EXPORT void ComScolvoCoreInterpreterAstAbstractColumn_init(ComScolvoCoreInterpreterAstAbstractColumn *self);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreInterpreterAstAbstractColumn)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreInterpreterAstAbstractColumn")
