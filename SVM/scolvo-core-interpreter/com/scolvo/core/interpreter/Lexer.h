//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/Lexer.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreInterpreterLexer")
#ifdef RESTRICT_ComScolvoCoreInterpreterLexer
#define INCLUDE_ALL_ComScolvoCoreInterpreterLexer 0
#else
#define INCLUDE_ALL_ComScolvoCoreInterpreterLexer 1
#endif
#undef RESTRICT_ComScolvoCoreInterpreterLexer

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreInterpreterLexer_) && (INCLUDE_ALL_ComScolvoCoreInterpreterLexer || defined(INCLUDE_ComScolvoCoreInterpreterLexer))
#define ComScolvoCoreInterpreterLexer_

@class ComScolvoCoreInterpreterToken;

@interface ComScolvoCoreInterpreterLexer : NSObject

#pragma mark Public

- (instancetype __nonnull)initWithNSString:(NSString *)text;

- (ComScolvoCoreInterpreterToken *)getNextToken;

#pragma mark Protected

- (ComScolvoCoreInterpreterToken *)parseAddition;

- (ComScolvoCoreInterpreterToken *)parseAnd;

- (ComScolvoCoreInterpreterToken *)parseAssignment;

- (ComScolvoCoreInterpreterToken *)parseColon;

- (ComScolvoCoreInterpreterToken *)parseComma;

- (ComScolvoCoreInterpreterToken *)parseDivision;

- (ComScolvoCoreInterpreterToken *)parseDot;

- (ComScolvoCoreInterpreterToken *)parseGreater;

- (ComScolvoCoreInterpreterToken *)parseLeftBracket;

- (ComScolvoCoreInterpreterToken *)parseLeftCurlyBracket;

- (ComScolvoCoreInterpreterToken *)parseLeftParenthesis;

- (ComScolvoCoreInterpreterToken *)parseLess;

- (ComScolvoCoreInterpreterToken *)parseLogicalNegation;

- (ComScolvoCoreInterpreterToken *)parseModulus;

- (ComScolvoCoreInterpreterToken *)parseMultiplication;

- (ComScolvoCoreInterpreterToken *)parseOr;

- (ComScolvoCoreInterpreterToken *)parseRightBracket;

- (ComScolvoCoreInterpreterToken *)parseRightCurlyBracket;

- (ComScolvoCoreInterpreterToken *)parseRightParenthesis;

- (ComScolvoCoreInterpreterToken *)parseSemicolon;

- (ComScolvoCoreInterpreterToken *)parseSubtraction;

// Disallowed inherited constructors, do not use.

- (instancetype __nonnull)init NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(ComScolvoCoreInterpreterLexer)

FOUNDATION_EXPORT void ComScolvoCoreInterpreterLexer_initWithNSString_(ComScolvoCoreInterpreterLexer *self, NSString *text);

FOUNDATION_EXPORT ComScolvoCoreInterpreterLexer *new_ComScolvoCoreInterpreterLexer_initWithNSString_(NSString *text) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT ComScolvoCoreInterpreterLexer *create_ComScolvoCoreInterpreterLexer_initWithNSString_(NSString *text);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreInterpreterLexer)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreInterpreterLexer")
