//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/visitors/Visitor.java
//

#include "J2ObjC_source.h"
#include "com/scolvo/core/interpreter/visitors/Visitor.h"

@interface ComScolvoCoreInterpreterVisitorsVisitor : NSObject

@end

@implementation ComScolvoCoreInterpreterVisitorsVisitor

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "V", 0x401, 0, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 6, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 11, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 12, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 14, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 15, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 16, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 17, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 18, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 19, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 20, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 21, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 22, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 23, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 24, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 25, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 26, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 27, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 28, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 29, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 30, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 31, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 32, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 33, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 34, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 35, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 36, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 37, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 38, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 39, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 40, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 41, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 42, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 43, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 44, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 45, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 46, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 47, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 48, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 49, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 50, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 51, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 52, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 53, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 54, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 55, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(visitWithComScolvoCoreInterpreterAstBlock:);
  methods[1].selector = @selector(visitWithComScolvoCoreInterpreterAstVariableDecl:);
  methods[2].selector = @selector(visitWithComScolvoCoreInterpreterAstVariable:);
  methods[3].selector = @selector(visitWithComScolvoCoreInterpreterAstAssignment:);
  methods[4].selector = @selector(visitWithComScolvoCoreInterpreterAstBinaryOp:);
  methods[5].selector = @selector(visitWithComScolvoCoreInterpreterAstConstantValue:);
  methods[6].selector = @selector(visitWithComScolvoCoreInterpreterAstFunctionDecl:);
  methods[7].selector = @selector(visitWithComScolvoCoreInterpreterAstFunctionCall:);
  methods[8].selector = @selector(visitWithComScolvoCoreInterpreterAstParam:);
  methods[9].selector = @selector(visitWithComScolvoCoreInterpreterAstReturn:);
  methods[10].selector = @selector(visitWithComScolvoCoreInterpreterAstIf:);
  methods[11].selector = @selector(visitWithComScolvoCoreInterpreterAstFor:);
  methods[12].selector = @selector(visitWithComScolvoCoreInterpreterAstScript:);
  methods[13].selector = @selector(visitWithComScolvoCoreInterpreterAstTypeDefinition:);
  methods[14].selector = @selector(visitWithComScolvoCoreInterpreterAstTypeProperty:);
  methods[15].selector = @selector(visitWithComScolvoCoreInterpreterAstJsonObject:);
  methods[16].selector = @selector(visitWithComScolvoCoreInterpreterAstJsonArray:);
  methods[17].selector = @selector(visitWithComScolvoCoreInterpreterAstNull:);
  methods[18].selector = @selector(visitWithComScolvoCoreInterpreterAstQualifiedName:);
  methods[19].selector = @selector(visitWithComScolvoCoreInterpreterAstMenuMenu:);
  methods[20].selector = @selector(visitWithComScolvoCoreInterpreterAstMenuGroup:);
  methods[21].selector = @selector(visitWithComScolvoCoreInterpreterAstMenuItem:);
  methods[22].selector = @selector(visitWithComScolvoCoreInterpreterAstMenuSeparator:);
  methods[23].selector = @selector(visitWithComScolvoCoreInterpreterAstPage:);
  methods[24].selector = @selector(visitWithComScolvoCoreInterpreterAstFragment:);
  methods[25].selector = @selector(visitWithComScolvoCoreInterpreterAstFragmentContainer:);
  methods[26].selector = @selector(visitWithComScolvoCoreInterpreterAstOrderedList:);
  methods[27].selector = @selector(visitWithComScolvoCoreInterpreterAstColumn:);
  methods[28].selector = @selector(visitWithComScolvoCoreInterpreterAstForm:);
  methods[29].selector = @selector(visitWithComScolvoCoreInterpreterAstField:);
  methods[30].selector = @selector(visitWithComScolvoCoreInterpreterAstMasterDetails:);
  methods[31].selector = @selector(visitWithComScolvoCoreInterpreterAstFieldset:);
  methods[32].selector = @selector(visitWithComScolvoCoreInterpreterAstContainer:);
  methods[33].selector = @selector(visitWithComScolvoCoreInterpreterAstWithRole:);
  methods[34].selector = @selector(visitWithComScolvoCoreInterpreterAstListOrderDefinition:);
  methods[35].selector = @selector(visitWithComScolvoCoreInterpreterAstSpacer:);
  methods[36].selector = @selector(visitWithComScolvoCoreInterpreterAstAction:);
  methods[37].selector = @selector(visitWithComScolvoCoreInterpreterAstActionColumn:);
  methods[38].selector = @selector(visitWithComScolvoCoreInterpreterAstLazyExpression:);
  methods[39].selector = @selector(visitWithComScolvoCoreInterpreterAstFilter:);
  methods[40].selector = @selector(visitWithComScolvoCoreInterpreterAstClassReference:);
  methods[41].selector = @selector(visitWithComScolvoCoreInterpreterAstVariableClassReference:);
  methods[42].selector = @selector(visitWithComScolvoCoreInterpreterAstTryCatchFinally:);
  methods[43].selector = @selector(visitWithComScolvoCoreInterpreterAstCatchDecl:);
  methods[44].selector = @selector(visitWithComScolvoCoreInterpreterAstThrow:);
  methods[45].selector = @selector(visitWithComScolvoCoreInterpreterAstButton:);
  methods[46].selector = @selector(visitWithComScolvoCoreInterpreterAstLabel:);
  methods[47].selector = @selector(visitWithComScolvoCoreInterpreterAstSwitch:);
  methods[48].selector = @selector(visitWithComScolvoCoreInterpreterAstSlider:);
  methods[49].selector = @selector(visitWithComScolvoCoreInterpreterAstImageSet:);
  methods[50].selector = @selector(visitWithComScolvoCoreInterpreterAstInputField:);
  methods[51].selector = @selector(visitWithComScolvoCoreInterpreterAstSliderView:);
  methods[52].selector = @selector(visitWithComScolvoCoreInterpreterAstGoalPlanning:);
  methods[53].selector = @selector(visitWithComScolvoCoreInterpreterAstAssetManager:);
  methods[54].selector = @selector(visitWithComScolvoCoreInterpreterAstImage:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "visit", "LComScolvoCoreInterpreterAstBlock;", "LComScolvoCoreInterpreterAstVariableDecl;", "LComScolvoCoreInterpreterAstVariable;", "LComScolvoCoreInterpreterAstAssignment;", "LComScolvoCoreInterpreterAstBinaryOp;", "LComScolvoCoreInterpreterAstConstantValue;", "LComScolvoCoreInterpreterAstFunctionDecl;", "LComScolvoCoreInterpreterAstFunctionCall;", "LComScolvoCoreInterpreterAstParam;", "LComScolvoCoreInterpreterAstReturn;", "LComScolvoCoreInterpreterAstIf;", "LComScolvoCoreInterpreterAstFor;", "LComScolvoCoreInterpreterAstScript;", "LComScolvoCoreInterpreterAstTypeDefinition;", "LComScolvoCoreInterpreterAstTypeProperty;", "LComScolvoCoreInterpreterAstJsonObject;", "LComScolvoCoreInterpreterAstJsonArray;", "LComScolvoCoreInterpreterAstNull;", "LComScolvoCoreInterpreterAstQualifiedName;", "LComScolvoCoreInterpreterAstMenuMenu;", "LComScolvoCoreInterpreterAstMenuGroup;", "LComScolvoCoreInterpreterAstMenuItem;", "LComScolvoCoreInterpreterAstMenuSeparator;", "LComScolvoCoreInterpreterAstPage;", "LComScolvoCoreInterpreterAstFragment;", "LComScolvoCoreInterpreterAstFragmentContainer;", "LComScolvoCoreInterpreterAstOrderedList;", "LComScolvoCoreInterpreterAstColumn;", "LComScolvoCoreInterpreterAstForm;", "LComScolvoCoreInterpreterAstField;", "LComScolvoCoreInterpreterAstMasterDetails;", "LComScolvoCoreInterpreterAstFieldset;", "LComScolvoCoreInterpreterAstContainer;", "LComScolvoCoreInterpreterAstWithRole;", "LComScolvoCoreInterpreterAstListOrderDefinition;", "LComScolvoCoreInterpreterAstSpacer;", "LComScolvoCoreInterpreterAstAction;", "LComScolvoCoreInterpreterAstActionColumn;", "LComScolvoCoreInterpreterAstLazyExpression;", "LComScolvoCoreInterpreterAstFilter;", "LComScolvoCoreInterpreterAstClassReference;", "LComScolvoCoreInterpreterAstVariableClassReference;", "LComScolvoCoreInterpreterAstTryCatchFinally;", "LComScolvoCoreInterpreterAstCatchDecl;", "LComScolvoCoreInterpreterAstThrow;", "LComScolvoCoreInterpreterAstButton;", "LComScolvoCoreInterpreterAstLabel;", "LComScolvoCoreInterpreterAstSwitch;", "LComScolvoCoreInterpreterAstSlider;", "LComScolvoCoreInterpreterAstImageSet;", "LComScolvoCoreInterpreterAstInputField;", "LComScolvoCoreInterpreterAstSliderView;", "LComScolvoCoreInterpreterAstGoalPlanning;", "LComScolvoCoreInterpreterAstAssetManager;", "LComScolvoCoreInterpreterAstImage;" };
  static const J2ObjcClassInfo _ComScolvoCoreInterpreterVisitorsVisitor = { "Visitor", "com.scolvo.core.interpreter.visitors", ptrTable, methods, NULL, 7, 0x609, 55, 0, -1, -1, -1, -1, -1 };
  return &_ComScolvoCoreInterpreterVisitorsVisitor;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(ComScolvoCoreInterpreterVisitorsVisitor)
