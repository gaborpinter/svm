//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: /Users/kltz/Development/Scolvo/ScolvoWorkspace/scolvo-core-virtual-machine/target/classes/scolvo-core-interpreter/com/scolvo/core/interpreter/TokenType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_ComScolvoCoreInterpreterTokenType")
#ifdef RESTRICT_ComScolvoCoreInterpreterTokenType
#define INCLUDE_ALL_ComScolvoCoreInterpreterTokenType 0
#else
#define INCLUDE_ALL_ComScolvoCoreInterpreterTokenType 1
#endif
#undef RESTRICT_ComScolvoCoreInterpreterTokenType

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (ComScolvoCoreInterpreterTokenType_) && (INCLUDE_ALL_ComScolvoCoreInterpreterTokenType || defined(INCLUDE_ComScolvoCoreInterpreterTokenType))
#define ComScolvoCoreInterpreterTokenType_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

@class IOSObjectArray;

typedef NS_ENUM(NSUInteger, ComScolvoCoreInterpreterTokenType_Enum) {
  ComScolvoCoreInterpreterTokenType_Enum_ID = 0,
  ComScolvoCoreInterpreterTokenType_Enum_LCBRACKET = 1,
  ComScolvoCoreInterpreterTokenType_Enum_RCBRACKET = 2,
  ComScolvoCoreInterpreterTokenType_Enum_VAR = 3,
  ComScolvoCoreInterpreterTokenType_Enum_OPERATOR = 4,
  ComScolvoCoreInterpreterTokenType_Enum_LPAREN = 5,
  ComScolvoCoreInterpreterTokenType_Enum_RPAREN = 6,
  ComScolvoCoreInterpreterTokenType_Enum_LBRACKET = 7,
  ComScolvoCoreInterpreterTokenType_Enum_RBRACKET = 8,
  ComScolvoCoreInterpreterTokenType_Enum_DOT = 9,
  ComScolvoCoreInterpreterTokenType_Enum_IMPORT = 10,
  ComScolvoCoreInterpreterTokenType_Enum_INTEGER_CONST = 11,
  ComScolvoCoreInterpreterTokenType_Enum_DECIMAL_CONST = 12,
  ComScolvoCoreInterpreterTokenType_Enum_STRING_CONST = 13,
  ComScolvoCoreInterpreterTokenType_Enum_BOOLEAN_CONST = 14,
  ComScolvoCoreInterpreterTokenType_Enum_TRUE = 15,
  ComScolvoCoreInterpreterTokenType_Enum_FALSE = 16,
  ComScolvoCoreInterpreterTokenType_Enum_FUNCTION_DECL = 17,
  ComScolvoCoreInterpreterTokenType_Enum_RETURN = 18,
  ComScolvoCoreInterpreterTokenType_Enum_PARAM = 19,
  ComScolvoCoreInterpreterTokenType_Enum_COMMA = 20,
  ComScolvoCoreInterpreterTokenType_Enum_COLON = 21,
  ComScolvoCoreInterpreterTokenType_Enum_SEMICOLON = 22,
  ComScolvoCoreInterpreterTokenType_Enum_WITH_ROLE = 23,
  ComScolvoCoreInterpreterTokenType_Enum_IF = 24,
  ComScolvoCoreInterpreterTokenType_Enum_ELSE = 25,
  ComScolvoCoreInterpreterTokenType_Enum_FOR = 26,
  ComScolvoCoreInterpreterTokenType_Enum_TYPE = 27,
  ComScolvoCoreInterpreterTokenType_Enum_REQUIRED = 28,
  ComScolvoCoreInterpreterTokenType_Enum_AS = 29,
  ComScolvoCoreInterpreterTokenType_Enum_NULL = 30,
  ComScolvoCoreInterpreterTokenType_Enum_MENU = 31,
  ComScolvoCoreInterpreterTokenType_Enum_GROUP = 32,
  ComScolvoCoreInterpreterTokenType_Enum_ITEM = 33,
  ComScolvoCoreInterpreterTokenType_Enum_SEPARATOR = 34,
  ComScolvoCoreInterpreterTokenType_Enum_DEFAULT_CHILD = 35,
  ComScolvoCoreInterpreterTokenType_Enum_PAGE = 36,
  ComScolvoCoreInterpreterTokenType_Enum_FRAGMENT = 37,
  ComScolvoCoreInterpreterTokenType_Enum_FRAGMENT_CONTAINER = 38,
  ComScolvoCoreInterpreterTokenType_Enum_CONTAINER = 39,
  ComScolvoCoreInterpreterTokenType_Enum_SPACER = 40,
  ComScolvoCoreInterpreterTokenType_Enum_MASTER_DETAILS = 41,
  ComScolvoCoreInterpreterTokenType_Enum_MASTER_PART = 42,
  ComScolvoCoreInterpreterTokenType_Enum_DETAILS_PART = 43,
  ComScolvoCoreInterpreterTokenType_Enum_LIST = 44,
  ComScolvoCoreInterpreterTokenType_Enum_ACTIONS = 45,
  ComScolvoCoreInterpreterTokenType_Enum_FILTERS = 46,
  ComScolvoCoreInterpreterTokenType_Enum_ORDERABLE_BY = 47,
  ComScolvoCoreInterpreterTokenType_Enum_GROUP_ON = 48,
  ComScolvoCoreInterpreterTokenType_Enum_DEFAULT = 49,
  ComScolvoCoreInterpreterTokenType_Enum_PRIORITY = 50,
  ComScolvoCoreInterpreterTokenType_Enum_COLUMNS = 51,
  ComScolvoCoreInterpreterTokenType_Enum_HIDDEN = 52,
  ComScolvoCoreInterpreterTokenType_Enum_ID_COLUMN = 53,
  ComScolvoCoreInterpreterTokenType_Enum_FORM = 54,
  ComScolvoCoreInterpreterTokenType_Enum_FIELD = 55,
  ComScolvoCoreInterpreterTokenType_Enum_FIELDS = 56,
  ComScolvoCoreInterpreterTokenType_Enum_MAPPING = 57,
  ComScolvoCoreInterpreterTokenType_Enum_FIELDSET = 58,
  ComScolvoCoreInterpreterTokenType_Enum_OPTIONLIST = 59,
  ComScolvoCoreInterpreterTokenType_Enum_OPTIONLISTER = 60,
  ComScolvoCoreInterpreterTokenType_Enum_OPTION = 61,
  ComScolvoCoreInterpreterTokenType_Enum_BUTTON = 62,
  ComScolvoCoreInterpreterTokenType_Enum_LABEL = 63,
  ComScolvoCoreInterpreterTokenType_Enum_SWITCH = 64,
  ComScolvoCoreInterpreterTokenType_Enum_SLIDER = 65,
  ComScolvoCoreInterpreterTokenType_Enum_IMAGE_SET = 66,
  ComScolvoCoreInterpreterTokenType_Enum_INPUT_FIELD = 67,
  ComScolvoCoreInterpreterTokenType_Enum_SLIDER_VIEW = 68,
  ComScolvoCoreInterpreterTokenType_Enum_GOAL_PLANNING = 69,
  ComScolvoCoreInterpreterTokenType_Enum_ASSET_MANAGER = 70,
  ComScolvoCoreInterpreterTokenType_Enum_IMAGE = 71,
  ComScolvoCoreInterpreterTokenType_Enum_TRY = 72,
  ComScolvoCoreInterpreterTokenType_Enum_CATCH = 73,
  ComScolvoCoreInterpreterTokenType_Enum_FINALLY = 74,
  ComScolvoCoreInterpreterTokenType_Enum_THROW = 75,
  ComScolvoCoreInterpreterTokenType_Enum_EOF = 76,
};

@interface ComScolvoCoreInterpreterTokenType : JavaLangEnum

+ (ComScolvoCoreInterpreterTokenType * __nonnull)ID;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)LCBRACKET;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)RCBRACKET;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)VAR;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)OPERATOR;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)LPAREN;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)RPAREN;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)LBRACKET;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)RBRACKET;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)DOT;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)IMPORT;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)INTEGER_CONST;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)DECIMAL_CONST;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)STRING_CONST;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)BOOLEAN_CONST;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)TRUE_;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FALSE_;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FUNCTION_DECL;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)RETURN;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)PARAM;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)COMMA;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)COLON;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)SEMICOLON;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)WITH_ROLE;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)IF;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)ELSE;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FOR;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)TYPE;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)REQUIRED;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)AS;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)NULL_;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)MENU;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)GROUP;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)ITEM;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)SEPARATOR;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)DEFAULT_CHILD;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)PAGE;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FRAGMENT;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FRAGMENT_CONTAINER;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)CONTAINER;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)SPACER;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)MASTER_DETAILS;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)MASTER_PART;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)DETAILS_PART;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)LIST;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)ACTIONS;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FILTERS;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)ORDERABLE_BY;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)GROUP_ON;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)DEFAULT;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)PRIORITY;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)COLUMNS;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)HIDDEN;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)ID_COLUMN;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FORM;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FIELD;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FIELDS;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)MAPPING;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FIELDSET;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)OPTIONLIST;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)OPTIONLISTER;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)OPTION;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)BUTTON;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)LABEL;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)SWITCH;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)SLIDER;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)IMAGE_SET;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)INPUT_FIELD;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)SLIDER_VIEW;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)GOAL_PLANNING;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)ASSET_MANAGER;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)IMAGE;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)TRY;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)CATCH;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)FINALLY;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)THROW;

+ (ComScolvoCoreInterpreterTokenType * __nonnull)EOF_;

#pragma mark Public

+ (ComScolvoCoreInterpreterTokenType *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (ComScolvoCoreInterpreterTokenType_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(ComScolvoCoreInterpreterTokenType)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_values_[];

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_ID(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, ID)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_LCBRACKET(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, LCBRACKET)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_RCBRACKET(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, RCBRACKET)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_VAR(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, VAR)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_OPERATOR(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, OPERATOR)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_LPAREN(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, LPAREN)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_RPAREN(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, RPAREN)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_LBRACKET(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, LBRACKET)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_RBRACKET(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, RBRACKET)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_DOT(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, DOT)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_IMPORT(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, IMPORT)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_INTEGER_CONST(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, INTEGER_CONST)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_DECIMAL_CONST(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, DECIMAL_CONST)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_STRING_CONST(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, STRING_CONST)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_BOOLEAN_CONST(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, BOOLEAN_CONST)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_TRUE(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, TRUE)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FALSE(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FALSE)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FUNCTION_DECL(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FUNCTION_DECL)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_RETURN(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, RETURN)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_PARAM(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, PARAM)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_COMMA(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, COMMA)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_COLON(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, COLON)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_SEMICOLON(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, SEMICOLON)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_WITH_ROLE(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, WITH_ROLE)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_IF(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, IF)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_ELSE(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, ELSE)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FOR(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FOR)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_TYPE(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, TYPE)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_REQUIRED(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, REQUIRED)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_AS(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, AS)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_NULL(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, NULL)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_MENU(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, MENU)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_GROUP(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, GROUP)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_ITEM(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, ITEM)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_SEPARATOR(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, SEPARATOR)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_DEFAULT_CHILD(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, DEFAULT_CHILD)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_PAGE(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, PAGE)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FRAGMENT(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FRAGMENT)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FRAGMENT_CONTAINER(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FRAGMENT_CONTAINER)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_CONTAINER(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, CONTAINER)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_SPACER(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, SPACER)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_MASTER_DETAILS(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, MASTER_DETAILS)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_MASTER_PART(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, MASTER_PART)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_DETAILS_PART(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, DETAILS_PART)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_LIST(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, LIST)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_ACTIONS(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, ACTIONS)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FILTERS(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FILTERS)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_ORDERABLE_BY(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, ORDERABLE_BY)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_GROUP_ON(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, GROUP_ON)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_DEFAULT(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, DEFAULT)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_PRIORITY(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, PRIORITY)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_COLUMNS(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, COLUMNS)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_HIDDEN(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, HIDDEN)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_ID_COLUMN(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, ID_COLUMN)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FORM(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FORM)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FIELD(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FIELD)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FIELDS(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FIELDS)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_MAPPING(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, MAPPING)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FIELDSET(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FIELDSET)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_OPTIONLIST(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, OPTIONLIST)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_OPTIONLISTER(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, OPTIONLISTER)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_OPTION(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, OPTION)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_BUTTON(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, BUTTON)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_LABEL(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, LABEL)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_SWITCH(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, SWITCH)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_SLIDER(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, SLIDER)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_IMAGE_SET(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, IMAGE_SET)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_INPUT_FIELD(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, INPUT_FIELD)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_SLIDER_VIEW(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, SLIDER_VIEW)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_GOAL_PLANNING(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, GOAL_PLANNING)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_ASSET_MANAGER(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, ASSET_MANAGER)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_IMAGE(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, IMAGE)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_TRY(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, TRY)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_CATCH(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, CATCH)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_FINALLY(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, FINALLY)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_THROW(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, THROW)

inline ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_get_EOF(void);
J2OBJC_ENUM_CONSTANT(ComScolvoCoreInterpreterTokenType, EOF)

FOUNDATION_EXPORT IOSObjectArray *ComScolvoCoreInterpreterTokenType_values(void);

FOUNDATION_EXPORT ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT ComScolvoCoreInterpreterTokenType *ComScolvoCoreInterpreterTokenType_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(ComScolvoCoreInterpreterTokenType)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_ComScolvoCoreInterpreterTokenType")
