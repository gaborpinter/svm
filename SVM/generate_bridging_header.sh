#!/bin/sh
FILENAME="Bridging-Header-SVM.h"
DATE=$(date)
echo "// SVM Bridging Header" > $FILENAME
echo "// Generated by generate_bridging_header.sh on $DATE" >> $FILENAME
echo "#ifndef Bridging_Header_SVM_h" >> $FILENAME
echo "#define Bridging_Header_SVM_h" >> $FILENAME

#This is to generate a folder structured output
grep -rl '\"EMPTY\"' rxjava | xargs -I@ sed -i '' 's/\"EMPTY\"/\"EMPTYREPLACED\"/g' @
grep -rl '\*)EMPTY ' rxjava | xargs -I@ sed -i '' 's/\*)EMPTY /\*)EMPTYREPLACED /g' @
grep -rl '\*)EMPTY;' rxjava | xargs -I@ sed -i '' 's/\*)EMPTY;/\*)EMPTYREPLACED;/g' @
find . -name "*.h" | grep -v '/rxjava/io/reactivex/internal' | sed -e "s/ScolvoProduct/\.\./g" | xargs printf '#import "%s"\n'>> $FILENAME

#This is to generate a non-folder structured output
#find ScolvoProduct/SVM -type d | grep -v '/rxjava' | grep -v '/joda-time' | xargs ls | grep '\.h' | xargs printf '#import "%s"\n' >> $FILENAME
echo "#endif /* Bridging_Header_SVM_h */" >> $FILENAME
