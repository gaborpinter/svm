Pod::Spec.new do |spec|
  spec.name             = 'SVM'
  spec.version          = '0.97'
  spec.license          = 'MIT'
  spec.homepage         = 'https://bitbucket.org/ScolvoTeamDev/scolvo-core-virtual-machine-objc'
  spec.author           = 'Klespitz György'
  spec.summary          = 'Scolvo Core Virtual Machine'
  spec.source           = { :git => 'https://bitbucket.org/ScolvoTeamDev/scolvo-core-virtual-machine-objc', :branch => 'SCFR-1562-svm-frameworkbe-trsa', :submodules => true }
  spec.xcconfig         = { 'HEADER_SEARCH_PATHS' => '$(inherited) ${PROJECT_DIR}/../../j2objc/include',
                            'FRAMEWORK_SEARCH_PATHS' => '$(inherited) ${PROJECT_DIR}/../../j2objc/frameworks',
                            'LIBRARY_SEARCH_PATHS' => '$(inherited) ${PROJECT_DIR}/../../j2objc/lib',
                            'OTHER_LINK_FLAG' => '$(inherited) -ObjC'}
  # spec.prefix_header_file = "SVM/SVM.pch" 
  spec.requires_arc     = false
  spec.source_files = '**/*.{h,m}'
  # spec.private_header_files = '**/*.h'
  # spec.exclude_files = ['**/package-info .{h,m}', '**/metadata.properties' ]

  # spec.subspec 'gson' do |ss|
  #   ss.source_files = 'gson/**/*.{h,m}'
  #   ss.exclude_files = ['**/package-info.{h,m}', '**/metadata.properties' ]
  # end
  # spec.subspec 'reactive-streams' do |ss|
  #   ss.source_files = 'reactive-streams/**/*.{h,m}'
  #  ss.exclude_files = ['**/package-info.{h,m}', '**/metadata.properties' ]
  # end
  # spec.subspec 'rxjava' do |ss|
  #   ss.source_files = 'rxjava/**/*.{h,m}'
  #  ss.exclude_files = ['**/package-info.{h,m}', '**/metadata.properties' ]
  # end
  # spec.subspec 'scolvo-core-interpreter' do |ss|
  #   ss.source_files = 'scolvo-core-interpreter/**/*.{h,m}'
  #  ss.exclude_files = ['**/package-info.{h,m}', '**/metadata.properties' ]
  # end
  # spec.subspec 'scolvo-core-virtual-machine' do |ss|
  #   ss.source_files = 'scolvo-core-virtual-machine/**/*.{h,m}'
  #  ss.exclude_files = ['**/package-info.{h,m}', '**/metadata.properties' ]
  # end
  # spec.subspec 'scolvo-core-vm-api' do |ss|
  #   ss.source_files = 'scolvo-core-vm-api/**/*.{h,m}'
  #  ss.exclude_files = ['**/package-info.{h,m}', '**/metadata.properties' ]
  # end
end
